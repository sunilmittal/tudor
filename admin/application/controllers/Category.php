<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category extends BOLD_Controller
{
    private $plugin = 'categories';
    private $version = '1.1.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        $this->load->model(['category_model', 'team_model']);

        $this->load->library(['session', 'form_validation']);

        if ($this->session->has_userdata('user_id')) {
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else {
            redirect('team/login');
        }
    }

    public function index($show = NULL)
    {
        if (!$this->user)
            redirect('team/login');

        $this->view_data['title'] = 'Category Manager';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->view_data['categories'] = $this->category_model->get_rows();

        $this->load->view('header', $this->view_data);
        $this->load->view('categories/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function add()
    {
        $this->view_data['title'] = 'Add Category';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');
        $parent_categories = $this->category_model->get_rows(['parent_id' => null]);
        $parent_categories_options = ['' => 'Please select category'] + array_column($parent_categories, 'name', 'id');
        $this->view_data['parent_categories_options'] = $parent_categories_options;

        if ($this->input->post()) {

            $this->form_validation->set_rules('name', 'Name', 'required|trim|min_length[1]|max_length[100]|is_unique[category.name]');

            if ($this->form_validation->run()) {
                $data = array(
                    'name' => set_value('name'),
                    'parent_id' => set_value('parent_id') ?: null
                );

                if ($this->category_model->add_row($data)) {
                    $this->session->set_flashdata('success', 'Category added successfully.');
                    redirect('categories');
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('categories/add', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($id)
    {
        if (!$id) redirect('categories');

        $this->view_data['category'] = $this->category_model->get_row(
            array(
                'id' => $id
            )
        );
        if (!$this->view_data['category']) redirect('categories');

        $this->view_data['title'] = 'Edit category';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');
        $this->view_data['already_a_parent'] = false;
        $already_a_parent = $this->category_model->num_rows(
            array(
                'parent_id' => $id
            )
        );
        $this->view_data['already_a_parent'] = (bool)$already_a_parent;

        if (!$already_a_parent) {

            $parent_categories = $this->category_model->get_rows(['parent_id' => null, 'id !=' => $id]);
            $parent_categories_options = ['' => 'Please select category'] + array_column($parent_categories, 'name', 'id');
            $this->view_data['parent_categories_options'] = $parent_categories_options;
        }

        if ($this->input->post()) {

            $this->form_validation->set_rules('name', 'Name', 'required|trim|min_length[1]|max_length[100]');

            if ($this->form_validation->run()) {
                $data = array(
                    'name' => set_value('name'),
                    'parent_id' => set_value('parent_id') ?: null
                );

                if ($this->category_model->update_row(array('id' => $id), $data)) {
                    $this->session->set_flashdata('success', 'category updated successfully.');
                    redirect('categories');
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('categories/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($id)
    {
        if (!$id) redirect('categories');

        $this->view_data['category'] = $this->category_model->get_row(
            array(
                'id' => $id
            )
        );
        if (!$this->view_data['category']) redirect('categories');

        if ($this->category_model->delete_row(array('id' => $id))) {
            $this->session->set_flashdata('success', 'category deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('categories');
    }
}
