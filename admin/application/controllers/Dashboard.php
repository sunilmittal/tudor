<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends BOLD_Controller
{
    private $plugin = 'dashboard';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        $this->load->model(['dashboard_model', 'team_model', 'user_model', 'video_model', 'notes_model']);
        $this->load->library('session');

        if (!$this->plugins_model->plugin_is_set() || !$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->dashboard_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->dashboard_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }


        if ($this->session->has_userdata('user_id')) {
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');
    }

    public function index()
    {
        $this->view_data['title'] = 'Dashboard';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $general_users = $this->user_model->num_rows(['user_role' => USER_ROLE]);
        $teachers = $this->user_model->num_rows(['user_role' => TEACHER_ROLE]);
        $notes = $this->notes_model->num_rows(['status' => 1]);
        $videos = $this->video_model->num_rows();

        $this->view_data['statistics'] = [
            ['count' => $teachers, 'title' => 'Teachers'],
            ['count' => $general_users, 'title' => 'General users'],
            ['count' => $videos, 'title' => 'Videos uploaded'],
            ['count' => $notes, 'title' => 'Notes']
        ];

        $this->load->view('header', $this->view_data);
        $this->load->view('index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }
}
