<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Industry_sectors extends BOLD_Controller
{
    private $plugin = 'industry_sectors';
    private $version = '1.0.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        if (!in_array($this->plugin, array_keys($this->view_data['plugins'])) || !$this->view_data['plugins'][$this->plugin]['enabled'])
            redirect('dashboard');

        $this->load->model('industry_sectors_model');
        if (!$this->plugins_model->plugin_is_installed($this->plugin)) {
            $this->industry_sectors_model->plugin_install();

            if ($this->plugins_model->plugin_install($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Installed');
            }
        } elseif (!$this->plugins_model->plugin_is_actual($this->plugin, $this->version)) {
            $actual_version = $this->plugins_model->plugin_get_actual_version($this->plugin);
            $this->industry_sectors_model->plugin_upgrade($actual_version);

            if ($this->plugins_model->plugin_upgrade($this->plugin, $this->version)) {
                $this->load->library('session');
                $this->session->set_flashdata('success', 'Plugin Successfully Upgraded');
            }
        }

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->load->model('team_model');
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else
            redirect('team/login');
    }

    public function index()
    {
        $this->view_data['title'] = 'Industry Sectors';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->view_data['industry_sectors'] = $this->industry_sectors_model->get();

        $this->load->view('header', $this->view_data);
        $this->load->view('industry_sectors/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function add()
    {
        $this->view_data['title'] = 'Add Blog Category';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');

        if ($this->input->post()) {

            $this->form_validation->set_rules('name', 'Name', 'required|trim|min_length[1]|max_length[100]|is_unique[industry_sectors.name]');
			$this->form_validation->set_rules('sub_heading', 'Sub Heading', 'required|trim|min_length[1]|max_length[100]');

            if ($this->form_validation->run()) {
                $data = array(
                    'name' => set_value('name'),
					'sub_heading' => set_value('sub_heading')
                );

                if ($this->industry_sectors_model->add_row($data)) {
                    $this->session->set_flashdata('success', 'Blog Category created successfully.');
                    redirect('industry_sectors');
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('industry_sectors/add', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function edit($industry_sector_id)
    {
        if (!$industry_sector_id) redirect('industry_sectors');

        $this->view_data['industry_sector'] = $this->industry_sectors_model->get_row(
            array(
                'industry_sector_id' => $industry_sector_id
            )
        );
        if (!$this->view_data['industry_sector']) redirect('industry_sectors');

        $this->view_data['title'] = 'Edit Blog Category';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->load->library('form_validation');

        if ($this->input->post()) {

            $this->form_validation->set_rules('name', 'Name', 'required|trim|min_length[1]|max_length[100]');
			$this->form_validation->set_rules('sub_heading', 'Sub Heading', 'required|trim|min_length[1]|max_length[100]');

            if ($this->form_validation->run()) {
                $data = array(
                    'name' => set_value('name'),
					'sub_heading' => set_value('sub_heading')
                );

                if ($this->industry_sectors_model->update_row(array('industry_sector_id' => $industry_sector_id), $data)) {
                    $this->session->set_flashdata('success', 'Blog Category updated successfully.');
                    redirect('industry_sectors/edit/' . $industry_sector_id);
                } else {
                    $this->view_data['error'] = "Database error";
                }
            }
        }

        $this->load->view('header', $this->view_data);
        $this->load->view('industry_sectors/edit', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($industry_sector_id)
    {
        if (!$industry_sector_id) redirect('industry_sectors');

        $this->view_data['industry_sector'] = $this->industry_sectors_model->get_row(
            array(
                'industry_sector_id' => $industry_sector_id
            )
        );
        if (!$this->view_data['industry_sector']) redirect('industry_sectors');

        if ($this->industry_sectors_model->delete_row(array('industry_sector_id' => $industry_sector_id))) {
            $this->session->set_flashdata('success', 'Blog Category deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('industry_sectors');
  }
}
