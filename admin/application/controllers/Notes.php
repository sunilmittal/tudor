<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Notes extends BOLD_Controller
{
	private $plugin = 'notes';
    private $version = '1.1.0';
    private $view_data = array();
    private $user = NULL;

	public function __construct()
	{
		parent::__construct();

		$this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

		$this->load->helper(['url', 'file']);
		$this->load->model(['notes_model', 'team_model', 'resource_inappropriate_model', 'comment_model']);

		if ($this->session->has_userdata('user_id')) {
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else {
            redirect('team/login');
        }
	}

	/**
	 * index function to show the listing of the User's notes
	 * @return void
	 */
	public function index() {
		$notes = $this->notes_model->get_rows();

		$this->view_data['title'] = 'Notes Manager';
		$this->view_data['error'] = $this->session->flashdata('error');
		$this->view_data['success'] = $this->session->flashdata('success');

		$this->view_data['notes'] = $notes;
		$this->load->view('header', $this->view_data);
        $this->load->view('notes/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
	}

	/**
	 * edit function to show the data of User on profile page
	 * @return void
	 */
	public function edit($note_id) {

		if (!$note_id) redirect('notes');

        $note_details = $this->notes_model->get_row(
            array(
                'id' => $note_id
            )
		);
		
		$flages_details = $this->resource_inappropriate_model->get_notes_flags($note_id);
    	if (!$note_details) redirect('notes');

    	$this->view_data['title'] = 'Edit note';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');
		$this->view_data['note_details'] = $note_details;
		$this->view_data['flages'] = $flages_details;

		$postParams = $this->input->post();
		if ($postParams) {
			if($postParams['status']==1){ 
				$data['deleted_at'] = date('Y-m-d H:i:s');
				$this->resource_inappropriate_model->update_rows(['resource_id' => $note_id],$data);
				$this->session->set_flashdata('success', 'Flag Inappropriate Dismissed successfully.');
				redirect('notes');
			} else{
				$data = array(
					'status' => 0
				);
				if ($this->notes_model->update_row(['id' => $note_id], $data)) {
					$this->session->set_flashdata('success', 'Note updated successfully.');
					redirect('notes');
				} else {
					$this->view_data['error'] = "Database error";
				}
			}
		} else {
			$this->load->view('header', $this->view_data);
	        $this->load->view('notes/edit', $this->view_data);
	        $this->load->view('footer', $this->view_data);
		}
	}

	/**
	 * view function to show the data of User on profile page
	 * @return void
	 */
	public function view($note_id) {

		if (!$note_id) redirect('notes');

        $note_details = $this->notes_model->get_row(
            array(
                'id' => $note_id
            )
		);
		
		$flages_details = $this->resource_inappropriate_model->get_notes_flags($note_id);
    	if (!$note_details) redirect('notes');

    	$this->view_data['title'] = 'Edit note';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');
		$this->view_data['note_details'] = $note_details;
		$this->view_data['flages'] = $flages_details;

		$this->load->view('header', $this->view_data);
		$this->load->view('notes/view', $this->view_data);
		$this->load->view('footer', $this->view_data);
	}
	
	public function delete($id)
    {
        if (!$id) redirect('notes');

        $this->view_data['notes'] = $this->notes_model->get_row(
            array(
                'id' => $id
            )
        );
        if (!$this->view_data['notes']) redirect('notes');

        if ($this->notes_model->delete_row(array('id' => $id))) {

			// Delete comments //
			$this->comment_model->delete_rows(array('resource_id' => $id, 'type' =>'notes'));

			// Delete replies //
			$this->comment_model->delete_rows(array('parent_id' => $id, 'type' =>'notes'));

			// Delete resource inappropriate //
			$this->resource_inappropriate_model->delete_rows(array('resource_id' => $id, 'resource_type' =>'note'));

            $this->session->set_flashdata('success', 'notes deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('videos');
    }
}
