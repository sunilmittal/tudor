<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends BOLD_Controller
{
    private $plugin = 'users';
    private $version = '1.1.0';
    private $view_data = array();
    private $user = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

        $this->load->model(['user_model', 'team_model', 'comment_model', 'video_history_model', 'subscribe_model', 'notes_model', 'resource_inappropriate_model', 'video_model']);

        $this->load->library('session');

        if ($this->session->has_userdata('user_id')) {
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else {
            redirect('team/login');
        }
    }

    public function index($show = NULL)
    {
        if (!$this->user)
            redirect('team/login');

        $this->view_data['title'] = 'Users Manager';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

        $this->view_data['users'] = $this->user_model->get_rows(['deleted_at' => null]);

        $this->load->view('header', $this->view_data);
        $this->load->view('users/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
    }

    public function delete($id)
    {
        if (!$id) redirect('users');

        $this->view_data['users'] = $this->user_model->get_row(
            array(
                'id' => $id
            )
        );
        if (!$this->view_data['users']) redirect('users');

        if ($this->user_model->delete_row(array('id' => $id))) {
            
            // Delete videos and history of user //
            $channel_id = getChannelId($id);
            $this->video_model->delete_rows(['channel_id' => $channel_id]);
            $this->video_history_model->delete_rows(['user_id' => $id]);
            $this->subscribe_model->delete_rows(['channel_id' => $channel_id]);
            $this->subscribe_model->delete_rows(['subscriber_id' => $id]);
            $this->comment_model->delete_rows(['user_id' => $id,'type' => 'videos']);
    
            // Delete notes and comments of user //
            $this->notes_model->delete_rows(['user_id' => $id]);
            $this->comment_model->delete_rows(['user_id' => $id,'type' => 'notes']);
            $this->resource_inappropriate_model->delete_rows(['user_id' => $id,'resource_type' => 'note']);
    
            // Delete channel of user //
            $this->channel_model->delete_channels(['user_id' => $id]);

            $this->session->set_flashdata('success', 'user deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('users');
	}
}
