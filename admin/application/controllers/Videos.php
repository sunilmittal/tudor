<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Videos extends BOLD_Controller
{
	private $plugin = 'videos';
    private $version = '1.1.0';
    private $view_data = array();
    private $user = NULL;

	public function __construct()
	{
		parent::__construct();

		$this->view_data['slug'] = $this->plugin;
        $this->view_data['plugin_groups'] = $this->plugin_groups;
        $this->view_data['plugins'] = $this->plugins;

		$this->load->helper(['url', 'file']);
		$this->load->model(['video_model', 'team_model', 'comment_model', 'video_history_model']);

		if ($this->session->has_userdata('user_id')) {
            $this->user = $this->team_model->get_row(array(
                'user_id' => $this->session->userdata('user_id')
            ));
            $this->view_data['active_user'] = $this->user;
        } else {
            redirect('team/login');
        }
	}

	/**
	 * index function to show the listing of the User's videos
	 * @return void
	 */
	public function index() {
		$videos = $this->video_model->get_rows();

		$this->view_data['title'] = 'Videos Manager';
		$this->view_data['error'] = $this->session->flashdata('error');
		$this->view_data['success'] = $this->session->flashdata('success');

		$this->view_data['videos'] = $videos;
		$this->load->view('header', $this->view_data);
        $this->load->view('videos/index', $this->view_data);
        $this->load->view('footer', $this->view_data);
	}

	/**
	 * edit function to show the data of User on profile page
	 * @return void
	 */
	public function edit($video_id) {

		if (!$video_id) redirect('videos');

        $video_details = $this->video_model->get_row(
            array(
                'id' => $video_id
            )
        );

    	if (!$video_details) redirect('videos');

    	$this->view_data['title'] = 'Edit video';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

		$this->view_data['video_details'] = $video_details;

		$postParams = $this->input->post();
		if ($postParams) {
			$data = array(
            	'is_featured' => set_value('is_featured') ?: null
			);

			if ($this->video_model->update_row(['id' => $video_id], $data)) {
                $this->session->set_flashdata('success', 'Video updated successfully.');
                redirect('videos');
            } else {
                $this->view_data['error'] = "Database error";
            }
		} else {
			$this->load->view('header', $this->view_data);
	        $this->load->view('videos/edit', $this->view_data);
	        $this->load->view('footer', $this->view_data);
		}
	}
	
	public function delete($id)
    {
        if (!$id) redirect('videos');

        $this->view_data['videos'] = $this->video_model->get_row(
            array(
                'id' => $id
            )
        );
        if (!$this->view_data['videos']) redirect('videos');

        if ($this->video_model->delete_row(array('id' => $id))) {

			// Delete comments //
			$this->comment_model->delete_rows(array('resource_id' => $id, 'type' =>'videos'));

			// Delete replies //
			$this->comment_model->delete_rows(array('parent_id' => $id, 'type' =>'videos'));

			// Delete video history //
			$this->video_history_model->delete_rows(array('video_id' => $id));

            $this->session->set_flashdata('success', 'video deleted successfully.');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }

        redirect('videos');
	}
	
	/**
	 * view function to show the data of Video on page
	 * @return void
	 */
	public function view($video_id) {

		if (!$video_id) redirect('videos');

        $video_details = $this->video_model->get_row(
            array(
                'id' => $video_id
            )
        );

    	if (!$video_details) redirect('videos');

    	$this->view_data['title'] = 'View video';
        $this->view_data['error'] = $this->session->flashdata('error');
        $this->view_data['success'] = $this->session->flashdata('success');

		$this->view_data['video_details'] = $video_details;

		$this->load->view('header', $this->view_data);
		$this->load->view('videos/view', $this->view_data);
		$this->load->view('footer', $this->view_data);
	}
}
