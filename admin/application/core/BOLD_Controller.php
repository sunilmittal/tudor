<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BOLD_Controller extends CI_Controller
{
    protected $plugin_groups = array();
    protected $plugins = array();

    public function __construct()
    {
        parent::__construct();

        $plugins_dir = scandir(APPPATH . 'plugins');

        $plugins = array();
        foreach ($plugins_dir as $file)
            if (preg_match("/(.*?)\.php/si", $file)) include_once APPPATH . 'plugins' . DIRECTORY_SEPARATOR . $file;

        if (count($plugins) > 0) {
            foreach ($plugins as $plugin) {
                $this->plugins[$plugin['slug']] = $plugin;

                if (!in_array($plugin['group'], $this->plugin_groups)) $this->plugin_groups[] = $plugin['group'];
            }
        }
    }
}