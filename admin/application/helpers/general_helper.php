<?php

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('specialCharToHyphen')) {
    /**
     *
     * @param    string $string
     * @return    string
     */
    function specialCharToHyphen()
    {
        $args = func_get_args();
        $string = join(" ", $args);
        $string = str_replace(' ', '-', $string);
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        return strtolower(preg_replace('/-+/', '-', $string));
    }
}



if (!function_exists('json_response')) {
    /**
     *
     * @param    array $array
     * @param    string $keyToFlat
     * @param    string $keyToAdd
     * @param    string $keyValueGetFrom
     * @return    string
     */
    function json_response($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        die;
    }
}

if (!function_exists('getUserRole')) {
    /**
     *
     * @param    array $array
     * @param    string $keyToFlat
     * @param    string $keyToAdd
     * @param    string $keyValueGetFrom
     * @return    string
     */
    function getUserRole($user)
    {
        return $user->user_role == USER_ROLE ? 'Learner' : 'Teacher';
    }
}

if (!function_exists('request_wants_json')) {
    /**
     *
     * @param    array $array
     * @param    string $keyToFlat
     * @param    string $keyToAdd
     * @param    string $keyValueGetFrom
     * @return    string
     */
    function request_wants_json()
    {
        $headers = apache_request_headers();
        if (isset($headers['Accept']) && strpos('application/json', $headers['Accept']) !== false) {
            return true;
        }
        return false;
    }
}

if (!function_exists('getDateTimeFormat')) {
    function getDateTimeFormat($date) {
        return date('d-M-Y h:i A',strtotime($date));
    }
}

if (!function_exists('getDateInFormat')) {
    // embed youtube URLs
    function getDateInFormat($date) {
        return date("M d, Y", strtotime($date));
    }
}

if (!function_exists('getEmbedYouTubeURL')) {
    // embed youtube URLs
    function getEmbedYouTubeURL($text) {
        $text = preg_replace('~(?#!js YouTubeId Rev:20160125_1800)
            # Match non-linked youtube URL in the wild. (Rev:20130823)
            https?://          # Required scheme. Either http or https.
            (?:[0-9A-Z-]+\.)?  # Optional subdomain.
            (?:                # Group host alternatives.
              youtu\.be/       # Either youtu.be,
            | youtube          # or youtube.com or
              (?:-nocookie)?   # youtube-nocookie.com
              \.com            # followed by
              \S*?             # Allow anything up to VIDEO_ID,
              [^\w\s-]         # but char before ID is non-ID char.
            )                  # End host alternatives.
            ([\w-]{11})        # $1: VIDEO_ID is exactly 11 chars.
            (?=[^\w-]|$)       # Assert next char is non-ID or EOS.
            (?!                # Assert URL is not pre-linked.
              [?=&+%\w.-]*     # Allow URL (query) remainder.
              (?:              # Group pre-linked alternatives.
                [\'"][^<>]*>   # Either inside a start tag,
              | </a>           # or inside <a> element text contents.
              )                # End recognized pre-linked alts.
            )                  # End negative lookahead assertion.
            [?=&+%\w.-]*       # Consume any URL (query) remainder.
            ~ix', 'https://www.youtube.com/embed/$1',
            $text);
        // return $text;
        return "https://www.youtube.com/embed/$text";
    }
}

if (!function_exists('getChannelId')) {
	function getChannelId($user_id = null) {
		$CI = &get_instance();
		$channel_id = 0;

		$CI->load->model('channel_model');
		$channel = $CI->channel_model->get_row(['user_id' => $user_id]);
		
		if ($channel) {
			$channel_id = $channel->id;
		}

		return $channel_id;
	}
}
