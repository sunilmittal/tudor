<?php
/**
 * REQUEST
 *
 */

class Req
{
    static public $ajaxResponse = array(
            'error' => array(),
            'res' => array()
        );


    /**
     * Function startAjax
     */
    static public function startAjax()
    {
        if (empty($_SERVER['HTTP_X_REQUESTED_WITH']))
            exit('Error page: x_request');
    }

    static public function isError()
    {
        if (self::$ajaxResponse['error'])
            return true;
        else
            return false;
    }

    static public function addError($key = false, $value = false)
    {
        $error = array(
            'key' => $key,
            'value' => $value
        );

        array_push(self::$ajaxResponse['error'], $error);
    }

    static public function addResponse($action = false, $key = false, $value = false, $attrName = false)
    {
        $res = array(
            'action' => $action,
            'key' => $key,
            'value' => $value
        );
        if ($attrName)
            $res['attrName'] = $attrName;

        array_push(self::$ajaxResponse['res'], $res);
    }

    /**
     * Function responseAjax
     * @param bool $response
     */
    static public function responseAjax($response = false)
    {
        if (is_array($response)) {
            foreach ($response AS $key => $value) {
                if (is_array($value)) {
                    self::$ajaxResponse[$key] = array();

                    foreach ($value AS $k => $v) {
                        self::$ajaxResponse[$key][$k] = $v;
                    }
                } else {
                    self::$ajaxResponse[$key] = $value;
                }
            }
            //self::$ajaxResponse = array_merge(self::$ajaxResponse, $response);
        }
    }

    /**
     * Function endAjax
     * @param bool $response
     */
    static public function endAjax()
    {
        echo json_encode(self::$ajaxResponse);
        exit;
    }

    /**
     * Adding error to response and exit from page processing
     * @param bool $error
     */
    static public function returnError($error = false)
    {
        if ($error) {
            $response['error'] = $error;
            self::responseAjax($response);
        }

        echo json_encode(self::$ajaxResponse);
        exit;
    }
}
/* End of file */