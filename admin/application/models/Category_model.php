<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

    protected $table = 'category';

    private function make_select_line($list = []) {
        return implode(',', array_map(function($element) {
            return $this->table.'.'.$element;
        }, $list));
    }

    /**
     * add_row function add the row in the table that is define as property of the model class.
     * @param array $data 
     * @return mixed
    */
    public function add_row($data, $needId = false) {
        $data['created_at'] = date('Y-m-d H:i:s');
        $query = $this->db->insert($this->table, $data);

        if ($query) {
            return $needId ? $this->db->insert_id() : true;
        } else {
            return false;
        }
    }

    /**
     * get_row function get the single row from the table that is define as property of the model class.
     * @param array $where 
     * @param bool $returnAsArray 
     * @param array $or_where 
     * @return mixed
    */
    public function get_row($where = [], $returnAsArray = false, $or_where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }
        $this->db->where($this->table.'.deleted_at', null);

        return $returnAsArray ? $this->db->get($this->table)->row_array() : $this->db->get($this->table)->row();
    }

    /**
     * get_rows function get the multiple rows from the table that is define as property of the model class.
     * @param array $where 
     * @param bool $returnAsArray 
     * @param array $or_where 
     * @param string $raw_where 
     * @return mixed
    */
    public function get_rows($where = [], $returnAsArray = false, $or_where = [], $raw_where = null) {
        // $this->db->select($this->make_select_line(['id', 'name']));
        $this->db->select('category.id, category.name, parent_category.name as parent_category_name');
        $this->db->join('category as parent_category', 'category.parent_id = parent_category.id', 'left');
        foreach ($where as $column => $value) {
            $this->db->where($this->table.'.'.$column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }
        if ($raw_where) {
            $this->db->where($raw_where);
        }
        $this->db->where($this->table.'.deleted_at', null);

        return $returnAsArray ? $this->db->get($this->table)->result_array() : $this->db->get($this->table)->result();
    }

    /**
     * update_row function update the row in the table that is define as property of the model class.
     * @param array $where 
     * @param array $data 
     * @return bool
    */
    public function update_row($where = [], $data = []) {
        if ($where && $data) {

            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
            $this->db->where($this->table.'.deleted_at', null);

            $this->db->limit(1);
            return $this->db->update($this->table, $data);
        }
        return false;
    }

    /**
     * delete_row function update the row in the table that is define as property of the model class.
     * @param array $where 
     * @param array $data 
     * @return bool
    */
    public function delete_row($where = []) {
        if ($where) {

            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
            $this->db->limit(1);
            return $this->db->update($this->table, ['deleted_at' => date('Y-m-d H:i:s')]);
        }
        return false;
    }

    /**
     * get_rows function get the multiple rows from the table that is define as property of the model class.
     * @param array $where 
     * @param bool $returnAsArray 
     * @param array $or_where 
     * @param string $raw_where 
     * @return mixed
    */
    public function num_rows($where = [], $or_where = [], $raw_where = null, $with_trashed = false) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        if ($or_where) {
            $this->db->group_start();
            foreach ($or_where as $column => $value) {
                $this->db->or_where($column, $value);
            }
            $this->db->group_end();
        }
        
        if ($raw_where) {
            $this->db->where($raw_where);
        }

        if (!$with_trashed) {
            $this->db->where($this->table.'.deleted_at', null);
        }

        return $this->db->get($this->table)->num_rows();
    }
}