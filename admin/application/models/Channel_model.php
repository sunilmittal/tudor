<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class channel_model extends CI_Model {

    protected $table = 'channels';

    /**
     * get_row function get the single row from the table that is define as property of the model class.
     * @param array $where 
     * @param bool $returnAsArray 
     * @param array $or_where 
     * @return mixed
    */
    public function get_row($where = [], $returnAsArray = false, $or_where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }

        return $returnAsArray ? $this->db->get($this->table)->row_array() : $this->db->get($this->table)->row();
    }

    public function delete_channels($where = []){
		if ($where) {
			foreach ($where as $column => $value) {
				$this->db->where($column, $value);
			}
			return $this->db->delete($this->table);
		}
		return false;
	}
}