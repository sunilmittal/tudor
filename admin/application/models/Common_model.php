<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common_model extends CI_Model
{
	public function is_slug_exists($table_name, $field_value, $field_name = 'slug') {
        return $this->db->where($field_name, $field_value)->get($table_name)->row();
    }
}