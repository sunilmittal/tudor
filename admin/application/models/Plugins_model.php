<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Plugins_model extends CI_Model
{
    public function plugin_is_set()
    {
        $this->db->query("SHOW TABLES LIKE 'plugins';");
        return $this->db->affected_rows() > 0;
    }

    public function plugin_is_installed($slug)
    {
        $this->db->where("slug", $slug);
        $this->db->limit(1);
        return (bool)$this->db->count_all_results('plugins');
    }

    public function plugin_is_actual($slug, $version)
    {
        $this->db->where("slug", $slug);
        $this->db->where("version", $version);
        $this->db->limit(1);
        return (bool)$this->db->count_all_results('plugins');
    }

    public function plugin_get_actual_version($slug)
    {
        $this->db->where("slug", $slug);
        $this->db->limit(1);
        return $this->db->get('plugins')->row()->version;
    }

    public function plugin_install($slug, $version)
    {
        return $this->db->insert('plugins', array(
            'slug' => $slug,
            'version' => $version
        ));
    }

    public function plugin_upgrade($slug, $version)
    {
        $this->db->where("slug", $slug);
        $this->db->limit(1);
        return $this->db->update('plugins', array(
            'version' => $version
        ));
    }

}