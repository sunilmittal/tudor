<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Resource_inappropriate_model extends CI_Model {

	protected $table = 'resource_inappropriate';

	private function make_select_line($list = []) {
		return implode(',', array_map(function($element) {
			return $this->table.'.'.$element;
		}, $list));
	}

	/**
	 * update_row function update the row in the table that is define as property of the model class.
	 * @param array $where 
	 * @param array $data 
	 * @return bool
	*/
    public function update_row($where = [], $data = []) {
    	if ($where && $data) {

			$data['updated_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
    }

    /**
	 * get_row function get the single row from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @return mixed
	*/
    public function get_row($where = [], $returnAsArray = false, $or_where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }

        $this->db->where($this->table.'.deleted_at', null);

        $note = $this->db->get($this->table)->row();

        return $note;
    }

    /**
	 * get_rows function get the multiple rows from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @param string $raw_where 
	 * @return mixed
	*/
    public function get_rows($where = [], $returnAsArray = false, $or_where = [], $raw_where = null, $with_trashed = false) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        if ($or_where) {
        	$this->db->group_start();
	        foreach ($or_where as $column => $value) {
	            $this->db->or_where($column, $value);
	        }
        	$this->db->group_end();
        }
        
        if ($raw_where) {
        	$this->db->where($raw_where);
        }

        if (!$with_trashed) {
        	$this->db->where($this->table.'.deleted_at', null);
        }

        $notes = $this->db->get($this->table)->result();
        if ($notes) {
            foreach ($notes as $note) {
                $this->getUserDetails($note);
                $note->flag_exists = $this->flag_exists($note);
            }
        }
        return $notes;
    }

    public function flag_exists($note)
    {
        $this->db->where(['resource_id'=>$note->id,'resource_type'=>'note','deleted_at'=>NULL]);
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0){
            return true;
        } else{
            return false;
        }
    }

    public function get_notes_flags($note_id){
		$notes_flag =  $this->db->select('resource_inappropriate.user_id,resource_inappropriate.message')
                    ->join('notes', 'resource_inappropriate.resource_id = notes.id', 'left')
                    ->where(['resource_id'=>$note_id,'resource_type'=>'note'])
                    ->get($this->table)->result();
        if ($notes_flag) {
            foreach ($notes_flag as $flag) {
                $this->getUserDetails($flag);
            }
        }
        return $notes_flag;
	}

    private function getUserDetails($data) {
    	$data->user = $this->db->select('name as user_name, image, user_role')
                                ->where(['id' => $data->user_id])->get('users')->row();
	}

	/**
	 * search function search rows in the table that is define as property of the model class.
	 * @return array of object or JSON
	*/
	public function search($where = [], $searchEle = [], $limit = 2, $offset = 0, $sort_by = [], $json = false) {

        $response = [];

        extract($searchEle);
        if (empty($term)) $term = null;
        if (empty($category)) $category = null;

        $raw_where = null;

        $notes = collect($this->get_rows($where));

        if ($term) {
            $notes = $notes->reject(function ($element) use ($term) {
                return mb_strpos(strtolower($element->title), strtolower($term)) === false && mb_strpos(strtolower($element->description), strtolower($term)) === false;
            })->values();
        }

        if ($category) {
            $notes = $notes->reject(function ($element) use ($category) {
                return mb_strpos(strtolower(getCategoryNamesLine($element)), strtolower($category)) === false;
            })->values();
        }

        if ($limit) {
            $notes = $notes->slice($offset, $limit)->values();
        }

        $response = $notes;
        if (!$json) {
            return $response;
        }
        json_response($response);
    }
    
    /**
	 * update_row function update the rows in the table that is define as property of the model class.
	 * @param array $where 
	 * @param array $data 
	 * @return bool
	*/
    public function update_rows($where = [], $data = []) {
    	if ($where && $data) {
			$data['updated_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        return $this->db->update($this->table, $data);
    	}
    	return false;
    }

    /**
     * delete_row function update the row in the table that is define as property of the model class.
     * @param array $where 
     * @param array $data 
     * @return bool
    */
    public function delete_rows($where = []) {
        if ($where) {

            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
           
            return $this->db->update($this->table, ['deleted_at' => date('Y-m-d H:i:s')]);
        }
        return false;
    }
}