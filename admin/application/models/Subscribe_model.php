<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class subscribe_model extends CI_Model {

    protected $table = 'subscribe';

    /**
	 * delete_row function add the row in the table that is define as property of the model class.
	 * @param array $data 
	 * @return mixed
	*/
	public function delete_row($where) {
    	if ($where) {
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->delete($this->table);
    	}
    	return false;
	}
	
	public function delete_rows($where) {
    	if ($where) {
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        return $this->db->delete($this->table);
    	}
    	return false;
    }
}