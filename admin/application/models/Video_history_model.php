<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Video_history_model extends CI_Model {

    protected $table = 'video_history';

	private function make_select_line($list = []) {
		return implode(',', array_map(function($element) {
			return $this->table.'.'.$element;
		}, $list));
	}

	/**
     * delete_row function update the row in the table that is define as property of the model class.
     * @param array $where 
     * @param array $data 
     * @return bool
    */
    public function delete_rows($where = []) {
        if ($where) {

            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
           
            return $this->db->update($this->table, ['deleted_at' => date('Y-m-d H:i:s')]);
        }
        return false;
    }
}