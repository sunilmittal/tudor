<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'categories',
    'title' => 'Categories',
    'enabled' => TRUE,
    'icon' => ms_site_url('assets/css/backend/images/dashboard-icons/team-manager.png'),
	'group_icon' => ms_site_url('assets/css/backend/images/dashboard-icons/team-management.png'),
    'group' => 'Category Management',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_enabled' => FALSE
);
$plugins[] = array(
    'slug' => 'industry_sectors',
    'title' => 'Blog Categories',
    'enabled' => TRUE,
    'icon' => ms_site_url('assets/css/backend/images/dashboard-icons/team-manager.png'),
	'group_icon' => ms_site_url('assets/css/backend/images/dashboard-icons/team-management.png'),
    'group' => 'Category Management',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_enabled' => FALSE
);