<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'videos',
    'title' => 'Videos Manager',
    'enabled' => TRUE,
    'icon' => ms_site_url('assets/css/backend/images/dashboard-icons/video-manager.png'),
	'group_icon' => ms_site_url('assets/css/backend/images/dashboard-icons/video.png'),
    'group' => 'Content Management',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_enabled' => FALSE
);

$plugins[] = array(
    'slug' => 'notes',
    'title' => 'Notes Manager',
    'enabled' => TRUE,
    'icon' => ms_site_url('assets/css/backend/images/dashboard-icons/vacancy-manager.png'),
	'group_icon' => ms_site_url('assets/css/backend/images/dashboard-icons/notes-manager.png'),
    'group' => 'Content Management',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_enabled' => FALSE
);
$plugins[] = array(
    'slug' => 'blog',
    'title' => 'Blog',
    'enabled' => TRUE,
    'icon' => ms_site_url('assets/css/backend/images/dashboard-icons/content-management/blog-news.png'),
	'group_icon' => ms_site_url('assets/css/backend/images/dashboard-icons/content-management.png'),
    'group' => 'Content Management',
    'widget_statistic_enabled' => TRUE,
    'widget_statistic_title' => "Blog Posts",
    'widget_block_enabled' => FALSE,
    'widget_block_sort' => 5,
    'widget_block_view_data' => function ($CI) {
        $view_data = array();

        $CI->load->model('blog_model');
        $view_data['blogs'] = $CI->blog_model->get(array(), 7, 0, 'date', 'DESC');

        return $view_data;
    }
);