<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'team',
    'title' => 'Team Manager',
    'enabled' => TRUE,
    'icon' => ms_site_url('assets/css/backend/images/dashboard-icons/team-manager.png'),
	'group_icon' => ms_site_url('assets/css/backend/images/dashboard-icons/team-management.png'),
    'group' => 'Team Management',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_enabled' => FALSE
);