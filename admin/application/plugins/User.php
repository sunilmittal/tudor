<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$plugins[] = array(
    'slug' => 'users',
    'title' => 'Users',
    'enabled' => TRUE,
    'icon' => ms_site_url('assets/css/backend/images/dashboard-icons/team-manager.png'),
	'group_icon' => ms_site_url('assets/css/backend/images/dashboard-icons/team-management.png'),
    'group' => 'User Management',
    'widget_statistic_enabled' => FALSE,
    'widget_statistic_title' => NULL,
    'widget_block_enabled' => FALSE
);