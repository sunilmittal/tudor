<link rel="stylesheet" href="<?php echo ms_base_url('assets/css/backend/extra/bootstrap-combined.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo ms_base_url('assets/css/backend/extra/bootstrap-datetimepicker.min.css'); ?>" />
<div id="control-container">
	<div id="button-holder">
		<a href="<?php echo site_url('blog'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
		<div class="clr"></div>
	</div>
	<h1>
		<i class="fas fa-rss"></i>Blog <i class="fas fa-caret-right"></i>Edit
	</h1>
	<hr />
	<?php if (isset($success) && $success) { ?>
		<div class="success">
			<i class="fas fa-check-circle"></i><?php echo $success; ?>
		</div>
	<?php } ?>
	<?php if (isset($error) && $error) { ?>
		<div class="error">
			<i class="fas fa-check-circle"></i><?php echo $error; ?>
		</div>
	<?php } ?>
	<?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
	<form action="<?php echo site_url('blog/edit/' . $blog->blog_id); ?>" method="post" enctype="multipart/form-data">
		<div class="form-section">
			<span class="heading">General</span>
			<div class="col half_column_left">
				<label for="category_id">
					Select Blog Category
				</label>
				<select name="category_id" id="category_id" required>
					<?php if (isset($sectors) && is_array($sectors) && count($sectors) > 0) { ?>
						<option value="">Please select category</option>
						<?php foreach ($sectors as $sector) { ?>
							<?php $selectedCategory = ($sector->industry_sector_id == $blog->category_id) ? 'selected' : ''; ?>
							<option <?= $selectedCategory ?> value="<?php echo $sector->industry_sector_id; ?>"><?php echo $sector->name ?></option>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
			<div class="col half_column_right">
				<label for="subject_id">
					Select Subject
				</label>
				<select name="subject_id" id="subject_id" required>
					<?php if (isset($subjects) && is_array($subjects) && count($subjects) > 0) { ?>
						<option value="">Please select subject</option>
						<?php foreach ($subjects as $subject) { ?>
							<?php $selectedSubject = ($subject->id == $blog->subject_id) ? 'selected' : ''; ?>
							<option <?= $selectedSubject ?> value="<?php echo $subject->id; ?>"><?php echo $subject->name ?></option>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
			<div class="col half_column_left">
				<label for="thumbnail">
					Thumbnail Image (size should be 700x630)
					<?php if ($blog->thumbnail) { ?>
						<a href="<?php echo ms_base_url('admin/uploads/images/' . $blog->thumbnail); ?>" target="_blank">
							<i class="fa fa-download"></i>
						</a>
					<?php } ?>
					<small><i>(Image files must be under <?php echo file_upload_max_size_format() ?>, and JPG, GIF or PNG format)</i></small>
				</label>
				<input type="file" name="thumbnail" id="thumbnail" class="inputfile" />
			</div>
			<div class="col half_column_right">
				<label for="image">
					Main Image
					<?php if ($blog->image) { ?>
						<a href="<?php echo ms_base_url('admin/uploads/images/' . $blog->image); ?>" target="_blank">
							<i class="fa fa-download"></i>
						</a>
					<?php } ?>
					<small><i>(Image files must be under <?php echo file_upload_max_size_format() ?>, and JPG, GIF or PNG format)</i></small>
				</label>
				<input type="file" name="image" id="image" class="inputfile" />
			</div>
			<div class="col half_column_left">
				<label for="title">
					Blog Title
				</label>
				<input type="text" name="title" id="title" value="<?php echo set_value('title') ? set_value('title') : ($blog->title ? $blog->title : "") ?>">
			</div>
			<div class="col half_column_right">
				<label for="author_by">
					Blog By
				</label>
				<input type="text" name="author_by" id="author_by" value="<?php echo set_value('author_by') ? set_value('author_by') : ($blog->author_by ? $blog->author_by : "") ?>">
			</div>

			<div class="col half_column_left">
				<label for="photo_caption">
					Photo Caption
				</label>
				<input type="text" name="photo_caption" id="photo_caption" value="<?php echo set_value('photo_caption') ? set_value('photo_caption') : ($blog->photo_caption ? $blog->photo_caption : "") ?>">
			</div>

			<div class="col half_column_right">
				<label for="reading_time">
					Reading Time
				</label>
				<input type="text" name="reading_time" id="reading_time" value="<?php echo set_value('reading_time') ? set_value('reading_time') : ($blog->reading_time ? $blog->reading_time : "") ?>">
			</div>
			<div class="col half_column_left">
				<label for="date">
					Published Date
				</label>
				<div id="datetimepicker" class="input-append date">
					<input type="text" placeholder="Select Date Time" name="date" id="date" required value="<?php echo set_value('date') ? set_value('date') : ($blog->date ? $blog->date : "") ?>" readonly style="width:97%">
					<span class="add-on cusdattimee">
						<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
					</span>
				</div>
			</div>
			<div class="col half_column_right">
				<label for="short_description">
					Short Description
				</label>
				<textarea name="short_description" id="short_description"><?php echo set_value('short_description') ? set_value('short_description') : ($blog->short_description ? $blog->short_description : "") ?></textarea>
			</div>
			<div class="col half_column_left">
				<label for="quote_title">
					Quote Title
				</label>
				<input type="text" name="quote_title" id="quote_title" value="<?php echo set_value('quote_title') ? set_value('quote_title') : ($blog->quote_title ? $blog->quote_title : "") ?>">
			</div>
			<div class="col half_column_right">
				<label for="quote_author">
					Quote Author
				</label>
				<input type="text" name="quote_author" id="quote_author" value="<?php echo set_value('quote_author') ? set_value('quote_author') : ($blog->quote_author ? $blog->quote_author : "") ?>">
			</div>
			<div class="col half_column_left">
				<label for="overview">
					Overview
				</label>
				<textarea name="overview" id="overview"><?php echo set_value('overview') ? set_value('overview') : ($blog->overview ? $blog->overview : "") ?></textarea>
			</div>
			<div class="col half_column_right">
				<label for="is_featured">
					Is Featured
				</label>
				<input type="checkbox" name="is_featured" id="is_featured" <?= set_value('is_featured') == 1 ? 'checked' : (($blog->is_featured == 1) ? 'checked' : '' ) ?> value="1">
			</div>
			<div class="clr"></div>
		</div>
		<div class="form-section">
			<span class="heading">Page Content</span>
			<div class="col full_column">
				<textarea name="content" id="content" rows="20"><?php echo set_value('content', $blog->content, FALSE); ?></textarea>
			</div>
			<div class="clr"></div>
		</div>
		<div class="form-section">
			<span class="heading">On-page SEO</span>
			<div class="col half_column_left">
				<label for="meta_title">
					Meta Title<a href="https://moz.com/learn/seo/title-tag" target="_blank"><i class="fas fa-info-circle"></i></a>
				</label>
				<input type="text" name="meta_title" id="meta_title" value="<?php echo set_value('meta_title') ? set_value('meta_title') : ($blog->meta_title ? $blog->meta_title : "") ?>">
			</div>
			<div class="col half_column_right">
				<label for="meta_keywords">
					Meta Keywords<a href="https://moz.com/learn/seo/what-are-keywords" target="_blank"><i class="fas fa-info-circle"></i></a>
				</label>
				<input type="text" name="meta_keywords" id="meta_keywords" value="<?php echo set_value('meta_keywords') ? set_value('meta_keywords') : ($blog->meta_keywords ? $blog->meta_keywords : "") ?>">
			</div>
			<div class="col half_column_left">
				<label for="meta_desc">
					Meta Description<a href="https://moz.com/learn/seo/meta-description" target="_blank"><i class="fas fa-info-circle"></i></a>
				</label>
				<input type="text" name="meta_desc" id="meta_desc" value="<?php echo set_value('meta_desc') ? set_value('meta_desc') : ($blog->meta_desc ? $blog->meta_desc : "") ?>">
			</div>

			<div class="col half_column_right">
				<label for="slug">
					URL Slug<a href="https://moz.com/blog/15-seo-best-practices-for-structuring-urls" target="_blank"><i class="fas fa-info-circle"></i></a>
				</label>
				<input type="text" name="slug" id="slug" value="<?php echo set_value('slug') ? set_value('slug') : ($blog->slug ? $blog->slug : "") ?>">
			</div>
			<div class="clr"></div>
		</div>
		
		<div class="form-section">
			<button type="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
			<a href="<?php echo site_url('blog'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
			<div class="clr"></div>
		</div>
	</form>
</div>

<link rel="stylesheet" href="<?php echo ms_base_url('assets/plugins/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') ?>">
<script src="<?php echo ms_base_url('assets/plugins/ckeditor/ckeditor.js') ?>"></script>
<script src="<?php echo ms_base_url('assets/plugins/ckeditor/samples/js/sample.js'); ?>"></script>
<script>
	var company_name = '<?php echo isset($company_name) ? $company_name : ''; ?>';
	var website_url = '<?php echo ms_site_url(); ?>';

	$(function() {
		$("#title").keyup(function() {
			if ($(this).val().trim().length > 0) {
				$("#meta_title").val(company_name + " | " + $(this).val().trim());
				$("#slug").val("" + $(this).val().trim().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase());
				$("#canonical").val(website_url + "article/" + $(this).val().trim().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase());
			} else {
				$("#meta_title").val("");
				$("#slug").val("");
				$("#canonical").val("");
			}
		});

		var editor = CKEDITOR.replace('content', {
			htmlEncodeOutput: false,
			wordcount: {
				showWordCount: true,
				showCharCount: true,
				countSpacesAsChars: true,
				countHTML: false,
			},
			removePlugins: 'zsuploader',

			filebrowserBrowseUrl: '<?php echo ms_base_url('assets/plugins/kcfinder/browse.php?opener=ckeditor&type=files'); ?>',
			filebrowserImageBrowseUrl: '<?php echo ms_base_url('assets/plugins/kcfinder/browse.php?opener=ckeditor&type=images'); ?>',
			filebrowserUploadUrl: '<?php echo ms_base_url('assets/plugins/kcfinder/upload.php?opener=ckeditor&type=files'); ?>',
			filebrowserImageUploadUrl: '<?php echo ms_base_url('assets/plugins/kcfinder/upload.php?opener=ckeditor&type=images'); ?>'
		});
	});
</script>
<script type="text/javascript"
src="<?php echo ms_base_url('assets/css/backend/extra/bootstrap.min.js'); ?>" />
</script>
<script type="text/javascript"
src="<?php echo ms_base_url('assets/css/backend/extra/bootstrap-datetimepicker.min.js'); ?>" />
</script>
<script type="text/javascript"
src="<?php echo ms_base_url('assets/css/backend/extra/bootstrap-datetimepicker.pt-BR.js'); ?>" />
</script>
<script type="text/javascript">
	$('#datetimepicker').datetimepicker({
		format: 'yyyy-MM-dd hh:mm:ss'
	});
</script>