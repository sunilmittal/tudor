<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('blog/add'); ?>" class="btn add">
            <i class="fas fa-plus-circle"></i> Add New Blog Post
        </a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-rss"></i>Blog
    </h1>
    <hr />
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <table id="blog">
        <thead>
            <tr>
                <th align="center">ID</th>
                <th>Title</th>
                <th align="center">Options</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Options</th>
            </tr>
        </tfoot>
        <tbody>
            <?php if (isset($blog) && is_array($blog) && count($blog)) { ?>
                <?php foreach ($blog as $blog_row) { ?>
                    <tr>
                        <td align="center">
                            <?php echo $blog_row->blog_id; ?>
                        </td>
                        <td>
                            <?php echo $blog_row->title; ?>
                        </td>
                        <td align="center">
                            <a href="#" onclick="share_linkedin(this);" data-url="<?php echo ms_site_url('article/' . $blog_row->slug); ?>" class="icon fab fa-fw fa-linkedin tooltip" title="Share To LinkedIn"></a>
                            <a href="#" onclick="share_facebook(this);" data-url="<?php echo ms_site_url('article/' . $blog_row->slug); ?>" class="icon fab fa-fw fa-facebook tooltip" title="Share To Facebook"></a>
                            <a href="#" onclick="share_twitter(this);" data-url="<?php echo ms_site_url('article/' . $blog_row->slug); ?>" class="icon fab fa-fw fa-twitter tooltip" title="Share To Twitter"></a>
                            <a href="#" onclick="copy_link_modal(this);" data-url="<?php echo ms_site_url('article/' . $blog_row->slug); ?>" class="icon fa fa-fw fa-link tooltip" title="Copy Link"></a>
                            <a href="<?php echo ms_site_url('article/' . $blog_row->slug); ?>" class="icon fa fa-fw fa-eye tooltip" target="_blank" title="View blog"></a>
                            <a href="<?php echo site_url('blog/edit/' . $blog_row->blog_id); ?>" class="icon fa fa-fw fa-pencil-alt tooltip" title="Edit"></a>
                            <a href="<?php echo site_url('blog/delete/' . $blog_row->blog_id); ?>" class="icon fa fa-fw fa-trash tooltip" title="Delete"></a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
</div>

<div id="copy-link-dialog" class="copy_link_modal" title="Copy Link">
    <input type="text" id="copy_link_profile_url" class="copy_link_modal_input" title="Copy Link" readonly value="">
</div>

<script>
    function share_linkedin(e) {
        window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + $(e).data('url'), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
        return false;
    }

    function share_twitter(e) {
        window.open('https://twitter.com/intent/tweet?url=' + $(e).data('url'), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
        return false;
    }

    function share_facebook(e) {
        window.open('https://www.facebook.com/sharer/sharer.php?u=' + $(e).data('url'), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
        return false;
    }

    function copy_link_modal(el) {
        $('#copy_link_profile_url').val($(el).data('url'));
        $('#copy_link_profile_url').tooltipster('content', 'Copy Link');

        $("#copy-link-dialog").dialog({
            resizable: false,
            height: "auto",
            width: 600,
            modal: true,
            buttons: {
                "Copy": function() {
                    var copyText = document.getElementById("copy_link_profile_url");
                    copyText.select();
                    document.execCommand("copy");

                    $('#copy_link_profile_url').tooltipster('content', 'Copied');
                    $('#copy_link_profile_url').tooltipster('open');
                },
                Cancel: function() {
                    $('#copy_link_profile_url').tooltipster('close');
                    $(this).dialog("close");
                }
            }
        });
    }

    $(function() {
        $('#copy_link_profile_url').tooltipster({
            selfDestruction: false
        });

        var table = $('#blog').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [
                [2, 'asc']
            ],
            "aoColumnDefs": [{
                'bSortable': false,
                'aTargets': [2]
            }],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#blog tfoot th').each(function() {
            var title = $('#blog tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function(a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function(a) {
            $("input", table.column(a).footer()).on("keyup change", function() {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>