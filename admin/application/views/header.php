
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>
        <?php echo isset($title) && $title ? $title . ' / ' : ''; ?>
        Tudor Learning
    </title>

    <link rel="stylesheet" href="<?php echo ms_base_url('assets/css/backend/main.css'); ?>" />
    <link rel="stylesheet" href="<?php echo ms_base_url('assets/css/backend/jquery-ui.css'); ?>" />
    <link rel="stylesheet" href="<?php echo ms_base_url('assets/css/backend/all.css'); ?>" />
    <link rel="stylesheet" href="<?php echo ms_base_url('assets/plugins/data-tables/jquery.datatables.css'); ?>" />
    <link rel="stylesheet" href="<?php echo ms_base_url('assets/css/backend/jquery_scroll.css'); ?>" />
    <link rel="stylesheet" type="text/css"
        href="<?php echo ms_base_url('assets/plugins/tooltipster-master/'); ?>dist/css/tooltipster.bundle.min.css" />
    <link rel="stylesheet" type="text/css"
        href="<?php echo ms_base_url('assets/plugins/tooltipster-master/'); ?>dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-borderless.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    <link rel="shortcut icon" href="<?php echo ms_base_url('images/logo.png'); ?>" type="image/png" />
    <link type="image/x-icon" rel="shortcut icon" href="<?php echo ms_base_url('assets/images/backend/favicon.png'); ?>" />
	<link type="image/x-icon" rel="icon" href="<?php echo ms_base_url('assets/images/backend/favicon.png'); ?>" />

    <script src="<?php echo ms_base_url('assets/js/backend/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?php echo ms_base_url('assets/js/backend/jquery-ui.min.js'); ?>"></script>
    <script src="<?php echo ms_base_url('assets/js/backend/jquery.scrollbar.js'); ?>"></script>
    <script src="<?php echo ms_base_url('assets/js/backend/function.js'); ?>"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script
        src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.2/b-flash-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js">
    </script>
    <script src="<?php echo ms_base_url('assets/plugins/tooltipster-master/dist/js/tooltipster.bundle.min.js'); ?>">
    </script>


    <script type="text/javascript">
    $(function() {
        $(document).ready(function() {
            $('.tooltip').tooltipster({
                animation: 'grow',
                delay: 100,
                theme: 'tooltipster-borderless',
                trigger: 'hover',
                timer: 1000
            });
        });
        $('.fa-trash').confirm({
            buttons: {
                tryAgain: {
                    text: 'Yes, delete',
                    btnClass: 'btn-red',
                    action: function() {
                        location.href = this.$target.attr('href');
                    }
                },
                cancel: function() {}
            },
            icon: 'fas fa-exclamation-triangle',
            title: 'Are you sure?',
            content: 'Are you sure you wish to delete this item? Please re-confirm this action.',
            type: 'red',
            typeAnimated: true,
            boxWidth: '30%',
            useBootstrap: false,
            theme: 'modern',
            animation: 'scale',
            backgroundDismissAnimation: 'shake',
            draggable: false
        });

        $('.success').delay(10000).fadeOut('fast');
        $('.error').delay(10000).fadeOut('fast');

        var inputs = document.querySelectorAll('.inputfile');
        Array.prototype.forEach.call(inputs, function(input) {
            var label = input.nextElementSibling,
                labelVal;

            if (label) labelVal = label.innerHTML;

            input.addEventListener('change', function(e) {
                var fileName = '';
                if (this.files && this.files.length > 1)
                    fileName = (this.getAttribute('data-multiple-caption') || '').replace(
                        '{count}', this.files.length);
                else
                    fileName = e.target.value.split('\\').pop();

                if (fileName)
                    label.querySelector('span').innerHTML = fileName;
                else
                    label.innerHTML = labelVal;
            });
        });

    });



    $(document).ready(function() {

        $("#left-column-nav .nav>ul>li .sub-drop-items li").each(function() {
            if ($(this).hasClass("active")) {
                $(this).parents("li.subheader").addClass("active")
            }
        });


        $("#left-column-nav .nav > ul > li.subheader").hover(function() {
                var currlihe = $(this).height() / 2;
                var currlipo = $(this).position().top;
                var arrowpos = currlipo + currlihe - 160;

                console.log(arrowpos);

                var currsublihe = 'translateY(-' + $(this).find(".sub-drop-items ul").height() / 2 + 'px)';
                $(this).find(".sub-drop-items ul").css("margin-top", arrowpos);
                $(this).find(".sub-drop-items ul").css("transform", currsublihe);
            },
            function() {
                $("#left-column-nav .nav > ul > li.subheader .sub-drop-items ul").css("margin-top", 0);
            })
    });
    </script>
</head>

<div id="top-bar">


    <a href="<?php echo site_url('team/logout'); ?>" class="logout">
        Logout <img src="<?php echo ms_site_url('assets/css/backend/images/logout.png'); ?>">
    </a>
</div>


<div id="left-column-nav">
    <div class="admin-profile">
        <div class="admin-profile-pic">
            <?php
        if($active_user->image!="")
        {
        ?>
            <img src="<?php echo ms_base_url('admin/uploads/images/'.$active_user->image); ?>">
            <?php } else { ?>
            <img src="<?php echo ms_base_url('assets/images/backend/default-team.jpg'); ?>">
            <?php } ?>
        </div>
        <span class="welcomeback"> Welcome back,</span>
        <div class="admin-name"><?php echo $active_user->firstname; ?> <?php echo $active_user->lastname; ?></div>
    </div>


    <div class="nav">
        <ul>
            <li class="<?php echo isset($slug) && $slug === "dashboard" ? 'active' : ''; ?>">
                <a href="<?php echo site_url("dashboard"); ?>">
                    <div class="mh-icon"><img
                            src="<?php echo ms_site_url('assets/css/backend/images/dashboard-icons/dashboard.png'); ?>">
                    </div>
                    <span>Dashboard</span>
                </a>
            </li>

            <?php

            foreach ($plugin_groups as $group_title) {

                $group_plugins = array_filter($plugins, function ($plugin) use ($group_title) {
                    return $plugin['group'] === $group_title && $plugin['enabled'] === TRUE;
                });

                if (count($group_plugins) > 0) {

					$firstRec = reset($group_plugins);
					
					$group_icon = isset($firstRec['group_icon']) ? $firstRec['group_icon'] : '';
					
					
                    ?>
            <li class="subheader">

                <?php if(!empty($group_icon)){				?>
                <a>
                    <div class="mh-icon"><img src="<?php echo $group_icon;?>"> </div>

                    <?php } ?>
                    <span><?php echo $group_title; ?></span>
                </a>
                <div class="sub-drop-items">
                    <ul>
                        <?php
						
                    foreach ($group_plugins as $plugin) {

                        if (!isset($plugin['hidden']) || (isset($plugin['hidden']) && $plugin['hidden'] === FALSE)) {
                            ?>
                        <li class="<?php echo isset($slug) && $slug === $plugin['slug'] ? 'active' : ''; ?>">
                            <a href="<?php echo site_url($plugin['slug']); ?>">
                                <div class="sub-icon"><img src="<?php echo $plugin['icon']; ?>"></div>
                                <div class="submenu-text"><?php echo $plugin['title']; ?></div>
                            </a>
                        </li>
                        <?php
                        }
                    }
						
                    ?>
                    </ul>
                </div>
            </li>

            <?php
                }
            }

            ?>
        </ul>

    </div>

</div>