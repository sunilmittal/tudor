<div id="control-container">
	<div id="button-holder">
		<a href="<?php echo site_url('notes'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
		<div class="clr"></div>
	</div>
	<h1>
		<i class="fas fa-map-marker"></i>Note <i class="fas fa-caret-right"></i>Edit
	</h1>
	<hr/>
	<?php if (isset($success) && $success) { ?>
	<div class="success">
		<i class="fas fa-check-circle"></i><?php echo $success; ?>
	</div>
	<?php } ?>
	<?php if (isset($error) && $error) { ?>
	<div class="error">
		<i class="fas fa-check-circle"></i><?php echo $error; ?>
	</div>
	<?php } ?>
	<?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
	<form action="<?php echo site_url('notes/edit/' . $note_details->id); ?>"
		method="post"
		enctype="multipart/form-data">
		<div class="form-section">
			<span class="heading">General</span>
			<div class="col full_column">
				<label for="is_featured">
				Title
				</label>
				<input type="text" name="title" id="title" value="<?php echo set_value('title') ? set_value('title') : ($note_details->title ? $note_details->title : "") ?>" readonly="readonly">
			</div>
			<div class="clr"></div>
			<div class="col full_column">
				<label>
				Description
				</label>
				<textarea name="description" id="notes_description"
					readonly="readonly"><?php echo set_value('description') ? set_value('description') : ($note_details->description ? $note_details->description : "") ?></textarea>
			</div>
			<?php if($note_details->image){ ?>
			<div class="clr"></div>
			<div class="col full_column">
				<label>
				Image
				</label>
				<img src="<?= ms_site_url('uploads/notes/'.$note_details->image.'') ?>" />
			</div>
			<?php } ?>
			<div class="clr"></div>
			<div class="col full_column">
				<label for="">
				Publish Date
				</label>
				<input type="text" name="date" id="date" value="<?php echo set_value('date') ? set_value('date') :
					($note_details->date ? getDateInFormat($note_details->date) : "") ?>" readonly="readonly">
			</div>
			<div class="clr"></div>
			<div class="col full_column">
				<label for="is_featured">
				Flag Request
				</label>
				<input id="down_status" name="status" type="radio" class="" value="0" required />
				<label for="down_status" class="d-inline">Note Down</label>
				<input id="dismiss_status" name="status" type="radio" class="" value="1" />
				<label for="dismiss_status" class="d-inline">Dismiss</label>
			</div>
			<div class="clr"></div>
			<div class="col full_column flag_users_list">
				<label for="">Flag Inappropriate Given By Below Users</label>
				<ul>
					<?php foreach($flages as $flag){ ?>
					<li> <?= $flag->message ?: '' ?> <b> - <?= $flag->user->user_name; ?> <?= ($flag->user->user_role==0) ? "(Learner)" : "(Teacher)";  ?></b></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="form-section">
			<button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
			<a href="<?php echo site_url('notes'); ?>" class="btn cancel"><i
				class="fas fa-ban"></i>Cancel</a>
			<div class="clr"></div>
		</div>
	</form>
</div>