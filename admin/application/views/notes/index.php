<div id="control-container">
    <div id="button-holder">
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-map-marker"></i>Notes
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
    <div class="success">
        <i class="fas fa-check-circle"></i><?php echo $success; ?>
    </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
    <div class="error">
        <i class="fas fa-check-circle"></i><?php echo $error; ?>
    </div>
    <?php } ?>
    <div class="table-responsive">
    <table id="notes_list" class="table">
        <thead>
            <tr>
                <th align="center">ID</th>
                <th>Title</th>
                <th>Flag As Inappropriate</th>
                <th>Author</th>
                <th align="center">Options</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Flag As Inappropriate</th>
                <th>Author</th>
                <th>Options</th>
            </tr>
        </tfoot>
        <tbody>
            <?php if (isset($notes) && is_array($notes) && count($notes)) { ?>
            <?php foreach ($notes as $note) { ?>
            <tr>
                <td align="center">
                    <?php echo $note->id; ?>
                </td>
                <td>
                    <?php echo $note->title; ?>
                </td>
                <td>
                    <?php if($note->flag_exists){
                        echo 'Yes';
                        }
                        else{
                        echo 'n/a';
                        }
                        ?>
                </td>
                <td>
                    <?= $note->user->user_name ?>
                </td>
                <td align="center">
                    <a href="<?php echo site_url('notes/view/' . $note->id); ?>"
                           class="icon fa fa-fw fa-eye tooltip" title="View"></a>
                    <?php if($note->flag_exists && $note->status==1){ ?>
                    <a href="<?php echo site_url('notes/edit/' . $note->id); ?>"
                        class="icon fa fa-fw fa-pencil-alt tooltip" title="Edit"></a>
                    <?php } ?>
                    <a href="<?php echo site_url('notes/delete/' . $note->id); ?>"
                           class="icon fa fa-fw fa-trash tooltip" title="Delete"></a>
                </td>
            </tr>
            <?php } ?>
            <?php } ?>
        </tbody>
    </table>
    </div>
</div>
<script>
    $(function () {
        var table = $('#notes_list').DataTable({
            "sPaginationType": "full_numbers",
            // "aaSorting": [[0, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#notes_list tfoot th').each(function () {
            var title = $('#notes_list tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>