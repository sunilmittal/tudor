<div id="control-container">
	<div id="button-holder">
		<a href="<?php echo site_url('notes'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Back</a>
		<div class="clr"></div>
	</div>
	<h1>
		<i class="fas fa-map-marker"></i>Note <i class="fas fa-caret-right"></i>View
	</h1>
	<hr/>
	<form action="">
		<div class="form-section">
			<span class="heading">General</span>
			<div class="col full_column">
				<label for="is_featured">
				Title
				</label>
				<p><?php echo set_value('title') ? set_value('title') : ($note_details->title ? $note_details->title : "") ?></p>
				
			</div>
			<div class="clr"></div>
			<div class="col full_column">
				<label>
				Description
				</label>
				<p class="description-admin-notes"><?php echo set_value('description') ? set_value('description') : ($note_details->description ? $note_details->description : "") ?></p>
				
			</div>
			<?php if($note_details->image){ ?>
			<div class="clr"></div>
			<div class="col full_column">
				<label>
				Image
				</label>
				<img src="<?= ms_site_url('uploads/notes/'.$note_details->image.'') ?>" />
			</div>
			<?php } ?>
			<div class="clr"></div>
			<div class="col full_column">
				<label for="">
				Publish Date
				</label>
				<p><?php echo set_value('date') ? set_value('date') :
					($note_details->date ? getDateInFormat($note_details->date) : "") ?></p>
				
			</div>
			<div class="clr"></div>
			<div class="col full_column flag_users_list">
				<label for="">Flag Inappropriate Given By Below Users</label>
				<ul>
					<?php foreach($flages as $flag){ ?>
					<li> <?= $flag->message ?: '' ?> <b> - <?= $flag->user->user_name; ?> <?= ($flag->user->user_role==0) ? "(Learner)" : "(Teacher)";  ?></b></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="form-section">
			<a href="<?php echo site_url('notes'); ?>" class="btn cancel"><i
				class="fas fa-ban"></i>Back</a>
			<div class="clr"></div>
		</div>
	</form>
</div>