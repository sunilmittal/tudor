<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('team'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-users"></i>Team <i class="fas fa-caret-right"></i>New
    </h1>
    <hr />
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('team/add'); ?>" method="post" enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">General</span>
            <div class="col half_column_left">
                <label for="firstname">
                    First Name
                </label>
                <input type="text" name="firstname" id="firstname" value="<?php echo set_value('firstname'); ?>" required>
            </div>
            <div class="col half_column_right">
                <label for="lastname">
                    Last Name
                </label>
                <input type="text" name="lastname" id="lastname" value="<?php echo set_value('lastname'); ?>" required>
            </div>
            <div class="col half_column_left">
                <label for="email">
                    Email
                </label>
                <input type="email" name="email" id="email" value="<?php echo set_value('email'); ?>" required>
            </div>
            <div class="col half_column_right">
                <label for="tel">
                    Telephone Number
                </label>
                <input type="tel" name="tel" id="tel" value="<?php echo set_value('tel'); ?>">
            </div>
            <div class="col half_column_left">
                <label for="password">
                    Password
                </label>
                <input type="password" name="password" id="password" value="<?php echo set_value('password'); ?>">
            </div>
            <div class="col half_column_right">
                <label for="image">
                    Image
                    <small><i>(Image files must be under <?php echo file_upload_max_size_format() ?>, and JPG, GIF or PNG format)</i></small>
                </label>
                <input type="file" name="image" id="image" class="inputfile" />
            </div>
            <div class="col half_column_left">
                <label for="slug">
                    Slug
                </label>
                <input type="text" name="slug" id="slug" value="<?php echo set_value('slug'); ?>">
            </div>


            <div class="clr"></div>
        </div>
        <div class="form-section">
            <span class="heading">Details</span>
            <div class="col half_column_left">
                <label for="title">
                    Title
                </label>
                <input type="text" name="title" id="title" value="<?php echo set_value('title'); ?>">
            </div>
           <!--  <div class="col half_column_right">
                <label for="recruitment_type">
                    Recruitment Type
                </label>
                <input type="text" name="recruitment_type" id="recruitment_type" value="<?php echo set_value('recruitment_type'); ?>">
            </div>
            <div class="col half_column_left">
                <label for="secret_talent">
                    secret_talent
                </label>
                <input type="text" name="secret_talent" id="secret_talent" value="<?php echo set_value('secret_talent'); ?>">
            </div> -->
            <div class="col half_column_right">
                <label for="active">
                    Display profile on Team page
                </label>
                <input type="checkbox" name="active" id="active" value="1">
            </div>
            <!-- <div class="col half_column_left">
                <label for="info">
                    info
                </label>
                <input type="text" name="info" id="info" value="<?php echo set_value('info'); ?>">
            </div>
            <div class="col half_column_right">
                <label for="only_image">
                    Show Only Image
                </label>
                <input type="checkbox" name="only_image" id="only_image" value="1">
            </div> -->








            <div class="col half_column_left">
                <label for="linkedin">
                    LinkedIn URL
                </label>
                <input type="text" name="linkedin" id="linkedin" value="<?php echo set_value('linkedin'); ?>">
            </div>
            <div class="col half_column_right">
                <label for="linkedin">
                    Twitter URL
                </label>
                <input type="text" name="twitter" id="twitter" value="<?php echo set_value('twitter'); ?>">
            </div>
            <div class="col half_column_left">
                <label for="linkedin">
                    Skype
                </label>
                <input type="text" name="skype" id="skype" value="<?php echo set_value('skype'); ?>">
            </div>
            <div class="col half_column_right">
                <label for="sort_order">
                    Order
                </label>
                <input type="number" name="sort_order" maxlength="5" id="sort_order" value="<?php echo set_value('sort_order'); ?>">
            </div>
            <div class="clr"></div>
            <div class="col full_column">
                <label for="description">
                    Description
                </label>
                <textarea name="description" id="description" rows="20"><?php echo set_value('description'); ?></textarea>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('team'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>

<link rel="stylesheet" href="<?php echo ms_base_url('assets/plugins/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') ?>">
<script src="<?php echo ms_base_url('assets/plugins/ckeditor/ckeditor.js') ?>"></script>
<script src="<?php echo ms_base_url('assets/plugins/ckeditor/samples/js/sample.js'); ?>"></script>
<script>
    $(function() {
        $("#firstname").keyup(function() {
            if ($(this).val().trim().length > 0) {
                $("#slug").val(
                    $(this).val().trim().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase() +
                    "-" +
                    $("#lastname").val().trim().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase()
                );
            } else {
                $("#slug").val("");
            }
        });

        $("#lastname").keyup(function() {
            if ($(this).val().trim().length > 0) {
                $("#slug").val(
                    $("#firstname").val().trim().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase() +
                    "-" +
                    $(this).val().trim().replace(/[^a-zA-Z0-9]+/g, '-').toLowerCase()
                );
            } else {
                $("#slug").val("");
            }
        });

        var editor = CKEDITOR.replace('description', {
            htmlEncodeOutput: false,
            wordcount: {
                showWordCount: true,
                showCharCount: true,
                countSpacesAsChars: true,
                countHTML: false,
            },
            removePlugins: 'zsuploader',

            filebrowserBrowseUrl: '<?php echo ms_base_url('assets/plugins/kcfinder/browse.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageBrowseUrl: '<?php echo ms_base_url('assets/plugins/kcfinder/browse.php?opener=ckeditor&type=images'); ?>',
            filebrowserUploadUrl: '<?php echo ms_base_url('assets/plugins/kcfinder/upload.php?opener=ckeditor&type=files'); ?>',
            filebrowserImageUploadUrl: '<?php echo ms_base_url('assets/plugins/kcfinder/upload.php?opener=ckeditor&type=images'); ?>'
        });
    });
</script>