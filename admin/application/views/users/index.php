<div id="control-container">
    <h1>
        <i class="fas fa-users"></i>Users
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <div class="table-responsive">
    <table id="users_list" class="table">

        <thead>
        <tr>
            <th align="center">ID</th>
            <th>Full Name</th>
            <th>Email</th>
            <th>User Role</th>
            <th>Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Ful Name</th>
            <th>Email</th>
            <th>User Role</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($users) && is_array($users) && count($users)) { ?>
            <?php foreach ($users as $user) { ?>
                <tr>
                    <td align="center">
                        <?php echo $user->id; ?>
                    </td>
                    <td>
                        <?php echo $user->name; ?>
                    </td>
                    <td>
                        <?php echo $user->email; ?>
                    </td>
                    <td>
                        <?php echo getUserRole($user); ?>
                    </td>
                    <td>
                        <a href="<?php echo site_url('user/delete/' . $user->id); ?>"
                           class="icon fa fa-fw fa-trash tooltip" title="Delete"></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
</div>

<script>
    $(function () {
        var table = $('#users_list').DataTable({
            "sPaginationType": "full_numbers",
            "aaSorting": [[2, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#users_list tfoot th').each(function () {
            var title = $('#users_list tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>