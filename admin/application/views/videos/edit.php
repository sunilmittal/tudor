<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('videos'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Cancel</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-map-marker"></i>Video <i class="fas fa-caret-right"></i>Edit
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <?php echo validation_errors('<div class="error"><i class="fas fa-check-circle"></i>', '</div>'); ?>
    <form action="<?php echo site_url('videos/edit/' . $video_details->id); ?>"
          method="post"
          enctype="multipart/form-data">
        <div class="form-section">
            <span class="heading">General</span>
            <div class="col full_column">
                <label for="is_featured">
                    Title
                </label>
                <input type="text" name="title" id="title"
                       value="<?php echo set_value('title') ? set_value('title') : ($video_details->title ? $video_details->title : "") ?>" readonly="readonly">
            </div>
            <div class="clr"></div>
            <div class="col full_column">
                <label for="is_featured">
                    Feature
                </label>
                <?= form_checkbox('is_featured', '1', $video_details->is_featured); ?>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <button type="submit" name="submit" class="btn submit"><i class="fas fa-save"></i>Save Changes</button>
            <a href="<?php echo site_url('videos'); ?>" class="btn cancel"><i
                        class="fas fa-ban"></i>Cancel</a>
            <div class="clr"></div>
        </div>
    </form>
</div>