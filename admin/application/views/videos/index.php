<div id="control-container">
    <div id="button-holder">
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-map-marker"></i>Videos
    </h1>
    <hr/>
    <?php if (isset($success) && $success) { ?>
        <div class="success">
            <i class="fas fa-check-circle"></i><?php echo $success; ?>
        </div>
    <?php } ?>
    <?php if (isset($error) && $error) { ?>
        <div class="error">
            <i class="fas fa-check-circle"></i><?php echo $error; ?>
        </div>
    <?php } ?>
    <div class="table-responsive">
    <table id="videos_list" class="table">

        <thead>
        <tr>
            <th align="center">ID</th>
            <th>Title</th>
            <th>Featured</th>
            <th>Author</th>
            <th align="center">Options</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Featured</th>
            <th>Author</th>
            <th>Options</th>
        </tr>
        </tfoot>
        <tbody>
        <?php if (isset($videos) && is_array($videos) && count($videos)) { ?>
            <?php foreach ($videos as $video) { ?>
                <tr>
                    <td align="center">
                        <?php echo $video->id; ?>
                    </td>
                    <td>
                        <?php echo $video->title; ?>
                    </td>
                    <td>
                        <?php echo $video->is_featured ? 'Yes' : 'No'; ?>
                    </td>
                    <td>
                        <?= $video->user_and_channel->user_name ?>
                    </td>
                    <td align="center">
                        <a href="<?php echo site_url('videos/view/' . $video->id); ?>"
                           class="icon fa fa-fw fa-eye tooltip" title="View"></a>
                        <a href="<?php echo site_url('videos/edit/' . $video->id); ?>"
                           class="icon fa fa-fw fa-pencil-alt tooltip" title="Edit"></a>
                        <a href="<?php echo site_url('videos/delete/' . $video->id); ?>"
                           class="icon fa fa-fw fa-trash tooltip" title="Delete"></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $(function () {
        var table = $('#videos_list').DataTable({
            "sPaginationType": "full_numbers",
            // "aaSorting": [[0, 'asc']],
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [2]}],
            "iDisplayLength": 25,
            "stateSave": true,
            "colReorder": true
        });
        // Column Filter
        $('#videos_list tfoot th').each(function () {
            var title = $('#videos_list tfoot th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" value="" />');
        });
        var state = table.state.loaded();
        state && (table.columns().eq(0).each(function (a) {
            var b = state.columns[a].search;
            b.search && $("input", table.column(a).footer()).val(b.search)
        }), table.draw()), table.columns().eq(0).each(function (a) {
            $("input", table.column(a).footer()).on("keyup change", function () {
                table.column(a).search(this.value).draw()
            })
        });
    });
</script>