<div id="control-container">
    <div id="button-holder">
        <a href="<?php echo site_url('videos'); ?>" class="btn cancel"><i class="fas fa-ban"></i>Back</a>
        <div class="clr"></div>
    </div>
    <h1>
        <i class="fas fa-map-marker"></i>Video <i class="fas fa-caret-right"></i>View
    </h1>
    <hr/>
    <form action="">
        <div class="form-section">
            <span class="heading">General</span>
            <div class="col full_column">
                <label for="is_featured">
                    Title
                </label>
                <p><?php echo $video_details->title; ?></p>
            </div>
            <div class="clr"></div>
            <div class="col full_column">
                <label for="is_featured">
                    Video Link
                </label>
                <?php $embed_url = getEmbedYouTubeURL($video_details->external_video_id); ?>
                <iframe src="<?= $embed_url ?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
            <div class="clr"></div>
        </div>
        <div class="form-section">
            <a href="<?php echo site_url('videos'); ?>" class="btn cancel"><i
                        class="fas fa-ban"></i>Back</a>
            <div class="clr"></div>
        </div>
    </form>
</div>