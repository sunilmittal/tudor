<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'app/home';
$route['404_override'] = 'app/page_not_found';
$route['about-us']['GET'] = 'app/aboutUs';
$route['contact-us'] = 'app/contactUs';
$route['faq']['GET'] = 'app/faq';
$route['community-guidelines']['GET'] = 'app/cgp';
$route['tutors']['GET'] = 'app/tutors';
$route['learners']['GET'] = 'app/learners';
$route['privacy-policy']['GET'] = 'app/privacyPolicy';
$route['terms-of-service']['GET'] = 'app/termsOfService';
$route['spread-the-word']['GET'] = 'app/spreadTheWord';
$route['getting-started']['GET'] = 'app/gettingStarted';
$route['getting-started']['GET'] = 'app/gettingStarted';
$route['sharing-videos']['GET'] = 'app/sharingVideos';
$route['sharing-notes']['GET'] = 'app/sharingNotes';


$route['blog-overview']['GET'] = 'blog/blogOverview';
$route['blogs/(:any)'] = 'blog/blogOverview/$1';
$route['blog-detail/(:any)'] = 'blog/blogDetail/$1';

$route['translate_uri_dashes'] = FALSE;

$route['register'] = 'auth/register';
$route['login'] = 'auth/login';
$route['logout']['GET'] = 'auth/logout';
$route['forgot-password']['POST'] = 'auth/forgotPassword';
$route['reset-password/(:any)'] = 'auth/resetPassword/$1';

$route['dashboard'] = 'dashboard/index';
$route['settings'] = 'dashboard/settings';

$route['home'] = 'videos/index';
$route['classroom-videos'] = 'videos/classroomVideos';
$route['classroom-videos/(:num)'] = 'videos/classroomVideos/$1';
$route['me/videos'] = 'videos/myVideos';
$route['videos/add'] = 'videos/add';
$route['videos/(:any)'] = 'videos/details/$1';
$route['classroom/(:num)'] = 'classroom/access/$1';
$route['send-access-request'] = 'classroom/sendRequest';

$route['notes'] = 'notes/index';
$route['notes/add'] = 'notes/add';
$route['notes/edit/(:any)'] = 'notes/edit/$1';
$route['notes/(:any)'] = 'notes/details/$1';
$route['notes/(:any)/comments']['POST'] = 'comments/add/$1';
$route['notes/(:any)/comments']['GET'] = 'comments/index/$1';

$route['comments/(:num)']['DELETE'] = 'comments/delete/$1';
$route['comments/(:num)']['PUT'] = 'comments/edit/$1';
$route['comments/(:num)/replies']['POST'] = 'comments/reply/$1';
$route['comments/(:num)/replies']['GET'] = 'comments/replies/$1';

$route['videos/edit/(:any)'] = 'videos/edit/$1';
$route['videos/(:any)/comments']['POST'] = 'comments/add/$1';
$route['videos/(:any)/comments']['GET'] = 'comments/index/$1';

$route['user/subscriptions'] = 'user/subscriptions';
$route['user/history'] = 'user/history';
$route['user/liked-videos'] = 'user/likedVideos';
$route['classroom'] = 'classroom/index';
$route['classroom/add'] = 'classroom/add';
$route['classroom/edit/(:num)'] = 'classroom/edit/$1';
$route['classroom-requests'] = 'classroom/classroomRequests';
$route['user/subscribers'] = 'user/subscribers';
$route['user/(:num)/profile'] = 'user/profile/$1';
$route['user/(:num)/subscribe'] = 'user/subscribe/$1';
$route['user/(:num)/unsubscribe'] = 'user/unsubscribe/$1';
$route['me'] = 'user/me';

$route['app/categories/(:num)'] = 'app/categories/$1';

$route['geocode'] = 'app/getLatLong';
$route['getPostcodes'] = 'app/getPostcodes';

$route['sitemap\.xml'] = "seo/sitemap";

$route['blank']['GET'] = 'dashboard/blank';

$route['resource_inappropriate/flag']['POST'] = 'resource_inappropriate/flag';

$route['user/delete']['POST'] = 'user/delete';

$route['subjects/(:any)'] = 'videos/index/$1';

$route['subjects/(:any)/(:any)'] = 'videos/index/$1/$2';

$route['search'] = 'videos/index';

$route['sitemap'] = "seo/html_sitemap";

$route['confirm-role'] = "auth/selectRole";