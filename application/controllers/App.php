<?php
defined('BASEPATH') or exit('No direct script access allowed');

class App extends CI_Controller
{
	private $data;
	private $google_client;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['url']);
		$this->load->model(['category_model', 'video_model', 'common_model']);
		$this->data = [];
		$this->google_client = "" ;

		if (isUserLoggedIn()) {
			$this->data['user_data'] = getLoggedInUserDetails();
		}
		$all_categories = $this->category_model->get_rows([], true);

		$parent_cat = array_values(array_filter($all_categories, function($cat) {
			return !$cat['parent_id'];
		}));

		$all_categories = array_map(function($value) use ($all_categories) {
			$value['sub_cat'] = array_values(array_filter($all_categories, function($cat) use($value) {
				return $cat['parent_id'] == $value['id'];
			}));
			return $value;
		}, $parent_cat);

		$all_categories = collect($all_categories);

		$category_ids = array_column($this->video_model->getPopularCategoryBasedOnVideosCount(), 'category_id');
		$all_categories = $all_categories->sortBy(function($category) use ($category_ids) {
			return array_search($category['id'], $category_ids);
		})->values();

		// google authentication

		$this->google_client = new Google_Client();
		$this->google_client->setClientId($this->config->item( 'google_client_id' )); //Define your ClientID
		$this->google_client->setClientSecret($this->config->item( 'google_client_secret' )); //Define your Client Secret Key
		$this->google_client->setRedirectUri(base_url() . 'login'); //Define your Redirect Uri
		$this->google_client->addScope('email');
		$this->google_client->addScope('profile');
		$this->data['login_button'] = '<a href="'.$this->google_client->createAuthUrl().'"><i class="fa fa-google" aria-hidden="true"></i> Login</a>';

		// end of google authentication

		$this->data['all_categories'] = $all_categories;
	}

	public function home() {
		$this->data['page_title'] = 'Learning and Teaching Made for You – Start Sharing Notes and Videos With the Community';
		$this->data['page_meta_keyword'] = 'best learning platform online, top online education platform, top online education platform, online teaching ideas, online teaching methods and pedagogy, language learning and teaching, the best online learning platform, free online academy';
		$this->data['page_meta_description'] = 'Learning and Teaching Made for You – Start Sharing Notes and Videos With the Community. Tudor learning is free online learning platform where you can Create interactive video & article lessons for students.';
		$this->data['path'] = 'app/home';
		$this->load->view('layout/app', $this->data);
	}

	public function aboutUs() {
		$this->data['page_title'] = 'Best Platform For Online Tutoring - Teaching And Learning Strategies';
		$this->data['page_meta_keyword'] = 'learning teaching, teaching learning resources, language learning and teaching, teaching and learning strategies, best platform for online tutoring, second language teaching and learning, Tudor learning community';
		$this->data['page_meta_description'] = 'The best platform for online tutoring is designed to help students to keep them interested and engaged in subjects from math, art, economics, physics to language.';
		$this->data['path'] = 'app/about-us';
		$this->load->view('layout/app', $this->data);
	}

	public function blogOverview() {
		$this->data['page_title'] = 'Blog Overivew';
		$this->data['page_meta_keyword'] = 'Blog Overivew';
		$this->data['page_meta_description'] = 'Blog Overivew';
		$this->data['path'] = 'app/blog-overview';
		$this->load->view('layout/app-blog', $this->data);
	}
	public function blogs() {
		$this->data['page_title'] = 'Blog Overivew';
		$this->data['page_meta_keyword'] = 'Blog Overivew';
		$this->data['page_meta_description'] = 'Blog Overivew';
		$this->data['path'] = 'app/blogs';
		$this->load->view('layout/app-blog', $this->data);
	}
	public function blogDetail() {
		$this->data['page_title'] = 'Blog Overivew';
		$this->data['page_meta_keyword'] = 'Blog Overivew';
		$this->data['page_meta_description'] = 'Blog Overivew';
		$this->data['path'] = 'app/blog-detail';
		$this->load->view('layout/app-blog', $this->data);
	}

	public function privacyPolicy() {
		$this->data['page_title'] = 'Privacy Policy | '.config_item('company_name');
		$this->data['path'] = 'app/privacy-policy';
		$this->load->view('layout/app', $this->data);
	}

	public function termsOfService() {
		$this->data['page_title'] = 'Terms of Service | '.config_item('company_name');
		$this->data['path'] = 'app/terms-of-service';
		$this->load->view('layout/app', $this->data);
	}

	public function spreadTheWord() {
		$this->data['page_title'] = 'Community-Based Learning - Education - Tudor Learning';
		$this->data['page_meta_keyword'] = 'community based learning, community based curriculum, community based activities for students, community based service learning, popular online learning platforms, community based education';
		$this->data['page_meta_description'] = 'Community-based learning is a teaching platform. More people on Tudor means more diverse ideas and contents that make up the community-based education.';
		$this->data['path'] = 'app/spread-the-word';
		$this->load->view('layout/app', $this->data);
	}

	public function gettingStarted() {
		$this->data['page_title'] = 'Getting Started | '.config_item('company_name');
		$this->data['path'] = 'app/getting-started';
		$this->load->view('layout/app', $this->data);
	}

	public function sharingVideos() {
		$this->data['page_title'] = 'Sharing Videos | '.config_item('company_name');
		$this->data['path'] = 'app/sharing-videos';
		$this->load->view('layout/app', $this->data);
	}

	public function sharingNotes() {
		$this->data['page_title'] = 'Sharing Notes | '.config_item('company_name');
		$this->data['path'] = 'app/sharing-notes';
		$this->load->view('layout/app', $this->data);
	}

	public function contactUs() {
		$this->data['page_title'] = 'Contact Us | '.config_item('company_name');
		$postParams = $this->input->post();
		if ($postParams && isset($postParams['submit'])) {
			$this->form_validation->set_rules('name', 'Name', 'required|min_length[3]|max_length[50]|regex_match[/^([a-z ])+$/i]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('subject', 'Subject', 'required|min_length[2]');
			$this->form_validation->set_rules('message', 'Message', 'required|min_length[6]|max_length[255]');
			// $isCaptchaValid = $this->google_validate_recaptcha_v3();
			// if ($this->form_validation->run() == false && $isCaptchaValid) {
			if ($this->form_validation->run() == false) {
				$this->data['path'] = 'app/contact-us';
				$this->load->view('layout/app-inner', $this->data);
			} else {
				$data = array(
					'name' => $this->input->post('name'),
					'email' => $this->input->post('email'),
					'message' => $this->input->post('message'),
					'subject' => $this->input->post('subject')
				);
				$recordInserted = $this->db->insert('contactus', $data);

				if ($recordInserted) {

					// an email to the user who insert the data.
					$to = $postParams['email'];
					$to_name = $postParams['name'];
					$subject = "Contact us | " . config_item('company_name');
					$emailContent = "<html>
					<body>
					<div>  <img src='".ms_site_url('assets/images/frontend/logo.png')."' alt='Logo' style='width:200px;'> </div>
					<p> Dear " . $to_name . "</p>
					<p>Thank you for your contacting Tudor.</p>
					<p>Your message has been received and someone will be in touch with you ASAP to discuss your enquiry in further detail.</p>
					<p>Thanks very much,</p>   
					<p>The Tudor team</p>
					</body>
					</html>";
					sendEmail($to, $subject, $emailContent, $to_name);

					// send an email to the admin of the website.
					$adminEmailContent = "<!DOCTYPE html>
					<html>
					<body>
					New Contact Request From <br>
					<p> Name: " . $to_name . "</p>
					<p> Email: " . $to . "</p>
					<p> Message: " . $postParams['message'] . "</p>

					</body>
					</html>";
					sendEmail(config_item('email')['admin_email'], $subject, $adminEmailContent);

					$this->session->set_flashdata('success', 'Enquiry sent successfully');
					redirect('contact-us');
				} else {
					$this->session->set_flashdata('error', 'Error while saving record!');
				}
				redirect('/contact-us', 'refresh');
			}
		} else {
			$this->data['path'] = 'app/contact-us';
			$this->load->view('layout/app', $this->data);
		}
	}

	public function sitemap() {
		$this->load->view('app/sitemap', $this->data);
	} 

	public function page_not_found() {
		redirect('/');
		$this->data['page_title'] = '404 | '.config_item('company_name');
		$this->data['path'] = 'app/page-not-found';
		$this->load->view('layout/app', $this->data);
	}

	public function faq() {
		$this->data['page_title'] = 'FAQ | '.config_item('company_name');
		$this->data['path'] = 'app/faq';
		$this->load->view('layout/app', $this->data);
	}

	public function cgp() {
		$this->data['page_title'] = 'Community Guidelines and Policies | '.config_item('company_name');
		$this->data['path'] = 'app/cgp';
		$this->load->view('layout/app', $this->data);
	}
	public function tutors() {
		$this->data['page_title'] = 'Best Free Online Teaching Platforms - Online Tutoring Services';
		$this->data['page_meta_keyword'] = 'free online teaching , best free online teaching platforms, virtual classroom platforms, free online teaching platform, Online tutoring services, the best online teaching platforms';
		$this->data['page_meta_description'] = "Tudor Learning is a dynamic free online teaching platform. A more meaningful approach towards educating and teaching your learners. Sign up today. It's free!";
		$this->data['path'] = 'app/tutor';
		$this->load->view('layout/app', $this->data);
	}
	public function learners() {
		$this->data['page_title'] = 'The Best Online Learning Platforms - Virtual Classroom Platforms';
		$this->data['page_meta_keyword'] = 'virtual classroom platforms, best virtual classroom platforms, online learning platforms free, the best online learning platforms, best virtual classroom platforms 2021, best virtual classroom platform';
		$this->data['page_meta_description'] = "Tudor Learning is a dynamic online learning platform free. We hope you get the best learning experience possible with as many diverse learning opportunities. Sign up today.";
		$this->data['path'] = 'app/student';
		$this->load->view('layout/app', $this->data);
	}

	public function getLatLong() {
		$postcode = @$_GET['address'];
		if ($postcode) {
			$postcode = str_replace(' ', '', $postcode);
			$data = $this->db->where("replace(postcode, ' ', '') LIKE '%$postcode%'")->get('postcodelatlng')->row();
			if ($data) {
				json_response([
					'status' => 'OK',
					'lat' => $data->latitude,
					'lng' => $data->longitude
				]);
			} else {
				json_response([
					'status' => 'ERROR LEVEL 1',
					'lat' => '',
					'lng' => ''
				]);
			}
		}
		json_response([
			'status' => 'ERROR LEVEL 2',
			'lat' => '',
			'lng' => ''
		]);
	}

	public function getPostcodes() {
		$postcode = @$_GET['postcode'];
		if ($postcode) {
			$postcode = str_replace(' ', '', $postcode);
			$data = $this->db->select('postcode, latitude, longitude')->limit(10)->where("replace(postcode, ' ', '') LIKE '%$postcode%'")->get('postcodelatlng')->result();
			if ($data) {
				json_response([
					'status' => true,
					'data' => $data
				]);
			} else {
				json_response([
					'status' => false,
					'data' => []
				]);
			}
		}
		json_response([
			'status' => false,
			'data' => []
		]);
	}

	public function categories($category_id = null) {
		$response = [
			'status' => false,
			'categories' => []
		];

		try {
			if ($category_id) {
				$categories = $this->category_model->get_rows(['parent_id' => $category_id], true);
				$response = [
					'status' => true,
					'categories' => $categories
				];
			}            
		} catch (Exception $e) {
			$response = [
				'status' => false,
				'categories' => [],
				'message' => $e->getMessage()
			];
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function newsletter(){
		
		if($this->input->post()){
			$data = $this->input->post();
			$exist = $this->common_model->getNewsletter($data, true);
			// echo "<pre>"; print_r($exist); die;
			if($exist){
				$response = [
					'status' => false,
					'message' => 'Email already exists'
				];
			}else{
				if($this->common_model->saveNewsletter($data)){
					$response = [
						'status' => true,
						'message' => 'You are subscribed successfully.'
					];
				}else{
					$response = [
						'status' => false,
						'message' => 'Something went wrong.'
					];
				}
			}
		}else{
			$response = [
				'status' => false,
				'message' => 'No data found.'
			];
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}
}
