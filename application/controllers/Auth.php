<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once(APPPATH.'traits/additional_validation_methods.php');

// include_once FCPATH . "vendor/autoload.php";

class Auth extends CI_Controller
{
	use AdditionalValidationMathods;

	private $data;
	private $google_client;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['url', 'file']);
		$this->load->model(['user_model', 'category_model', 'video_model', 'channel_model']);

		$this->data = array();
		$this->google_client = "" ;

		if (isUserLoggedIn()) {
			$this->data['user_data'] = getLoggedInUserDetails();
		}

		$all_categories = $this->category_model->get_rows([], true);

		$parent_cat = array_values(array_filter($all_categories, function($cat) {
			return !$cat['parent_id'];
		}));

		$all_categories = array_map(function($value) use ($all_categories) {
			$value['sub_cat'] = array_values(array_filter($all_categories, function($cat) use($value) {
				return $cat['parent_id'] == $value['id'];
			}));
			return $value;
		}, $parent_cat);

		$all_categories = collect($all_categories);

		$category_ids = array_column($this->video_model->getPopularCategoryBasedOnVideosCount(), 'category_id');
		$all_categories = $all_categories->sortBy(function($category) use ($category_ids) {
			return array_search($category['id'], $category_ids);
		})->values();

		// google authentication

		$this->google_client = new Google_Client();
  		$this->google_client->setClientId($this->config->item( 'google_client_id' )); //Define your ClientID
  		$this->google_client->setClientSecret($this->config->item( 'google_client_secret' )); //Define your Client Secret Key
  		$this->google_client->setRedirectUri(base_url() . 'login'); //Define your Redirect Uri
  		$this->google_client->addScope('email');
  		$this->google_client->addScope('profile');
  		$this->data['login_button'] = '<a href="'.$this->google_client->createAuthUrl().'"><i class="fa fa-google" aria-hidden="true"></i> Login</a>';

		// end of google authentication

  		$this->data['all_categories'] = $all_categories;
  		$this->data['page_title'] = 'Authentication | '.config_item('company_name');
  	}

	/**
	 * The register function register the user of the website.
	 * @return void
	 */
	public function register() {
		$this->data['page_title'] = 'Sign Up | '.config_item('company_name');
		redirectAuthUser();

		$postParams = $this->input->post();

		if ($postParams && isset($postParams['submit'])) {
			$this->form_validation->set_rules('name', 'Full Name', 'required|min_length[3]|max_length[50]|regex_match[/^([a-z ])+$/i]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[100]|valid_email|callback_soft_delete_email_check['. $postParams['email'].']');
			$this->form_validation->set_rules('password', 'password', 'required|min_length[6]|max_length[20]');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|min_length[6]|max_length[20]|matches[password]');
			$this->form_validation->set_rules('user_role', 'User role', 'required|trim|in_list[0,1]');
			$this->form_validation->set_rules('privacy_policy', 'Privacy Policy', 'required|in_list[1]',
				array(
					'required' => 'Please accept the privacy policy.',
					'in_list' => 'The selected value for privacy policy is invalid.',
				)
			);
			
			if ($this->form_validation->run() == false) {
				$this->data['path'] = 'auth/register';
				$this->load->view('layout/app', $this->data);
			} else {
				$data = array(
					'name' => $this->input->post('name'),
					'email' => $this->input->post('email'),
					'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
					'user_role' => $this->input->post('user_role')
				);
				$userSavedAndId = $this->user_model->add_row($data, true);

				if ($userSavedAndId) {
					$user = $this->user_model->get_row(['id' => $userSavedAndId]);

					$this->channel_model->add_row(['user_id' => $user->id, 'name' => $user->name]);

					$to = $postParams['email'];
					$to_name = $postParams['name'];
					// $subject = 'Thanks for registering with us on Tudor!';
					$subject = 'Thanks for registering with us on Tudor!';
					
					$subject2 = "New Registration Request | " . config_item('company_name');
					/* $emailContent = "<html>
	                <body>
						<div>  <img src=".ms_site_url('assets/images/frontend/logo.png')." alt='Logo' style='width:200px;'> </div>
						<p>Dear " . $to_name . ",</p>
						<p>Thank you for Registration with ". config_item('company_name') ."</p>
						<p>Your details have been submitted to our team and we will be in touch if we have any live roles that we feel are relevant to you.</p>
						<p>Thank you,</p>   
                        <p>The ". config_item('company_name') ." Team</p>
                    </body>
                    </html>"; */

                    $data = array(
                    	'to_name'=> $postParams['name'],
                    	'company_name'=> config_item('company_name'),
                    );

                    $emailContent = $this->load->view('emails/register.php',$data,TRUE);

					//echo $subject;
					//die();

                    sendEmail($to, $subject, $emailContent, $to_name);

					// send an email to the admin of the website.
                    $adminEmailContent = "<!DOCTYPE html>
                    <html>
                    <body>
                    New Registration Request From <br>
                    <p> Name: " . $to_name . "</p>
                    <p> Email: " . $to . "</p>
                    <p> User Role: " . getUserRole($user) . "</p>
                    </body>
                    </html>";
                    sendEmail(config_item('email')['admin_email'], $subject2, $adminEmailContent);
                    $this->session->set_flashdata('success', 'User register Successfully.');
                    redirect('/login');
                } else {
                	$this->session->set_flashdata('error', 'Error while saving user!');
                	redirect('/register', 'refresh');
                }
            }
        }

        $this->data['path'] = 'auth/register';
        $this->load->view('layout/app', $this->data);
    }

	/**
	 * The login function check the user and make session of the website.
	 * @return void
	 */
	public function login() {
		$this->data['page_title'] = 'Sign In | '.config_item('company_name');
		redirectAuthUser();

		$redirect_url = $this->input->get('redirect_url');

		$postParams = $this->input->post();

		if ($postParams && isset($postParams['submit'])) {
			$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[100]|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[20]');
			if ($this->form_validation->run() == false) {
				$this->data['path'] = 'auth/login';
				$this->load->view('layout/app', $this->data);
			} else {
				$email = $this->input->post('email');
				$password = $this->input->post('password');
				$user = $this->user_model->get_row(['email' => $email, 'deleted_at' => null], true);
				if ($user) {
					if (password_verify($password, $user->password)) {
						$this->session->set_userdata('isUserLoggedIn', TRUE);
						$this->session->set_userdata('user_id', $user->id);
						$this->session->set_flashdata('success', 'User logged in.');
						$remember_token = generate_random_string();
						// remember me
						if(!empty($this->input->post("remember"))) {
							set_cookie("remember_token", $remember_token, time() + config_item('remember_cookie_invalid'));
							$this->user_model->update_row(['email' => $email], ['remember_token' => $remember_token]);
						} else {
							set_cookie("remember_token", $remember_token, time() + config_item('remember_cookie_short_time'));
						}

						if ($user->user_role == TEACHER_ROLE) {
							if ($redirect_url) {
								redirect($redirect_url);
							} else {
								redirect('dashboard');
							}
						} else {
							if ($redirect_url) {
								redirect($redirect_url);
							} else {
								redirect('/home');
							}
						}
						
					} else {
						$this->session->set_flashdata('error', 'Incorrect username or password');
						redirect('/login', 'refresh');
					}
				} else {
					$this->session->set_flashdata('error', 'Incorrect username or password');
					redirect('/login', 'refresh');
				}
			}
		} 


		// google authentication

		
		if(isset($_GET["code"]))
		{
			$token = $this->google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

			if(!isset($token["error"]))
			{
				$this->google_client->setAccessToken($token['access_token']);

				$this->session->set_userdata('access_token', $token['access_token']);

				$google_service = new Google_Service_Oauth2($this->google_client);

				$data = $google_service->userinfo->get();

				if($this->user_model->Is_already_register($data['id']))
				{
 					//update data
					$user_data['oauth_provider'] = 'google'; 
					$user_data['oauth_uid']	= $data['id']; 
					$user_data['name']	= $data['given_name'] . ' ' . $data['family_name']; 
					$user_data['email']	= $data['email']; 
					$user_data['gender']	= !empty($data['gender']) ? $data['gender'] : ''; 
					$user_data['locale']	= !empty($data['locale']) ? $data['locale'] : ''; 
					$user_data['deleted_at']	= null; 

					$user = $this->user_model->Update_user_data($user_data, $data['id']);
				}
				else
				{
     				//insert data
					$user_data['oauth_provider'] = 'google'; 
					$user_data['oauth_uid']	= $data['id']; 
					$user_data['name']	= $data['given_name'] . ' ' . $data['family_name']; 
					$user_data['email']	= $data['email']; 
					$user_data['gender']	= !empty($data['gender']) ? $data['gender'] : ''; 
					$user_data['locale']	= !empty($data['locale']) ? $data['locale'] : ''; 
					$user_data['deleted_at']	= null; 

					if(!empty($data['picture'])){
						$file_name = generate_random_string(32).".jpg"; 
						$content = file_get_contents($data['picture']);
						if(file_put_contents(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'avatars' . DIRECTORY_SEPARATOR . $file_name, $content)){
							$user_data['image']	= $file_name; 
						}
					}
					$user = $this->user_model->Insert_user_data($user_data);
				}
				$this->session->set_userdata('isUserLoggedIn', TRUE);
				$this->session->set_userdata('user_id', $user);
				$this->session->set_flashdata('success', 'User logged in.');
			}
		}
		if($this->session->userdata('user_id'))
		{
			redirect('confirm-role');
		}

		// end of google authentication

		$this->data['path'] = 'auth/login';
		$this->load->view('layout/app', $this->data);
	}

	/**
	 * The logout function destroy the session and rediret the user to the auth page.
	 * @return void
	 */
	public function logout() {
		$user_id = getLoggedInUserId();

		$this->user_model->update_row(['id' => $user_id], ['remember_token' => null]);

		if (get_cookie('remember_token')) {
			delete_cookie('remember_token');
		}

		$this->session->unset_userdata('ci_seesion_key');
		$this->session->unset_userdata('ci_session_key_generate');
		$this->session->unset_userdata('user_id');

		$this->session->sess_destroy();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");

		$this->session->set_flashdata('success', 'Logged Out Successfully !');

		redirect('/login', 'refresh');
	}

	public function forgotPassword() {
		redirectAuthUser();
		$postParams = $this->input->post();

		if ($postParams && isset($postParams['submit'])) {
			$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[100]|valid_email');
			if ($this->form_validation->run() == false) {
				redirectAndShowFirstError('/login');
			} else {
				$email = $this->input->post('email');
				$user = $this->user_model->get_row(['email' => $email]);
				if ($user) {
					$reset_password_token = generate_random_string(15);
					if ($this->user_model->update_row(['email' => $email], ['email_token' => $reset_password_token])) {
						$subject = "Reset Password | ". config_item('company_name');
						$token_url = ms_site_url("/reset-password/".$reset_password_token);
						$emailContent = "<html>
						<body>
						<div>  <img src=".ms_site_url('assets/images/frontend/logo.png')." alt='Logo' style='width:200px;'> </div>

						<p>We hear you need a new password.</p>
						<p>You can reset it by using this link to access your account. </p>
						<a href='".$token_url."' target='_blank'>
						<span>                                          
						Reset Password
						</span> 
						</a>
						<p>Or copy the following link and paste on the browser</p>
						<p>".$token_url."</p>
						<p>Thanks very much,</p>   
						<p>The ". config_item('company_name') ." Team</p>
						</body>
						</html>";
						sendEmail($email, $subject, $emailContent, $user->name);

						$this->session->set_flashdata('success', "Reset Password Email Sent Successfully Please Check Your Email For Further Processing.");
						redirect('/login', 'refresh');
					}
				} else {
					$this->session->set_flashdata('error', 'The record doesn\'t exist in our database.');
					redirect('/login', 'refresh');
				}
			}
		} else {
			redirect('/login', 'refresh');
		}
	}

	public function resetPassword($token) {
		$this->data['page_title'] = 'Reset Password | '.config_item('company_name');
		redirectAuthUser();

		$user = $this->user_model->get_row(['email_token' => $token]);

		if (!$user) {
			$this->session->set_flashdata('error', 'The token is either invalid or expired');
			redirect('/login', 'refresh');
		} else {

			$postParams = $this->input->post();

			if ($postParams && isset($postParams['submit'])) {
				$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
				$this->form_validation->set_rules('password', 'password', 'required|min_length[6]|max_length[20]');
				$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|min_length[6]|max_length[20]|matches[password]');
				if ($this->form_validation->run() == false) {
					$this->data['path'] = "auth/reset-password";
					$this->load->view('layout/app', $this->data);
				} else {
					$email = $this->input->post('email');
					$password = $this->input->post('password');
					$user = $this->user_model->get_row(['email' => $email, 'email_token' => $token]);
					if ($user) {
						if ($this->user_model->update_row(['email' => $email, 'email_token' => $token], ['email_token' => null, 'password' => password_hash($password, PASSWORD_DEFAULT)])) {

							$subject = "Password Reset Successfully | ". config_item('company_name');
							$emailContent = "<html>
							<body>
							<div><img src=".ms_site_url('assets/images/frontend/logo.png')." alt='Logo' style='width:200px;'> </div>
							<p>Dear " . $user->name . ",</p>
							<p>Thank you, Your Password has been reset successfully.</p>
							<p>Now login with new password</p>
							<p>Thanks very much,</p>   
							<p>The ". config_item('company_name') ." Team</p>
							</body>
							</html>";
							sendEmail($email, $subject, $emailContent, $user->name);
							$this->session->set_flashdata('success', "Your Password Reset Successfully. Please Login With New Password.");
							redirect('/login');
						}
					} else {
						$this->session->set_flashdata('error', 'The token is either invalid or expired');
						redirect('/login', 'refresh');
					}
				}
			} else {
				$this->data['path'] = "auth/reset-password";
				$this->load->view('layout/app', $this->data);
			}
		}
	}

	public function selectRole(){
		redirectFromProtectedRoutes();
		$redirect_url = $this->input->get('redirect_url');
		$user_id = getLoggedInUserId();
		$user_info = $this->user_model->get_row(['id' => $user_id]);
		if($user_info->user_role != null){ redirect('dashboard'); }

		$postParams = $this->input->post();

		if ($postParams && isset($postParams['user_role'])) {
			// echo "<pre>"; print_r($postParams); die("sasa");
			$user_data['user_role'] = $postParams['user_role'];
			if($this->user_model->update_row(['id' => $user_id], $user_data)){
				$user = $this->user_model->get_row(['id' => $user_id]);
				// echo "<pre>"; print_r($user); die;
				if ($user->user_role == TEACHER_ROLE) {
					if ($redirect_url) {
						redirect($redirect_url);
					} else {
						redirect('dashboard');
					}
				} else {
					if ($redirect_url) {
						redirect($redirect_url);
					} else {
						redirect('/home');
					}
				}
			}else{
				$this->session->set_flashdata('error', 'Something went wrong.');	
			}
		}else{
			$this->session->set_flashdata('error', 'Please select your role.');
		}

		$this->data['user_info'] = $user_info;
		$this->data['path'] = "auth/confirm-role";
		$this->load->view('layout/app', $this->data);
	}
}