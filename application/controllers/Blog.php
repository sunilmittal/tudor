<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Blog extends CI_Controller
{
	private $data;
	private $google_client;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['url']);
		$this->load->model(['category_model', 'video_model', 'blog_model']);
		$this->data = [];
		$this->google_client = "" ;

		if (isUserLoggedIn()) {
			$this->data['user_data'] = getLoggedInUserDetails();
		}
		$all_categories = $this->category_model->get_rows([], true);

		$parent_cat = array_values(array_filter($all_categories, function($cat) {
			return !$cat['parent_id'];
		}));

		$all_categories = array_map(function($value) use ($all_categories) {
			$value['sub_cat'] = array_values(array_filter($all_categories, function($cat) use($value) {
				return $cat['parent_id'] == $value['id'];
			}));
			return $value;
		}, $parent_cat);

		$all_categories = collect($all_categories);

		$category_ids = array_column($this->video_model->getPopularCategoryBasedOnVideosCount(), 'category_id');
		$all_categories = $all_categories->sortBy(function($category) use ($category_ids) {
			return array_search($category['id'], $category_ids);
		})->values();

		// google authentication

		$this->google_client = new Google_Client();
		$this->google_client->setClientId($this->config->item( 'google_client_id' )); //Define your ClientID
		$this->google_client->setClientSecret($this->config->item( 'google_client_secret' )); //Define your Client Secret Key
		$this->google_client->setRedirectUri(base_url() . 'login'); //Define your Redirect Uri
		$this->google_client->addScope('email');
		$this->google_client->addScope('profile');
		$this->data['login_button'] = '<a href="'.$this->google_client->createAuthUrl().'"><i class="fa fa-google" aria-hidden="true"></i> Login</a>';

		// end of google authentication

		$this->data['all_categories'] = $all_categories;
	}

	public function blogOverview($category = null) {

		$category_exist = $featured = "";
		if($category){
			if(empty($this->blog_model->get_categories(['industry_sector_id' => $category]) ) ) { redirect("blog-overview"); }
			$blogs = $this->blog_model->get_blogs(['category_id' => $category]);
			$category_exist = $category;
		}else{
			$blogs = @array_slice($this->blog_model->get_blogs(), 5);
			$featured = $this->blog_model->get_blogs([], false, ['blog_id' => 'desc'], 5);
		}

		// echo "<pre>"; print_r($featured); die;
		$this->data['blogs'] = $blogs;
		$this->data['featured'] = $featured;
		$this->data['category_exist'] = $category_exist;
		$this->data['blog_category'] = $this->blog_model->get_categories();

		$this->data['page_title'] = 'Blog Overivew';
		$this->data['page_meta_keyword'] = 'Blog Overivew';
		$this->data['page_meta_description'] = 'Blog Overivew';
		$this->data['path'] = 'app/blog-overview';
		$this->load->view('layout/app-blog', $this->data);
	}

	public function blogs() {
		$this->data['page_title'] = 'Blog Overivew';
		$this->data['page_meta_keyword'] = 'Blog Overivew';
		$this->data['page_meta_description'] = 'Blog Overivew';
		$this->data['path'] = 'app/blogs';
		$this->load->view('layout/app-blog', $this->data);
	}

	public function blogDetail($slug = null) {
		$this->data['blog'] = $this->blog_model->get_blogs(['slug' => $slug], true);
		if( $slug && !empty($this->data['blog']) ) {
			$this->data['blogs'] = $this->blog_model->get_blogs(["is_featured" => false, "slug !=" => $slug], false, ['blog_id' => 'desc'], 5);	
			$this->data['featured'] = $this->blog_model->get_blogs(["is_featured" => true, "slug !=" => $slug], false, ['blog_id' => 'desc'], 5);

			//  meta details set
			$this->data['page_title'] = $this->data['blog']->title;
			$this->data['page_meta_keyword'] = $this->data['blog']->meta_keywords;
			$this->data['page_meta_description'] = $this->data['blog']->meta_desc;
			$value = base_url() . 'admin/uploads/images/' . $this->data['blog']->image;
			$this->data['settings']['value'] = $value;
			$this->data['settings'] = (object) $this->data['settings'];
			$this->data['url'] = base_url(). 'blog-detail/' . $slug;
			$this->data['path'] = 'app/blog-detail';
			$this->load->view('layout/app-blog', $this->data);
		}else{
			redirect("/blog-overview");
		}
	}

}
