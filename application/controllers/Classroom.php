<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Classroom extends CI_Controller
{
	private $data;
	private $google_client;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url','file'));
		$this->load->library(array('form_validation','session','upload'));
		$this->load->model(['user_model', 'category_model', 'classroom_model','classroom_users_model', 'channel_model', 'subscribe_model', 'video_model']);
		$this->data = array();
		$this->google_client = "" ;

		$this->upload->initialize(array(
			'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR,
			'allowed_types' => config_item('image')['allowed_ext'],
			'max_size' => config_item('image')['allowed_size'] * 1024,
			'encrypt_name' => TRUE
		));

		if (isUserLoggedIn()) {
			$this->data['user_data'] = getLoggedInUserDetails();
		}

		$all_categories = $this->category_model->get_rows([], true);

		$parent_cat = array_values(array_filter($all_categories, function($cat) {
			return !$cat['parent_id'];
		}));

		$all_categories = array_map(function($value) use ($all_categories) {
			$value['sub_cat'] = array_values(array_filter($all_categories, function($cat) use($value) {
				return $cat['parent_id'] == $value['id'];
			}));
			return $value;
		}, $parent_cat);

		$all_categories = collect($all_categories);

		$category_ids = array_column($this->video_model->getPopularCategoryBasedOnVideosCount(), 'category_id');
		$all_categories = $all_categories->sortBy(function($category) use ($category_ids) {
			return array_search($category['id'], $category_ids);
		})->values();

		// google authentication

		$this->google_client = new Google_Client();
		$this->google_client->setClientId($this->config->item( 'google_client_id' )); //Define your ClientID
		$this->google_client->setClientSecret($this->config->item( 'google_client_secret' )); //Define your Client Secret Key
		$this->google_client->setRedirectUri(base_url() . 'login'); //Define your Redirect Uri
		$this->google_client->addScope('email');
		$this->google_client->addScope('profile');
		$this->data['login_button'] = '<a href="'.$this->google_client->createAuthUrl().'"><i class="fa fa-google" aria-hidden="true"></i> Login</a>';

		// end of google authentication


		$this->data['all_categories'] = $all_categories;
		$this->data['page_title'] = 'Classroom | '.config_item('company_name');
	}

	/**
	 * index function to show the list of created classrooms
	 * @return void
	 */
	public function index() {
		$this->data['page_title'] = 'Classrooms | '.config_item('company_name');
		$this->data['main_line'] = 'Don\'t miss classroom classrooms';
		
		if (!isUserLoggedIn()) {
			$this->data['path'] = 'dashboard/blank';
			$this->load->view('layout/app', $this->data);
			return;
		}

		redirectIfUserRoleIsNormal();

		$user_id = getLoggedInUserId();
		$classrooms = $this->classroom_model->get_rows([
			'user_id' => $user_id,'deleted_at' => NULL
		]);
		$this->data['classrooms'] = $classrooms;
		$this->data['path'] = 'classroom/index';
		$this->load->view('layout/app', $this->data);
	}

	/**
	 * Add function to show the data of User on profile page
	 * @return void
	 */
	public function add() {
		$this->data['page_title'] = 'Add Classroom | '.config_item('company_name');
		redirectFromProtectedRoutes();
		redirectIfUserRoleIsNormal();

		$user_id = getLoggedInUserId();

		try {
			$postParams = $this->input->post();
			if ($postParams) {
				$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[3]|max_length[250]');
				$this->form_validation->set_rules('description', 'Description', 'trim|min_length[8]|max_length[1000]');

				if ($this->form_validation->run() == false) {
					$this->data['path'] = 'classroom/add';
					$this->load->view('layout/app', $this->data);
				} else {

					$data = array(
						'title' => set_value('title'),
						'description' => set_value('description') ? set_value('description') : NULL
					);

					$data['user_id'] = $user_id;
					$data['channel_id'] = getChannelId();

					if (isset($_FILES['image']) && $_FILES['image']['error'] !== UPLOAD_ERR_NO_FILE) {
						$this->upload->initialize(array(
							'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR,
							'allowed_types' => config_item('image')['allowed_ext'],
							'max_size' => config_item('image')['allowed_size'] * 1024,
							'encrypt_name' => TRUE
						));

						if ($this->upload->do_upload('image')) {
							chmod($this->upload->data('full_path'), 0777);
							$data['image'] = $this->upload->data('file_name');
						} else {
							$data['image'] = null;
	                        // $upload_errors[] = "Image: " . $this->upload->display_errors('', '');
						}
					}

					$this->classroom_model->add_row($data);
					$this->session->set_flashdata('success', 'Classroom created Successfully.');
					redirect('/classroom', 'refresh');
				}
			} else {
				$this->data['path'] = 'classroom/add';
				$this->load->view('layout/app', $this->data);
			}
		} catch (Exception $e) {
			$this->session->set_flashdata('error', $e->getMessage());
			redirect('/classrooms/add', 'refresh');
		}
	}

	/**
	 * edit function to show the data of User on profile page
	 * @return void
	 */
	public function edit($classroom_id) {
		$this->data['page_title'] = 'Edit classroom | '.config_item('company_name');
		redirectFromProtectedRoutes();
		redirectIfUserRoleIsNormal();

		$user_id = getLoggedInUserId();

		if (!$classroom_id) redirect('/classroom');

		$classroom_details = $this->classroom_model->get_row(
			array(
				'id' => $classroom_id,
				'user_id' => $user_id
			)
		);

		if (!$classroom_details) redirect('classroom');

		$this->data['classroom_details'] = $classroom_details;
		$postParams = $this->input->post();

		if ($postParams) {
			$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[3]|max_length[250]');
			$this->form_validation->set_rules('description', 'Description', 'trim|min_length[8]|max_length[1000]');

			if ($this->form_validation->run() == false) {
				$this->data['path'] = 'classrooms/edit';
				$this->load->view('layout/app', $this->data);
			} else {
				$data = array(
					'title' => set_value('title'),
					'description' => set_value('description') ? set_value('description') : NULL
				);
				
				if (isset($_FILES['image']) && $_FILES['image']['error'] !== UPLOAD_ERR_NO_FILE) {
					$this->upload->initialize(array(
						'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR,
						'allowed_types' => config_item('image')['allowed_ext'],
						'max_size' => config_item('image')['allowed_size'] * 1024,
						'encrypt_name' => TRUE
					));

					if ($this->upload->do_upload('image')) {
						chmod($this->upload->data('full_path'), 0777);
						$data['image'] = $this->upload->data('file_name');
					} else {
						$data['image'] = null;
                        // $upload_errors[] = "Image: " . $this->upload->display_errors('', '');
					}
				}

				$this->classroom_model->update_row(['id' => $classroom_id], $data);
				$this->session->set_flashdata('success', 'Classroom updated Successfully.');
				redirect('classroom', 'refresh');
			}
		} else {
			$this->data['path'] = 'classroom/edit';
			$this->load->view('layout/app', $this->data);
		}
	}

	/**
	 * delete function to show the data of User on profile page
	 * @return void
	 */
	public function delete($classroom_id) {
		redirectFromProtectedRoutes();
		redirectIfUserRoleIsNormal();
		$user_id = getLoggedInUserId();

		if (!$classroom_id) redirect('classroom');

		$classroom_details = $this->classroom_model->get_row(
			array(
				'id' => $classroom_id,
				'user_id' => $user_id
			)
		);

		if (!$classroom_details) redirect('classroom');
		$this->classroom_model->delete_row(['id' => $classroom_id]);
		$this->classroom_users_model->delete_classroom_users(['classroom_id' => $classroom_id]);
		if($classroom_details->video_ids){
			$updateData = ['classroom_id'=> NULL,'visibility'=> "0",'updated_at' => date('Y-m-d H:i:s')];
			$this->db->where('classroom_id',$classroom_id)->update('videos', $updateData);
		}
		$this->session->set_flashdata('success', 'Classroom deleted Successfully.');
		redirect('/classroom', 'refresh');
	}

	public function access($classroom_id){
		$this->data['page_title'] = 'Access classroom | '.config_item('company_name');
		redirectFromProtectedRoutes();	
		if (!$classroom_id) redirect('/home');

		$user_id = getLoggedInUserId();

		$classroom_details = $this->classroom_model->get_row(
			array(
				'id' => $classroom_id
			)
		);

		if (!$classroom_details) redirect('videos');

		$related_videos = $this->video_model->get_rows(
			array(
				'classroom_id' => $classroom_id
			)
		);

		$access_status = $this->classroom_users_model->get_row(['classroom_id' => $classroom_id,'user_id' => $user_id]);
		// if(isset($access_status->request_access) && ($access_status->request_access == 2)) redirect('/home');
		$this->session->set_userdata('access_classroom_id', $classroom_id);
		$this->data['access_status'] = $access_status;
		$this->data['related_videos'] = $related_videos;
		$this->data['classroom_details'] = $classroom_details;
		$this->data['path'] = 'classroom/access';
		$this->load->view('layout/app', $this->data);
		
	}

	public function sendRequest(){
		$access_classroom_id = $this->session->userdata('access_classroom_id');
		$this->session->unset_userdata('access_classroom_id');
		$data['user_id'] = getLoggedInUserId();
		$data['classroom_id'] = $access_classroom_id;

		$class_exists = $this->classroom_users_model->get_row(['classroom_id' => $access_classroom_id,'user_id' => $data['user_id']]);
		if($class_exists){
			$this->classroom_users_model->delete_classroom_users(['classroom_id' => $access_classroom_id,'user_id' => $data['user_id']]);
			$status['message'] = "cancelled";
		}else{
			$status = $this->classroom_users_model->add_user_classroom($data,['classroom_id' => $data['classroom_id'],'user_id' => $data['user_id'],'request_access'=> 1]);
		}
		if($status){
			$this->session->set_flashdata('success', 'Request has been '. $status['message'] .' Successfully');
		}else{
			$this->session->set_flashdata('error', 'Something went wrong, Please try again');
		}
		redirect('classroom/'.$access_classroom_id);
	}

	public function classroomRequests(){
		$this->data['page_title'] = 'Classroom Requests | '.config_item('company_name');
		redirectFromProtectedRoutes();
		redirectIfUserRoleIsNormal();

		$user_id = getLoggedInUserId();
		$classrooms = $this->db->select('id')->where(['user_id' => $user_id,'deleted_at'=>NULL])->get('classrooms')->result_array();

		if($classrooms){
			$count =0;
			$dataArray = [];
			foreach ($classrooms as $classroom) {
				$classroomUser = $this->db->where(['classroom_id' => $classroom['id']])->order_by('date','desc')->get('classroom_users')->result_array();
				if($classroomUser){
					$dataArray[] = $classroomUser;
					$count++;
				}
			}
			
			$this->data['classroom_user_listing'] = $dataArray;
			$this->data['path'] = 'classroom/access-requests';
			$this->load->view('layout/app', $this->data);
		}else{
			$this->session->set_flashdata('error', 'No classroom found');
			redirect("classroom");
		}
	}

	public function grant_access($classroom_id,$user_id){
		if(($user_id) && ($classroom_id)){
			$where = ['user_id' => $user_id,'classroom_id'=> $classroom_id];
			$get_row = $this->classroom_users_model->get_row($where);
			if($get_row){
				$updateData['request_access'] = ($get_row->request_access == 1) ? 2 : 1;
				$status = $this->classroom_users_model->update_row($where,$updateData);
				if($status){
					if($updateData['request_access'] == 2){
						$this->session->set_flashdata('success', 'Request has been given access Successfully');	
					}else{
						$this->session->set_flashdata('success', 'Request has been cancelled access Successfully');	
					}
				}
			}else{
				$this->session->set_flashdata('error', 'Something went wrong, Please try again');
			}
			redirect('classroom-requests');
		}else{
			$this->session->set_flashdata('error', 'Access not allowed');
			redirect('/');
		}
	}

	public function delete_classroom_user($classroom_id,$user_id){
		if($classroom_id && $user_id){
			$checkResult = $this->classroom_users_model->delete_classroom_users(['classroom_id' => $classroom_id,'user_id'=> $user_id]);
			if($checkResult){
				$this->session->set_flashdata('success', 'Request has been deleted Successfully');
			}else{
				$this->session->set_flashdata('error', 'Error while delete request');
			}
			redirect('classroom-requests');
		}
	}
}
