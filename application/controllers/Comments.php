<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Comments extends CI_Controller
{
    private $data;
    private $perPage = 10;
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['url', 'file']);
		$this->load->model(['category_model','comments_model','video_model']);
		$this->data = array();

		if (isUserLoggedIn()) {
			$this->data['user_data'] = getLoggedInUserDetails();
		}
	
        $all_categories = $this->category_model->get_rows([], true);

		$parent_cat = array_values(array_filter($all_categories, function($cat) {
			return !$cat['parent_id'];
		}));

		$all_categories = array_map(function($value) use ($all_categories) {
			$value['sub_cat'] = array_values(array_filter($all_categories, function($cat) use($value) {
				return $cat['parent_id'] == $value['id'];
			}));
			return $value;
		}, $parent_cat);

		$all_categories = collect($all_categories);

		$category_ids = array_column($this->video_model->getPopularCategoryBasedOnVideosCount(), 'category_id');
		$all_categories = $all_categories->sortBy(function($category) use ($category_ids) {
			return array_search($category['id'], $category_ids);
		})->values();
		
		$this->data['all_categories'] = $all_categories;
		$this->data['page_title'] = 'Comments | '.config_item('company_name');
	}

    /**
	 * index function to show the listing of the User's comments
	 * @return void
	 */
	public function index($resource_slug) {
        //redirectFromProtectedRoutes();
        
        $start = !empty($this->input->get("page")) ? ($this->input->get("page") * $this->perPage) : 0;
        $resource_type = !empty($this->input->get("resource_type")) ? ($this->input->get("resource_type")) : 'notes';
        $model = ($resource_type=='videos') ? 'video_model' : 'notes_model';
        $resource_id = getResourceIdFromSlug($resource_slug,$model);
        
        $comments = $this->comments_model->get_rows(
            array('resource_id' => $resource_id, 'type' => $resource_type, 'parent_id' => NULL
        ),$this->perPage, $start);
        
        $this->data['comments'] = $comments;
        $this->data['limit'] = $this->perPage;
        $this->load->view('comments/index', $this->data);
    }

     /**
	 * function to add the data of comment
	 * @return void
	 */
	public function add($resource_id = NULL) {
		redirectFromProtectedRoutes();

		$user_id = getLoggedInUserId();
        $response = [
            'status' => false,
        ];
    
        try {
            if ($resource_id) {

                $this->form_validation->set_rules('comment', 'Comment', 'trim|required|min_length[3]');

                if ($this->form_validation->run() == false) {
                    $response = [
                        'status' => false,
                        'error' => getFirstError(),
                    ];
                }
                else{
                    $data = array(
                        'comment' => $this->input->post('comment'),
                        'resource_id' => $resource_id,
                        'user_id' => $user_id,
                        'type' => $this->input->post('resource_type'),
                        'parent_id' => set_value('parent_id', NULL),
                    );
                    $this->comments_model->add_row($data);
                    $totalComments = $this->comments_model->getCommentsCount($resource_id,$this->input->post('resource_type'));
                    $response = [
                        'status' => true,
                        'totalComments' => $totalComments
                    ];
                }
            }            
        } catch (Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /**
	 * delete function to delete comment data
	 * @return void
	 */
	public function delete($cmt_id) {
		redirectFromProtectedRoutes();
        $user_id = getLoggedInUserId();
        
        $response = [
            'status' => false,
        ];

		if (!$cmt_id) redirect('notes');

		$comment_details = $this->comments_model->get_row(
			array(
				'id' => $cmt_id,
			)
        );

		if (!$comment_details) redirect('notes');
        $this->comments_model->delete_row(['id' => $cmt_id]);
        $this->comments_model->delete_rows(['parent_id' => $cmt_id]);
        $totalComments = $this->comments_model->getCommentsCount($comment_details->resource_id,$comment_details->type);
        $response = [
            'status' => true,
            'totalComments' => $totalComments
        ];

		header('Content-Type: application/json');
        echo json_encode($response);
    }
    
    /**
	 * function to add the data of comment
	 * @return void
	 */
	public function edit($cmt_id = NULL) {
		redirectFromProtectedRoutes();

		$user_id = getLoggedInUserId();
        $response = [
            'status' => false,
        ];

        if (!$cmt_id) redirect('notes');

		$comment_details = $this->comments_model->get_row(
			array(
				'id' => $cmt_id,
				'user_id' => $user_id
			)
        );
    
        try {
            if ($cmt_id) {
                $data = array(
					'comment' => $this->input->input_stream('comment'),
                );
                $this->comments_model->update_row(['id' => $cmt_id], $data);

                $totalComments = $this->comments_model->getCommentsCount($comment_details->resource_id,$comment_details->type);
                $response = [
                    'status' => true,
                    'totalComments' => $totalComments
                ];
            }            
        } catch (Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    /**
	 * function to add the data of comment reply
	 * @return void
	 */
	public function reply($cmt_id = NULL) {
		redirectFromProtectedRoutes();

		$user_id = getLoggedInUserId();
        $response = [
            'status' => false,
        ];

        $resource_type = !empty($this->input->get("resource_type")) ? ($this->input->get("resource_type")) : 'notes';
    
        try {
            if ($cmt_id) {
                $this->form_validation->set_rules('reply', 'Reply', 'trim|required|min_length[3]');

                if ($this->form_validation->run() == false) {
                    $response = [
                        'status' => false,
                        'error' => getFirstError(),
                    ];
                }
                else{
                    $data = array(
                        'comment' => $this->input->post('reply'),
                        'resource_id' => $this->input->post('resource_id'),
                        'user_id' => $user_id,
                        'type' => $resource_type,
                        'parent_id' => $cmt_id,
                    );
                    $this->comments_model->add_row($data);

                    $response = [
                        'status' => true,
                    ];
                }
            }            
        } catch (Exception $e) {
            $response = [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function replies($comment_id){
        $offset = $this->input->get('offset');
        $result  = $this->comments_model->getReplies($comment_id,$this->perPage,$offset);
        $this->data['offset'] =$offset + $this->perPage;
        $this->data['limit'] = $this->perPage;
        $this->data['replies'] = $result;
		$this->data['replies_count'] = $this->comments_model->getRepliesCount($comment_id);
        $this->load->view('comments/replies', $this->data);
      }

}
