<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once(APPPATH.'traits/additional_validation_methods.php');
use Carbon\Carbon;
class Dashboard extends CI_Controller
{
	use AdditionalValidationMathods;
	private $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['url', 'file']);
		$this->load->library(array('upload'));
		$this->load->model(['user_model', 'category_model', 'video_model', 'subscribe_model']);
		$this->data = array();

		if (isUserLoggedIn()) {
			$this->data['user_data'] = getLoggedInUserDetails();
		}
// echo "<pre>"; print_r(getLoggedInUserDetails()); die;
		$this->upload->initialize(array(
			'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'avatars' . DIRECTORY_SEPARATOR,
			'allowed_types' => config_item('image')['allowed_ext'],
			'max_size' => config_item('image')['allowed_size'] * 1024,
			'encrypt_name' => TRUE
		));

		$all_categories = $this->category_model->get_rows([], true);

		$parent_cat = array_values(array_filter($all_categories, function($cat) {
			return !$cat['parent_id'];
		}));

		$all_categories = array_map(function($value) use ($all_categories) {
			$value['sub_cat'] = array_values(array_filter($all_categories, function($cat) use($value) {
				return $cat['parent_id'] == $value['id'];
			}));
			return $value;
		}, $parent_cat);

		$all_categories = collect($all_categories);

		$category_ids = array_column($this->video_model->getPopularCategoryBasedOnVideosCount(), 'category_id');
		$all_categories = $all_categories->sortBy(function($category) use ($category_ids) {
		    return array_search($category['id'], $category_ids);
		})->values();
		
		$this->data['all_categories'] = $all_categories;
		$this->data['page_title'] = 'Dashboard | '.config_item('company_name');
	}

	/**
	 * index function to show the data of User on dashboard page
	 * @return void
	 */
	public function index() {
		redirectFromProtectedRoutes();
		redirectIfUserRoleIsNormal();
		
		$channel_id = getChannelId();
		$dateForFilter = Carbon::now()->subDays(7)->hour(0)->minute(0)->second(0);

		$videos = collect($this->video_model->get_rows([
			'channel_id' => $channel_id
		]));

		$subscribers = collect($this->subscribe_model->get_rows([
			'channel_id' => $channel_id
		]));

		$this->data['stat'] = [
			'views' => [
				'total' => $videos->sum('view_count'),
				'last_seven_days' => $videos->filter(function ($item) use ($dateForFilter) {
				    return $item->created_at >= $dateForFilter;
				})->sum('view_count')
			],
			'subscribers' => [
				'total' => $subscribers->count(),
				'last_seven_days' => $subscribers->filter(function ($item) use ($dateForFilter) {
				    return $item->created_at >= $dateForFilter;
				})->count()
			],
			'comments' => [
				'total' => $videos->sum('comment_count'),
				'last_seven_days' => $videos->filter(function ($item) use ($dateForFilter) {
				    return $item->created_at >= $dateForFilter;
				})->sum('comment_count')
			]
		];
		// echo "<pre>"; print_r($this->data['stat']); die;
		$this->data['path'] = 'dashboard/index';
		$this->load->view('layout/app', $this->data);
	}

	/**
	 * Dashboard function to show the data of User on dashboard page
	 * @return void
	 */
	public function settings() {
		$this->data['page_title'] = 'Settings | '.config_item('company_name');
		redirectFromProtectedRoutes();
		$user_id = getLoggedInUserId();
		$user_details = getLoggedInUserDetails();

		$postParams = $this->input->post();

		if ($postParams && isset($postParams['submit'])) {
			$this->form_validation->set_rules('name', 'Full Name', 'required|min_length[3]|max_length[50]|regex_match[/^([a-z ])+$/i]');
			$this->form_validation->set_rules('description', 'Description', 'trim|min_length[8]|max_length[1000]');

			if (isset($postParams['current_password']) && $postParams['current_password']) {
				$this->form_validation->set_rules('current_password', 'current password', 'required|min_length[6]|max_length[20]');
				$this->form_validation->set_rules('password', 'password', 'required|min_length[6]|max_length[20]');
				$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|min_length[6]|max_length[20]|matches[password]');
			}
			if (!empty($_FILES['image']['name'])) {
				$this->form_validation->set_rules('image', 'Profile Image', 'callback_file_check[image-' . implode('-', config_item('image')) . ']');
			}

			if (!empty($_FILES['user_banner']['name'])) {
				$this->form_validation->set_rules('user_banner', 'Banner', 'callback_file_check[user_banner-' . implode('-', config_item('image')) . ']');
			}
			if ($this->form_validation->run() == false) {
				$this->data['path'] = 'dashboard/settings';
				$this->load->view('layout/app', $this->data);
			} else {
				$data = array(
					'name' => $this->input->post('name'),
	                'description' => set_value('description') ? set_value('description') : NULL
				);
				
				$data['image'] = $data['banner'] = null;	
				if (isset($_FILES['image']) && $_FILES['image']['error'] !== UPLOAD_ERR_NO_FILE) {

					if ($this->upload->do_upload('image')) {
						chmod($this->upload->data('full_path'), 0777);
						$data['image'] = $this->upload->data('file_name');
					} else {
						$data['image'] = null;
					}
				}
				if ($postParams['delete_image'] == 1) {
					if ($user_details->image) {
						$path = FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'avatars' . DIRECTORY_SEPARATOR;
						if(file_exists($path.$user_details->image)){
							unlink($path.$user_details->image);
						  }
					}
					if (!$data['image']) {
						$data['image'] = null;					
					}
				} else if (!$data['image'] && $postParams['delete_image']==0) {
				    unset($data['image']);
				}

           		if (isset($_FILES['user_banner']) && $_FILES['user_banner']['error'] !== UPLOAD_ERR_NO_FILE) {

                    if ($this->upload->do_upload('user_banner')) {
                        chmod($this->upload->data('full_path'), 0777);
                        $data['banner'] = $this->upload->data('file_name');
                    } else {
                    	$data['banner'] = null;
                    }
				}
				
				/*if (isset($_FILES['user_banner']) && $_FILES['user_banner']['error'] !== UPLOAD_ERR_NO_FILE) {

					if ($this->upload->do_upload('user_banner')) {
						chmod($this->upload->data('full_path'), 0777);
						$data['banner'] = $this->upload->data('file_name');
					} else {
						$data['banner'] = null;
					}
				}*/ 
				
				if ($postParams['delete_second_image'] == 1) {
					if ($user_details->banner) {
						$path = FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'avatars' . DIRECTORY_SEPARATOR;
						if(file_exists($path.$user_details->banner)){
							unlink($path.$user_details->banner);
						  }
					}
					if (!$data['banner']) {
						$data['banner'] = null;					
					}
				} else if (!$data['banner'] && $postParams['delete_second_image']==0) {
				    unset($data['banner']);
				}

				if (isset($postParams['current_password']) && $postParams['current_password'] && password_verify($this->input->post('current_password'), $this->data['user_data']->password)) {
					$data['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
				}

				$this->user_model->update_row(['id' => $user_id], $data);
				$this->session->set_flashdata('success', 'User Updated Successfully.');
				redirectAuthUser();
			}
		} else {
			$this->data['path'] = 'dashboard/settings';
			$this->load->view('layout/app', $this->data);
		}		
	}

	public function blank() {
		$this->data['path'] = 'dashboard/blank';
		$this->load->view('layout/app', $this->data);
	}
}
