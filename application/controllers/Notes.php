<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once(APPPATH.'traits/additional_validation_methods.php');

class Notes extends CI_Controller
{
	use AdditionalValidationMathods;
	private $data;
	public $perPage = 10;
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['url', 'file']);
		$this->load->library(array('upload'));
		$this->load->model(['category_model', 'video_model','notes_model','comments_model','user_model']);
		$this->data = array();

		if (isUserLoggedIn()) {
			$this->data['user_data'] = getLoggedInUserDetails();
		}
		$this->upload->initialize(array(
			'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'notes' . DIRECTORY_SEPARATOR,
			'allowed_types' => config_item('image')['allowed_ext'],
			'max_size' => config_item('image')['allowed_size'] * 1024,
			'encrypt_name' => TRUE
		));
        $all_categories = $this->category_model->get_rows([], true);

		$parent_cat = array_values(array_filter($all_categories, function($cat) {
			return !$cat['parent_id'];
		}));

		$all_categories = array_map(function($value) use ($all_categories) {
			$value['sub_cat'] = array_values(array_filter($all_categories, function($cat) use($value) {
				return $cat['parent_id'] == $value['id'];
			}));
			return $value;
		}, $parent_cat);

		$all_categories = collect($all_categories);

		$category_ids = array_column($this->video_model->getPopularCategoryBasedOnVideosCount(), 'category_id');
		$all_categories = $all_categories->sortBy(function($category) use ($category_ids) {
			return array_search($category['id'], $category_ids);
		})->values();
		
		$this->data['all_categories'] = $all_categories;
		$this->data['page_title'] = 'Notes Feed | '.config_item('company_name');
	}

    /**
	 * index function to show the listing of the User's notes
	 * @return void
	 */
	public function index() {
		redirectFromProtectedRoutes();

		$this->data['page_title'] = 'User Notes | '.config_item('company_name');
		$user_id = getLoggedInUserId();
		$notes = $this->notes_model->get_rows([
			'user_id' => $user_id
		]);
		$this->data['notes'] = $notes;
		$this->data['path'] = 'notes/index';
		$this->load->view('layout/app', $this->data);
    }
    
    /**
	 * Profile function to add the data of notes
	 * @return void
	 */
	public function add() {
		redirectFromProtectedRoutes();

		$this->data['page_title'] = 'Add Note | '.config_item('company_name');
		$user_id = getLoggedInUserId();
		$this->data['categories'] = $this->category_model->get_rows(['parent_id' => null]);
		$this->data['sub_categories'] = [];
		if ($category_id = set_value('category_id')) {
			$this->data['sub_categories'] = $this->category_model->get_rows(['parent_id' => $category_id]);
		}

		try {
			$postParams = $this->input->post();
			if ($postParams){
				$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[8]');
				$this->form_validation->set_rules('description', 'Description', 'trim|required|min_length[3]');

				if ($this->form_validation->run() == false) {
					$this->data['path'] = 'notes/add';
					$this->load->view('layout/app', $this->data);
				} else {
				$title_with_limit = substr($postParams['title'], 0, 80);
				$slug =  url_title($title_with_limit, 'dash', true).'-'.rand(100000, 999999);
				$data = array(
					'title' => set_value('title'),
					'slug' => $slug,
					'description' => set_value('description', NULL, false),
					'user_id' => $user_id,
					'category_id' => set_value('category_id') ?: NULL,
					'sub_category_id' => set_value('sub_category_id') ?: NULL,
					'date' => date('Y-m-d H:i:s'),
				);

				if (isset($_FILES['image']) && $_FILES['image']['error'] !== UPLOAD_ERR_NO_FILE) {

					if ($this->upload->do_upload('image')) {
						chmod($this->upload->data('full_path'), 0777);
						$data['image'] = $this->upload->data('file_name');
					} else {
						$data['image'] = null;
					}
				}

				$this->notes_model->add_row($data);
				$this->session->set_flashdata('success', 'Note added Successfully.');
				redirect('/notes', 'refresh');
				}
			} else {
				$this->data['path'] = 'notes/add';
				$this->load->view('layout/app', $this->data);
			}
		} catch (Exception $e) {
			$this->session->set_flashdata('error', $e->getMessage());
			redirect('/notes/add', 'refresh');
		}
	}

	/**
	 * edit function to edit note data
	 * @return void
	 */
	public function edit($slug = NULL) {
		redirectFromProtectedRoutes();

		$note_id = getResourceIdFromSlug($slug,'notes_model');

		$this->data['page_title'] = 'Edit note | '.config_item('company_name');
		$user_id = getLoggedInUserId();

		if (!$note_id) redirect('/notes');

		$note_details = $this->notes_model->get_row(
			array(
				'id' => $note_id,
				'user_id' => $user_id
			)
		);

		if (!$note_details) redirect('notes');

		$category_id = $note_details->category_id ?: set_value('sector_id', $note_details->category_id);
		$this->data['categories'] = $this->category_model->get_rows(['parent_id' => null]);

		$this->data['sub_categories'] = [];
		if ($category_id) {
			$this->data['sub_categories'] = $this->category_model->get_rows(['parent_id' => $category_id]);
		}

		$this->data['note_details'] = $note_details;
		$postParams = $this->input->post();

		if ($postParams) {
			$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[8]');
			$this->form_validation->set_rules('description', 'Description', 'trim|required|min_length[3]');
			//$this->form_validation->set_rules('date', 'Date', 'required|callback_date_check['.$postParams['date'].']');

			if ($this->form_validation->run() == false) {
				$this->data['path'] = 'notes/edit';
				$this->load->view('layout/app', $this->data);
			} else {

				$title_with_limit = substr($postParams['title'], 0, 80);
				$notes_slug = explode('-',$note_details->slug);
				$random_id = end($notes_slug);
				$slug =  url_title($title_with_limit, 'dash', true).'-'.$random_id;
				$data = array(
					'title' => set_value('title'),
					'slug' => $slug,
					'description' => set_value('description', NULL, false),
					'user_id' => $user_id,
					'category_id' => set_value('category_id') ? set_value('category_id') : NULL,
					'sub_category_id' => set_value('sub_category_id') ? set_value('sub_category_id') : NULL,
				);
					
				$data['image'] = null;	
				if (isset($_FILES['image']) && $_FILES['image']['error'] !== UPLOAD_ERR_NO_FILE) {

					if ($this->upload->do_upload('image')) {
						chmod($this->upload->data('full_path'), 0777);
						$data['image'] = $this->upload->data('file_name');
					} else {
						$data['image'] = null;
					}
				}
				if ($postParams['delete_image'] == 1) {
					if ($note_details->image) {
						$path = FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'notes' . DIRECTORY_SEPARATOR;
						if(file_exists($path.$note_details->image)){
							unlink($path.$note_details->image);
						  }
					}
					if (!$data['image']) {
						$data['image'] = null;					
					}
				} else if (!$data['image'] && $postParams['delete_image']==0) {
				    unset($data['image']);
				}

				$this->notes_model->update_row(['id' => $note_id], $data);
				$this->session->set_flashdata('success', 'Note updated Successfully.');
				redirect('notes', 'refresh');
			}
		} else {
			$this->data['path'] = 'notes/edit';
			$this->load->view('layout/app', $this->data);
		}
	}

    /**
	 * delete function to delete note data
	 * @return void
	 */
	public function delete($note_id) {
		redirectFromProtectedRoutes();
		$user_id = getLoggedInUserId();

		if (!$note_id) redirect('notes');

		$note_details = $this->notes_model->get_row(
			array(
				'id' => $note_id,
				'user_id' => $user_id
			)
		);

		if (!$note_details) redirect('notes');
		$this->notes_model->delete_row(['id' => $note_id]);
		$this->comments_model->delete_rows(['resource_id' => $note_id,'type' => 'notes']);
		$this->session->set_flashdata('success', 'Note deleted Successfully.');
		redirect('/notes', 'refresh');
	}

	/**
	 * show details of note
	 * @return void
	 */
    public function details($slug) {
		//redirectFromProtectedRoutes();

    	$this->data['page_title'] = 'Note Details | '.config_item('company_name');

    	if (!$slug) redirect('/home');

    	$note_details = $this->notes_model->get_row(
    		array(
				'slug' => $slug,
				'status' => 1
    		)
		);
		
		if (!$note_details) redirect('/home');

		$note_id = $note_details->id;

		$category_id = $note_details->category_id ?: set_value('sector_id', $note_details->category_id);

		$this->data['note_details'] = $note_details;
		$this->data['categories'] = $this->category_model->get_rows(['parent_id' => null]);

		$this->data['sub_categories'] = [];
		if ($category_id) {
			$this->data['sub_categories'] = $this->category_model->get_rows(['parent_id' => $category_id]);
		}

		$related_notes = $this->notes_model->get_rows(
			array(
				'id !=' => $note_id
			), false, [
				'category_id' => $note_details->category_id,
				'sub_category_id' => $note_details->sub_category_id
			]
		);

		$this->data['related_notes'] = $related_notes;
		$totalComments = $this->comments_model->getCommentsCount($note_id,'notes');
		$this->data['comments_count'] = $totalComments;
		$this->data['total_pages']  = ceil($totalComments/$this->perPage);
		$this->data['path'] = 'notes/details';
		$this->load->view('layout/app', $this->data);

	}

}
