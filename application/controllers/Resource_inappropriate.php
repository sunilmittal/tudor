<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once(APPPATH.'traits/additional_validation_methods.php');

class Resource_inappropriate extends CI_Controller
{
	use AdditionalValidationMathods;
	private $data;
	public function __construct()
	{
		parent::__construct();
		$this->load->model(['resource_inappropriate_model']);
		$this->load->helper(['url']);
		$this->data = array();

		if (isUserLoggedIn()) {
			$this->data['user_data'] = getLoggedInUserDetails();
		}
	}

    /**
	 * save function to flag the note
	 * @return void
	 */
	public function flag() {
		$this->data['page_title'] = 'Add Note Flag | '.config_item('company_name');
		redirectFromProtectedRoutes();
		$user_id = getLoggedInUserId();
	
		try {
			$postParams = $this->input->post();
			
			if ($postParams) {

				$data = array(
					'resource_id' => set_value('note_id'),
					'message' => set_value('message') ? set_value('message') : NULL,
					'user_id' => $user_id,
				);

				$this->resource_inappropriate_model->add_row($data);
				$this->session->set_flashdata('success', 'Inappropriate Note flag added Successfully.');
				redirect('/home', 'refresh');
			} else {
				redirect('/home', 'refresh');
			}
		} catch (Exception $e) {
			$this->session->set_flashdata('error', $e->getMessage());
			redirect('/home', 'refresh');
		}
	}
}
