<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Seo extends CI_Controller
{
    function sitemap()
    {
        
        $static_urls = $this->static_urls();
        
        $this->mapSlugsBasedOnLang($static_urls, "");

        $contentPageSlugs = $this->getResouceSlugs('category',array('deleted_at' => null));
        $removeKey = array_search('subjects', array_column($contentPageSlugs, 'slug'));

        if($removeKey){unset($contentPageSlugs[$removeKey]); }
        $this->removeSlashesFromSlug($contentPageSlugs);

        $this->mapSlugsBasedOnLang($contentPageSlugs, "subjects");

        $slugs = array_merge($static_urls, $contentPageSlugs);
    
        $this->removeSlashesFromSlug($slugs);

        $data['slugs'] = $slugs;
        header("Content-Type: text/xml;charset=iso-8859-1");
        if (isset($_GET['download']) && $_GET['download'] == 1) {
            $xml_string = "<?xml version='1.0' encoding='UTF-8' ?>\n<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>\n<url>\n<loc>". ms_site_url()."</loc>\n<priority>1.0</priority>\n</url>\n";
            foreach($slugs as $slug) {
                $xml_string .= "<url>\n";
                $xml_string .= "<loc>". ms_site_url().$slug['slug'] ."</loc>\n";
                if ($slug['date']) {
                    $xml_string .= "<lastmod>". $slug['date'] ."</lastmod>\n";
                }
                $xml_string .= "<priority>0.5</priority>\n";
                $xml_string .= "</url>\n";
            }
            $xml_string .= "</urlset>\n";
            $label = "sitemap";
            ob_clean();
            header('Content-type: text/plain; charset=UTF-8');
            header('Content-Disposition: attachment; filename="' . $label . '.xml"');
            echo ltrim($xml_string);
            ob_flush();
        } else {
            $this->load->view("seo/sitemap", $data);
        }
    }

    function html_sitemap()
    {

        $static_urls = $this->static_urls();
        $this->mapSlugsBasedOnLang($static_urls, "");

        $contentPages = $this->getResouceSlugs('category',array('deleted_at' => null, 'parent_id' => null));
        $removeKey = array_search('subjects', array_column($contentPages, 'slug'));

        if($removeKey){unset($contentPages[$removeKey]); }
        $this->removeSlashesFromSlug($contentPages);
        $this->mapSlugsBasedOnLang($contentPages, "subjects");
        
        $contentPages = array_merge($static_urls, $contentPages);
        $this->removeSlashesFromSlug($contentPages);

        $data['pages'] = $contentPages;
        $this->load->view("seo/html-sitemap", $data);
    }

    function static_urls()
    {
        $default_date_time = '2020-03-09 13:30:13';
        $static_urls = [
			[
                'name' => 'About Us',
                'slug' => 'about-us',
				'created_at' => $default_date_time,
            ],
            [
                'name' => 'Contact Us',
				'slug' => 'contact-us',
				'created_at' => $default_date_time,
            ],
            [
                'name' => 'Privacy Policy',
				'slug' => 'privacy-policy',
				'created_at' => $default_date_time,
            ],
            [
                'name' => 'Term of Service',
				'slug' => 'terms-of-service',
				'created_at' => $default_date_time,
            ],
            [
                'name' => 'Spread The World',
				'slug' => 'spread-the-word',
				'created_at' => $default_date_time,
            ],
            [
                'name' => 'FAQ',
				'slug' => 'faq',
				'created_at' => $default_date_time,
            ],
            [
                'name' => 'Getting Started',
				'slug' => 'getting-started',
				'created_at' => $default_date_time,
            ],
            [
                'name' => 'Community Guidelines',
				'slug' => 'community-guidelines',
				'created_at' => $default_date_time,
            ],
            [
                'name' => 'Tutors And Teachers',
				'slug' => 'tutors',
				'created_at' => $default_date_time,
            ],
            [
                'name' => 'Learners',
				'slug' => 'learners',
				'created_at' => $default_date_time,
            ]
        ];
        return $static_urls;
    }

    function getResouceSlugs($table = null, $where = [])
    {
        if (!$table) {
            return [];
        }
        
        $columns = 'slug, name, created_at';
        $sort = "created_at";

        if($where){
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        }
    
        return $blogSlugs = $this->db->select($columns)
            ->from($table)
            ->order_by($sort, 'ASC')
            ->get()
            ->result_array();
    }

    function mapSlugsBasedOnLang(&$array, $prefix = null)
    {
        $array = array_map(function ($item) use ($prefix) {

            if (!empty($item['slug'])) {

                $item['slug'] = $prefix ? ("/" . $prefix . "/" . $item['slug']) : ("/" . $item['slug']);
            }

            $item['date'] = $item['created_at'] ? \DateTime::createFromFormat('Y-m-d H:i:s', trim($item['created_at']))->format('Y-m-d\TH:i:s') : '';

            return $item;
        }, $array);
        return $array;
    }

    function removeSlashesFromSlug(&$array)
    {
        foreach ($array as &$item) {
            if (!empty($item['slug'])) {
                $item['slug'] = strpos('/', $item['slug']) == 0 ? ltrim($item['slug'], '/') : $item['slug'];
                $item['slug'] = strrpos($item['slug'], '/') == (strlen($item['slug']) - 1) ? $item['slug'] : $item['slug'] . "/";
            }
        }
        return $array;
    }
}