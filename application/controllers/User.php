<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Support\Collection;
class User extends CI_Controller
{
	private $data;
	private $google_client;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['url', 'file']);
		$this->load->library(array('upload'));
		$this->load->model(['user_model', 'category_model', 'classroom_model','classroom_users_model', 'video_model', 'channel_model', 'subscribe_model','history_model', 'notes_model', 'comments_model', 'resource_inappropriate_model', '']);
		$this->data = array();
		$this->google_client = "" ;

		if (isUserLoggedIn()) {
			$this->data['user_data'] = getLoggedInUserDetails();
		}

		$this->upload->initialize(array(
			'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'avatars' . DIRECTORY_SEPARATOR,
			'allowed_types' => config_item('image')['allowed_ext'],
			'max_size' => config_item('image')['allowed_size'] * 1024,
			'encrypt_name' => TRUE
		));

		$all_categories = $this->category_model->get_rows([], true);

		$parent_cat = array_values(array_filter($all_categories, function($cat) {
			return !$cat['parent_id'];
		}));

		$all_categories = array_map(function($value) use ($all_categories) {
			$value['sub_cat'] = array_values(array_filter($all_categories, function($cat) use($value) {
				return $cat['parent_id'] == $value['id'];
			}));
			return $value;
		}, $parent_cat);

		$all_categories = collect($all_categories);

		$category_ids = array_column($this->video_model->getPopularCategoryBasedOnVideosCount(), 'category_id');
		$all_categories = $all_categories->sortBy(function($category) use ($category_ids) {
			return array_search($category['id'], $category_ids);
		})->values();

		// google authentication

		$this->google_client = new Google_Client();
		$this->google_client->setClientId($this->config->item( 'google_client_id' )); //Define your ClientID
		$this->google_client->setClientSecret($this->config->item( 'google_client_secret' )); //Define your Client Secret Key
		$this->google_client->setRedirectUri(base_url() . 'login'); //Define your Redirect Uri
		$this->google_client->addScope('email');
		$this->google_client->addScope('profile');
		$this->data['login_button'] = '<a href="'.$this->google_client->createAuthUrl().'"><i class="fa fa-google" aria-hidden="true"></i> Login</a>';

		// end of google authentication

		
		$this->data['all_categories'] = $all_categories;
		$this->data['page_title'] = 'User Profile | '.config_item('company_name');
	}

	/**
	 * Profile function to show the data of User on profile page
	 * @return void
	 */
	public function profile($id) {
		// redirectFromProtectedRoutes();

		if (!$id) {
			redirectAuthUser();
		}

		$user_id = getLoggedInUserId();
		$user_info = $this->user_model->get_row(['id' => $id]);

		if (!$user_info) {
			redirectAuthUser();
		}

		$show_subscribe_btn = true;

		if (
			!isUserLoggedIn() ||
			($user_id == $user_info->id) || 
			(userRoleIsTeacher() && $user_info->user_role != TEACHER_ROLE)
		) {
			$show_subscribe_btn = false;
		}

		$channel_id = getChannelId($user_info->id);

		$all_videos = collect($this->video_model->get_rows([
			'channel_id' => $channel_id
		]))->map(function($data){
			$data->resource_type = 'videos';
			return $data;
		});

		$other_channels = $this->channel_model->get_rows([
			'id !=' => $channel_id
		]);

		if(isset($user_info->my_classroom_ids) && !empty($user_info->my_classroom_ids)){
			$this->data['teacherClassrooms'] = $this->db->select("*")->where(['deleted_at'=>null])->where_in('id',$user_info->my_classroom_ids)->get('classrooms')->result();
		}

		$all_notes = collect($this->notes_model->get_rows([
			'user_id' => $id
		]))->map(function($data){
			$data->resource_type = 'notes';
			return $data;
		});

		Collection::macro('sortByDate', function ($column = 'created_at', $order = SORT_DESC) {
			/* @var $this Collection */
			return $this->sortBy(function ($datum) use ($column) {
				return strtotime($datum->$column);
			}, SORT_REGULAR, $order == SORT_DESC);
		});
		
		$feeds = collect([]);
		$feeds = $feeds->concat($all_videos);
		$feeds = $feeds->concat($all_notes);
		$feeds = $feeds->sortByDate('created_at', true)->values();

		$this->data['user_info'] = $user_info;
		$this->data['all_videos'] = $all_videos;
		$this->data['all_notes'] = $all_notes;
	//	$this->data['all_videos'] = $all_videos->shuffle()->slice(0, 8);
		$this->data['videos'] = $all_videos->slice(0, 8);
		$this->data['other_channels'] = $other_channels;
		$this->data['show_subscribe_btn'] = $show_subscribe_btn;
		$this->data['show_video_section'] = $user_info->user_role == TEACHER_ROLE;

		$this->data['path'] = 'user/profile';
		$this->data['feeds'] = $feeds;
		$this->load->view('layout/app', $this->data);
	}

	/**
	 * subscribers function to show the data of User on dashboard page
	 * @return void
	 */
	public function subscribers() {
		$this->data['page_title'] = 'Subscribers | '.config_item('company_name');
		$this->data['main_line'] = 'Start getting connected';
		$this->data['main_sub_line'] = 'Sign in to see what everyone’s up to';
		if (!isUserLoggedIn()) {
			$this->data['path'] = 'dashboard/blank';
			$this->load->view('layout/app', $this->data);
			return;
		}
		redirectFromProtectedRoutes();

		$loggedInUser = getLoggedInUserDetails();

		$subscriber_user_ids = $loggedInUser->subscriber_user_ids;
		$subscribers = [];

		if ($subscriber_user_ids) {
			$subscribers = $this->user_model->get_rows(null, null, 'id in ('.implode(',', $loggedInUser->subscriber_user_ids).')');
			if($subscribers){
				$today = date("Y-m-d H:i:s");
				foreach($subscribers as $subscriber){
					$channel = $this->channel_model->get_row(['user_id' => $subscriber->id]);
					$subscriber->total_notes = $this->notes_model->num_rows(["status" => 1,"date<=" => $today, 'deleted_at' => NULL, 'user_id' => $subscriber->id]);
					$subscriber->total_videos = $this->video_model->num_rows(['deleted_at' => NULL, 'channel_id' => $channel->id]);
				}
			}
		}
		
		$this->data['subscribers'] = $subscribers;

		$this->data['path'] = 'dashboard/subscribers';
		$this->load->view('layout/app', $this->data);
	}

	/**
	 * subscriptions function to show the data of User on dashboard page
	 * @return void
	 */
	public function subscriptions() {
		$this->data['page_title'] = 'Subscriptions | '.config_item('company_name');
		$this->data['main_line'] = 'Don’t miss new content';
		$this->data['main_sub_line'] = 'Sign in to see updates from your favorite teachers';

		if (!isUserLoggedIn()) {
			$this->data['path'] = 'dashboard/blank';
			$this->load->view('layout/app', $this->data);
			return;
		}

		redirectFromProtectedRoutes();

		$loggedInUser = getLoggedInUserDetails();

		$subscribed_user_ids = $loggedInUser->subscribed_user_ids;
		$subscriptions = [];

		if ($subscribed_user_ids) {
			$subscriptions = $this->user_model->get_rows(null, null, 'id in ('.implode(',', $loggedInUser->subscribed_user_ids).')');
			if($subscriptions){
				$today = date("Y-m-d H:i:s");
				foreach($subscriptions as $subscription){
					$channel = $this->channel_model->get_row(['user_id' => $subscription->id]);
					$subscription->total_notes = $this->notes_model->num_rows(["status" => 1,"date<=" => $today, 'deleted_at' => NULL, 'user_id' => $subscription->id]);
					$subscription->total_videos = $this->video_model->num_rows(['deleted_at' => NULL, 'channel_id' => $channel->id]);
				}
			}
		}
		
		$this->data['subscriptions'] = $subscriptions;
		
		$this->data['path'] = 'dashboard/subscriptions';
		$this->load->view('layout/app', $this->data);
	}
	/**
	 * subscriptions function to show the data of User on dashboard page
	 * @return void
	 */
	public function history() {
		$this->data['page_title'] = 'History | '.config_item('company_name');
		$this->data['main_line'] = 'Don\'t miss history';
		
		if (isUserLoggedIn()) {
			$user_id = getLoggedInUserId();
			$history_videos = $this->history_model->get_rows(['user_id' => $user_id,'deleted_at' => NULL]);
			$this->data['history_videos'] = $history_videos;
			$this->data['history_videos_count'] = count($history_videos);
			$this->data['path'] = 'history/index';
			$this->load->view('layout/app', $this->data);
			return;
		}
		redirect('/');
	}

	/**
	 * subscriptions function to show the data of User on dashboard page
	 * @return void
	 */
	public function likedVideos() {
		$this->data['page_title'] = 'Liked Videos | '.config_item('company_name');
		$this->data['main_line'] = 'Don\'t miss new videos';
		
		if (!isUserLoggedIn()) {
			$this->data['path'] = 'dashboard/blank';
			$this->load->view('layout/app', $this->data);
			return;
		}
		redirectAuthUser();
	}

	/**
	 * classroom function to show the data of User on dashboard page
	 * @return void
	 */
	public function classroom() {
		//die("fghfg");
		$this->data['page_title'] = 'Classroom | '.config_item('company_name');
		$this->data['main_line'] = 'Join your teacher’s class';
		$this->data['main_sub_line'] = 'Sign in to join your classmates and learn through Tudor Classrooms';
		
		if (!isUserLoggedIn()) {
			$this->data['path'] = 'dashboard/blank';
			$this->load->view('layout/app', $this->data);
			return;
		}
		
		$loggedInUser = getLoggedInUserDetails();
		
		$classroom_user_ids = $loggedInUser->my_requested_classroom_ids;
		
		if(!$classroom_user_ids){ redirect('/home'); }
		$classrooms = [];
		if ($classroom_user_ids) {
			$classrooms = $this->classroom_users_model->get_rows(['user_id' => $loggedInUser->id], null, null );
		}
		$this->data['classroom_user_listing'] = $classrooms;
		$this->data['path'] = 'classroom/user-access-requests';
		$this->load->view('layout/app', $this->data);
		// redirectAuthUser();
	}

	/**
	 * subscribe function to show the data of User on dashboard page
	 * @return void
	 */
	public function subscribe($user_id) {
		redirectFromProtectedRoutes();

		$redirect_url = $this->input->get('redirect_url');

		if (!$user_id) {
			redirectAuthUser();
		}

		// use who subscribe to the other user.
		$loggedInUser = getLoggedInUserDetails();

		// user whom is going to subscribe.
		$subscriberUserDetails = $this->user_model->get_row(['id' => $user_id]);

		if (
			(!$subscriberUserDetails) ||
			($loggedInUser->id == $user_id) ||
			($loggedInUser->user_role == TEACHER_ROLE && $subscriberUserDetails->user_role == USER_ROLE) 
			|| (in_array(getLoggedInUserId(), $subscriberUserDetails->subscriber_user_ids))
		) {
			redirectAuthUser();
		}
		$channel_id = getChannelId($user_id);

		if ($channel_id) {
			$this->subscribe_model->add_row([
				'subscriber_id' => $loggedInUser->id,
				'channel_id' => $channel_id
			]);
		} else {
			$this->session->set_flashdata('error', 'Something went wrong');
		}

		if ($redirect_url) {
			redirect($redirect_url);
		} else {
			redirect('user/'.$user_id.'/profile');
		}
	}

	/**
	 * unsubscribe function to show the data of User on dashboard page
	 * @return void
	 */
	public function unsubscribe($user_id) {
		redirectFromProtectedRoutes();

		$redirect_url = $this->input->get('redirect_url');

		if (!$user_id) {
			redirectAuthUser();
		}

		// use who unsubscribe to the other user.
		$loggedInUser = getLoggedInUserDetails();

		// user whom is going to unsubscribe.
		$unsubscriberUserDetails = $this->user_model->get_row(['id' => $user_id]);

		if (
			(!$unsubscriberUserDetails) ||
			($loggedInUser->id == $user_id) ||
			($loggedInUser->user_role == TEACHER_ROLE && $unsubscriberUserDetails->user_role == USER_ROLE) 
			|| (!in_array(getLoggedInUserId(), $unsubscriberUserDetails->subscriber_user_ids))
		) {
			redirectAuthUser();
		}
		$channel_id = getChannelId($user_id);

		if ($channel_id) {
			$this->subscribe_model->delete_row([
				'subscriber_id' => $loggedInUser->id,
				'channel_id' => $channel_id
			]);
		} else {
			$this->session->set_flashdata('error', 'Something went wrong');
		}

		if ($redirect_url) {
			redirect($redirect_url);
		} else {
			redirect('user/'.$user_id.'/profile');
		}
	}
	/**
	 * Profile function to show the data of User on profile page
	 * @return void
	 */
	public function me() {
		$user_id = getLoggedInUserId();

		$user_info = $this->user_model->get_row(['id' => $user_id]);
		$this->data['main_line'] = 'Create your Tudor profile';
		$this->data['main_sub_line'] = 'Sign in to start sharing learning content with the community';

		if (!isUserLoggedIn()) {
			$this->data['path'] = 'dashboard/blank';
			$this->load->view('layout/app', $this->data);
			return;
		}

		$show_subscribe_btn = true;
		if (!isUserLoggedIn() || ($user_id == $user_info->id) || (userRoleIsTeacher() && $user_info->user_role != TEACHER_ROLE)) {
			$show_subscribe_btn = false;
		}
		
		if($user_info){
			$channel_id = getChannelId($user_info->id);
			$all_videos = collect($this->video_model->get_rows([
				'channel_id' => $channel_id
			]))->map(function($data){
				$data->resource_type = 'videos';
				return $data;
			});

			$other_channels = $this->channel_model->get_rows([
				'id !=' => $channel_id
			]);

			$all_notes = collect($this->notes_model->get_rows([
				'user_id' => $user_id
			]))->map(function($data){
				$data->resource_type = 'notes';
				return $data;
			});
			
			Collection::macro('sortByDate', function ($column = 'created_at', $order = SORT_DESC) {
				/* @var $this Collection */
				return $this->sortBy(function ($datum) use ($column) {
					return strtotime($datum->$column);
				}, SORT_REGULAR, $order == SORT_DESC);
			});
			
			$feeds = collect([]);
			$feeds = $feeds->concat($all_videos);
			$feeds = $feeds->concat($all_notes);
			$feeds = $feeds->sortByDate('created_at', true)->values();

			$this->data['all_videos'] = $all_videos;
			$this->data['all_notes'] = $all_notes;
			$this->data['videos'] = $all_videos->slice(0, 8);
			$this->data['other_channels'] = $other_channels;
			$this->data['feeds'] = $feeds;
			$this->data['show_video_section'] = $user_info->user_role == TEACHER_ROLE;
		}

		if(isset($user_info->my_classroom_ids) && !empty($user_info->my_classroom_ids)){
			$this->data['teacherClassrooms'] = $this->db->select("*")->where(['deleted_at'=>null])->where_in('id',$user_info->my_classroom_ids)->get('classrooms')->result();
		}

		$this->data['user_info'] = $user_info;
		
		$this->data['show_subscribe_btn'] = $show_subscribe_btn;
		$this->data['path'] = 'user/me';
		$this->load->view('layout/app', $this->data);
	}

	/**
	 * delete user account
	 */
	public function delete() {

		redirectFromProtectedRoutes();

		$user_id = getLoggedInUserId();
		if (!$user_id) redirect('/');

		try {

			$user_details = $this->user_model->get_row(
				array(
					'id' => $user_id,
				)
			);

			if (!$user_details) redirect('/');

				// Delete videos and history of user //
			$channel_id = getChannelId();
			$this->video_model->delete_rows(['channel_id' => $channel_id]);
			$this->history_model->delete_rows(['user_id' => $user_id]);
			$this->subscribe_model->delete_rows(['channel_id' => $channel_id]);
			$this->subscribe_model->delete_rows(['subscriber_id' => $user_id]);
			$this->comments_model->delete_rows(['user_id' => $user_id,'type' => 'videos']);

				// Delete notes and comments of user //
			$this->notes_model->delete_rows(['user_id' => $user_id]);
			$this->comments_model->delete_rows(['user_id' => $user_id,'type' => 'notes']);
			$this->resource_inappropriate_model->delete_rows(['user_id' => $user_id,'resource_type' => 'note']);

				// Delete channel of user //
			$this->channel_model->delete_channels(['user_id' => $user_id]);

			$this->user_model->update_row(['id' => $user_id], ['remember_token' => null]);
			if (get_cookie('remember_token')) {
				delete_cookie('remember_token');
			}

			$this->user_model->delete_row(['id' => $user_id]);

			$this->session->unset_userdata('ci_seesion_key');
			$this->session->unset_userdata('ci_session_key_generate');
			$this->session->unset_userdata('user_id');

			$this->session->sess_destroy();
			$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
			$this->output->set_header("Pragma: no-cache");


			redirect('/', 'refresh');

		} catch (Exception $e) {
			$this->session->set_flashdata('error', $e->getMessage());
			redirect('/home', 'refresh');
		}
	}
}
