<?php
defined('BASEPATH') or exit('No direct script access allowed');

use GuzzleHttp\Client;
require_once(APPPATH.'traits/additional_validation_methods.php');

class Videos extends CI_Controller
{
	use AdditionalValidationMathods;

	private $data;
	public $perPage = 10;
	private $google_client;
	public $perPageNewLimit = 5;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['url', 'file']);
		$this->load->library(array('upload'));
		$this->load->model(['user_model', 'category_model', 'video_model','classroom_model','history_model','notes_model','comments_model','channel_model']);
		$this->data = array();
		$this->google_client = "" ;

		if (isUserLoggedIn()) {
			$this->data['user_data'] = getLoggedInUserDetails();
		}
		$this->upload->initialize(array(
			'upload_path' => FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR,
			'allowed_types' => config_item('image')['allowed_ext'],
			'max_size' => config_item('image')['allowed_size'] * 1024,
			'encrypt_name' => TRUE
		));

		$all_categories = $this->category_model->get_rows([], true);

		$parent_cat = array_values(array_filter($all_categories, function($cat) {
			return !$cat['parent_id'];
		}));

		$all_categories = array_map(function($value) use ($all_categories) {
			$value['sub_cat'] = array_values(array_filter($all_categories, function($cat) use($value) {
				return $cat['parent_id'] == $value['id'];
			}));
			return $value;
		}, $parent_cat);

		$all_categories = collect($all_categories);

		$category_ids = array_column($this->video_model->getPopularCategoryBasedOnVideosCount(), 'category_id');
		$all_categories = $all_categories->sortBy(function($category) use ($category_ids) {
			return array_search($category['id'], $category_ids);
		})->values();

		// google authentication

		$this->google_client = new Google_Client();
		$this->google_client->setClientId($this->config->item( 'google_client_id' )); //Define your ClientID
		$this->google_client->setClientSecret($this->config->item( 'google_client_secret' )); //Define your Client Secret Key
		$this->google_client->setRedirectUri(base_url() . 'login'); //Define your Redirect Uri
		$this->google_client->addScope('email');
		$this->google_client->addScope('profile');
		$this->data['login_button'] = '<a href="'.$this->google_client->createAuthUrl().'"><i class="fa fa-google" aria-hidden="true"></i> Login</a>';

		// end of google authentication

		
		$this->data['all_categories'] = $all_categories;
		$this->data['page_title'] = 'Videos Feed | '.config_item('company_name');
	}

	/**
	 * index function to show the listing of the User's videos
	 * @return void
	 */
	public function index($category=null, $sub_category=null) {

		$getParams = $this->input->get();

		if(!empty($category)) {
			$category = $this->category_model->get_row(['slug'=> $category]);
			if($category) {
				$getParams['category_id'] = $category->id; 
			}
			else {
				redirect('/');
			}
		}

		if(!empty($sub_category)) { 
			$sub_category = $this->category_model->get_row(['slug'=> $sub_category]);
			if($sub_category) {
				$getParams['sub_category_id'] = $sub_category->id; 
			}
			else {
				redirect('/');
			}
		}

		$searched_videos = [];
		$all_notes = [];
		$all_videos = [];
		$history_videos = [];
		$this->data['history_videos'] = $this->data['history_videos_count'] = $this->data['filter_user_type'] = [];
		$this->data['show_filter_class'] = "display_none";

		$today = date("Y-m-d H:i:s");

		if ($getParams ) {
			$searched_videos = $this->video_model->search(['deleted_at' => NULL], $getParams);
			$search_videos_ids = array_column($searched_videos->toArray(), 'id');

			$all_notes = $this->notes_model->search(["status"=>1, "date<="=>$today], $getParams)->toArray();

			$this->data['recommended_notes'] = $this->notes_model->search(["status" => 1,"date<=" => $today, 'deleted_at' => NULL], $getParams)->toArray();

			if (isUserLoggedIn()) {
				$user_id = getLoggedInUserId();
				$history_videos = $search_videos_ids ? $this->history_model->get_rows(['user_id' => $user_id, 'deleted_at' => NULL], NULL, ['video_id' => $search_videos_ids]) : [];
			}
			
			if(isset($getParams['term'])){
				$searched_users = $this->channel_model->getContentFilter($getParams);
				$this->data['filter_user_type'] = $searched_users;
				if($searched_users){
					$this->data['show_filter_class'] = "";
				}
			}

		} else {
			$all_videos = $this->video_model->get_rows();
			$all_notes = $this->notes_model->get_rows(["status" => 1,"date<=" => $today, 'deleted_at' => NULL]);
			$this->data['recommended_notes'] = collect($all_notes)->shuffle()->slice(0,8)->toArray();

			if (isUserLoggedIn()) {
				$user_id = getLoggedInUserId();
				$history_videos = $this->history_model->get_rows(['user_id' => $user_id, 'deleted_at' => NULL]);
			}

		}

		$this->data['history_videos'] = $history_videos;
		$this->data['history_videos_count'] = count($history_videos);

		if (count($history_videos) > 0) {
			$getHighestCountVideos = $this->db->select("count(video_history.video_id) as count, video_history.video_id, videos.category_id")->where('video_history.user_id', $user_id)->join('videos', 'videos.id = video_history.video_id')->group_by('video_history.video_id')->from('video_history')->order_by('count', 'desc')->get()->result();
			if($getHighestCountVideos){
				//$ids = array_column($getHighestCountVideos, 'video_id');
				//$category_ids = array_column($getHighestCountVideos, 'category_id');
				//$all_videos = $this->video_model->get_rows([],false,[],null,false,["id" => $ids]);
				$videos = $this->video_model->get_rows([],false,[],null,false);
				/*$videos = collect($all_videos)->filter(function($value) {
					return !$value->is_featured && $value->visibility == 0;
				})->values();	*/
			}
		} else {
			$videos = collect($all_videos)->filter(function($value) {
				return !$value->is_featured && $value->visibility == 0;
			})->values();
		}
		$featured_videos = collect($all_videos)->filter(function($value) {
			return $value->is_featured;
		})->values();

		if ($this->input->is_ajax_request()) {
			$start = $getParams['page_id'] * $this->perPageNewLimit;
			$this->data['notes'] = collect($all_notes)->slice($start,$this->perPageNewLimit)->toArray();
			header("Content-Type: text/html");
			$this->load->view('ajax/notes', $this->data);	
		}else{

			$this->data['total_pages'] = round(count($all_notes)/$this->perPageNewLimit);

		// $this->data['notes'] = $all_notes;
			$this->data['notes'] = collect($all_notes)->slice(0,$this->perPageNewLimit)->toArray();
			$this->data['videos'] = $videos;
			$this->data['featured_videos'] = $featured_videos;
			$this->data['searched_videos'] = $searched_videos;
			$this->data['path'] = 'videos/index';
			$this->load->view('layout/app', $this->data);
		}
	}

	/**
	 * index function to show the listing of the User's videos
	 * @return void
	 */
	public function myVideos() {
		$this->data['page_title'] = 'User videos | '.config_item('company_name');
		redirectFromProtectedRoutes();
		redirectIfUserRoleIsNormal();
		$channel_id = getChannelId();
		$videos = $this->video_model->get_rows([
			'channel_id' => $channel_id
		]);
		$this->data['videos'] = $videos;
		$this->data['path'] = 'videos/my-videos';
		$this->load->view('layout/app', $this->data);
	}

	public function classroomVideos($classroomId = false){
		if (isUserLoggedIn()) {
			$videos = "";
			if($classroomId){
				$videos = $this->video_model->get_rows(['classroom_id'=> $classroomId]);
			}else{
				$userData = getLoggedInUserDetails();
				if($userData->classroom_ids){
					$videos = $this->video_model->get_rows([],null, [],'classroom_id in ('.implode(',', $userData->classroom_ids).')');
				}
			}
			if($videos){
				$this->data['videos'] = $videos;
				$this->data['path'] = 'videos/classroom-videos';
				$this->load->view('layout/app', $this->data);
			}else{
				redirect('/home');	
			}
		}else{
			redirect('/home');
		}
	}

	/**
	 * Profile function to show the data of User on profile page
	 * @return void
	 */
	public function add() {
		
		$this->data['page_title'] = 'Add Video | '.config_item('company_name');

		redirectFromProtectedRoutes();
		redirectIfUserRoleIsNormal();

		$user_id = getLoggedInUserId();
		$this->data['categories'] = $this->category_model->get_rows(['parent_id' => null]);

		$this->data['sub_categories'] = [];
		if ($category_id = set_value('category_id')) {
			$this->data['sub_categories'] = $this->category_model->get_rows(['parent_id' => $category_id]);
		}
		
		try {
			$postParams = $this->input->post();
			if ($postParams) {
				$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[3]|max_length[250]');
				$this->form_validation->set_rules('video_id', 'Video ID', 'trim|required|min_length[1]');
				//$this->form_validation->set_rules('visibility', 'Visibility', 'trim|required|min_length[1]');
				$this->form_validation->set_rules('description', 'Description', 'trim|min_length[8]');
				/*if (isset($postParams['visibility']) && $postParams['visibility'] == 1) {
					$this->form_validation->set_rules('classroom_id', 'Classroom', 'trim|required|max_length[250]');
				}*/
				/*if (empty($_FILES['thumbnail']['name'])) {
					$this->form_validation->set_rules('thumbnail', 'Thumbnail', 'required');
				}*/

				// $this->form_validation->set_rules('thumbnail', 'Thumbnail', 'callback_file_check[thumbnail-' . implode('-', config_item('image')) . ']');

				if ($this->form_validation->run() == false) {
					$this->data['path'] = 'videos/add';
					$this->load->view('layout/app', $this->data);
				} else {

					$video_details = $this->getExternalVideoDetails($postParams['video_id']);
					if ($video_details && property_exists($video_details, 'pageInfo') && $video_details->pageInfo->totalResults == 0) {
						$this->session->set_flashdata('error', "You have inserted invalid video id.");
						redirect('/videos/add', 'refresh');
					}

					$video_details = reset($video_details->items);

					$extra_data = [
						'view_count' => $video_details->statistics->viewCount ? $video_details->statistics->viewCount : null,
						'like_count' => $video_details->statistics->likeCount ? $video_details->statistics->likeCount : null,
						'dislike_count' => $video_details->statistics->dislikeCount ? $video_details->statistics->dislikeCount : null,
						'favorite_count' => $video_details->statistics->favoriteCount ? $video_details->statistics->favoriteCount : null,
						'comment_count' => $video_details->statistics->commentCount ? $video_details->statistics->commentCount : null
					];

					$title_with_limit = substr($postParams['title'], 0, 80);
					$slug =  url_title($title_with_limit, 'dash', true).'-'.rand(100000, 999999);

					$data = array(
						'title' => set_value('title'),
						'slug' => $slug,
						'external_video_id' => set_value('video_id'),
						//'visibility' => set_value('visibility'),
						'description' => set_value('description') ? set_value('description') : NULL,
						'category_id' => set_value('category_id') ? set_value('category_id') : NULL,
						'classroom_id' => set_value('classroom_id') ? set_value('classroom_id') : NULL,
						'sub_category_id' => set_value('sub_category_id') ? set_value('sub_category_id') : NULL
					);
					// duration
					if($video_details->contentDetails->duration){
						$interval = new DateInterval($video_details->contentDetails->duration);
						$data['duration'] = $interval->days * 86400 + $interval->h * 3600 + $interval->i * 60 + $interval->s;
					}

					if (property_exists($video_details->snippet->thumbnails, 'standard')) {
						$thumbnail_url = $video_details->snippet->thumbnails->standard->url;
						$extension = explode(".", $thumbnail_url);
						$extension = last($extension);
						$filename =  generate_random_string(32).'.'.$extension;
						file_put_contents(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR.$filename, file_get_contents($thumbnail_url));
						$data['thumbnail'] = $filename;
					}

					$data += $extra_data;

					/* $data['image'] = null;
					if (isset($_FILES['thumbnail']) && $_FILES['thumbnail']['error'] !== UPLOAD_ERR_NO_FILE) {

						if ($this->upload->do_upload('thumbnail')) {
							chmod($this->upload->data('full_path'), 0777);
							$data['thumbnail'] = $this->upload->data('file_name');
						} else {
							$data['thumbnail'] = null;
	                        // $upload_errors[] = "Image: " . $this->upload->display_errors('', '');
						}
					} */

					$data['thumbnail'] = null;	
					if (isset($_FILES['thumbnail']) && $_FILES['thumbnail']['error'] !== UPLOAD_ERR_NO_FILE) {

						if ($this->upload->do_upload('thumbnail')) {
							chmod($this->upload->data('full_path'), 0777);
							$data['thumbnail'] = $this->upload->data('file_name');
						} else {
							$data['thumbnail'] = null;
						}
					}
					if ($postParams['delete_image'] == 1) {
						if ($video_details->thumbnail) {
							$path = FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR;
							if(file_exists($path.$video_details->thumbnail)){
								unlink($path.$video_details->thumbnail);
							}
						}
						if (!$data['thumbnail']) {
							$data['thumbnail'] = null;					
						}
					} else if (!$data['thumbnail'] && $postParams['delete_image']==0) {
						unset($data['thumbnail']);
					}

					$data['channel_id'] = createOrGetChannelId(null, true);

					$this->video_model->add_row($data);
					$this->session->set_flashdata('success', 'Video added Successfully.');
					redirect('/me/videos', 'refresh');
				}
			} else {
				$this->data['path'] = 'videos/add';
				$this->load->view('layout/app', $this->data);
			}
		} catch (Exception $e) {
			$this->session->set_flashdata('error', $e->getMessage());
			redirect('/videos/add', 'refresh');
		}
	}

	/**
	 * edit function to show the data of User on profile page
	 * @return void
	 */
	public function edit($slug = NULL) {
		$this->data['page_title'] = 'Edit Video | '.config_item('company_name');
		redirectFromProtectedRoutes();
		redirectIfUserRoleIsNormal();
		$user_id = getLoggedInUserId();

		$video_id = getResourceIdFromSlug($slug,'video_model');

		if (!$video_id) redirect('me/videos');

		$channel_id = createOrGetChannelId();

		$video_details = $this->video_model->get_row(
			array(
				'id' => $video_id,
				'channel_id' => $channel_id
			)
		);
		$classrooms = $this->classroom_model->get_rows(['user_id' => $user_id]);

		if (!$video_details) redirect('me/videos');

		$category_id = $video_details->category_id ?: set_value('sector_id', $video_details->category_id);

		$this->data['classrooms'] = $classrooms;
		$this->data['video_details'] = $video_details;
		$this->data['categories'] = $this->category_model->get_rows(['parent_id' => null]);

		$this->data['sub_categories'] = [];
		if ($category_id) {
			$this->data['sub_categories'] = $this->category_model->get_rows(['parent_id' => $category_id]);
		}
		
		$postParams = $this->input->post();
		if ($postParams) {
			$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[3]|max_length[250]');
			// $this->form_validation->set_rules('video_id', 'Video ID', 'trim|required|min_length[1]');
			//$this->form_validation->set_rules('visibility', 'Visibility', 'trim|required|min_length[1]');
			$this->form_validation->set_rules('description', 'Description', 'trim|min_length[8]');
			/*if (isset($postParams['visibility']) && $postParams['visibility'] == 1) {
				$this->form_validation->set_rules('classroom_id', 'Classroom', 'trim|required|max_length[250]');
			}*/
			if ($this->form_validation->run() == false) {
				$this->data['path'] = 'videos/edit';
				$this->load->view('layout/app', $this->data);
			} else {

				$title_with_limit = substr($postParams['title'], 0, 80);
				$video_slug = explode('-',$video_details->slug);
				$random_id = end($video_slug);
				$slug =  url_title($title_with_limit, 'dash', true).'-'.$random_id;

				$data = array(
					'title' => set_value('title'),
					'slug' => $slug,
                	// 'external_video_id' => set_value('video_id'),
					//'visibility' => set_value('visibility'),
					'description' => set_value('description') ? set_value('description') : NULL,
					'category_id' => set_value('category_id') ? set_value('category_id') : NULL,
					'classroom_id' => set_value('classroom_id') ? set_value('classroom_id') : NULL,
					'sub_category_id' => set_value('sub_category_id') ? set_value('sub_category_id') : NULL
				);

				$external_video_details = $this->getExternalVideoDetails($postParams['video_id']);
				if ($external_video_details && property_exists($external_video_details, 'pageInfo') && $external_video_details->pageInfo->totalResults == 0) {
					$this->session->set_flashdata('error', "You have inserted invalid video id.");
					redirect('/videos/add', 'refresh');
				}

				$external_video_details = reset($external_video_details->items);

				$extra_data = [
					'view_count' => $external_video_details->statistics->viewCount ? $external_video_details->statistics->viewCount : null,
					'like_count' => $external_video_details->statistics->likeCount ? $external_video_details->statistics->likeCount : null,
					'dislike_count' => $external_video_details->statistics->dislikeCount ? $external_video_details->statistics->dislikeCount : null,
					'favorite_count' => $external_video_details->statistics->favoriteCount ? $external_video_details->statistics->favoriteCount : null,
					'comment_count' => $external_video_details->statistics->commentCount ? $external_video_details->statistics->commentCount : null
				];

				$data += $extra_data;
				

				$data['thumbnail'] = null;	
				if (isset($_FILES['thumbnail']) && $_FILES['thumbnail']['error'] !== UPLOAD_ERR_NO_FILE) {

					if ($this->upload->do_upload('thumbnail')) {
						chmod($this->upload->data('full_path'), 0777);
						$data['thumbnail'] = $this->upload->data('file_name');
					} else {
						$data['thumbnail'] = null;
					}
				}

				if ($postParams['delete_image'] == 1) {
					if ($video_details->thumbnail) {
						$path = FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR;
						if(file_exists($path.$video_details->thumbnail)){
							unlink($path.$video_details->thumbnail);
						}
					}
					if (!$data['thumbnail']) {
						$data['thumbnail'] = null;					
					}
				} else if (!$data['thumbnail'] && $postParams['delete_image']==0) {
					unset($data['thumbnail']);
				}

				$this->video_model->update_row(['id' => $video_id], $data);
				$this->session->set_flashdata('success', 'Video updated Successfully.');
				redirect('/me/videos', 'refresh');
			}
		} else {
			$this->data['path'] = 'videos/edit';
			$this->load->view('layout/app', $this->data);
		}
	}

    /**
	 * edit function to show the data of User on profile page
	 * @return void
	 */
    public function details($slug) {
    	$this->data['page_title'] = 'Video | '.config_item('company_name');
		//redirectFromProtectedRoutes();

    	$video_id = getResourceIdFromSlug($slug,'video_model'); 

    	if (!$video_id) redirect('me/videos');

    	$video_details = $this->video_model->get_row(
    		array(
    			'id' => $video_id
    		)
    	);
    	if (!$video_details) redirect('/home');
    	$show_subscribe_btn = true;
    	$privateStatus = false;
    	if (
    		!isUserLoggedIn() ||
    		(getLoggedInUserId() == $video_details->user_and_channel->user_id) || 
    		(userRoleIsTeacher() && $video_details->user_and_channel->user_role != TEACHER_ROLE)
    	) {
    		$show_subscribe_btn = false;
    	}

    	if (isUserLoggedIn()) {
    		$user_id = getLoggedInUserId();

    		$user_channel_id = getChannelId();

			// todo: get the subscribed channels ids.
			$user_subscribed_channels = []; //$this->getSubscribedChannels();

			array_push($user_subscribed_channels, $user_channel_id);

			if ($video_details->visibility == VIDEO_PRIVATE_VISIBILITY && !(in_array($video_details->channel_id, $user_subscribed_channels))) {
				$privateStatus = TRUE;
			}
		} else {
			if ($video_details->visibility == VIDEO_PRIVATE_VISIBILITY) {
				$privateStatus = TRUE;
			}
		}


		if(($privateStatus) && ($this->CheckUserVideoAccess($video_details))){
			if($video_details->classroom_id){
				redirect('classroom/'.$video_details->classroom_id); 
			}else{
				// $this->video_model->update_row(['id' => $video_details->id],['visibility'=> '0']);
				redirect('videos');
			}
		}

		if (isUserLoggedIn()) {
			// for saving the hostory for video
			$user_id = getLoggedInUserId();
			$current_date = date('Y-m-d');
			$check_current_history = $this->history_model->get_row(['video_id' => $video_id,'user_id'=> $user_id,'DATE(created_at)'=>$current_date]);
			if($check_current_history){
				$this->history_model->update_row(
					array('video_id' => $video_id,'user_id'=> $user_id)
				);
			}else{
				$this->history_model->add_row(
					array('video_id' => $video_id,'user_id'=> $user_id)
				);
			}
		}
    	// if (!$video_details) redirect('videos');
		$category_id = $video_details->category_id ?: set_value('sector_id', $video_details->category_id);

		$this->data['video_details'] = $video_details;
		$this->data['categories'] = $this->category_model->get_rows(['parent_id' => null]);

		$this->data['sub_categories'] = [];
		if ($category_id) {
			$this->data['sub_categories'] = $this->category_model->get_rows(['parent_id' => $category_id]);
		}

		$related_videos = $this->video_model->get_rows(
			array(
				'id !=' => $video_id
			), false, [
				'channel_id' => $video_details->channel_id,
				'category_id' => $video_details->category_id,
				'sub_category_id' => $video_details->sub_category_id
			]
		);

		$this->data['related_videos'] = $related_videos;
		$this->data['show_subscribe_btn'] = $show_subscribe_btn;
		$totalComments = $this->comments_model->getCommentsCount($video_id,'videos');
		$this->data['total_pages']  = ceil($totalComments/$this->perPage);
		$this->data['comments_count'] = $totalComments;

		$this->data['path'] = 'videos/details';
		$this->load->view('layout/app', $this->data);

	}

	public function CheckUserVideoAccess($video_details){
		redirectFromProtectedRoutes();
		if($video_details){
			$userData = getLoggedInUserDetails();
			$check_role_classes = $userData->all_classroom_ids;
			if(in_array($video_details->classroom_id, $check_role_classes)){
				return false;
			}else{
				return true;
			}
		}else{
			return false;
		}
	}
	/**
	 * delete function to show the data of User on profile page
	 * @return void
	 */
	public function delete($video_id) {
		redirectFromProtectedRoutes();
		redirectIfUserRoleIsNormal();
		$user_id = getLoggedInUserId();

		if (!$video_id) redirect('me/videos');

		$channel_id = createOrGetChannelId();

		$video_details = $this->video_model->get_row(
			array(
				'id' => $video_id,
				'channel_id' => $channel_id
			)
		);

		if (!$video_details) redirect('me/videos');

		$category_id = $video_details->category_id ?: set_value('sector_id', $video_details->category_id);

		$this->data['video_details'] = $video_details;

		$this->video_model->delete_row(['id' => $video_id]);
		$this->history_model->delete_row(['video_id' => $video_id]);
		$this->session->set_flashdata('success', 'Video deleted Successfully.');
		redirect('/me/videos', 'refresh');
	}

	private function getExternalVideoDetails($video_id) {
		$response = null;
		if ($video_id) {
			try {
				$client = new Client();
				$query_params = [
					'key' => config_item('youtube_api_key'),
					'id' => $video_id,
					'kind' => "youtube#video",
					'part' => "contentDetails,snippet,status,statistics",
					'maxResults' => 1
				];
				$url = "https://www.googleapis.com/youtube/v3/videos?".http_build_query($query_params);
				$response = $client->request('GET', $url);
				$response = json_decode($response->getBody());
			} catch (Exception $e) {
				throw new Exception($e->getResponse()->getStatusCode()."-".$e->getResponse()->getReasonPhrase(), 1);
			}
		}
		return $response;
	}


	public function classroom($classroom = false) {
		if($classroom){
			$user_id = getLoggedInUserId();
			$response = [
				'status' => false,
				'classroom' => []
			];
			try {
				if ($classroom) {
					$classroom = $this->classroom_model->get_rows(['user_id' => $user_id,'deleted_at'=> NULL]);
					$response = [
						'status' => true,
						'classroom' => $classroom
					];
				}            
			} catch (Exception $e) {
				$response = [
					'status' => false,
					'classroom' => [],
					'message' => $e->getMessage()
				];
			}
		}else{
			$response = [
				'status' => false,
				'classroom' => [],
				'message' => "No classroom found"
			];
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}
}
