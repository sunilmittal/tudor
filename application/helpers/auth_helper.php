<?php

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('isUserLoggedIn')) {
	function isUserLoggedIn() {
		$CI = &get_instance();
		$CI->load->model('user_model');
		if($CI->user_model->get_row(['id' => $CI->session->userdata('user_id'), 'deleted_at' => null])){
			return $CI->session->userdata('isUserLoggedIn');
		}
	}
}

if (!function_exists('getLoggedInUserId')) {
	function getLoggedInUserId() {
		if (isUserLoggedIn()) {
			$CI = &get_instance();
			return $CI->session->userdata('user_id');
		} else {
			return null;
		}
	}
}

if (!function_exists('getLoggedInUserDetails')) {
	function getLoggedInUserDetails() {
		if (isUserLoggedIn()) {
			$CI = &get_instance();
			$CI->load->model('user_model');
			return $CI->user_model->get_row(['id' => getLoggedInUserId()]);
		} else {
			return null;
		}
	}
}

if (!function_exists('userRoleIsTeacher')) {
	function userRoleIsTeacher() {
		return isUserLoggedIn() && getLoggedInUserDetails()->user_role == TEACHER_ROLE;
	}
}

if (!function_exists('redirectFromProtectedRoutes')) {
	function redirectFromProtectedRoutes() {
		if (!isUserLoggedIn()) {
			$CI = &get_instance();
			$CI->session->set_flashdata('error', 'Please login or register.');
			redirect("/login");
		}		
	}
}

if (!function_exists('redirectIfUserRoleIsTeacher')) {
	function redirectIfUserRoleIsTeacher() {
		if (isUserLoggedIn() && userRoleIsTeacher()) {
			$CI = &get_instance();
			$CI->session->set_flashdata('error', 'You don\'t have access to this page');
			redirect("/dashboard");
		}		
	}
}

if (!function_exists('redirectIfUserRoleIsNormal')) {
	function redirectIfUserRoleIsNormal() {
		if (isUserLoggedIn() && !userRoleIsTeacher()) {
			$CI = &get_instance();
			$CI->session->set_flashdata('error', 'You don\'t have access to this page');
			redirect("/home");
		}
	}
}


if (!function_exists('redirectAuthUser')) {
	function redirectAuthUser() {
		if (isUserLoggedIn()) {
			if (userRoleIsTeacher()) {
				redirect("/dashboard");
			} else {
				redirect("/home");
			}		
		}
	}
}

if (!function_exists('redirectAndShowFirstError')) {
	function redirectAndShowFirstError($redirect_url) {
		if (!isUserLoggedIn()) {
			$CI = &get_instance();
			$errors = $CI->form_validation->error_array();
            $firstError = reset($errors);
            $CI->session->set_flashdata('error', $firstError);
			redirect($redirect_url);
		}		
	}
}

if (!function_exists('createOrGetChannelId')) {
	function createOrGetChannelId($user_id = null, $createNew = false) {
		$CI = &get_instance();
		$channel_id = 0;
		$CI->load->model('channel_model');
		$user_id = $user_id ?: getLoggedInUserId();
		$channel = $CI->channel_model->get_row(['user_id' => $user_id]);

		if (!$channel && $createNew) {
			$user_data = getLoggedInUserDetails();
			$channel_id = $CI->channel_model->add_row(['user_id' => $user_id, 'name' => $user_data->name], true);
		} else if($channel) {
			$channel_id = $channel->id;
		}
		return $channel_id;
	}
}

if (!function_exists('getChannelId')) {
	function getChannelId($user_id = null) {
		$CI = &get_instance();
		$channel_id = 0;
		$user_id = $user_id ?: getLoggedInUserId();

		$CI->load->model('channel_model');
		$channel = $CI->channel_model->get_row(['user_id' => $user_id]);
		
		if ($channel) {
			$channel_id = $channel->id;
		}

		return $channel_id;
	}
}
