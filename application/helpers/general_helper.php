<?php

defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

if (!function_exists('specialCharToHyphen')) {
    /**
     *
     * @param    string $string
     * @return    string
     */
    function specialCharToHyphen()
    {
        $args = func_get_args();
        $string = join(" ", $args);
        $string = str_replace(' ', '-', $string);
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        return strtolower(preg_replace('/-+/', '-', $string));
    }
}
if (!function_exists('convert_seconds')) {
    function convert_seconds($seconds) {
        if($seconds){
            $init = $seconds;
            $hours = floor($init / 3600);
            $minutes = floor(($init / 60) % 60);
            $seconds = $init % 60;
            $completeTime = '';
            if($hours){ $completeTime = $hours.":"; }
            if($minutes){ $completeTime .= $minutes.":"; }
            if($seconds){ $completeTime .= $seconds; }
            return $completeTime;
        }else{
            return false;
        }
    }
}
if (!function_exists('json_response')) {
    /**
     *
     * @param    array $array
     * @param    string $keyToFlat
     * @param    string $keyToAdd
     * @param    string $keyValueGetFrom
     * @return    string
     */
    function json_response($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        die;
    }
}

if (!function_exists('escape_html')) {
    /**
     *
     * @param    array $array
     * @param    string $keyToFlat
     * @param    string $keyToAdd
     * @param    string $keyValueGetFrom
     * @return    string
     */
    function escape_html($string)
    {
        return htmlspecialchars_decode(stripslashes(paraToBr($string)));
    }
}


if (!function_exists('paraToBr')) {
    /**
     *
     * @param    array $array
     * @param    string $keyToFlat
     * @param    string $keyToAdd
     * @param    string $keyValueGetFrom
     * @return    string
     */
    function paraToBr($content)
    {
        $pattern = "/<p[^>]*><\\/p[^>]*>/";
        $content = preg_replace("/(\/[^>]*>)([^<]*)(<)/", "\\1\\3", preg_replace($pattern, '', $content));

        $newcontent = preg_replace("/<p[^>]*?>/", "", $content);
        $newcontent = str_replace("</p>", "<br />", $newcontent);
        return $newcontent;
    }
}

if (!function_exists('kConverter')) {
    function kConverter($number)
    {
        if ($number >= 1000) {
            return number_format(($number / 1000)) . 'k';
        } else {
            return $number;
        }
    }
}

if (!function_exists('thousandsFormat')) {
    function thousandsFormat($num) {
        if($num>1000) {
          $x = round($num);
          $x_number_format = number_format($x);
          $x_array = explode(',', $x_number_format);
          $x_parts = array('K', 'M', 'B', 'T');
          $x_count_parts = count($x_array) - 1;
          $x_display = $x;
          $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
          $x_display .= $x_parts[$x_count_parts - 1];
          return $x_display;
      }
      return $num;
  }
}

if (!function_exists('getStaticMeta')) {
    function getStaticMeta($pageContent, $data)
    {
        return array_merge($pageContent, $data);
    }
}

if (!function_exists('sendEmail')) {
    function sendEmail($to, $subject, $body, $to_name = null, $attachement = null)
    {
        // return true;
        $sent = false;

        $mail = new PHPMailer();
        $mailConfig = config_item('email');
        try {
            if ($_SERVER['HTTP_HOST'] == 'localhost') {
                $mail->SMTPDebug = SMTP::DEBUG_OFF;
                // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
                $mail->isSMTP();
                $mail->Password   = $mailConfig['password'];
            } else {
                $mail->SMTPDebug = SMTP::DEBUG_OFF;
                // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            }
            $mail->Host       = $mailConfig['host'];
            $mail->SMTPAuth   = true;
            $mail->SMTPSecure = 'tls';
            $mail->Username   = $mailConfig['username'];
            // $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port       = $mailConfig['port'];

            //Recipients
            $mail->setFrom($mailConfig['from_email'], $mailConfig['from_name']);
            if (is_array($to)) {
                foreach ($to as $address) {
                    $mail->addAddress($address);
                }
            } else {
                $mail->addAddress($to, $to_name);
            }

            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body    = $body;
            $mail->AltBody = $body;

            if ($attachement) {
                $mail->addAttachment($attachement);
            }

            $sent = $mail->send();
        } catch (Exception $e) {
            $sent = false;
        }
        return $sent;
    }
}
if (!function_exists('getJobStaticInfo')) {
    function getJobStaticInfo($languageid)
    {
        $data[2] = [
            'languageid' => $languageid,
            'home_header_title' => 'Latest Job Opportunities ',
            'home_header_alljobs_link' => 'VIEW ALL JOBS',

            'all_job_header_title' => 'Search Results',
            'all_job_detail_link' => 'VIEW DETAILS',
            'all_job_apply_btn_txt' => 'APPLY',

            'latest_job_title' => 'Latest Vacancies',
            'latest_all_job_link' => 'VIEW ALL JOBS',

            'apply_header_title' => 'Apply for this position',
            'apply_sub_title' => 'Send your details & CV if you are interested in our live jobs.',
            'apply_submit_btn_text' => 'Send',

            'search_all_specialism' => 'All Specialisms',
            'search_all_sector' => 'All Sectors',
            'search_location' => 'Location or postcode',
            'search_title' => 'Search for a job',
            'search_please_select' => 'Please Select',
            'search_please_choose' => 'Please Select',

            'search_btn' => 'Search Jobs',

            'search_location_label' => 'Location',
            'search_radius_label' => 'Radius',
            'search_jobtype_label' => 'Job Type',
            'search_salary_label' => 'Annual Salary',
            'search_daily_salary_label' => 'Daily Salary',
            'search_specialism_label' => 'Specialism',
            'search_sector_label' => 'Sector',
            'search_title_label' => 'Keyword Search',

            'search_contract_label' => 'Contract',
            'search_permanent_label' => 'Permanent'

        ];
        $data[1] = [
            'languageid' => $languageid,
            'home_header_title' => 'Dein nächster Job',
            'home_header_alljobs_link' => 'Alle Jobs anschauen',

            'all_job_header_title' => 'Suchergebnisse',
            'all_job_detail_link' => 'Details anzeigen',
            'all_job_apply_btn_txt' => 'Bewerben',

            'latest_job_title' => 'Finde Stellen',
            'latest_all_job_link' => 'Alle Stellen Sehen',

            'apply_header_title' => 'Bewerben Sie sich auf diese Stelle',
            'apply_sub_title' => 'Senden Sie uns Ihre Kontaktdaten & CV, wenn Sie an unseren aktuellen Stellen interessiert sind.',
            'apply_submit_btn_text' => 'Send',

            'search_all_specialism' => 'Bitte wählen',
            'search_all_sector' => 'All Sectors',
            'search_location' => 'Ort oder PLZ',
            'search_title' => 'Suche nach einem Job',
            'search_please_select' => 'Bitte auswählen',
            'search_please_choose' => 'Bitte wählen ',

            'search_btn' => 'Jobs suchen',

            'search_location_label' => 'Lageradi',
            'search_radius_label' => 'Radius',
            'search_jobtype_label' => 'Auftragsty',
            'search_salary_label' => 'Jahresgehalt',
            'search_daily_salary_label' => 'Tägliches Gehalt',
            'search_specialism_label' => 'Spezialisierungen ',
            'search_sector_label' => 'Sektor',
            'search_title_label' => 'Schlagwortsuche',

            'search_contract_label' => 'Vertrag',
            'search_permanent_label' => 'Permanent'

        ];
        $data[3] = [
            'languageid' => $languageid,
            'home_header_title' => 'Latest Job Opportunities ',
            'home_header_alljobs_link' => 'VIEW ALL JOBS',

            'all_job_header_title' => 'Search Results',
            'all_job_detail_link' => 'VIEW DETAILS',
            'all_job_apply_btn_txt' => 'APPLY',

            'latest_job_title' => 'Latest Vacancies',
            'latest_all_job_link' => 'VIEW ALL JOBS',

            'apply_header_title' => 'Apply for this position',
            'apply_sub_title' => 'Send your details & resume if you are interested in our live jobs.',
            'apply_submit_btn_text' => 'Send',

            'search_all_specialism' => 'All Specialisms',
            'search_all_sector' => 'All Sectors',
            'search_location' => 'Location or postcode',
            'search_title' => 'Keywords',
            'search_please_select' => 'Please Select',
            'search_please_choose' => 'Please Select',

            'search_btn' => 'Search Jobs',

            'search_location_label' => 'Location',
            'search_radius_label' => 'Radius',
            'search_jobtype_label' => 'Job Type',
            'search_salary_label' => 'Annual Salary',
            'search_daily_salary_label' => 'Daily Salary',
            'search_specialism_label' => 'Specialism',
            'search_sector_label' => 'Sector',
            'search_title_label' => 'Keyword Search',

            'search_contract_label' => 'Contract',
            'search_permanent_label' => 'Permanent'
        ];


        return $data[$languageid];
    }
}

if (!function_exists('vincentyGreatCircleDistance')) {
    /**
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    function vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
        pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return round(($angle * $earthRadius) * 0.000621371, 2);
        // return round(($angle * $earthRadius) / 1000, 2);
    }
}


if (!function_exists('getClientIp')) {
    function getClientIp()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}

if (!function_exists('set_value_get')) {
    function set_value_get($field, $default = '', $html_escape = TRUE)
    {
        $CI = &get_instance();

        $value = $CI->input->get($field, FALSE);

        isset($value) or $value = $default;
        return ($html_escape) ? html_escape($value) : $value;
    }
}


if (!function_exists('generate_random_string')) {

    function generate_random_string($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

if (!function_exists('getEmbedYouTubeURL')) {
    // embed youtube URLs
    function getEmbedYouTubeURL($text) {
        $text = preg_replace('~(?#!js YouTubeId Rev:20160125_1800)
            # Match non-linked youtube URL in the wild. (Rev:20130823)
            https?://          # Required scheme. Either http or https.
            (?:[0-9A-Z-]+\.)?  # Optional subdomain.
            (?:                # Group host alternatives.
            youtu\.be/       # Either youtu.be,
            | youtube          # or youtube.com or
            (?:-nocookie)?   # youtube-nocookie.com
            \.com            # followed by
            \S*?             # Allow anything up to VIDEO_ID,
            [^\w\s-]         # but char before ID is non-ID char.
            )                  # End host alternatives.
            ([\w-]{11})        # $1: VIDEO_ID is exactly 11 chars.
            (?=[^\w-]|$)       # Assert next char is non-ID or EOS.
            (?!                # Assert URL is not pre-linked.
            [?=&+%\w.-]*     # Allow URL (query) remainder.
            (?:              # Group pre-linked alternatives.
            [\'"][^<>]*>   # Either inside a start tag,
            | </a>           # or inside <a> element text contents.
            )                # End recognized pre-linked alts.
            )                  # End negative lookahead assertion.
            [?=&+%\w.-]*       # Consume any URL (query) remainder.
            ~ix', 'https://www.youtube.com/embed/$1',
            $text);
        // return $text;
        return "https://www.youtube.com/embed/$text";
    }
}

if (!function_exists('getVideoVisibility')) {
    // embed youtube URLs
    function getVideoVisibility($video) {
        return $video->visibility == VIDEO_PUBLIC_VISIBILITY ? 'Public' : 'Classroom';
    }
}

if (!function_exists('getDateInFormat')) {
    // embed youtube URLs
    function getDateInFormat($date) {
        return date("M d, Y", strtotime($date));
    }
}

if (!function_exists('getDateTimeFormat')) {
    function getDateTimeFormat($date) {
        return date('d-M-Y h:i A',strtotime($date));
    }
}

if (!function_exists('getCategoryNamesLine')) {
    // embed youtube URLs
    function getCategoryNamesLine($video) {
        return $video->category_names ? implode(', ', array_column($video->category_names, 'slug')) : 'N/A';
    }
}

if (!function_exists('getUserRole')) {
    /**
     *
     * @param    array $array
     * @param    string $keyToFlat
     * @param    string $keyToAdd
     * @param    string $keyValueGetFrom
     * @return    string
     */
    function getUserRole($user)
    {
        return $user->user_role == USER_ROLE ? 'Learner' : 'Teacher';
    }
}

if (!function_exists('getFirstError')) {
    function getFirstError() {
        $CI = &get_instance();
        $errors = $CI->form_validation->error_array();
        return $firstError = reset($errors);
    }
}

if (!function_exists('makeNameShort')) {
    function makeNameShort($name) {
        $name = explode(' ', $name);
        return $name ? $name[0] : 'n/a';
    }
}

if (!function_exists('getUserName')) {
    function getUserName($user_id)
    {
        $CI = &get_instance();
        $result = $CI->db->select('name')->where(['id' => $user_id])->get('users')->result_array();
        if($result){
            $result_name = implode(array_column($result, 'name'));
            return ucwords($result_name);
        }else{
            return false;
        }
    }
}

if (!function_exists('getClassName')) {
    function getClassName($class_id)
    {
        $CI = &get_instance();
        $result = $CI->db->select('title')->where(['id' => $class_id])->get('classrooms')->result_array();
        if($result){
            $result_title = implode(array_column($result, 'title'));
            return ucwords($result_title);
        }else{
            return false;
        }
    }
}

if (!function_exists('generateReadMore')) {
    function generateReadMore($string, $resouce_path, $slug)
    {
        // strip tags to avoid breaking any html
        $string = strip_tags($string);
        if (strlen($string) > 500 && $resouce_path && $slug) {
            // truncate string
            $stringCut = substr($string, 0, 500);
            $endPoint = strrpos($stringCut, ' ');

            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $url = ms_site_url($resouce_path.'/'.$slug);
            $string .= '... <a class="read-more" href="'.$url.'">Read more</a>';
        }
        return $string;
    }
}

if (!function_exists('convertDateToTz')) {
    function convertDateToTz($date, $from_tz, $to_tz)
    {
        $to_tz = $to_tz ?? date_default_timezone_get();
        $date = new DateTime($date, new DateTimeZone($from_tz));
        $date->setTimezone(new DateTimeZone($to_tz));
        return $date->format('M d, Y H:i A');
    }
}

if (!function_exists('generateDotsForMoreInfo')) {
    function generateDotsForMoreInfo($string, $charLength)
    {
        // strip tags to avoid breaking any html
        $string = strip_tags($string);
        if (strlen($string) > 30) {
            // truncate string
            $stringCut = substr($string, 0, $charLength);
            $endPoint = strrpos($stringCut, ' ');

            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...';
        }
        return $string;
    }
}

if (!function_exists('getResourceAutherId')) {
	function getResourceAutherId($resource_id) {
        $CI = &get_instance();
        $resource_type = $_GET["resource_type"];
        if($resource_type=="notes"){
            $CI->load->model('notes_model');
            $get_resource_auther_id = $CI->notes_model->get_row(
                array('id' => $resource_id));
            $resource_auther_id = $get_resource_auther_id->user_id;
        }
        else{
            $CI->load->model('video_model');
            $CI->load->model('channel_model');
            $get_resource_auther_id = $CI->video_model->get_row(
                array('id' => $resource_id));
            $resource_auther_id = $CI->channel_model->get_row(
                array('id' => $get_resource_auther_id->channel_id))->user_id;
        }
        return $resource_auther_id;
    }
}

if (!function_exists('getResourceIdFromSlug')) {
    function getResourceIdFromSlug($resource_slug, $model) {
        $CI = &get_instance();
        $CI->load->model($model);
        $get_resource_id = $CI->$model->get_row(
            array(
                'slug' => $resource_slug,
                'status' => '1'
            )
        );
        $resource_id = $get_resource_id->id;
        return $resource_id;
    }
}

if (!function_exists('getSubjectName')) {
    function getSubjectName($category_id)
    {
        $CI = &get_instance();
        $result = $CI->db->select('name')->where(['id' => $category_id])->get('category')->result_array();
        if($result){
            $result_name = implode(array_column($result, 'name'));
            return ucwords($result_name);
        }else{
            return false;
        }
    }
}

if (!function_exists('getSubjectSlug')) {
    function getSubjectSlug($category_id)
    {
        $CI = &get_instance();
        $result = $CI->db->select('slug')->where(['id' => $category_id])->get('category')->result_array();
        if($result){
            $result_name = implode(array_column($result, 'slug'));
            return $result_name;
        }else{
            return false;
        }
    }
}