<?php 
class AppCookieValidator
{
	private $ci;

	public function __construct()
	{
		$this->ci = &get_instance();
	}

	public function initialize()
	{
		$this->ci->load->library('session');
		$this->ci->load->model('user_model');

		if (!$this->ci->session->userdata("isUserLoggedIn")) {
			if (isset($_COOKIE['remember_token'])) {
				$remember_token = $_COOKIE['remember_token'];
				$user = $this->ci->user_model->get_row(['remember_token' => $remember_token]);
				if ($user) {
					$this->ci->session->set_userdata('isUserLoggedIn', TRUE);
					$this->ci->session->set_userdata('user_id', $user->id);
				}
			}
		}else{
            // for google authenticated user redirect for select role
			$user = $this->ci->user_model->get_row(['id' => $this->ci->session->userdata("user_id")]);
			if($user->user_role == null && $this->ci->uri->segment(1) != CONFIRM_ROLE && $this->ci->uri->segment(2) != "logout"){
				redirect('confirm-role');
			}
		}

	}

}   