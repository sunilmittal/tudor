<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Blog_model extends CI_Model
{
    protected $blog = 'blog';
    protected $category = 'industry_sectors';

        /**
     * get_rows function get the multiple rows from the table that is define as property of the model class.
     * @param array $where 
     * @param array $or_where 
     * @param string $raw_where 
     * @return mixed
    */
    public function get_categories($where = [], $or_where = [], $raw_where = null, $with_trashed = false) {

        if ($where) {
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        }

        if ($or_where) {
            $this->db->group_start();
            foreach ($or_where as $column => $value) {
                $this->db->or_where($column, $value);
            }
            $this->db->group_end();
        }

        if ($raw_where) {
            $this->db->where($raw_where);
        }

        $this->db->order_by($this->category.'.name', 'ASC');

        $blogs = $this->db->get($this->category)->result();

        return $blogs;
    }

    public function get_blogs($where = null, $isSingle = false, $orderBy = null, $limit = null, $offset = null)
    {
        $this->db->select('blog.*,industry_sectors.*,blog.slug as slug ,industry_sectors.name as category_name')->from('blog');

        if ($where) {
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        }
        $this->db->where('deleted', 0);

        if ($orderBy) {
            foreach ($orderBy as $column => $value) {
                $this->db->order_by($column, $value);
            }
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        if ($offset) {
            $this->db->offset($offset);
        }

        $this->db->join('industry_sectors', 'blog.category_id = industry_sectors.industry_sector_id','left');

        $records = $this->db->get();

        return $records->num_rows() > 0 ? $isSingle ? $records->row() : $records->result() : false;
    }
}   