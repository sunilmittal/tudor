<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

	protected $table = 'category';

	private function make_select_line($list = []) {
		return implode(',', array_map(function($element) {
			return $this->table.'.'.$element;
		}, $list));
	}

    /**
	 * get_row function get the single row from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @return mixed
	*/
    public function get_row($where = [], $returnAsArray = false, $or_where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }

        return $returnAsArray ? $this->db->get($this->table)->row_array() : $this->db->get($this->table)->row();
    }

    /**
	 * get_rows function get the multiple rows from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @param string $raw_where 
	 * @return mixed
	*/
    public function get_rows($where = [], $returnAsArray = false, $or_where = [], $raw_where = null) {
    	$this->db->select($this->make_select_line(['id', 'name', 'slug', 'parent_id']));
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }
        if ($raw_where) {
        	$this->db->where($raw_where);
        }
        $this->db->where($this->table.'.deleted_at', null);
        
        return $returnAsArray ? $this->db->get($this->table)->result_array() : $this->db->get($this->table)->result();
    }
}