<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Channel_model extends CI_Model {

	protected $table = 'channels';

	public function __construct() {
		parent::__construct();
		$this->load->model(['video_model','notes_model','user_model']);
	}

	/**
	 * add_row function add the row in the table that is define as property of the model class.
	 * @param array $data 
	 * @return mixed
	*/
	public function add_row($data, $needId = false) {
		$data['created_at'] = date('Y-m-d H:i:s');
		$query = $this->db->insert($this->table, $data);

		if ($query) {
			return $needId ? $this->db->insert_id() : true;
		} else {
			return false;
		}
	}

	/**
	 * update_row function update the row in the table that is define as property of the model class.
	 * @param array $where 
	 * @param array $data 
	 * @return bool
	*/
    public function update_row($where = [], $data = []) {
    	if ($where && $data) {

			$data['updated_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
    }

    /**
	 * get_row function get the single row from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @return mixed
	*/
    public function get_row($where = [], $returnAsArray = false, $or_where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }

        return $returnAsArray ? $this->db->get($this->table)->row_array() : $this->db->get($this->table)->row();
    }

    /**
	 * get_rows function get the multiple rows from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @param string $raw_where 
	 * @return mixed
	*/
    public function get_rows($where = [], $returnAsArray = false, $or_where = [], $raw_where = null) {
        foreach ($where as $column => $value) {
            $this->db->where($this->table.'.'.$column, $value);
        }
        if ($or_where) {
        	$this->db->group_start();
	        foreach ($or_where as $column => $value) {
	            $this->db->or_where($column, $value);
	        }
        	$this->db->group_end();
        }

        if ($raw_where) {
        	$this->db->where($raw_where);
        }

        $this->db->select('channels.user_id, channels.id, channels.name as channel_name, users.name as user_name, users.image, users.user_role as role');
        $this->db->join('users', 'channels.user_id = users.id', 'left');

        return $returnAsArray ? $this->db->get($this->table)->result_array() : $this->db->get($this->table)->result();
	}
	
	/**
	 * search function search rows in the table that is define as property of the model class.
	 * @return array of object or JSON
	*/
	public function search($where = [], $searchEle = [], $limit = 0, $offset = 0, $sort_by = [], $json = false) {

		$response = [];

        extract($searchEle);
		if (empty($term)) $term = null;

        $raw_where = null;

		$users = collect($this->get_rows($where));
        if ($term) {
            $users = $users->reject(function ($element) use ($term) {
                return mb_strpos(strtolower($element->channel_name), strtolower($term)) === false;
            })->values();
        }

        if ($limit) {
            $users = $users->slice($offset, $limit)->values();
        }
		
        $response = $users;
        if (!$json) {
            return $response;
        }
        json_response($response);
	}
	
	// Get Content filter type data //
	public function getContentFilter($getParams) {
		$user_id = getLoggedInUserId();
		$searched_channels = $this->search(['user_id!='=>$user_id], $getParams, 0, 0, false, [], true);
		if($searched_channels){
			foreach($searched_channels as $channel){
				$this->video_model->getChannelVideosCount($channel);
				$this->notes_model->getUserNotesCount($channel);
				$channel->show_subscribe_btn = true;
				if (!isUserLoggedIn() || ($user_id == $channel->user_id) || (userRoleIsTeacher() && $channel->role != TEACHER_ROLE)) {
					$channel->show_subscribe_btn = false;
				}
			}
		}
		return $searched_channels;
	}

	public function delete_channels($where = []){
		if ($where) {
			foreach ($where as $column => $value) {
				$this->db->where($column, $value);
			}
			return $this->db->delete($this->table);
		}
		return false;
	}
}