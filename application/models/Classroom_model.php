<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Classroom_model extends CI_Model {

	protected $table = 'classrooms';

	/**
	 * add_row function add the row in the table that is define as property of the model class.
	 * @param array $data 
	 * @return mixed
	*/
	public function add_row($data, $needId = false) {
		$data['created_at'] = date('Y-m-d H:i:s');
		$query = $this->db->insert($this->table, $data);

		if ($query) {
			return $needId ? $this->db->insert_id() : true;
		} else {
			return false;
		}
	}

	/**
	 * update_row function update the row in the table that is define as property of the model class.
	 * @param array $where 
	 * @param array $data 
	 * @return bool
	*/
    public function update_row($where = [], $data = []) {
    	if ($where && $data) {

			$data['updated_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
    }

    /**
	 * get_row function get the single row from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @return mixed
	*/
    public function get_row($where = [], $returnAsArray = false, $or_where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }

        $classroom = $this->db->get($this->table)->row();

        if($classroom){
        	$this->getClassroomDetails($classroom);
    	}

        return $classroom;
    }

    /**
	 * get_rows function get the multiple rows from the table that is define as property of the model class.
	 * @param array $where 
	 * @param array $or_where 
	 * @param string $raw_where 
	 * @return mixed
	*/
    public function get_rows($where = [], $or_where = [], $raw_where = null, $with_trashed = false) {

    	if ($where) {
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
    	}

        if ($or_where) {
        	$this->db->group_start();
	        foreach ($or_where as $column => $value) {
	            $this->db->or_where($column, $value);
	        }
        	$this->db->group_end();
        }
        
        if ($raw_where) {
        	$this->db->where($raw_where);
        }

        $this->db->order_by($this->table.'.created_at', 'DESC');

        $classrooms = $this->db->get($this->table)->result();

        if ($classrooms) {
            foreach ($classrooms as $classroom) {
            	$this->getClassroomDetails($classroom);
            }
        }       

        return $classrooms;
    }

    /**
	 * add_rows function add the multiple rows in the table that is define as property of the model class.
	 * @param array $data 
	 * @return bool
	*/
    public function add_rows($data) {
		$query = $this->db->insert_batch($this->table, $data); 

		if ($query) {
			return true;
		} else {
			return false;
		}
	}

	private function getSubscribersUserInfo($channel_id) {
		return $this->db->select('distinct(subscriber_id)')
				->where('channel_id', $channel_id)
				->get('subscribe')->result();
	}

	private function getSubscribedUserInfo($user_id) {
		return $this->db->select('distinct(user_id)')
				->where('subscriber_id', $user_id)
				->join('channels', 'subscribe.channel_id = channels.id', 'left')
				->get('subscribe')->result();
	}

	private function getClassroomDetails($classroom) {
    	if ($classroom) {
    		$classroom->video_ids = array_column($this->getVideos($classroom->id), 'id');
        }
    }

    public function delete_row($where = []) {
    	if ($where) {
			$data['deleted_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
	}
	
	public function delete_rows($where = []) {
    	if ($where) {
			$data['deleted_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        return $this->db->update($this->table, $data);
    	}
    	return false;
	}

	private function getVideos($category_id = []) {
		if ($category_id) {
			return $this->db->select('id')->where('classroom_id', $category_id)->get('videos')->result_array();
		}
	}
}