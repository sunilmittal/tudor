<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Classroom_users_model extends CI_Model {

	protected $table = 'classroom_users';

    public function get_row($where = [], $returnAsArray = false, $or_where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }

        $classroom = $this->db->get($this->table)->row();

        return $classroom;
    }

    public function get_rows($where = [], $or_where = [], $raw_where = null, $with_trashed = false) {

    	if ($where) {
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
    	}

        if ($or_where) {
        	$this->db->group_start();
	        foreach ($or_where as $column => $value) {
	            $this->db->or_where($column, $value);
	        }
        	$this->db->group_end();
        }
        
        if ($raw_where) {
        	$this->db->where($raw_where);
        }

        $this->db->order_by($this->table.'.classroom_id', 'DESC');

        $classrooms = $this->db->get($this->table)->result();

        if ($classrooms) {
            foreach ($classrooms as $classroom) {
            	$this->getClassroomDetails($classroom);
            }
        }       

        return $classrooms;
    }

	private function getClassroomDetails($classroom) {
    	if ($classroom) {
    		$classroom->video_ids = array_column($this->getVideos($classroom->classroom_id), 'id');
        }
    }

	private function getVideos($category_id = []) {
		if ($category_id) {
			return $this->db->select('id')->where(['classroom_id' => $category_id,'deleted_at' => null])->get('videos')->result_array();
		}
	}

    public function update_row($where = [], $data = []) {
    	if ($where && $data) {
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
    }

	public function add_user_classroom($data,$where = [], $grant_access = false) {
		$result = $this->db->where($where)->get($this->table)->result_array();
		if($result){
			if($grant_access){
				$query = $this->db->where(['classroom_id' => $data['classroom_id'],'user_id' => $data['user_id']])->update($this->table);
				return  ($query) ? ['status'=>true,'message'=>'Request Granted'] : false;
			}else{
				$query = $this->db->where(['classroom_id' => $data['classroom_id'],'user_id' => $data['user_id']])->delete($this->table);
				return ($query) ? ['status'=>true,'message'=>'Deleted'] : false;
			}
		}else{
			$data['request_access'] = 1;
			$query = $this->db->insert($this->table, $data);
			return ($query) ?  ['status'=>true,'message'=>'Sent'] : false;
		}
	}

	public function delete_classroom_users($where = []){
		if ($where) {
			foreach ($where as $column => $value) {
				$this->db->where($column, $value);
			}
			return $this->db->delete($this->table);
		}
		return false;
	}
}