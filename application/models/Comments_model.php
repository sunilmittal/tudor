<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comments_model extends CI_Model 
{
	protected $table = 'resource_comments';
    
      /**
	 * get_rows function get the multiple rows from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @param string $raw_where 
	 * @return mixed
	*/
    public function get_rows($where = [], $limit, $start, $returnAsArray = false, $or_where = [], $raw_where = null, $with_trashed = false, $where_in = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        if ($or_where) {
        	$this->db->group_start();
	        foreach ($or_where as $column => $value) {
	            $this->db->or_where($column, $value);
	        }
        	$this->db->group_end();
        }
        
        if ($raw_where) {
        	$this->db->where($raw_where);
        }

        if ($where_in) {
            foreach ($where_in as $column => $value) {
                $this->db->where_in($column, $value);
            }
        }

        if (!$with_trashed) {
        	$this->db->where($this->table.'.deleted_at', null);
        }

		$this->db->order_by($this->table.'.created_at', 'DESC');
		
        $this->db->limit($limit, $start);

		$comments = $this->db->get($this->table)->result();
        if ($comments) {
            foreach ($comments as $comment) {
                $comment->replies = $this->getReplies($comment->id,$limit, $start);
				$this->getResourceUserDetails($comment);
				$comment->replies_count = $this->getRepliesCount($comment->id);
				$client_tz = $_COOKIE['client_tz'] ?? date_default_timezone_get();
				$comment->created_at = convertDateToTz($comment->created_at, date_default_timezone_get(), $client_tz);
            }
        }
        return $comments;
	}
	
	private function getResourceUserDetails($comment) {
    	$comment->user = $this->db->select('name as user_name, image, user_role, id as user_id')
								->where(['id' => $comment->user_id])->get('users')->row();
		
    }
    
    /**
     * get comment replies
     */
    public function getReplies($comment_id,$limit,$offset) {
        $this->db->order_by($this->table.'.created_at', 'DESC');
        $this->db->limit($limit, $offset);
        $replies = $this->db->where(array('parent_id'=>$comment_id, 'deleted_at'=>NULL))->get($this->table)->result();
        if($replies){
            foreach ($replies as $reply) {
				$this->getResourceUserDetails($reply);
				$reply->created_at =  convertDateToTz($reply->created_at,date_default_timezone_get(),$_COOKIE['client_tz']);
            }
		}
		return $replies;
        
	}

    /**
	 * add_row function add the row in the table that is define as property of the model class.
	 * @param array $data 
	 * @return mixed
	*/
	public function add_row($data, $needId = false) {
		$data['created_at'] = date('Y-m-d H:i:s');
		$query = $this->db->insert($this->table, $data);

		if ($query) {
			return $needId ? $this->db->insert_id() : true;
		} else {
			return false;
		}
    }
    
    public function delete_row($where = []) {
    	if ($where) {
			$data['deleted_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
	}

	public function delete_rows($where = []) {
    	if ($where) {
			$data['deleted_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        return $this->db->update($this->table, $data);
    	}
    	return false;
	}

    /**
	 * get_row function get the single row from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @return mixed
	*/
    public function get_row($where = [], $returnAsArray = false, $or_where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }
		$this->db->where($this->table.'.deleted_at', null);
		$comments = $this->db->get($this->table)->row();
		if ($comments) {
        }
        return $comments;
	}
	
	/**
	 * update_row function update the row in the table that is define as property of the model class.
	 * @param array $where 
	 * @param array $data 
	 * @return bool
	*/
    public function update_row($where = [], $data = []) {
    	if ($where && $data) {

			$data['updated_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
	}

	/**
	 * get comments count.
	 */
	public function getCommentsCount($note_id,$type){
		$result = $this->db->where(array('resource_id'=>$note_id,'parent_id'=>NULL,'deleted_at'=>NULL,'type'=>$type))->count_all_results($this->table);
		return $result;
	}

	/**
	 * get comments replies count.
	 */
	public function getRepliesCount($cmt_id){
		$result = $this->db->where(array('parent_id'=>$cmt_id,'deleted_at'=>NULL))->count_all_results($this->table);
		return $result;
	}

}