<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Common_model extends CI_Model
{

    function saveContact($data)
    {
        $query = $this->db->insert('contactus', $data);
        return $query;
    }
    function saveJoinus($data)
    {
        $query = $this->db->insert('enquiry', $data);
        return $query;
    }


    public function getTestimonial($where = null, $isSingle = false, $orderBy = null,  $limit = null)
    {
        $this->db->select('*')->from('testimonials');

        if ($where) {
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        }

        if ($orderBy) {
            foreach ($orderBy as $column => $value) {
                $this->db->order_by($column, $value);
            }
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $records = $this->db->get();

        if ($isSingle == true) {
            if ($records->num_rows() > 0) {
                return $records->row();
            } else {
                return FALSE;
            }
        }

        if ($records->num_rows() > 0) {
            return $records->result();
        } else {
            return FALSE;
        }
    }

    public function getBlogs($where = null, $isSingle = false, $orderBy = null,  $limit = null, $offset = null)
    {
        $this->db->select('*')->from('blog');

        if ($where) {
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        }
        $this->db->where('deleted', 0);

        if ($orderBy) {
            foreach ($orderBy as $column => $value) {
                $this->db->order_by($column, $value);
            }
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        if ($offset) {
            $this->db->offset($offset);
        }

        $records = $this->db->get();

        if ($isSingle == true) {
            if ($records->num_rows() > 0) {
                return $records->row();
            } else {
                return FALSE;
            }
        }

        if ($records->num_rows() > 0) {
            return $records->result();
        } else {
            return FALSE;
        }
    }

    public function getSectors($where = null, $isSingle = false, $orderBy = null,  $limit = null, $offset = null)
    {
        $this->db->select('*')->from('industry_sectors');

        if ($where) {
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        }
        // $this->db->where('deleted', 0);

        if ($orderBy) {
            foreach ($orderBy as $column => $value) {
                $this->db->order_by($column, $value);
            }
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        if ($offset) {
            $this->db->offset($offset);
        }

        $records = $this->db->get();

        if ($isSingle == true) {
            if ($records->num_rows() > 0) {
                return $records->row();
            } else {
                return FALSE;
            }
        }

        if ($records->num_rows() > 0) {
            return $records->result();
        } else {
            return FALSE;
        }
    }

    public function getLocations($where = null, $isSingle = false, $orderBy = null,  $limit = null, $offset = null)
    {
        $this->db->select('*')->from('locations');

        if ($where) {
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        }
        // $this->db->where('deleted', 0);

        if ($orderBy) {
            foreach ($orderBy as $column => $value) {
                $this->db->order_by($column, $value);
            }
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        if ($offset) {
            $this->db->offset($offset);
        }

        $records = $this->db->get();

        if ($isSingle == true) {
            if ($records->num_rows() > 0) {
                return $records->row();
            } else {
                return FALSE;
            }
        }

        if ($records->num_rows() > 0) {
            return $records->result();
        } else {
            return FALSE;
        }
    }

    public function getSettings($where = null, $isSingle = false, $orderBy = null,  $limit = null, $offset = null)
    {
        $this->db->select('*')->from('settings');

        if ($where) {
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        }

        if ($orderBy) {
            foreach ($orderBy as $column => $value) {
                $this->db->order_by($column, $value);
            }
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        if ($offset) {
            $this->db->offset($offset);
        }

        $records = $this->db->get();

        if ($isSingle == true) {
            if ($records->num_rows() > 0) {
                return $records->row();
            } else {
                return FALSE;
            }
        }

        if ($records->num_rows() > 0) {
            return $records->result();
        } else {
            return FALSE;
        }
    }


    public function getContentPage($where = null, $isSingle = false, $orderBy = null,  $limit = null, $offset = null)
    {
        $this->db->select('*')->from('content_pages');

        if ($where) {
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        }
        $this->db->where('deleted', 0);

        if ($orderBy) {
            foreach ($orderBy as $column => $value) {
                $this->db->order_by($column, $value);
            }
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        if ($offset) {
            $this->db->offset($offset);
        }

        $records = $this->db->get();

        if ($isSingle == true) {
            if ($records->num_rows() > 0) {
                return $records->row();
            } else {
                return FALSE;
            }
        }

        if ($records->num_rows() > 0) {
            return $records->result();
        } else {
            return FALSE;
        }
    }
    public function getContentBlock($where = null, $isSingle = false, $orderBy = null,  $limit = null, $offset = null)
    {
        $this->db->select('*')->from('content_blocks');

        if ($where) {
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        }
        $this->db->where('deleted', 0);

        if ($orderBy) {
            foreach ($orderBy as $column => $value) {
                $this->db->order_by($column, $value);
            }
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        if ($offset) {
            $this->db->offset($offset);
        }

        $records = $this->db->get();

        if ($isSingle == true) {
            if ($records->num_rows() > 0) {
                return $records->row();
            } else {
                return FALSE;
            }
        }

        if ($records->num_rows() > 0) {
            return $records->result();
        } else {
            return FALSE;
        }
    }

    function saveNewsletter($data)
    {
        $query = $this->db->insert('newsletter', $data);
        return $query;
    }

    function getNewsletter($where = null, $isSingle = false, $orderBy = null,  $limit = null, $offset = null)
    {
        $this->db->select('*')->from('newsletter');
        if ($where) {
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        }
        $records = $this->db->get();

        if ($isSingle == true) {
            if ($records->num_rows() > 0) {
                return $records->row();
            } else {
                return FALSE;
            }
        }

        if ($records->num_rows() > 0) {
            return $records->result();
        } else {
            return FALSE;
        }
    }
}

