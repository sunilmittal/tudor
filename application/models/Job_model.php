<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Job_model extends CI_Model {

    protected $table = 'vacancies';

	public function getAll($where = [], $limit = null, $offset = null, $orderBy = [], $rawWhere = null) {
        $this->db->select('*')->from($this->table);

        if ($where) {
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        }

        if ($rawWhere) {
        	$this->db->where($rawWhere);
        }

        $this->db->where('deleted', 0);

        if ($orderBy) {
            foreach ($orderBy as $column => $value) {
                $this->db->order_by($column, $value);
            }
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        if ($offset) {
            $this->db->offset($offset);
        }

        $jobs = $this->db->get()->result();
        if ($jobs) {
            foreach ($jobs as $job) {
                $job->team = $this->getJobTeam($job->consultant_id);

                $jobLocations = $this->getJobLocations($job->vacancy_id);
                $job->locations = $jobLocations;
                $job->location_ids = collect($jobLocations)->pluck('location_id')->toArray();
                $job->location_names = collect($jobLocations)->pluck('name')->toArray();

                $jobSectors = $this->getJobSectors($job->vacancy_id);
                $job->sectors = $jobSectors;
                $job->sector_ids = collect($jobSectors)->pluck('industry_sector_id')->toArray();
                $job->sector_names = collect($jobSectors)->pluck('name')->toArray();
            }
        }

        return $jobs;
    }

    public function search($where = [], $searchEle = [], $limit = 2, $offset = 0, $sort_by = [], $json = false) {

        $response = [];

        extract($searchEle);
        if (empty($postcode)) $postcode = null;
        if (empty($distance)) $distance = null;
        if (empty($salary_range)) $salary_range = null;

        if (empty($term)) $term = null;
        if (empty($sector_id)) $sector_id = null;
        if (empty($location_id)) $location_id = null;
        if (empty($type)) $type = null;
        if (empty($job_tag)) $job_tag = null;

        if (empty($latitude)) $latitude = null;
        if (empty($longitude)) $longitude = null;

        $raw_where = null;

        if ($salary_range) {
        	$raw_where = '(';
        	foreach ($salary_range as $key => $range) {
        		list($salary_from, $salary_to) = explode('-', $range);
	            $salary_from = (int) $salary_from;
	            $salary_to = (int) $salary_to;

        		$raw_where .= "(
        			salary_from >= $salary_from AND salary_to <= $salary_to
        		)";
        		$raw_where .= $key + 1 < count($salary_range) ? ' OR ' : ''; 
        	}
        	$raw_where .= ')';
        }

        $jobs = collect($this->getAll($where, null, null, $sort_by, $raw_where));

        // if ($latitude && $longitude && $distance) {
        //     $postcodes = $this->getPostcodeAndDistance($latitude, $longitude, $distance);
        //     if ($postcodes) {
        //         $postcodes = array_column($postcodes, 'postcode');
        //         $jobs = $jobs->filter(function ($job) use ($postcodes) {
        //             return in_array($job->postcode, $postcodes);
        //         })->values();
        //     }
            /*$jobs = $jobs->map(function($job) use ($latitude, $longitude) {
                $job->distance = vincentyGreatCircleDistance($latitude, $longitude, $job->lat, $job->lng);
                return $job;
            })->filter(function($job) use ($distance) {
                return $job->distance <= $distance;
            })->sortBy('distance')->values();*/
        // }

        if ($term) {
            $jobs = $jobs->reject(function ($element) use ($term) {
                return mb_strpos(strtolower($element->title), strtolower($term)) === false && mb_strpos(strtolower($element->description), strtolower($term)) === false;
            })->values();
        }

        if ($job_tag) {
            $jobs = $jobs->filter(function ($element) use ($job_tag) {
                return strtolower($element->title) == strtolower($job_tag);
            })->values();
        }

        if ($type) {
        	$jobs = $jobs->filter(function ($job) use ($type) {
                return in_array($job->contract_type, $type);
            })->values();
        }

        if ($postcode) {
            $jobs = $jobs->reject(function ($element) use ($postcode) {
                return mb_strpos(strtolower($element->postcode), strtolower($postcode)) === false;
            })->values();
        }

        if ($sector_id) {
            if (is_array($sector_id)) {
                $jobs = !array_product($sector_id) ? $jobs : $jobs->filter(function ($job) use ($sector_id) {
                    return count($job->sectors) && count(array_intersect($sector_id, $job->sector_ids));
                })->values();
            } else {
                $jobs = $jobs->filter(function ($job) use ($sector_id) {
                    return count($job->sectors) && in_array($sector_id, $job->sector_ids);
                })->values();
            }
        }

        if ($location_id) {
            $jobs = !array_product($location_id) ? $jobs : $jobs->filter(function ($job) use ($location_id) {
                return count($job->locations) && count(array_intersect($location_id, $job->location_ids));
            })->values();
        }


        if ($limit) {
            $jobs = $jobs->slice($offset, $limit)->values();
        }

        $response = $jobs;
        if (!$json) {
            return $response;
        }
        json_response($response);
    }

    /**
     * get_row function get the single row from the table that is define as property of the model class.
     * @param array $where 
     * @param array $or_where 
     * @return mixed
    */
    public function get_row($where = [], $or_where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }

        $job = $this->db->get($this->table)->row();

        if ($job) {
            $job->team = $this->getJobTeam($job->consultant_id);

            $jobLocations = $this->getJobLocations($job->vacancy_id);
            $job->locations = $jobLocations;
            $job->location_ids = collect($jobLocations)->pluck('location_id')->toArray();
            $job->location_names = collect($jobLocations)->pluck('name')->toArray();

            $jobSectors = $this->getJobSectors($job->vacancy_id);
            $job->sectors = $jobSectors;
            $job->sector_ids = collect($jobSectors)->pluck('industry_sector_id')->toArray();
            $job->sector_names = collect($jobSectors)->pluck('name')->toArray();
        }

        return $job;
    }

	public function getSingleJobDetail($slug_name)
	{
		$this->db->select('vacancies.*, vacancies_locations.location_id as locationId, locations.name as locationName, industry_sectors.name as jobCategoryName, team.firstname, team.lastname, team.email, team.tel, team.image as user_pic, team.title as userjobTitle');
		$this->db->from('vacancies');
		$this->db->join('vacancies_locations', 'vacancies.vacancy_id = vacancies_locations.vacancy_id', 'left');
		$this->db->join('vacancies_sectors', 'vacancies.vacancy_id = vacancies_sectors.vacancy_id', 'left');
		$this->db->join('locations', 'locations.location_id = vacancies_locations.location_id', 'left');
		$this->db->join('industry_sectors', 'industry_sectors.industry_sector_id = vacancies_sectors.industry_sector_id	', 'left');
		$this->db->join('team', 'vacancies.consultant_id = team.user_id', 'left');
		$this->db->where('vacancies.slug', $slug_name);

		$data = $this->db->get()->row();

		if ($data) {
			$data->locations = $this->getJobLocations($data->vacancy_id);
			$data->sectors = $this->getJobSectors($data->vacancy_id);
		}

		return $data;
	}

    public function getJobTeam($consultant_id = null) {
        $data = false;
        if ($consultant_id) {
            $data = $this->db->select('team.firstname, team.lastname, team.user_id, team.tel, team.email, team.image, team.title, team.linkedin')
                ->from('team')
                ->where('user_id', $consultant_id)
                ->get()
                ->row();
        }
        return $data;
    }

	public function getJobLocations($jobId = null) {
		$data = [];
		if ($jobId) {
			$data = $this->db->select('locations.name, locations.location_id')
				->from('vacancies_locations')
				->join('locations', 'vacancies_locations.location_id = locations.location_id', 'right')
				->where('vacancies_locations.vacancy_id', $jobId)
				->get()
				->result();
		}
		return $data;
	}

	public function getJobSectors($jobId = null) {
		$data = [];
		if ($jobId) {
			$data = $this->db->select('industry_sectors.name, industry_sectors.industry_sector_id')
				->from('vacancies_sectors')
				->join('industry_sectors', 'vacancies_sectors.industry_sector_id = industry_sectors.industry_sector_id', 'right')
				->where('vacancies_sectors.vacancy_id', $jobId)
				->get()
				->result();
		}
		return $data;
	}

	public function searchWithInRadius($distance)
	{
		$sql = "SELECT
                      *, (
                        3959 * acos (
                          cos ( radians(28.4089) )
                          * cos( radians( location_lat ) )
                          * cos( radians( location_long ) - radians(77.3178) )
                          + sin ( radians(28.4089) )
                          * sin( radians( location_lat ) )
                        )
                      ) AS distance
                    FROM vacancies
                    HAVING distance < " . $distance . "
                    ORDER BY distance
                    LIMIT 0 , 20";

		$query = $this->db->query($sql);

		// Returns the result as an object
		$jobs = $query->result();

		if ($jobs) {
			foreach ($jobs as $job) {
				$job->locations = $this->getJobLocations($job->vacancy_id);
				$job->sectors = $this->getJobSectors($job->vacancy_id);
			}
		}
		return $jobs;
	}

    private function getPostcodeAndDistance($lat, $long, $distance) {

        $sql = "
        select replace(postcode, ' ', '') as postcode, 
               ( 3959 * acos( cos( radians($lat) ) 
                      * cos( radians( postcodelatlng.latitude ) ) 
                      * cos( radians( postcodelatlng.longitude ) - radians($long) ) 
                      + sin( radians($lat) ) 
                      * sin( radians( postcodelatlng.latitude ) ) ) ) AS distance 
        from postcodelatlng
        having distance <= $distance ORDER BY distance LIMIT 100;
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}
