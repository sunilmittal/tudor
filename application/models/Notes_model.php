<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notes_model extends CI_Model 
{
	protected $table = 'notes';
	public function __construct() {
		parent::__construct();
		$this->load->model(['resource_inappropriate_model']);
	}
    
      /**
	 * get_rows function get the multiple rows from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @param string $raw_where 
	 * @return mixed
	*/
    public function get_rows($where = [], $returnAsArray = false, $or_where = [], $raw_where = null, $with_trashed = false, $where_in = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        if ($or_where) {
        	$this->db->group_start();
	        foreach ($or_where as $column => $value) {
	            $this->db->or_where($column, $value);
	        }
        	$this->db->group_end();
        }
        
        if ($raw_where) {
        	$this->db->where($raw_where);
        }

        if ($where_in) {
            foreach ($where_in as $column => $value) {
                $this->db->where_in($column, $value);
            }
        }

        if (!$with_trashed) {
        	$this->db->where($this->table.'.deleted_at', null);
        }

        $this->db->order_by($this->table.'.created_at', 'DESC');

		$notes = $this->db->get($this->table)->result();
		if ($notes) {
            foreach ($notes as $note) {
				$this->getNoteUserDetails($note);
				$note->flag_exists = $this->resource_inappropriate_model->flag_exists($note);
				$note->category_ids = array_unique(array_filter([$note->category_id, $note->sub_category_id]));
    			$note->category_names = $this->getCategories($note->category_ids);
            }
        }
        return $notes;
	}
	
	private function getNoteUserDetails($note) {
    	$note->user = $this->db->select('name as user_name, image, user_role, id as user_id')
								->where(['id' => $note->user_id])->get('users')->row();
		
	}

    /**
	 * add_row function add the row in the table that is define as property of the model class.
	 * @param array $data 
	 * @return mixed
	*/
	public function add_row($data, $needId = false) {
		$data['created_at'] = date('Y-m-d H:i:s');
		$query = $this->db->insert($this->table, $data);

		if ($query) {
			return $needId ? $this->db->insert_id() : true;
		} else {
			return false;
		}
    }
    
    public function delete_row($where = []) {
    	if ($where) {
			$data['deleted_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
	}
	
	private function getCategories($category_ids = []) {
		if ($category_ids) {
			return $this->db->select('id, name, slug')->where_in('id', $category_ids)->get('category')->result_array();
		}
	}

    /**
	 * get_row function get the single row from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @return mixed
	*/
    public function get_row($where = [], $returnAsArray = false, $or_where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }
		$this->db->where($this->table.'.deleted_at', null);
		$notes = $this->db->get($this->table)->row();
		if ($notes) {
			$this->getNoteUserDetails($notes);
			$notes->flag_exists = $this->resource_inappropriate_model->flag_exists($notes);
			$notes->category_ids = array_unique(array_filter([$notes->category_id, $notes->sub_category_id]));
    		$notes->category_names = $this->getCategories($notes->category_ids);
        }
        return $notes;
	}
	
	/**
	 * update_row function update the row in the table that is define as property of the model class.
	 * @param array $where 
	 * @param array $data 
	 * @return bool
	*/
    public function update_row($where = [], $data = []) {
    	if ($where && $data) {

			$data['updated_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
	}
	
	/**
	 * search function search rows in the table that is define as property of the model class.
	 * @return array of object or JSON
	*/
	public function search($where = [], $searchEle = [], $limit = null, $offset = 0, $sort_by = [], $json = false, $group_by = false) {
        $response = [];

        extract($searchEle);
        if (empty($term)) $term = null;
        if (empty($category_id)) $category_id = null;
		if (empty($sub_category_id)) $sub_category_id = null;

        $raw_where = null;

		$notes = collect($this->get_rows($where));
		
		if($group_by){
			$this->db->group_by('user_id'); 
		}

        if ($term) {
            $notes = $notes->reject(function ($element) use ($term) {
                return mb_strpos(strtolower($element->title), strtolower($term)) === false && mb_strpos(strtolower($element->description), strtolower($term)) === false;
            })->values();
        }
		
		if ($category_id) {
			$notes = $notes->filter(function ($element) use ($category_id) {
                return $element->category_id == $category_id;
            })->values();
		}
		
		if ($sub_category_id) {
			$notes = $notes->filter(function ($element) use ($sub_category_id) {
                return $element->sub_category_id == $sub_category_id;
            })->values();
        }

        if ($limit) {
            $notes = $notes->slice($offset, $limit)->values();
        }

        $response = $notes;
        if (!$json) {
            return $response;
        }
        json_response($response);
	}

	/**
	 * Get Videos Count Of A User.
	 */
	public function getUserNotesCount($channel){
		$channel->total_notes = $this->db->where(array('user_id'=>$channel->user_id,'deleted_at'=>NULL))->count_all_results($this->table);
	}

	/**
     * get_rows function get the multiple rows from the table that is define as property of the model class.
     * @param array $where 
     * @param bool $returnAsArray 
     * @param array $or_where 
     * @param string $raw_where 
     * @return mixed
    */
    public function num_rows($where = [], $or_where = [], $raw_where = null) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }
        if ($raw_where) {
            $this->db->where($raw_where);
        }

        return $this->db->get($this->table)->num_rows();
	}
	
	/**
     * delete_row function update the row in the table that is define as property of the model class.
     * @param array $where 
     * @param array $data 
     * @return bool
    */
    public function delete_rows($where = []) {
        if ($where) {

            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
           
            return $this->db->update($this->table, ['deleted_at' => date('Y-m-d H:i:s')]);
        }
        return false;
    }

}