<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Resource_inappropriate_model extends CI_Model {

    protected $table = 'resource_inappropriate';
    
      /**
	 * get_rows function get the multiple rows from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @param string $raw_where 
	 * @return mixed
	*/
    public function get_rows($where = [], $returnAsArray = false, $or_where = [], $raw_where = null, $with_trashed = false, $where_in = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        if ($or_where) {
        	$this->db->group_start();
	        foreach ($or_where as $column => $value) {
	            $this->db->or_where($column, $value);
	        }
        	$this->db->group_end();
        }
        
        if ($raw_where) {
        	$this->db->where($raw_where);
        }

        if ($where_in) {
            foreach ($where_in as $column => $value) {
                $this->db->where_in($column, $value);
            }
        }

        if (!$with_trashed) {
        	$this->db->where($this->table.'.deleted_at', null);
        }

        $this->db->order_by($this->table.'.created_at', 'DESC');

		$inappropriates = $this->db->get($this->table)->result();
		if ($inappropriates) {
            foreach ($inappropriates as $inappropriate) {
            	$this->getAppropriateDetails($inappropriate);
            }
        }
        return $inappropriates;
	}
	
	private function getAppropriateDetails($inappropriate) {
    	$inappropriate->user = $this->db->select('name as user_name, image, user_role')
                                ->where(['id' => $inappropriate->user_id])->get('users')->row();
        
	}

    /**
	 * add_row function add the row in the table that is define as property of the model class.
	 * @param array $data 
	 * @return mixed
	*/
	public function add_row($data, $needId = false) {
		$data['created_at'] = date('Y-m-d H:i:s');
		$query = $this->db->insert($this->table, $data);

		if ($query) {
			return $needId ? $this->db->insert_id() : true;
		} else {
			return false;
		}
    }
    
    public function delete_row($where = []) {
    	if ($where) {
			$data['deleted_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
	}
	
	public function delete_rows($where = []) {
    	if ($where) {
			$data['deleted_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        return $this->db->update($this->table, $data);
    	}
    	return false;
	}

    /**
	 * get_row function get the single row from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @return mixed
	*/
    public function get_row($where = [], $returnAsArray = false, $or_where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }

        $notes = $this->db->get($this->table)->row();
        return $notes;
	}
	
	/**
	 * update_row function update the row in the table that is define as property of the model class.
	 * @param array $where 
	 * @param array $data 
	 * @return bool
	*/
    public function update_row($where = [], $data = []) {
    	if ($where && $data) {

			$data['updated_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
	}
	
	public function flag_exists($note)
    {
        $this->db->where(['resource_id'=>$note->id,'user_id'=>getLoggedInUserId(),'resource_type'=>'note']);
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0){
            return true;
        } else{
            return false;
        }
    }

}