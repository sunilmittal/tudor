<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Team_model extends CI_Model
{

    public function getTeam($where = null, $isSingle = false, $orderBy = null,  $limit = null, $offset = null)
    {
        $this->db->select('*')->from('team');

        if ($where) {
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        }
        $this->db->where('deleted', 0);

        if ($orderBy) {
            foreach ($orderBy as $column => $value) {
                $this->db->order_by($column, $value);
            }
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        if ($offset) {
            $this->db->offset($offset);
        }

        $records = $this->db->get();

        if ($isSingle == true) {
            if ($records->num_rows() > 0) {
                return $records->row();
            } else {
                return FALSE;
            }
        }

        if ($records->num_rows() > 0) {
            return $records->result();
        } else {
            return FALSE;
        }
    }
}

