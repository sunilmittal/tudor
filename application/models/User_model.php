<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	protected $table = 'users';

	/**
	 * add_row function add the row in the table that is define as property of the model class.
	 * @param array $data 
	 * @return mixed
	*/
	public function add_row($data, $needId = false) {
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['verified'] = '1';
		$query = $this->db->insert($this->table, $data);

		if ($query) {
			return $needId ? $this->db->insert_id() : true;
		} else {
			return false;
		}
	}

	/**
	 * update_row function update the row in the table that is define as property of the model class.
	 * @param array $where 
	 * @param array $data 
	 * @return bool
	*/
	public function update_row($where = [], $data = []) {
		if ($where && $data) {

			$data['updated_at'] = date('Y-m-d H:i:s');
			foreach ($where as $column => $value) {
				$this->db->where($column, $value);
			}
			$this->db->limit(1);
			return $this->db->update($this->table, $data);
		}
		return false;
	}

    /**
	 * get_row function get the single row from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @return mixed
	*/
    public function get_row($where = [], $returnAsArray = false, $or_where = []) {
    	foreach ($where as $column => $value) {
    		$this->db->where($column, $value);
    	}
    	foreach ($or_where as $column => $value) {
    		$this->db->or_where($column, $value);
    	}

    	$user = $this->db->get($this->table)->row();

    	$this->getUserDetails($user);

    	return $user;
    }

    /**
	 * get_rows function get the multiple rows from the table that is define as property of the model class.
	 * @param array $where 
	 * @param array $or_where 
	 * @param string $raw_where 
	 * @return mixed
	*/
    public function get_rows($where = [], $or_where = [], $raw_where = null, $with_trashed = false) {

    	if ($where) {
    		foreach ($where as $column => $value) {
    			$this->db->where($column, $value);
    		}
    	}

    	if ($or_where) {
    		$this->db->group_start();
    		foreach ($or_where as $column => $value) {
    			$this->db->or_where($column, $value);
    		}
    		$this->db->group_end();
    	}

    	if ($raw_where) {
    		$this->db->where($raw_where);
    	}

    	$this->db->order_by($this->table.'.created_at', 'DESC');

    	$users = $this->db->get($this->table)->result();

    	if ($users) {
    		foreach ($users as $user) {
    			$this->getUserDetails($user);
    		}
    	}       

    	return $users;
    }

    /**
	 * add_rows function add the multiple rows in the table that is define as property of the model class.
	 * @param array $data 
	 * @return bool
	*/
    public function add_rows($data) {
    	$query = $this->db->insert_batch($this->table, $data); 

    	if ($query) {
    		return true;
    	} else {
    		return false;
    	}
    }

    private function getSubscribersUserInfo($channel_id) {
    	return $this->db->select('distinct(subscriber_id)')
    	->where('channel_id', $channel_id)
    	->get('subscribe')->result();
    }

    private function getSubscribedUserInfo($user_id) {
    	return $this->db->select('distinct(user_id)')
    	->where('subscriber_id', $user_id)
    	->join('channels', 'subscribe.channel_id = channels.id', 'left')
    	->get('subscribe')->result();
    }

    private function getClassRoomsInfo($user_id) {
    	return $this->db->select('distinct(classroom_id)')
    	->where(['classroom_users.user_id' => $user_id,'request_access'=>2])
    	->join('classrooms', 'classroom_users.user_id = classrooms.id', 'left')
    	->get('classroom_users')->result();
    }

    private function getRequestedClassRoomsInfo($user_id) {
    	return $this->db->select('distinct(classroom_id)')
    	->where(['classroom_users.user_id' => $user_id])
    	->join('classrooms', 'classroom_users.user_id = classrooms.id', 'left')
    	->get('classroom_users')->result();
    }

    private function getMyClassRoomsInfo($user_id) {
    	return $this->db->select('distinct(id)')
    	->where(['classrooms.user_id' => $user_id,'deleted_at' => null])
    	->get('classrooms')->result();
    }

    private function getUserDetails($user) {
    	if ($user) {
    		$subscribed_info = $this->getSubscribedUserInfo($user->id);
    		$classroom_info = $this->getClassRoomsInfo($user->id);
    		$my_classroom_info = $this->getMyClassRoomsInfo($user->id);
    		$my_requested_classroom_info = $this->getRequestedClassRoomsInfo($user->id);
    		$channel_id = getChannelId($user->id);

    		$subscriber_info = $channel_id ? $this->getSubscribersUserInfo($channel_id) : null;

    		$user->subscribed_user_ids = $subscribed_info ? array_column($subscribed_info, 'user_id') : [];
    		$user->subscriber_user_ids = $subscriber_info ? array_column($subscriber_info, 'subscriber_id') : [];
    		$user->classroom_ids = $classroom_info ? array_column($classroom_info, 'classroom_id') : [];
    		$user->my_classroom_ids = $my_classroom_info ? array_column($my_classroom_info, 'id') : [];
    		$user->my_requested_classroom_ids = $my_requested_classroom_info ? array_column($my_requested_classroom_info, 'classroom_id') : [];
    		$user->all_classroom_ids = array_merge($user->classroom_ids,$user->my_classroom_ids);
    	}
    }

    public function delete_row($where = []) {
    	if ($where) {
    		$data['deleted_at'] = date('Y-m-d H:i:s');
    		foreach ($where as $column => $value) {
    			$this->db->where($column, $value);
    		}
    		$this->db->limit(1);
    		return $this->db->update($this->table, $data);
    	}
    	return false;
    }

    /**
	 * Models for google authentication
	 * @return void
	 */
    public function Is_already_register($id) {
    	$this->db->where('oauth_uid', $id);
    	$query = $this->db->get($this->table);
    	if($query->num_rows() > 0)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }

    public function Update_user_data($data, $id) {
    	$this->db->where('oauth_uid', $id);
    	$this->db->update($this->table, $data);
    	return $this->db->where('oauth_uid', $id)->get($this->table)->row()->id;
    }

    public function Insert_user_data($data) {
    	$this->db->insert($this->table, $data);
    	return $this->db->insert_id();
    }
}