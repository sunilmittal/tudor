<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_sectors_model extends CI_Model {

	protected $table = 'user_sectors';

	/**
	 * add_row function add the row in the table that is define as property of the model class.
	 * @param array $data 
	 * @return mixed
	*/
	public function add_row($data, $needId = false) {
		$query = $this->db->insert($this->table, $data);

		if ($query) {
			return $needId ? $this->db->insert_id() : true;
		} else {
			return false;
		}
	}

	/**
	 * update_row function update the row in the table that is define as property of the model class.
	 * @param array $where 
	 * @param array $data 
	 * @return bool
	*/
    public function update_row($where = [], $data = []) {
    	if ($where && $data) {
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
    }

    /**
	 * get_row function get the single row from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @return mixed
	*/
    public function get_row($where = [], $returnAsArray = false, $or_where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }

        return $returnAsArray ? $this->db->get($this->table)->row_array() : $this->db->get($this->table)->row();
    }

    /**
	 * get_rows function get the multiple rows from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @param string $raw_where 
	 * @return mixed
	*/
    public function get_rows($where = [], $returnAsArray = false, $or_where = [], $raw_where = null) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }
        if ($raw_where) {
        	$this->db->where($raw_where);
        }

        return $returnAsArray ? $this->db->get($this->table)->result_array() : $this->db->get($this->table)->result();
    }

    /**
	 * add_rows function add the multiple rows in the table that is define as property of the model class.
	 * @param array $data 
	 * @return bool
	*/
    public function add_rows($data) {
		$query = $this->db->insert_batch($this->table, $data); 

		if ($query) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * delete_rows function delete the multiple rows in the table that is define as property of the model class.
	 * @param array $where 
	 * @return bool
	*/
	public function delete_rows($where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        return $this->db->delete($this->table);
    }
}