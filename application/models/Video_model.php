<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Video_model extends CI_Model {

	protected $table = 'videos';

	private function make_select_line($list = []) {
		return implode(',', array_map(function($element) {
			return $this->table.'.'.$element;
		}, $list));
	}

	private function getLikeDislikes($video_id) {
		return $this->db->select('resource_like_dislikes.user_id, resource_like_dislikes.type as type, resource_like_dislikes.is_liked')->where(['type' => 'video', 'resource_id' => $video_id])->get('resource_like_dislikes')->result();
	}


	private function getComments($video_id) {
		return $this->db->select('resource_comments.user_id, resource_comments.comment, resource_comments.created_at,  users.name as user_name, users.image')
			->join('users', 'resource_comments.user_id = users.id', 'left')
			->where(['type' => 'video', 'resource_id' => $video_id])->get('resource_comments')->result();
	}

	private function getUserAndChannel($channel_id) {
		return $this->db->select('channels.user_id, channels.name as channel_name, users.name as user_name, users.image, users.user_role')
			->join('users', 'channels.user_id = users.id', 'left')
			->where(['channels.id' => $channel_id])->get('channels')->row();
	}

	private function getCategories($category_ids = []) {
		if ($category_ids) {
			return $this->db->select('id, name, slug')->where_in('id', $category_ids)->get('category')->result_array();
		}
	}

	private function getVideoDetails($video) {
		$video->comments = $this->getComments($video->id);
        $video->likes = array_values(array_filter($this->getLikeDislikes($video->id), function ($likeDislike) {
    		return $likeDislike->is_liked;
    	}));
        $video->dislikes = array_values(array_filter($this->getLikeDislikes($video->id), function ($likeDislike) {
    		return !$likeDislike->is_liked;
    	}));

    	$video->user_and_channel = $this->getUserAndChannel($video->channel_id);
    	$video->category_ids = array_unique(array_filter([$video->category_id, $video->sub_category_id]));
		$video->category_names = $this->getCategories($video->category_ids);
	}


	/**
	 * add_row function add the row in the table that is define as property of the model class.
	 * @param array $data 
	 * @return mixed
	*/
	public function add_row($data, $needId = false) {
		$data['created_at'] = date('Y-m-d H:i:s');
		$query = $this->db->insert($this->table, $data);

		if ($query) {
			return $needId ? $this->db->insert_id() : true;
		} else {
			return false;
		}
	}

	/**
	 * update_row function update the row in the table that is define as property of the model class.
	 * @param array $where 
	 * @param array $data 
	 * @return bool
	*/
    public function update_row($where = [], $data = []) {
    	if ($where && $data) {

			$data['updated_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
    }

    /**
	 * get_row function get the single row from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @return mixed
	*/
    public function get_row($where = [], $returnAsArray = false, $or_where = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }

        $this->db->where($this->table.'.deleted_at', null);

        $video = $this->db->get($this->table)->row();

        if ($video) {
	        $this->getVideoDetails($video);
        }

        return $video;
    }

    /**
	 * get_rows function get the multiple rows from the table that is define as property of the model class.
	 * @param array $where 
	 * @param bool $returnAsArray 
	 * @param array $or_where 
	 * @param string $raw_where 
	 * @return mixed
	*/
    public function get_rows($where = [], $returnAsArray = false, $or_where = [], $raw_where = null, $with_trashed = false, $where_in = []) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        if ($or_where) {
        	$this->db->group_start();
	        foreach ($or_where as $column => $value) {
	            $this->db->or_where($column, $value);
	        }
        	$this->db->group_end();
        }
        
        if ($raw_where) {
        	$this->db->where($raw_where);
        }

        if ($where_in) {
            foreach ($where_in as $column => $value) {
                $this->db->where_in($column, $value);
            }
        }

        if (!$with_trashed) {
        	$this->db->where($this->table.'.deleted_at', null);
        }

        $this->db->order_by($this->table.'.created_at', 'DESC');

        $videos = $this->db->get($this->table)->result();

        if ($videos) {
            foreach ($videos as $video) {
            	$this->getVideoDetails($video);
            }
        }

        return $videos;
    }

    /**
	 * add_rows function add the multiple rows in the table that is define as property of the model class.
	 * @param array $data 
	 * @return bool
	*/
    public function add_rows($data) {
		$query = $this->db->insert_batch($this->table, $data); 

		if ($query) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * search function search rows in the table that is define as property of the model class.
	 * @return array of object or JSON
	*/
	public function search($where = [], $searchEle = [], $limit = 0, $offset = 0, $sort_by = [], $json = false, $group_by = false) {

        $response = [];

        extract($searchEle);
        if (empty($term)) $term = null;
		if (empty($category_id)) $category_id = null;
		if (empty($sub_category_id)) $sub_category_id = null;

        $raw_where = null;

		if($group_by){
			$this->db->group_by('channel_id'); 
		}

		$videos = collect($this->get_rows($where));

        if ($term) {
            $videos = $videos->reject(function ($element) use ($term) {
                return mb_strpos(strtolower($element->title), strtolower($term)) === false && mb_strpos(strtolower($element->description), strtolower($term)) === false;
            })->values();
        }

        if ($category_id) {
			$videos = $videos->filter(function ($element) use ($category_id) {
                return $element->category_id == $category_id;
            })->values();
		}
		
		if ($sub_category_id) {
			$videos = $videos->filter(function ($element) use ($sub_category_id) {
                return $element->sub_category_id == $sub_category_id;
            })->values();
        }

        if ($limit) {
            $videos = $videos->slice($offset, $limit)->values();
        }
		
        $response = $videos;
        if (!$json) {
            return $response;
        }
        json_response($response);
    }

    /**
	 * delete_row function update the row in the table that is define as property of the model class.
	 * @param array $where 
	 * @param array $data 
	 * @return bool
	*/
    public function delete_row($where = []) {
    	if ($where) {

			$data['deleted_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        $this->db->limit(1);
	        return $this->db->update($this->table, $data);
    	}
    	return false;
	}
	
	public function delete_rows($where = []) {
    	if ($where) {
			$data['deleted_at'] = date('Y-m-d H:i:s');
	        foreach ($where as $column => $value) {
	            $this->db->where($column, $value);
	        }
	        return $this->db->update($this->table, $data);
    	}
    	return false;
    }

    public function getPopularCategoryBasedOnVideosCount() {
        return $this->db->select('count(id) as total_videos, category_id')
        		->group_by('category_id')
        		->order_by('total_videos', 'DESC')
        		->get('videos')->result();
	}
	
	/**
     * get_rows function get the multiple rows from the table that is define as property of the model class.
     * @param array $where 
     * @param bool $returnAsArray 
     * @param array $or_where 
     * @param string $raw_where 
     * @return mixed
    */
    public function num_rows($where = [], $or_where = [], $raw_where = null) {
        foreach ($where as $column => $value) {
            $this->db->where($column, $value);
        }
        foreach ($or_where as $column => $value) {
            $this->db->or_where($column, $value);
        }
        if ($raw_where) {
            $this->db->where($raw_where);
        }

        return $this->db->get($this->table)->num_rows();
    }

	/**
	 * Get Videos Count Of A User.
	 */
	public function getChannelVideosCount($channel){
		$channel->total_videos = $this->db->where(array('channel_id'=>$channel->id,'deleted_at'=>NULL))->count_all_results($this->table);
	}
}