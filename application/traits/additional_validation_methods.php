<?php

trait AdditionalValidationMathods{
	/*
	 * file value and type check during validation
	*/
	public function file_check($value, $params)
	{
		list($fieldName, $allowedSize, $allowedExt) = explode('-', $params);
		$allowedExtArr = explode('|', $allowedExt);

		$allowed_mime_type_arr = array($allowedExtArr);
		$mime = get_mime_by_extension($_FILES[$fieldName]['name']);


		if (isset($_FILES[$fieldName]['name']) && $_FILES[$fieldName]['name'] != "") {
			$path_parts = pathinfo($_FILES[$fieldName]["name"]);
			$extension = $path_parts['extension'];
			if (in_array($extension, $allowedExtArr) && $_FILES[$fieldName]['size'] <= $allowedSize * 1000000) {
				return true;
			} else if (!in_array($extension, $allowedExtArr)) {
				$this->form_validation->set_message('file_check', 'Please select only ' . implode(', ', $allowedExtArr) . ' file for {field}.');
				return false;
			} else {
				$this->form_validation->set_message('file_check', 'The selected file should be less than ' . $allowedSize . ' MB.');
				return false;
			}
		} else {
			$this->form_validation->set_message('file_check', 'Please choose a file to upload.');
			return false;
		}
	}
	/*
	 * date value and type check during validation
	*/
	public function date_check($date)
	{
		$today = strtotime(date('Y-m-d'));
		if($today > strtotime($date)){
			$this->form_validation->set_message('date_check', 'The publish date must be a date after or equal to current date.');
			return false;
		}
		else{
			return true;
		}
	}
	/*
	 * unique email with soft delete
	*/
	public function soft_delete_email_check($email)
	{
		$this->db->select('*');
		$this->db->where(['email' => $email, 'deleted_at' => null]);
		$query = $this->db->get('users');
		if ($query->num_rows() > 0){
			$this->form_validation->set_message('soft_delete_email_check', 'The Email field must contain a unique value.');
			return false;
		}
		else{
			return true;
		}
	}
}