<?php if($notes) { ?>
	<?php foreach($notes as $note) { ?>
		<div class="notes-repeat">
			<div class="user-name">
				<a href="<?= ms_site_url('user/'.$note->user_id.'/profile') ?>">
					<h5><?= $note->user->user_name; ?>
					<span
					class="user-role-custom"><?= ($note->user->user_role==0) ? "Learner" : "Teacher"  ?></span>
				</h5>
				<span class="user-role-badge">
					<?php
					if ($note->user->image) { ?>
						<img
						src="<?= ms_site_url('uploads/avatars/'.$note->user->image) ?>">
					<?php } else { ?>
						<?= strtoupper($note->user->user_name[0]) ?>
					<?php }
					?>
				</span>
			</a>
		</div>

		<a href="<?= ms_site_url('notes/'.$note->slug) ?>">
			<div class="img-sec">
				<?php if ($note->image) { ?>
					<img src="<?= ms_site_url('uploads/notes/'.$note->image.'') ?>">
				<?php } else{  ?>
					<img src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
				<?php } ?>
			</div>
		</a>
		<div class="notes-right-part">
			<a href="<?= ms_site_url('notes/'.$note->slug) ?>">
				<div class="title custom-title-notes">
					<h5><?php echo $note->title; ?></h5>
				</div>
			</a>

			<div class="clearfix"></div>
			<div class="description">
				<p><?php echo generateReadMore($note->description, 'notes', $note->id); ?>
			</p>
		</div>
		<div class="date"><span> <i class="fa fa-calendar" aria-hidden="true"></i>
			<?= getDateInFormat($note->date) ?>
		</span>
	</div>
	<?php if(!$note->flag_exists && $note->user_id!=getLoggedInUserId()){ ?>
		<div class="flag-icon"><a data-toggle="modal" data-target="#noteFlageModal"
			data_note_id="<?= $note->id ?>" class="open_noteFlageModal"><i
			class="fa fa-flag" aria-hidden="true"></i> Flag</a></div>
		<?php } ?>
		<div class="notes-category">
			<span><i class="fa fa-list-alt" aria-hidden="true"></i>
				<?= getCategoryNamesLine($note) ?>
			</div>
		</div>
	</div>
<?php } ?>
<?php } ?>