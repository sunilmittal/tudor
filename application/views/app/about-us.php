<section class="about-page-bg">
  <div class="banner-sec about-banner about-banner-new">
    <div class="inner-sec">
      <div class="left-sec">
        <div class="left-cont">
          <h1 class="main-title"> 
            <!-- Tudor is the best environment for both educators and learners with quality
                        content provided for you by you. --> 
            Come help us reinvent                                                           online education.
            One lesson at a time. </h1>
          
          <!-- <a href="javascript:void(0);" class="cmn-btn">Get started</a> --> 
        </div>
      </div>
      <div class="right-sec">
        <!-- <div class="img-sec"><img src="<?= ms_Site_url('assets/images/frontend/our-mission.png') ?>" alt="Our Mission"></div> -->
      </div>
      <div class="clearfix"></div>
      <div class="arrow-down bounce"> <a class="scroll-down" href="#tudor-work">Scroll down <img
                        src="<?= ms_Site_url('assets/images/frontend/arrow-down.png') ?>" alt="Scroll Down"></a> </div>
    </div>
  </div>
</section>
<section id="tudor-work" class="quotes-block">
  <div class="tudor-work vision-sec new-about">
    <div class="cust-container">
      <div class="inner-sec">
        <div class="row">
          <div class="col-lg-6 Col-12">
            <h2>About Tudor Learning</h2>
            <p>Teaching is in the relationship business. That's the 
              idea that started the Tudor Learning website. We know 
              online has the potential to be the best learning experience
              possible.</p>
            <p> What it needed was a way to bring the teacher and
              the learner together. Now it just needs you.</p>
            <p> The Tudor 
              experience is all about getting learners the help they need
              and teachers the platform to make that a guarantee. </p>
          </div>
          <div class="col-lg-6 Col-12">
            <h2>Our Mission</h2>
            <p>We're not here to fix the education system because it's not 
              broken. Instead, we're here to modernize it. As the world 
              moves online, we want to ensure that education is not left
              behind. Taking a first-hand look into the needs of both teachers
              and students, we aim to create that same great engagement 
              online as in class, and more. At the same time we're making a 
              one-stop shop for everyone's educational needs, and we're
              doing so one lesson at a time.</p>
            <p> Join us on our journey to not just reinvent online education for ourselves, but also to reinvent ourselves for online education. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="discover-block about-new">
  <div class="custm-container text-center">
    <h2>We challenge you to make the best out of  online learning with Tudor Learning. </h2>
    <a href="<?= ms_Site_url('login') ?>" class="btn-outline-theme"> <span class="sign_in_btn"> Get Started </span> </a> 

     <p>Have a (non-academic) question for our team? Feel free to visit our <a href="<?= ms_Site_url('contact-us') ?>">Contact Us</a> page and we’ll clear anything up for you.</p>
  </div>


</section>


