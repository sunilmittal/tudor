<section class="blog-detail">
	<div class="custm-container">
		<div class="blog-detail-left">
			<div class="blog-heading-inner">
				<a href="<?= ms_Site_url('blog-overview') ?>" class="back-to-blogs">Back</a>
				<h1><?= @$blog->title ?></h1>
			</div>
			<div class="date-container">
				<div class="date-left">
					<p><span>By Tudor Team</span> - <?= date("F d, Y", strtotime(@$blog->date)) ?></p>
				</div>
				<div class="date-right">
					<p><span>Home</span> - <?= @$blog->category_name ?></p>
				</div>
			</div>

			<div class="blog-detail-img">
				<?php if ($blog->image) { ?>
					<img src="<?= ms_Site_url('admin/uploads/images/' . $blog->image) ?>" alt="<?= $blog->title ?>">
				<?php }else{ ?>
					<img src="<?= ms_Site_url('assets/images/frontend/blog-detail.png') ?>" alt="<?= $blog->title ?>">
				<?php } ?>
			</div>
			<figcaption><?= @$blog->photo_caption ?></figcaption>

			<div class="blog-detail-date">
				<p><?= date("F d, Y", strtotime(@$blog->created_at)) ?></p>
			</div>

			<?= $blog->content ?>

			<blockquote>"<?= $blog->quote_title ?>" <span>-<?= $blog->quote_author ?></span></blockquote>

			<h4>Overview</h4>
			<p><?= $blog->overview ?></p>

			<div class="social-icos social-icos-blog-detail">
				<h6>Share On :</h6>
				<p>
					<a onclick="share_facebook(this);" data-url="<?= ms_Site_url('blog-detail/'. $blog->slug) ?>" href="#" target="__blank"><i class="fa fa-facebook" aria-hidden="true"></i></a> 
				</p>
				<p>
					<a onclick="share_linkedin(this);" data-url="<?= ms_Site_url('blog-detail/'. $blog->slug) ?>" href="#" target="__blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a> 
				</p>
				<p><a onclick="share_twitter(this);" data-url="<?= ms_Site_url('blog-detail/'. $blog->slug) ?>" href="#" target="__blank"><i class="fa fa-twitter" aria-hidden="true"></i>
				</a>
			</p>
		</div>
	</div>

	<div class="blog-detail-right">
		<?php if($blogs){ ?>
			<div class="related-blogs-sidebbar">
				<h3>More Blogs For You</h3>
				<div class="related-blogs-sidebbar-inner">
					<ul>
						<?php foreach ($blogs as $blog) { ?>
							<li>
								<p><a href="<?= ms_Site_url('blog-detail/'. $blog->slug) ?>"><?= $blog->title ?></a></p>
								<div class="time-info">
									<p><a href="<?= ms_Site_url('subjects/'. getSubjectSlug($blog->subject_id)) ?>"><?= getSubjectName($blog->subject_id) ?></a> | <a href=""><?= $blog->reading_time ?></a></p>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		<?php } ?>
		<?php if($featured){ ?>
			<div class="related-blogs-sidebbar">
				<h3>Featured Posts</h3>
				<div class="related-blogs-sidebbar-inner">
					<ul>
						<?php foreach ($featured as $blog) { ?>
							<li>
								<p><a href="<?= ms_Site_url('blog-detail/'. $blog->slug) ?>"><?= $blog->title ?></a></p>
								<div class="time-info">
									<p><a href="<?= ms_Site_url('subjects/'. getSubjectSlug($blog->subject_id)) ?>"><?= getSubjectName($blog->subject_id) ?></a> | <a href="<?= ms_Site_url('blog-detail/'. $blog->slug) ?>"><?= $blog->reading_time ?></a></p>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		<?php } ?>
		<div class="add-space">
			<img src="<?= ms_Site_url('assets/images/frontend/add.png') ?>" alt="Advertisement">
		</div>
	</div>

</div>
</section>
<?php  $this->load->view('common/newsletter-blog'); ?>