<?php if(!$category_exist){ ?>
	<section class="blog-heading">
		<div class="custm-container">
			<div class="blog-heading-inner text-center wow fadeInUp">
				<h1>Our <span>Tips</span>, <span>Tricks</span>, and <span>Stories</span></h1>
				<h2>Stay in the know with a blog for every passionate <span>learner</span> and <span>teacher</span>. </h2>
			</div>
		</div>
	</section>

	<?php if($featured){ ?>
		<section class="blog-grid">
			<div class="custm-container">
				<div class="blog-inner-container">
					<?php foreach ($featured as $blog) { ?>
						<div class="blog-item">
							<div class="blog-pic">
								<a href="<?= ms_Site_url('blog-detail/'. $blog->slug) ?>">
									<?php if ($blog->thumbnail) { ?>
										<img src="<?= ms_Site_url('admin/uploads/images/' . $blog->thumbnail) ?>" alt="<?= $blog->title ?>">
									<?php }else{ ?>
										<img src="<?= ms_Site_url('assets/images/frontend/blog1.png') ?>" alt="<?= $blog->title ?>">
									<?php } ?>
								</a>
								<div class="blog-content">
									<div class="blog-tag">
										<span><a href="<?= ms_Site_url('blog-detail/' . $blog->slug) ?>"><?= $blog->category_name ?></a></span>
									</div>
									<h3><a href="<?= ms_Site_url('blog-detail/' . $blog->slug) ?>"><?= $blog->short_description ?></a></h3>
								</div>
							</div>
						</div>
					<?php } } ?>
					<div class="clearfix"></div>
				</div>
			</div>
		</section>
	<?php } ?>
	<?php if($blog_category){ ?>
		<section class="blog-categories">
			<div class="custm-container">
				<ul>
					<?php foreach ($blog_category as $category) { ?>
						<li class="<?= ($category->industry_sector_id == $category_exist) ? 'active' : ''  ?>" >
							<h4><a href="<?= ms_Site_url('blogs/' . $category->industry_sector_id) ?>"><?= $category->name ?></a></h4>
							<p><a href="<?= ms_Site_url('blogs/' . $category->industry_sector_id) ?>"><?= $category->sub_heading ?></a></p>
						</li>
					<?php } ?>
				</ul>
			</div>
		</section>
	<?php } ?>
	<section class="all-blogs">
		<div class="custm-container">
			<div class="row">
				<?php if($blogs){ ?>
					<?php foreach ($blogs as $blog) { ?>
						<div class="col-sm-6 col-lg-4">
							<div class="blog-all-item">
								<div class="realted-blog-pic">
									<a href="<?= ms_Site_url('blog-detail/'. $blog->slug) ?>">
										<?php if ($blog->thumbnail) { ?>
											<img src="<?= ms_Site_url('admin/uploads/images/' . $blog->thumbnail) ?>" alt="<?= $blog->title ?>">
										<?php }else{ ?>
											<img src="<?= ms_Site_url('assets/images/frontend/blog6.png') ?>" alt="<?= $blog->title ?>">
										<?php } ?>
									</a>
								</div>
								<div class="related-blog-content">
									<h3><?= $blog->title ?> </h3>
									<p><?= $blog->short_description ?></p>
									<a href="<?= ms_Site_url('blog-detail/' . $blog->slug) ?>" class="read-story">Read Story</a>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php }else{ ?>
					<div class="no-blogs"> No Blog Found </div>
				<?php } ?>
			</div>
		</div>
	</section>
	<?php  $this->load->view('common/newsletter-blog'); ?>
	<section class="ready-to-share">
		<div class="custm-container">
			<div class="ready-to-share-inner text-center">
				<h5>Ready to share what you learned from our blogs? <a href="<?= ms_Site_url('/') ?>">Join the Tudor Community</a> Today!</h5>
			</div>
		</div>
	</section>