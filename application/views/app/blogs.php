<section class="blog-categories">
    <div class="custm-container">
        <ul>
            <li class="active">
                <h4><a href="<?= ms_Site_url('blogs') ?>">Reads for Learners</a></h4>
                <p><a href="<?= ms_Site_url('blogs') ?>">Articles for getting the best out of learning</a></p>
            </li>

            <li>
                <h4><a href="<?= ms_Site_url('blogs') ?>">Reads for Teachers and Tutors</a></h4>
                <p><a href="<?= ms_Site_url('blogs') ?>">Tips to making everyday a great teaching experience </a></p>
            </li>

            <li>
                <h4><a href="<?= ms_Site_url('blogs') ?>">Special Picks</a></h4>
                <p><a href="<?= ms_Site_url('blogs') ?>">A bit of our best to get the day going </a></p>
            </li>
        </ul>
    </div>
</section>

<section class="all-blogs">
    <div class="custm-container">
        <div class="row">
            <div class="col-sm-6 col-lg-4">
                <div class="blog-all-item">
                    <div class="realted-blog-pic">
                        <a href="<?= ms_Site_url('blog-detail') ?>">
                            <img src="<?= ms_Site_url('assets/images/frontend/blog6.png') ?>" alt="Tudor Stories">
                        </a>
                    </div>
                    <div class="related-blog-content">
                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. incididunt ut labore et </h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                        <a href="<?= ms_Site_url('blog-detail') ?>" class="read-story">Read Story</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4">
                <div class="blog-all-item">
                    <div class="realted-blog-pic">
                        <a href="<?= ms_Site_url('blog-detail') ?>">
                            <img src="<?= ms_Site_url('assets/images/frontend/blog7.png') ?>" alt="Tudor Stories">
                        </a>
                    </div>
                    <div class="related-blog-content">
                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. incididunt ut labore et </h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                        <a href="<?= ms_Site_url('blog-detail') ?>" class="read-story">Read Story</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4">
                <div class="blog-all-item">
                    <div class="realted-blog-pic">
                        <a href="<?= ms_Site_url('blog-detail') ?>">
                            <img src="<?= ms_Site_url('assets/images/frontend/blog8.png') ?>" alt="Tudor Stories">
                        </a>
                    </div>
                    <div class="related-blog-content">
                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. incididunt ut labore et </h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                        <a href="<?= ms_Site_url('blog-detail') ?>" class="read-story">Read Story</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4">
                <div class="blog-all-item">
                    <div class="realted-blog-pic">
                        <a href="<?= ms_Site_url('blog-detail') ?>">
                            <img src="<?= ms_Site_url('assets/images/frontend/blog6.png') ?>" alt="Tudor Stories">
                        </a>
                    </div>
                    <div class="related-blog-content">
                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. incididunt ut labore et </h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                        <a href="<?= ms_Site_url('blog-detail') ?>" class="read-story">Read Story</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4">
                <div class="blog-all-item">
                    <div class="realted-blog-pic">
                        <a href="<?= ms_Site_url('blog-detail') ?>">
                            <img src="<?= ms_Site_url('assets/images/frontend/blog7.png') ?>" alt="Tudor Stories">
                        </a>
                    </div>
                    <div class="related-blog-content">
                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. incididunt ut labore et </h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                        <a href="<?= ms_Site_url('blog-detail') ?>" class="read-story">Read Story</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4">
                <div class="blog-all-item">
                    <div class="realted-blog-pic">
                        <a href="<?= ms_Site_url('blog-detail') ?>">
                            <img src="<?= ms_Site_url('assets/images/frontend/blog8.png') ?>" alt="Tudor Stories">
                        </a>
                    </div>
                    <div class="related-blog-content">
                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. incididunt ut labore et </h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                        <a href="<?= ms_Site_url('blog-detail') ?>" class="read-story">Read Story</a>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>

<section class="newsletter-blog">
    <div class="custm-container">
        <div class="newsletter-inner-blog">
            <div class="newsletter-left">
                <h4>Subscribe to stay updated!</h4>
                <p>We hope you are having a great experience with us. When you subscribe via email, we will notify you on relevant Content and updates about us. You may unsubscribe at any time.</p>
            </div>
            <div class="newsletter-right">
                <form>
                  <div class="email-sec">                    
                    <input type="email" placeholder="Enter your email here">
                    <button type="submit" class="send-btn"><img src="<?= ms_Site_url('assets/images/frontend/send.png') ?>" alt="Send"></button>
                </div>
            </form>                
        </div>
    </div>
</div>
</section>

