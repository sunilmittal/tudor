<section>
    <div class="banner-sec faq-sec">
        <div class="inner-sec">
            <div class="left-sec">
                <div class="left-cont">
                    <h1 class="main-title">
                        Community Guidelines and Policies
                    </h1>
                    <p>Our platform relies on your trust and responsibility to ensure everyone has that same, great
                        experience. As we try to make Tudor the best it can, we ask that you follow our community
                        guidelines and policies. We really do appreciate your understanding towards them, even if you
                        aren’t making videos or resource content.</p>

                </div>
            </div>
            <div class="right-sec">
                <div class="img-sec"><img src="<?= ms_site_url('assets/images/frontend/community-banner.png') ?>" alt="Community Guidelines and Policies"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section class="tudor-faq guidelines-page guideline-block">
    <div class="cust-container">
        <div class="row cgp-content">
            <div class="col-md-6 text-right">
                <h3 class="main-title">Appropriate Content</h3>
                <p>We love our content creators who work so hard to deliver their educational videos and resources to
                    the world, however it is important that all content made public is for educational purposes only. In
                    other words, content shared onto the site must be made for the viewers learning benefit.</p>
            </div>

            <div class="col-md-6 cgp-img">
                <img src="<?= ms_Site_url('assets/images/frontend/cgp-img-1.png') ?>" alt="our content creators">
            </div>
        </div>

        <div class="row cgp-content">
            <div class="col-md-6 cgp-img">
                <img src="<?= ms_Site_url('assets/images/frontend/cgp-img-2.png') ?>" alt="Copyright and Intellectual Property Rights">
            </div>
            <div class="col-md-6 text-left">
                <h3 class="main-title">Copyright and Intellectual Property Rights</h3>
                <p>Encouraging creativity, innovation, and expression are key to making the best learning content for
                    viewers.This is why we take intellectual property rights and copyright seriously and enforce it
                    properly. We ask that everyone is committed to respecting the copyright of others and has obtained
                    the proper authorization to use someone else’s content.Remember, embedding video content that is not
                    yours is a violation of our guidelines and terms of use.
                    If you feel that a copyright violation has been made, try getting in contact with the user to
                    resolve the issue. If no resolution is made or further problems arise, contact us and we’ll do what
                    we can to help.
                </p>
            </div>
        </div>

        <div class="row cgp-content">
            <div class="col-md-6 text-right">
                <h3 class="main-title">Hate Speech</h3>
                <p>As an educational platform that emphasizes growth in learning through community, we do not support nor endorse hate or violence towards other individuals within or outside of our platform. This can include discriminatory acts including, but not limited to race, nationality, sex/gender, age, caste, or religion. This type of content can be reported, and if necessary, will be taken down.</p>
            </div>

            <div class="col-md-6 cgp-img">
                <img src="<?= ms_Site_url('assets/images/frontend/cgp-img-3.png') ?>" alt="educational platform">
            </div>
        </div>

        <div class="row cgp-content">
            <div class="col-md-6 cgp-img">
                <img src="<?= ms_Site_url('assets/images/frontend/cgp-img-4.png') ?>" alt="Harassment and Abusive Behavior">
            </div>
            <div class="col-md-6 text-left">
                <h3 class="main-title">Harassment and Abusive Behavior</h3>
                <p>Harassment and abusive behavior is never tolerated on this platform or outside of it. This includes acts such as making violent threats to an individual orthreatening to release personally identifiable information.We take this very seriously and encourage users to let us know of these malicious instances. Everyone on Tudor deserves to feel safe and part of a larger, respecting community.
                </p>
            </div>
        </div>

        <div class="row cgp-content">
            <div class="col-md-6 text-right">
                <h3 class="main-title">Graphic content</h3>
                <p>We understand there are certain subjects (Biology, Anatomy) that are by nature graphic. For video content, be sure to include a notice about the graphic content that will be included in the title, description, or thumbnail.</p>
            </div>

            <div class="col-md-6 cgp-img">
                <img src="<?= ms_Site_url('assets/images/frontend/cgp-img-5.png') ?>" alt="Graphic content">
            </div>
        </div>

        <div class="row cgp-content">
            <div class="col-md-6 cgp-img">
                <img src="<?= ms_Site_url('assets/images/frontend/cgp-img-6.png') ?>" alt="Sharing Personal Information">
            </div>
            <div class="col-md-6 text-left">
                <h3 class="main-title">Sharing Personal Information</h3>
                <p>Nobody should ever share any private information with other users including where you live or other personally identifiable information. We want to protect everybody's information from getting stolen or misused. It is okay to have conversations with other users, just be careful what information is given out about yourself or other people.
                </p>
            </div>
        </div>

        <div class="row cgp-content">
            <div class="col-md-6 text-right">
                <h3 class="main-title">Younger Learners</h3>
                <p>Tudor is a community of users from a wide age range. Please be especially mindful of younger users who may be on the platform. If you know your content will or might be somewhat disturbing for younger viewers (i.e. A dissection for Anatomy), please make a note of it in the description, title, or start of the video. Additionally, it is important that all the other policies above are followed carefully to ensure the best experience for them as well.</p>
            </div>

            <div class="col-md-6 cgp-img">
                <img src="<?= ms_Site_url('assets/images/frontend/cgp-img-7.png') ?>"  alt="Younger Learners">
            </div>
        </div>

    </div>
</section>


<section>
    <div class="cgp-note-block">
        <div class="inner-sec row">
            <div class="col-md-12">
                <h1 class="main-title">
                    Consequences of a Violation
                </h1>
                <p>The consequences for violating our community guidelines generally depend on the severity of the violation as well as the history of the user making the violation. If a violation is made, we may issue a warning or take down the content that made the violation. We may also restrict a profiles ability to create or embed content or disable their account. </p>
                <p>Remember, the community guidelines apply to everyone with no exceptions. It is asked that you follow them in spirit and dignity so that we can make Tudor a great place to learn, teach, and thrive.</p>
                <p>If you feel that someone has violated any of our community guidelines/policies be sure to report the content or get in touch with us and we will review the issue. Please make sure to describe the issue in detail and include all parties involved.</p>

               
            </div>
        </div>
    </div>
</section>