<section>
    <div class="contact-sec contact-us-form-cnt">
        <div class="cust-container">
            <div class="inner-sec">
                <div class="left-sec">
                    <div class="left-img">
                        <img src="<?= ms_Site_url('assets/images/frontend/contact-left.png') ?>" alt="Let’s get in touch">
                    </div>
                </div>
                <div class="right-sec">
                    <div class="right-cont">
                        <h2 class="sub-heading">Contact us</h2>
                        <p>We’re happy to hear from you. 
                            Let’s get in touch.
                        </p>
                        <form class="outer-form" method="POST" id="contact_us" name="contact_us" enctype="multipart/form-data">
                            <ul>
                                <li> 
                                    <input class="form-control" type="text" name="name" placeholder="Name (optional)"> 
                                    <?= form_error('name', '<span class="error">', '</span>'); ?>
                                </li>
                                <li>
                                    <input class="form-control" type="mail" name="email" placeholder="Email*">
                                    <?= form_error('email', '<span class="error">', '</span>'); ?>
                                </li>
                                <li> 
                                    <input class="form-control" type="text" name="subject" placeholder="Subject">
                                    <?= form_error('subject', '<span class="error">', '</span>'); ?>
                                </li>
                                <li>
                                    <textarea class="form-control" name="message" placeholder="Message*"></textarea>
                                    <?= form_error('message', '<span class="error">', '</span>'); ?>
                                </li>
                                <li>
                                    <input type="hidden" id="token" name="token">
                                    <input type="submit" class="cmn-btn" name="submit" value="Send">
                                    <div class="clearfix"></div>
                                </li>
                            </ul>
                        </form>

                        <div class="email-content">
                            <span>Send us an email at <a href="mailto:support@tudorlearning.com" target="_blank">support@tudorlearning.com</a> </span>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>