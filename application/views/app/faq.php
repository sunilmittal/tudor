<section>
    <div class="banner-sec faq-sec">
        <div class="inner-sec">
            <div class="left-sec">
                <div class="left-cont">
                    <h1 class="main-title">
                        Frequently asked questions
                    </h1>
                    <h3>Here are some common questions about getting started that we have answered for you.</h3>
                </div>
            </div>
            <div class="right-sec">
                <div class="img-sec"><img src="<?= ms_site_url('assets/images/frontend/faq-banner.png') ?>" alt="Frequently asked questions"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section class="tudor-faq">
    <div class="cust-container">
        <div id="accordion" class="accordion">
            <div class="card mb-0">
                <div class="acc-block">
                    <div class="card-header">
                        <a class="card-title">Who can join Tudor?</a>
                    </div>
                    <div class="card-body" style="display: none;">
                        <p>Tudor is free to join and available to anyone looking to learnor teach about an academic subject. Our goal is to reach as many people as possible to join the Tudor community!</p>
                    </div>
                </div>

                <div class="acc-block">
                    <div class="card-header">
                        <a class="card-title">What can I do as a Teacher? </a>
                    </div>
                    <div class="card-body" style="display: none;">
                        <p>Teachers can upload educational videos as well as articles to share with the community. Your goal is to add your own personal touch to your educational content and engage with your learners! </p>
                    </div>
                </div>

                <div class="acc-block">
                    <div class="card-header">
                        <a class="card-title">Do I have to be a teacher to add videos?</a>
                    </div>
                    <div class="card-body">
                        <p>You do not have to be a teacher or certified expert on any subject to add content onto the platform. We do suggest you are knowledgeable enough about what you are teaching and encourage the use of citations and sources when necessary.</p>
                    </div>
                </div>

                <div class="acc-block">
                    <div class="card-header">
                        <a class="card-title">What can I do as a Learner?</a>
                    </div>
                    <div class="card-body">
                        <p>As a learner you can view videos and articles created by other users on the platform. You can also create and share your own articles or notes with the community. If you ever have a question, ask in the comment sections under every article and video and wait for someone to answer!</p>
                    </div>
                </div>

                <div class="acc-block">
                    <div class="card-header">
                        <a class="card-title">How do I start embedding videos?</a>
                    </div>
                    <div class="card-body">
                        <p>It’s easy! Create a YouTube account using your Gmail. Upload your video and set its visibility to unlisted. Copy the video ID (the variation of characters at the end of the link) and paste it on the Add Video page where it asks for the YouTube video ID. Include the general information bout the video and you are ready to embed! For more detailed instructions, visit our Getting Started page!</p>
                    </div>
                </div>

                <div class="acc-block">
                    <div class="card-header">
                        <a class="card-title">What type of content can I add on Tudor?</a>
                    </div>
                    <div class="card-body">
                        <p>You can add content that is made for the learning benefit of users on the platform. There are two modes of doing this, videos, which only Tudor Teachers can add, and Tudor Notes which is available for all users on the platform!</p>
                    </div>
                </div>

                <div class="acc-block">
                    <div class="card-header">
                        <a class="card-title">Can I embed someone else’s video onto the platform?</a>
                    </div>
                    <div class="card-body">
                        <p>Only if you have received the proper permission to do so. Otherwise, embedding content that is not yours is a violation of our Community Guidelines and can be taken down.</p>
                    </div>
                </div>

                <div class="acc-block">
                    <div class="card-header">
                        <a class="card-title">Is Tudor associated with YouTube?</a>
                    </div>
                    <div class="card-body">
                        <p>Tudor is not associated with YouTube, however they do host all of Tudor’s video content. Because of YouTube, Tudor is able to deliver a greater experience with high quality videos.</p>
                    </div>
                </div>  

                <div class="acc-block">
                    <div class="card-header">
                        <a class="card-title">Can Teachers also follow Learners in addition to other Teachers?</a>
                    </div>
                    <div class="card-body">
                        <p>Learners are able to subscribe to both user types. Teachers however can only subscribe to other Teachers.</p>
                    </div>
                </div>  
            </div>
        </div>

        <div class="faq-contact">
            <p><strong>If you still have any questions about Tudor, feel free to contact us via our <a href="<?= ms_Site_url('contact-us') ?>" target="_blank">Contact Us</a> page or send us an email via <a href="mailto:support@tudorlearning.com" target="_blank">support@tudorlearning.com</a>. Our team will be glad to help you out!</strong></p>
        </div>
    </div>
</section>