<section>
    <div class="banner-sec faq-sec">
        <div class="inner-sec">
            <div class="left-sec">
                <div class="left-cont">
                    <h1 class="main-title">
                      Getting Started With Tudor
                    </h1>
                    <!--<p>Best environment for everyone to learn together that allow you to grow and expand your knowledge to use their talents to fulfill their potential</p>-->
                    
                </div>
            </div>
            <div class="right-sec">
                <div class="img-sec"><img src="<?= ms_site_url('assets/images/frontend/gettingstarted.png') ?>" alt="Getting Started With Tudor"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section class="tudor-faq started-steps">
    <div class="cust-container">
        <div class="row">
            <div class="col-md-6 started-steps-cell">
                <img src="<?= ms_site_url('assets/images/frontend/started-step-1.png') ?>" alt="Creating you profile">
                <h3 class="main-title">Creating Your Profile</h3>
                <p>Your bio is what helps other people know who you are and what you know. Be sure to include your favorite subjects that you are knowledgeable about and looking to share with the community. </p>
                <p>Feel free to put down anything that helps the community know a little bit about you, but do not share any personal information such as your address or other personally identifiable information.
                </p>
            </div>

            <div class="col-md-6 started-steps-cell">
                <img src="<?= ms_site_url('assets/images/frontend/started-step-2.png') ?>" alt="Sharing Videos">
                <h3 class="main-title">Sharing Videos</h3>
                <p>Videos offer learners the flexibility to play and pause lessons to fully grasp the concepts and ideas being taught. It allows self-study and independence in what and how we learn. Tudor gives you a place to share your knowledge in video format and share the benefits of video learning with the world.</p>
                <p>Getting your lessons on screen is easy. If you are currently teaching through distance communication try recording your lessons. You can also try recording yourself from your classroom or workspace.
                </p>
                <p>To start uploading videos, you must be signed up as a teacher/tutor. For help with uploading videos, <a href="<?= ms_Site_url('sharing-videos') ?>">click here</a>.</p>
            </div>

            <div class="col-md-6 started-steps-cell">
                <img src="<?= ms_site_url('assets/images/frontend/started-step-3.png') ?>" alt="Creating Notes">
                <h3 class="main-title">Creating Notes</h3>
                <p>Tudor gives all users the opportunity to show what they know through Tudor Notes. In Notes you can share useful with information as well as create short educational articles that can help others learn. Anyone can share their best learning tips, resources, or insights for any subject. </p>
                <p>Sharing your notes is simple. Just click add new note on the homepage and start typing. For articles, be sure to stay relevant and informative.
                </p>
                <p>For more info and tips on using Tudor Notes <a href="<?= ms_Site_url('sharing-notes') ?>">click here</a>.</p>
            </div>
        </div>
    </div>
</section>