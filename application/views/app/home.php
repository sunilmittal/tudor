<section class="cust-banner-block">
    <div class="banner-sec custm-container">
        <div class="inner-sec row">
            <div class="left-sec col-lg-5 cont-mob">
                <div class="left-cont">
                    <h1 class="main-title">
                        A Smarter Classroom
                    </h1>
                    <!-- <a href="<?= ms_Site_url('videos') ?>" class="cmn-btn">Get started</a> -->
                    <p>Moving the classroom online one student at a time. Make the best out of your lessons by giving students the lead.</p>
                    <a href="<?= ms_Site_url('register') ?>" class="btn-outline-theme">
                        <span class="sign_in_btn"> Get started </span>
                    </a>
                    <span class="student-learner-tagline">Tudor is for teachers who want more learning for less teaching. Turn any lesson, review, or set of notes into a forum for class discussions.</span>
                </div>
            </div>
            <div class="right-sec col-lg-7">
                <div class="img-sec"><img src="<?= ms_Site_url('assets/images/frontend/banner-feature-img.png') ?>"
                        alt="Online teaching and learning"></div>
            </div>
            <div class="clearfix"></div>
            <!-- <div class="arrow-down bounce">
                <a class="scroll-down" href="#tudor-work">Scroll down <img
                        src="<?= ms_Site_url('assets/images/frontend/arrow-down.png') ?>" alt="Scroll Down"></a>
            </div> -->
        </div>
    </div>
</section>

<section class="features-block features-teachers-block common-typo">
    <div class="custm-container">
        <div class="row feature-row">
            <div class="col-md-6 cont-mob">
                <h2>Teachers and Tutors</h2>
                <p>Bring Tudor to your class with a video makeover.</p>
                <ul class="custom-check-list">
                    <li>Share your video lessons and articles quick and easy</li>
                    <li>Create more classroom engagement</li>
                    <li>Monitor student’s learning</li>
                </ul>
                <a href="<?= ms_Site_url('tutors') ?>" class="btn-outline-theme">
                    <span class="sign_in_btn"> Learn More </span>
                </a>
                <!-- <?php if(!isUserLoggedIn()){ ?>
                        <a href="<?= ms_site_url('login?redirect_url=videos/add') ?>" class="btn-outline-theme">upload videos</a>
                    <?php } else if (isUserLoggedIn() && userRoleIsTeacher()) { ?>
                        <a href="<?= ms_site_url('videos/add') ?>" class="btn-outline-theme">upload videos</a>
                    <?php } ?> -->
            </div>

            <div class="col-md-6 left-img">
                <img src="<?= ms_Site_url('assets/images/frontend/feature-block-image.png') ?>" alt="">
            </div>
        </div>

        <div class="row feature-row learner-student-feature">
            <div class="col-md-6 right-img">
                <img src="<?= ms_Site_url('assets/images/frontend/feature-cell-1.png') ?>" alt="">
            </div>
            <div class="col-md-6 learner-student-content cont-mob">
                <h2>Learners and Students</h2>
                <p>Learn (and teach) with your friends. Make learning a more fun and engaging experience. </p>
                <ul class="custom-check-list">
                    <li> Engage with your friends as you learn</li>
                    <li> Create notes and academic articles to share with the world</li>
                    <li> Join a community that can help you learn and grow</li>
                </ul>
                <a href="<?= ms_Site_url('learners') ?>" class="btn-outline-theme">
                    <span class="sign_in_btn"> Learn More </span>
                </a>
<!-- 
                <a href="<?= ms_Site_url('videos') ?>" class="btn-outline-theme">Get Started</a>
         -->
            </div>
        </div>

        <div class="categories-row row">
            <h2>Categories</h2>
            <div class="categories-inner">
            <div class="categories-cell col-md-4 col-lg-4 col-sm-6">
                <a href="https://www.tudorlearning.com/subjects/arts"><div class="cat-inner">
                    <h2>Arts</h2>
                    <span class="cat-ico art"></span>
                </div></a>
                
            </div>
            <div class="categories-cell col-md-4 col-lg-4 col-sm-6">
                <a href="https://www.tudorlearning.com/subjects/social-studies"><div class="cat-inner">
                    <h2>Social Studies</h2>
                    <span class="cat-ico Social-Studies"></span>
                </div></a>
                
            </div>
            <div class="categories-cell col-md-4 col-lg-4 col-sm-6">
                <a href="https://www.tudorlearning.com/subjects/mathematics"><div class="cat-inner">
                    <h2>Mathematics</h2>
                    <span class="cat-ico Mathematics"></span>
                </div></a>
                
            </div>
            <div class="categories-cell col-md-4 col-lg-4 col-sm-6">
                <a href="https://www.tudorlearning.com/subjects/science"><div class="cat-inner">
                    <h2>Science</h2>
                    <span class="cat-ico Science"></span>
                </div></a>
                
            </div>
           <div class="categories-cell col-md-4 col-lg-4 col-sm-6">
               <a href="https://www.tudorlearning.com/subjects/world-languages"><div class="cat-inner">
                    <h2>World Languages </h2>
                    <span class="cat-ico World-Languages"></span>
                </div></a>
                
            </div>
        </div>
</div>
    </div>
</section>

<section class="features-block features-learn-block common-typo">
    <div class="custm-container">
        <div class="row feature-row learn-pace-block">
            <div class="col-md-6 learn-pace-block-left cont-mob">
                <div class="learn-pace-block-content">
                    <h2>Learn at your own pace, not anyone else’s.</h2>
                    <p>You don’t need to catch up, you’re just taking your own path to get there.</p>
                </div>
            </div>

            <div class="col-md-6 left-img">
                <img src="<?= ms_Site_url('assets/images/frontend/learn-feature-img.png') ?>" alt="">
            </div>
        </div>

        <div class="row feature-row choice-block">
            <div class="col-md-6 right-img">
                <img src="<?= ms_Site_url('assets/images/frontend/learning-feature-img.png') ?>" alt="">
            </div>
            <div class="col-md-6 learner-student-content cont-mob">
                <div class="learner-student-content-wrap">
                      <h2>Putting teachers and learners under one platform.</h2>
                      <p>To give the best learning experience, everyone has to be on the same page. </p>
                </div>
          

            </div>
        </div>

        <div class="row feature-row knoledge-feature-block">
            <div class="col-md-6 knoledge-feature-content cont-mob">
                <div class="knoledge-content-wrap">
                <h2>Give your knowledge <span>a shot.</span></h2>
                <p>Everyone has a favorite subject they know best. Why not give your knowledge a test run?</p>
</div>
            </div>

            <div class="col-md-6 left-img">
                <img src="<?= ms_Site_url('assets/images/frontend/knoledge-feature.png') ?>" alt="">
            </div>
        </div>

    </div>
</section>

<section class="discover-block">
    <div class="custm-container text-center">
         <div class="img-sec">
                <img src="<?= ms_Site_url('assets/images/frontend/discover-block-banner.png') ?>" alt="">
            </div>
        <h2>Help every student reach their learning potential by giving them the ability to <br>
        engage with the class and share what they know. Turn your lessons into vibrant <br>
        conversations by sharing your Tudor lesson today. Ready to find your spot in our classroom?
        </h2>
        <a href="<?= ms_Site_url('login') ?>" class="btn-outline-theme">
            <span class="sign_in_btn"> Get Started </span>
        </a>
    </div>
</section>

