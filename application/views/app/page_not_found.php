<div class="main-wrapper">
<?php $this->view('common/nav-menu-inner'); ?>
<div class="wrapper civils-banner profile-pages-small" style="background-image:url(<?php echo ms_site_url('assets/images/frontend/job-banner.jpg'); ?>">
    <?php $this->view('common/nav-menu-sidebar'); ?>
    <div class="red-con-sec red-con-sec-civils">
        <div class="bcs-sec">
            <h2 data-wow-delay=".3s" class="title wow fadeInUp ">
                404 - Page Not Found<span>.</span>
            </h2>
        </div>
    </div>
    <div class="cust-container">
        <div class="civil-banner-pattern"></div>
    </div>
</div>
<section class="we-are-sec we-are-sec-mob profile-page-content">
    <div class="cust-container content-page">
        <h3>Sorry Page Not Found</h3>
        <p>The page you are looking for cannot be found.</p>
    </div>
</section>