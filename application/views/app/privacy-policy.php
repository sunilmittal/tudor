<section>
    <div class="banner-sec faq-sec">
        <div class="inner-sec">
            <div class="left-sec">
                <div class="left-cont">
                    <h1 class="main-title">
                         Privacy Policy
                    </h1>
                    <p>Thanks for visiting our Tudor! Please take a moment to read our Privacy Policy before continue using our website.</p>
                    <p> This Privacy Policy describes our policies and procedures on the collection, use, disclosure, and sharing of your personal information when you use this website.
                    </p>
                    
                </div>
            </div>
            <div class="right-sec">
                <div class="img-sec"><img src="<?= ms_site_url('assets/images/frontend/privacy-policy-banner.png') ?>"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section class="tudor-faq guidelines-page privacy-t-c-page">
    <div class="cust-container">
    <h3 class="main-title">collection of personal information</h3>
    <p>Generally, we collect your personal information on a voluntary basis. However, if you decline to provide certain personal information that is marked mandatory, you may not be able to access certain Services or we may be unable to fully respond to your inquiry.</p>
    <p>Information we collect directly. We collect information that you provide to us, such as:</p>
     <ul>
        <li>when you provide contact information, and send other personal information to us.</li>
        <li>when you complete or submit a form through Tudor, for example, "Contact Us" or “More Information” forms.</li>
     </ul>

    <h3 class="main-title">how we use your personal information</h3>
    <p>While the purposes for which we may process personal information will vary depending upon the circumstances, in general we use personal information for the purposes set forth below. Where GDPR or other relevant laws apply, we have set forth the legal bases for such processing in parenthesis </p>
    <ol>
        <li><strong>Providing support and services:</strong> including, for example, to provide products and services you request (and send related information), operate Tudor; to communicate with you about your access to and use of our services; to respond to your inquiries; to provide troubleshooting, fulfil your requests and provide technical support; and for other customer service and support purposes. </li>

        <li><strong>Analysing and improving our business:</strong> including to better understand how users access and use Tudor, to evaluate and improve our services and business operations, and to develop new features, offerings, and services; to conduct surveys, and other evaluations, such as customer satisfaction surveys; and for other research and analytical purposes. </li>

        <li><strong>Defending our legal rights:</strong> including to manage and respond to actual and potential legal disputes and claims, and to otherwise establish, defend or protect our rights or interests, including in the context of anticipated or actual litigation with third parties.  </li>

        <li><strong>Complying with legal obligations: </strong> including to comply with the law, our legal obligations and legal process, such warrants, subpoenas, court orders, and regulatory or law enforcement requests.   </li>
     </ol>
    
     <h3 class="main-title">disclosure of personal information</h3>
     <p>We may disclose the personal information that we collect about you as set forth below or as otherwise described at the time of collection or sharing.</p>
     <ol>
        <li><strong>Service providers: </strong> We may disclose personal information with third-party service providers who use this information to perform services for us, such as hosting providers, auditors, advisors, consultants, and customer service and support providers. </li>

        <li><strong>Third Parties:  </strong> We may employ other companies and individuals to perform functions on our behalf. Examples include fulfilling orders for products or services, delivering packages, sending postal mail and e-mail, removing repetitive information from customer lists, analysing data, providing marketing assistance, processing payments, transmitting content, and providing customer service. These third-party service providers have access to personal information needed to perform their functions, but may not use it for other purposes. </li>

        <li><strong>Business transfers: </strong> We may disclose or transfer personal information as part of any actual or contemplated merger, sale, and transfer of our assets, acquisition, financing or restructuring of all or part of our business, bankruptcy or similar event, including related to due diligence conducted prior to such event where permitted by law. </li>

        <li><strong>Legally required: </strong> We may disclose personal information if we are required to do so by law (e.g., to law enforcement, courts or others, e.g., in response to a subpoena or court order). </li>

        <li><strong>Protect our rights:  </strong> We may disclose personal information where we believe it necessary to respond to claims asserted against us or, comply with legal process (e.g. warrants), enforce or administer our agreements and terms, for fraud prevention, risk assessment, investigation, and to protect the rights, property, or safety of us, our clients and customers or others. </li>
     </ol>
     
     <h3 class="main-title">how we protect your information</h3>
     <p>Our security measures do not guarantee that your information will not be accessed, disclosed, altered or destroyed by breach of such firewalls and secure server software. By using our Service, you acknowledge that you understand and agree to assume these risks</p>
     
     <h3 class="main-title">cookies</h3>
     <p>Cookies are small text files that a website transfers to your hard drive to store and sometimes collect information about your usage of websites, such as time spent on the websites, pages visited, language preferences, and other anonymous traffic data. You can control the way in which cookies are used by altering your browser settings. You may refuse to accept cookies by activating the setting on your browser that allows you to reject cookies. However, if you select such a setting, this may affect the functioning of Tudor. Unless you have adjusted your browser setting so that it will refuse cookies, our system will issue cookies when you access or log on to our Service.</p>
    <p>We may use the following types of cookies:</p>
    <ul>
        <li>Security: These cookies allow us to secure access to your account.</li>
        <li>Preference: These cookies are used to store your preferences like language choice and display of search results.</li>
        <li>Analytics: We track traffic patterns so we can identify popular content and potential problems.</li>
        <li>Features: We may also use cookies to split some users into test groups to test new features.</li>
        <li>Advertising: We use non-identifiable information about you to show you advertising on our services and third-party sites.</li>
        </li>
    </ul>

    <h3 class="main-title">third party analytics tools</h3>
     <p>Our website uses automated devices and applications operated by third parties, such as Google Analytics, which uses cookies and similar technologies to collect and analyze information about use of the websites and report on activities and trends. This service may also collect information regarding the use of other websites, apps and online resources. </p>
     <p> We gather statistical information about use of the Services in order to continually improve their design and functionality, understand how they are used and assist us with resolving questions regarding them.</p>

     <h3 class="main-title">do-not-track signals</h3>
     <p>Please note that our websites do not recognize or respond to any signal which your browser might transmit through the so-called 'Do Not Track' feature your browser might have. </p>

     <h3 class="main-title">security</h3>
     <p>We use technical, administrative, and physical controls in place to help protect Personal information from unauthorized access, use, and disclosure. Even so, despite our reasonable efforts, no security measure is ever perfect or impenetrable. In the event that the security of your account has been compromised, please immediately notify us by visiting (“Contact Us”). </p>

     <h3 class="main-title">do not sell my data</h3>
     <p>Except as otherwise stated in this Privacy Policy, we do not sell, trade, rent or otherwise share for marketing purposes your Personal Information with third parties without your consent.</p>

     <h3 class="main-title">data retention</h3>
     <p>We will retain your personal information for the period necessary to fulfill the purposes outlined in this privacy notice unless a longer retention period is required or permitted by law. </p>

     <h3 class="main-title">CCPA privacy rights</h3>
     <p>Under the CCPA, among other rights, California consumers have the right to:
        Request that a business that collects a consumer's personal data disclose the categories and specific pieces of personal data that a business has collected about consumers.
    </p>

    <h3 class="main-title">CCPA privacy rights</h3>
    <p>Under the CCPA, among other rights, California consumers have the right to:
    Request that a business that collects a consumer's personal data disclose the categories and specific pieces of personal data that a business has collected about consumers.
    </p>
        <ul>
            <li>Request that a business deletes any personal data about the consumer that a business has collected
            </li>
            <li>Request that a business that sells a consumer's personal data, not sell the consumer's personal data.</li>
        </ul>


    <h3 class="main-title">gdpr data protection rights</h3>
    <p>We would like to make sure you are fully aware of all of your data protection rights. Every European user is entitled to the following:
    </p>
     <p><strong>1. Right to access</strong></p>
     <p>If you can prove your identity, you have the right to obtain information about the processing of your data. Thus, you have the right to know the purposes of the processing, the categories of data concerned, the categories of recipients to whom the data are transmitted, the criteria used to determine the data retention period, and the rights that you can exercise on your data.</p>

     <p><strong>2. Right to rectification of your personal data</strong></p>
     <p>Inaccurate or incomplete personal data may be corrected.</p>

     <p><strong>3. Right to erasure (or “right to be forgotten”)</strong></p>
     <p>You also have the right to obtain the erasure of your personal data under the following assumptions:</p>
     <ul>
         <li>Your personal data are no longer necessary for the intended purposes.</li>
         <li>You withdraw your consent to the processing and there is no other legal ground for processing.</li>
         <li>You have validly exercised your right of opposition.</li>
         <li>Your data has been illegally processed.</li>
         <li>Your data must be deleted to comply with a legal obligation.</li>
     </ul>
     <p>The deletion of data is mainly related to visibility; it is possible that the deleted data are still temporarily stored.</p>

     <h3 class="main-title">4. Right to limitation of processing</h3>
     <p>In certain cases, you have the right to request the limitation of the processing of your personal data, especially in case of dispute as to the accuracy of the data, if the data are necessary in the context of legal proceedings or the time required to verify that you can validly exercise your right to erasure.
    </p>

    <h3 class="main-title">5. Right to object</h3>
     <p>You have the right to object at any time to the processing of your personal data for direct marketing purposes. We will stop processing your personal data unless it can demonstrate that there are compelling legitimate reasons for the processing which prevail over your right to object.
    </p>

    <h3 class="main-title">6. Right to data portability</h3>
     <p>You have the right to obtain any personal data which you have provided us in a structured, commonly used and machine-readable format. You are then free to transfer this data to a similar service provider.
    </p>

    <h3 class="main-title">7. Right to withdraw your consent</h3>
     <p>You may withdraw your consent to the processing of your personal data at any time, for example for personalized marketing communication purposes.</p>
     <p>If you make a request, we have one month to respond to you. If you would like to exercise any of these rights, please contact us at </p>

     <h3 class="main-title">third-party websites</h3>
     <p>The Services may contain links to third-party services. When you click on those links, you will go to a third-party website where you will be subject to that service’s privacy policy or similar statement and terms of use, and we encourage you to read that policy statement. We are not responsible for the privacy practices of other services, and we expressly disclaim any liability for their actions, including actions related to the use and disclosure of personal information by those third parties.</p>

     <h3 class="main-title">changes to this policy</h3>
     <p>We may amend this Policy at any time. If we make any material change in how we collect, use, disclose, or otherwise process personal information, we may not post a notice regarding such change on the Services. </p>

     <h3 class="main-title">log files</h3>
     <p>Most browsers collect certain information, such as your IP address, device type, screen resolution, operating system version and internet browser type and version. This information is gathered automatically and stored in log files.</p>

     <h3 class="main-title">contact us</h3>
     <p>If you believe we have used your Personal Data in a way that is not consistent with this Privacy Statement, or if you have any questions or comments about this Privacy Statement, please contact us at <a href="mailto:support&#64;tudorlearning.com" target="_blank">support&#64;tudorlearning.com</a></p>
    </div>
</section>