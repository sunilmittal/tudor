<section>
    <div class="banner-sec faq-sec sharing-video-banner">
        <div class="inner-sec">
            <div class="left-sec">
                <div class="left-cont">
                    <h1 class="main-title">
                        Start sharing your knowledge through Tudor Notes!
                    </h1>
                    <p>Whether it’s a short informative article or your class notes, it will be sure to benefit somebody who is stuck on a homework problem or has a gap in their knowledge. Here are a few tips you should use when creating your notes in order to provide the best learning experience for learners.</p>
                </div>
            </div>
            <div class="right-sec">
                <div class="img-sec"><img src="<?= ms_site_url('assets/images/frontend/sharing-notes-banner.png') ?>"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section class="tudor-faq guidelines-page guideline-block sharing-notes-block">
    <div class="cust-container">
        <div class="row cgp-content">
            <div class="col-md-12">
                <h3 class="main-title">Stay Relevant and Informative</h3>
                <p>Always be sure to have a specific purpose and topic which your article will revolve around. Try not to include unnecessary information that could confuse a learner. Also be sure to be as specific as possible so that your article is as informative as it can be.</p>
            </div>
        </div>


        <div class="row cgp-content">
             <div class="col-md-12">
                <h3 class="main-title">Be Organized and Simple</h3>
                <p>If you think you will be sharing alot of information, such as when transcribing your personal notes, portion your information into paragraphs or sections so that it is easier to read and follow. Avoid creating walls of text for your readers.
                </p>
            </div>
        </div>

        <div class="row cgp-content">
              <div class="col-md-12">
                <h3 class="main-title">Create Quality Content</h3>
                <p>You may think that there are already plenty of articles available about what you will teach. Although that is true, your goal is to provide your own perspective and view on a topic. That means don’t just explain the topic but provide your own personal touch to it. The goal of Tudor is to promote teachers to provide a new angle on a topic, not reiterate what is already out there.</p>
            </div>
        </div>

        <div class="row cgp-content">
              <div class="col-md-12">
                    <h3 class="main-title">Teach With Examples</h3>
                    <p>Your personal touch matters, and the examples you provide do as well. Providing examples and scenarios in your content allow readers to visualize your lesson and grasp the key points better.
                    </p>
            </div>
        </div>

        <div class="row cgp-content">
               <div class="col-md-12">
                    <h3 class="main-title">Engage</h3>
                    <p>There are comment sections under every Note where users can ask questions or provide feedback for your content. Be sure to get involved in your Note’s comment section and help answer any questions someone may have about your work.</p>
            </div>
        </div>

        <div class="row cgp-content">
             <div class="col-md-12">
                <h3 class="main-title">Stay Updated</h3>
                <p>Before creating a Note, make sure all information is updated and accurate to prevent misinformation. Additionally, go back to your older Notes every once in a while to make sure all information is up to date for learners.
                </p>
                <p><strong>After reading these tips, you can start creating and sharing your own Notes with the community!</strong></p>
            </div>
        </div>

    </div>
</section>