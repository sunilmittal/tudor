<section>
    <div class="banner-sec faq-sec sharing-video-banner">
        <div class="inner-sec">
            <div class="left-sec">
                <div class="left-cont">
                    <h1 class="main-title">
                        Sharing videos on Tudor is easy! Just follow the steps below
                    </h1>
                    <!--<p>Best environment for everyone to learn together that allow you to grow and expand your knowledge to use their talents to fulfill their potential</p>-->
                    
                </div>
            </div>
            <div class="right-sec">
                <div class="img-sec"><img src="<?= ms_site_url('assets/images/frontend/sharing-video-banner.png') ?>"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section class="sharing-video">
    <div class="cust-container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-timeline">
                    <div class="timeline">
                        <div class="col-md-7">

                            <div class="timeline-content">
                                <div class="timeline-icon"><i class="fa fa-user" aria-hidden="true"></i>
                                </div>
                                <h3 class="title">Step 1</h3>
                                <p class="description">
                                Create an account in YouTube using your Gmail. In the top right of the YouTube homepage click the Create icon and then click “Upload video” where you will then be asked to select the video from your computer. 
                                </p>
                            </div>
                          
                        </div>
                        <div class="col-md-5 steps-img-block">
                             <img src="<?= ms_site_url('assets/images/frontend/step-1.png') ?>">
                        </div>
                    </div>

                    <div class="timeline">
                        
                    <div class="col-md-5 steps-img-block">
                             <img src="<?= ms_site_url('assets/images/frontend/step-2.png') ?>">
                        </div>
                        <div class="col-md-7">
                            <div class="timeline-content">
                                <div class="timeline-icon"><i class="fa fa-video-camera" aria-hidden="true"></i></div>
                                <h3 class="title">Step 2</h3>
                                <p class="description">
                                Once you have selected your video, add the title. The description and thumbnail are not necessary yet as you will be adding it on Tudor in the last step. For audience, choose “Yes, it’s made for kids”.  In visibility, set your video to unlisted. It is important that you have it to this setting on all your videos when uploading. When finished, copy your link and <a href="#">Click Save</a>. 
                                </p>
                            </div>
                        </div>

                    </div>

                    <div class="timeline">
                        <div class="col-md-7">
                            <div class="timeline-content">
                                <div class="timeline-icon"><i class="fa fa-file-video-o" aria-hidden="true"></i></div>
                                <h3 class="title">Step 3</h3>
                                <p class="description">
                                On Tudor, click "<a href="#">Add Video</a>" at the top of the page. Paste the copied link into the YouTube Video ID space and delete the "<a href="#">https://youtu.be/</a>""  portion of the link, leaving behind the last 11 characters, or the YouTube video ID.
                                </p>
                            </div>
                        </div>

                        <div class="col-md-5 steps-img-block">
                             <img src="<?= ms_site_url('assets/images/frontend/step-3.png') ?>">
                        </div>
                    </div>

                    <div class="timeline">
                             
                        <div class="col-md-5 steps-img-block">
                             <img src="<?= ms_site_url('assets/images/frontend/step-4.png') ?>">
                        </div>

                        <div class="col-md-7">
                            <div class="timeline-content">
                                <div class="timeline-icon"><i class="fa fa-list-ul" aria-hidden="true"></i></div>
                                <h3 class="title">Step 4</h3>
                                <p class="description">
                                Give your video a title and select the category and subcategory that best fits the content. Include a description about the video as well as a thumbnail picture and you are ready to add video and share your knowledge with the world! 
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>