<section>
    <div class="spread-banner banner-sec">
        <div class="inner-sec row">
            <div class="col-md-12">
                <h1 class="main-title">
                More than learning with more of you!
                </h1>
                <p>Share the value of learning through community and help bring Tudor to more people.</p>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="service-sec spread-feature-block">
        <div class="cust-container">
            <div class="inner-sec row">
                <div class="col-md-6">
                    
                    <div class="spread-feature-cell">
                    <img src="<?= ms_Site_url('assets/images/frontend/buildthecommunity.png') ?>" alt="Build the Community">
                        <h2>Build the Community</h2>
                        <p>With more people on Tudor means more diverse ideas and content that make up the amazing community.</p>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="spread-feature-cell">
                        <img src="<?= ms_Site_url('assets/images/frontend/bringfriends.png') ?>" alt="Bring Friends Who Could Use Help">
                        <h2>Bring Friends Who Could Use Help</h2>
                        <p>Give a friend the resources and people that can help them learn and understand what they are missing in class.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="spread-Invite-block">
        <div class="cust-container">
            <div class="inner-sec row">
                <div class="col-md-5">
                    <div class="spread-feature-cell">
                        <img src="<?= ms_Site_url('assets/images/frontend/tellfriends.png') ?>" alt="Teach What They Know">
                    </div>
                </div>

                <div class="col-md-7">
                    <div class="spread-feature-cell"> 
                         <h2>Give Others the Chance to Teach What They Know</h2>
                        <p>Invite a colleague or friend who is passionate about the subjects they teach or have an interest in sharing their expertise.</p>
                        <div class="social-share">
                        <h3>Be sure to check us out on social media!</h3>
                            <ul class="social-network social-circle">
                                <li><a href="https://www.facebook.com/tudorlearning-106000797822171" class="icoFacebook" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.instagram.com/tudorlearning/" class="icoRss" title="Rss" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="https://twitter.com/Tudor_learning" class="icoTwitter" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
    
                            </ul>				
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>