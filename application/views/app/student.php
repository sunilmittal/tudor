<section>
    <div class="banner-sec faq-sec tutor-sec1 student-sec-banner-new">
        <div class="inner-sec">
            <div class="left-sec">
                <div class="left-cont">
                    <h1 class="main-title">
                       Watch lessons, discuss with your friends. Simple.
                    </h1>
                    <a href="http://localhost/tudor/login?redirect_url=videos/add" class="cmn-btn">Get started</a>
                    
                </div>
            </div>
            <div class="right-sec">
                <div class="img-sec"><img src="<?= ms_site_url('assets/images/frontend/learner-main-bg.png') ?>" alt="Redefine how you learn"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section class="features-block features-teachers-block common-typo feature-block-new">
    <div class="custm-container">
        <div class="row feature-row">
            <div class="col-md-6 left-img">
                <img src="<?= ms_Site_url('assets/images/frontend/learner-2.png') ?>" alt="">
            </div>
            <div class="col-md-6 cont-mob">
                 <div class="knoledge-content-wrap">
                <h2>Your learning. Your pace.</h2>
                <p>Watch your teacher’s video lessons, rewind, skip, rewind again. The only time limit on Tudor is the one you set.  </p>
               
               </div>
            </div>

            
        </div>

        <div class="row feature-row">
          
            <div class="col-md-6 learner-student-content cont-mob">
                 <div class="knoledge-content-wrap">
                <h2>Connect with the community </h2>
                <p>Learning is more fun when it’s with a community. Turn your notes and knowledge into a way of meeting other students. </p>
               
               </div>

            </div>
              <div class="col-md-6 right-img">
                <img src="<?= ms_Site_url('assets/images/frontend/learner-3.png') ?>" alt="">
            </div>
        </div>
         <div class="row feature-row">
            <div class="col-md-6 left-img">
                <img src="<?= ms_Site_url('assets/images/frontend/learner-4.png') ?>" alt="">
            </div>
            <div class="col-md-6 cont-mob">
                 <div class="knoledge-content-wrap">
                <h2>Collaborate on lessons</h2>
                <p>Be the first in your class to ask the important questions. Work with friends to understand the lesson and leave insightful comments for others to see.  </p>
               
               
            </div>

            
        </div>
</div>
        <div class="row feature-row">
          
            <div class="col-md-6 learner-student-content cont-mob">
                 <div class="knoledge-content-wrap">
                <h2>Get your teachers back in action</h2>
                <p>Tudor will give your teacher a better idea of where you may be in understanding the lesson. It will also give them a chance to help you outside the classroom. </p>
               
               </div>

            </div>
              <div class="col-md-6 right-img">
                <img src="<?= ms_Site_url('assets/images/frontend/learner-5.png') ?>" alt="">
            </div>
        </div>
    </div>
</section>

<section class="discover-block discover-new">
    <div class="custm-container text-center">
         
        <h2>Ready to reach your learning potential?
        </h2>
        <a href="<?= ms_Site_url('register') ?>" class="btn-outline-theme">
            <span class="sign_in_btn"> Get Started </span>
        </a>
    </div>
</section>

