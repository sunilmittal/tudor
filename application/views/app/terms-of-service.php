<section>
    <div class="banner-sec faq-sec">
        <div class="inner-sec">
            <div class="left-sec">
                <div class="left-cont">
                    <h1 class="main-title">
                       Terms and Conditions
                    </h1>
                    <p>Welcome to Tudor!Thank you for visiting our website!</p>
                </div>
            </div>
            <div class="right-sec">
                <div class="img-sec"><img src="<?= ms_site_url('assets/images/frontend/terms-condition.png') ?>" alt="Terms and Conditions - Tudor Learning"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section class="tudor-faq guidelines-page privacy-t-c-page">
    <div class="cust-container">

    <h3 class="main-title">Terms and Conditions    </h3>
    <p>When you use our services, you're agreeing to our terms. So please take a few minutes to read over the below mentioned Terms and Conditions before using our website.</p>                
    <strong class="note-t-c">If you do not agree with any term or provision of these terms and conditions, please exit this site immediately. Please be advised that your continued use of this site or the products or information provided thereby shall indicate your consent and agreement to these terms and conditions.</strong>  
    <p>In consideration of each member or user’s (each, a “Member” or “You” or “User”) access to and use of the Site, we require every members and user to act with integrity, to our rules for the Site, and to abide by these Terms and Conditions and each other rule, regulation or other policy of Tudor.</p>  
    
    <h3 class="main-title">Proprietary Rights</h3>
    <p>As between you and us, we own, solely and exclusively, all rights, title and interest in and to the Site, all the content (including, for example, audio, photographs, illustrations, graphics, other visuals, video and copy), software, code, data, and the look and feel, design and organization of the Site, and all materials and content related to our programs even if the materials or content are not accessed through the Site. Your use of the Site does not grant to you ownership of any content, software, code, data or materials you may access on the Site.</p>
    <p>The Company grants a limited, non-assignable, non-transferable, revocable license to use such materials solely to utilize such products or services. Such license will terminate when you no longer use the services.</p>
    <p>Except as permitted through the services or as otherwise permitted by us in writing, your license does not include the right to:</p>
    <p>We reserve the right to modify, suspend, or discontinue the services (in whole or in part) at any time, with or without notice to you. Any future release, update, or other addition to functionality of the services will be subject to these Terms, which may be updated from time to time. You agree that we will not be liable to you or to any third party for any modification, suspension, or discontinuation of the services or any part thereof.</p>

    <h3 class="main-title">Guidelines For Users</h3>
    <ol>
        <li>Users takes full responsibility for all information thathe/sheprovides on the Tudor website and must indemnify Tudor in relation to any liability incurred by Tudoras a result of such information.</li>
        <li>A User shall indemnify Tudor for all claims and liabilities arising out of any use by the Instructor of the Tudor website, including any associated costs and expenses incurred, whether direct or indirect.</li>
        <li>A User must not be in any way be abusive about another User on the Tudor website.</li>
        <li>A User who is under 18 years of age must have consent from a parent or a guardian to register. 

        </li>
        <li>Tudor is not responsible for any disputes regarding parental consent or any problems that a User or a patent/guardian has experienced with another User.</li>
        <li>Tudor does not offer any guarantee that any information provided on the Tudor website is accurate and correct.</li>
        <li>A User cannot act as an agent to promote the services or opportunities of a company.</li>
        <li>A Learner must not publish any abusive comments about an Instructor or another Learner on the Tudor website or any other place. This includes defamatory or derogatory comments.</li>
        <li>A Learner must use his/her own judgment about the services of Users detailed on the Website.</li> 
    </ol>
   
    <h3 class="main-title">Not Professional Advice</h3>
    <p>Any information provided on our Platform is not proposed to be a substitute for professional advice. You should rely on your own assessment, examination and evaluation as to the accuracy of any information made available within this program or via this Platform.</p>

    <h3 class="main-title">Registration Information </h3>
    <p>You may be required to provide information about yourself in order to register for and/or use certain services. You agree that any such information shall be accurate. You may also be asked to choose a user name and password. You are entirely responsible for maintaining the security of your user name and password and agree not to disclose such to any third party.</p>

    <h3 class="main-title">Prohibited Use</h3>
    <p>You agree that You will not use, and will not permit any end user to use, the services to:</p> 
        <ul class="roman-ul">
            <li> modify, disassemble, decompile, prepare derivative works of, reverse engineer or otherwise attempt to gain access to the source code of the services.
            </li>
            <li>knowingly or negligently use the services in a way that abuses, interferes with, or disrupts Tudor’s networks, Your accounts, or the services.</li>
            <li> engage in activity that is illegal, fraudulent, false, or misleading.</li> 
            <li> transmit through the services any material that may infringe the intellectual property or other rights of third parties.</li>
            <li>build or benchmark a competitive product or service, or copy any features, functions or graphics of the services.</li>
            <li>use the services to communicate any message or material that is harassing, libelous, threatening, obscene, indecent, would violate the intellectual property rights of any party or is otherwise unlawful, that would give rise to civil liability, or that constitutes or encourages conduct that could constitute a criminal offense, under any applicable law or regulation.</li> 
            <li>upload or transmit any software, Content or code that does or is intended to harm, disable, destroy or adversely affect performance of the services in any way or which does or is intended to harm or extract information or data from other hardware, software or networks of Tudor or other users of services.</li></li>
            <li>engage in any activity or use the services in any manner that could damage, disable, overburden, impair or otherwise interfere with or disrupt the services, or any servers or networks connected to the services or Tudor's security systems. </li>
            <li>use the services in violation of any Tudor policy or in a manner that violates applicable law, including but not limited to anti-spam, export control, privacy, and anti-terrorism laws and regulations and laws requiring the consent of subjects of audio and video recordings, and You agree that You are solely responsible for compliance with all such laws and regulations.</li>
        </ul>

        <h3 class="main-title">Limitations on use</h3>
        <p>You may not reproduce, resell, or distribute the services or any reports or data generated by the services for any purpose unless you have been specifically permitted to do so under a separate agreement with Tudor. You may not offer or enable any third parties to use the services purchased by You, display on any website or otherwise publish the services or any Content obtained from a service (other than Content created by You) or otherwise generate income from the services or use the services for the development, production or marketing of a service or product substantially similar to the services.</p>

        <h3 class="main-title">User Content</h3>
        <p>Certain features of our website (including but not limited to creating video courses, audio clip or tutorial) may allow you to publish or send content that can be viewed by others (“User Content”). You agree that we are not liable for User Content that is provided by others. We have no duty to pre-screen User Generated Content. You agree and acknowledge that Tudor has no control over and assumes no responsibility for the UserContent and by using the Tudor Platform, you expressly relieve Tudor from any and all liability arising from the UserContent.</p>
        <p>You represent and warrant that:</p>
        <ol>
            <li>you own all rights in and to your User Content and Name and/or Likeness and/or have obtained appropriate rights and permissions from any and all other persons and/or entities who own, manage or otherwise claim any rights with respect to such User Content and Name and/or Likeness, such that you have all necessary licenses, rights, consents and permissions to publish the User Content and Name and/or Likeness and to grant the rights granted herein, including permission from all person(s) appearing and/or performing in your User Content.</li>
            <li>the Licensed Parties’ use of your User Content and Name and/or Likeness as described herein will not violate the rights of any third party, or any law, rule or regulation, including but not limited to consumer protection, copyright, trademark, patent, trade secret, privacy, publicity, moral, proprietary or other rights and laws.</li>
            <li>the User Content and Name is not confidential, libelous, defamatory, obscene, pornographic, abusive, indecent, threatening, harassing, hateful, or offensive or otherwise unlawful; and
            You agree to pay all royalties and other amounts owed to any person or entity due to your Submission of any User Submissions to the Service.
            <p>You understand that Tudor shall have the right to delete, edit, modify, reformat, excerpt, or translate any materials, content or information submitted by you; and that all information publicly posted or privately transmitted through the Platform is the sole responsibility of the person from which such content originated and that Tudor will not be liable for any errors or omissions in any content; and that Tudor cannot guarantee the identity of any other users with whom you may interact in the course of using the Service.</p>
            <p>Tudor does not endorse any User Submission. Tudor cannot guarantee the authenticity of any data which users may provide about themselves or the persons or entities they are involved or affiliated with or employed by. You acknowledge that all Content accessed by you using the Service is at your own risk and you will be solely responsible for any damage or loss to any party resulting therefrom.</p>
            <p>You hereby fully release, discharge and agree to hold the Licensed Parties, and any person or entity acting on their behalf, harmless from any liability related in any way to the Licensed Parties’ use of your User Content and your Name and/or Likeness.
            </p>
        </li>
</ol>

        <h3 class="main-title">External Links </h3>
        <p>You may be able to link from the Site to third party websites and third party websites may link to the Site ("Linked Sites"). You acknowledge and agree that we have no responsibility for the content, products, services, advertising or other materials which may be provided by or through Linked Sites, even if they are owned or run by our affiliates. Links to Linked Sites do not constitute an endorsement or sponsorship by us of such websites or the information, content, products, services, advertising, code or other materials presented on or through such websites. Any reliance on the contents of a third-party website is done at your own risk and you assume all responsibilities and consequences resulting from such reliance.</p>

        <h3 class="main-title">Indemnification</h3>
        <p>You agree to defend, indemnify and hold us and our directors, officers, employees and agents harmless from any and all claims, liabilities, costs and expenses, including reasonable attorneys' fees, arising in any way from any content or other material you place on the Site or submit to us, or your breach or violation of the law or of these Terms and Conditions. We reserve the right, at our own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, and in such case, you agree to cooperate with our defense of such claim.</p>

        <h3 class="main-title">Using Our Services</h3>
        <p>To ensure smooth virtual sessions, you may requiremultiple compatible devices, high speed internet access. You are solely responsible for all computer hardware and other equipment and all fees for services (such as internet service and wireless services) required for access and use of our online content. You may regularly need to update our applications to use our services. You acknowledge and agree that such system requirements, which may be changed from time to time, are your responsibility and are to be procured at your own cost.</p>

        <h3 class="main-title">Advertisements</h3>
        <p>Any correspondence or business dealings with, or the participation in any promotions of, advertisers located on or through our Services, which may include the payment and/or delivery of such related goods and/or Services, and any such other term, condition, warranty and/or representation associated with such dealings, are and shall be solely between you and any such advertiser. Moreover, you herein agree that Tudor shall not be held responsible or liable for any loss or damage of any nature or manner incurred as a direct result of any such dealings or as a result of the presence of such advertisers on our website.</p>

        <h3 class="main-title">Disclaimer Of Warranties </h3>
        <ul class="roman-ul">
        <li>
           the site, including but not limited to all services, products, content, functions and materials contained or available on the site, is provided "as is," "as available", without warranty of any kind, either express or implied, including but not limited to any warranty regarding uptime or uninterrupted access, availability, accuracy, or usefulness, and any warranties of title, non-infringement, merchantability or fitness for a particular purpose. we hereby disclaim any and all such warranties, express and implied. we also assume no responsibility, and will not be liable for, any damages to, or viruses that may infect, your computer equipment, mobile device, or other property on account of your access to or use of the site or your downloading of any materials from the site. if you are dissatisfied with the site, your sole remedy is to discontinue using the site.</li>
           <li> we do not:
                <ul class="roman-ul">
                    <li>guarantee the accuracy, completeness, or usefulness of any third-party content on the site or any verification services done on our instructors or instructors, or </li>
                    <li> adopt, endorse or accept responsibility for the accuracy or reliability of any opinion, advice, or statement made by any instructor or instructor or any party that appears on the site. under no circumstances will we be responsible or liable for any loss or damage resulting from your reliance on information or other content posted on or available from the site. </li>
                </ul>
            </li>
        </ul>

            <h3 class="main-title">limitation of liability </h3>
            <p>No event, including but not limited to negligence, will we or any of our directors, officers, employees, agents or content or service providers (including instructors and instructors) (collectively, the "protected entities") be liable for any indirect, special, incidental, consequential, exemplary or punitive damages arising from, or directly or indirectly related to, the use of, or the inability to use, the site or the content, materials, products, services, and functions related to the site, your provision of information via the site, lost business or lost sales, even if such protected entity has been advised of the possibility of such damages. some jurisdictions do not allow the limitation or exclusion of liability for incidental or consequential damages so some of the above limitations may not apply to certain users to the extent required by applicable law.</p>

            <h3 class="main-title">Termination </h3>
            <p>We may terminate, change, suspend or discontinue any aspect of the Site or the Site's products or services at any time. We may restrict, suspend or terminate your access to the Site and/or its products or services if we believe you are in breach of these Terms and Conditions or applicable law, you are a repeat infringer of intellectual property rights, or for any other reason without notice or liability.</p>

            <h3 class="main-title">Modifications </h3>
            <p>We reserve the right, at our sole discretion, to modify any portion of these Terms and Conditions at any time. Changes in these Terms and Conditions will be effective when posted. Your continued use of the Site and/or the products or services offered on or through the Site after any changes to these Terms and Conditions are posted will be considered acceptance of those changes.</p>

            <h3 class="main-title">Waivers </h3>
            <p>Our failure to act with respect to a breach of these Terms and Conditions by you or others does not waive our right to act with respect to that breach or subsequent similar or other breaches.</p>

            <h3 class="main-title">Force Majeure </h3>
            <p>Neither party hereto shall be responsible for delays or failures in performance resulting from acts beyond its reasonable control and without its fault or negligence. Such excusable delays or failures may be caused by, among other things, strikes, lock-out, riots, rebellions, accidental explosions, floods, storms, acts of God and similar occurrences.</p>

            <h3 class="main-title">Governing Law And Jurisdiction</h3>
            <p>Any claim, action, lawsuit or proceeding arising out of or related to this website and the services and products provided, shall be instituted exclusively in the federal courts of the United States of America and the user hereof irrevocably submits to the exclusive jurisdiction of such courts in any claim, action, lawsuit or proceeding, and waives any objection based on improper venue.</p>

            <h3 class="main-title">Dmca Takedown Notice Procedure</h3>
            <p>Tudor will always respect the intellectual property of others, and we ask that all of our users do the same. With regards to appropriate circumstances and at its sole discretion, Tudor may disable and/or terminate the accounts of any user who violates our TOS and/or infringes the rights of others. If you feel that your work has been duplicated in such a way that would constitute copyright infringement, or if you believe your intellectual property rights have been otherwise violated, you should provide to us the following information:</p>

            <h3 class="main-title">Dmca Takedown Notice Procedure</h3>
            <ol>
                <li>	The electronic or the physical signature of the individual that is authorized on behalf of the owner of the copyright or other intellectual property interest.</li>
                <li>	A description of the copyrighted work or other intellectual property that you believe has been infringed upon.</li>
                <li>	A description of the location of the site which you allege has been infringing upon your work.</li>
                <li>	Your physical address, telephone number, and email address.</li>
                <li>	A statement, in which you state that the alleged and disputed use of your work is not authorized by the copyright owner, its agents or the law.</li>
                <li>And finally, a statement, made under penalty of perjury, that the aforementioned information in your notice is truthful and accurate, and that you are the copyright or intellectual property owner, representative or agent authorized to act on the copyright or intellectual property owner’s behalf.</li>
            </ol>

            <h3 class="main-title">Monitoring</h3>
            <p>Tudor will monitor user activity as it has a duty of care to the copyright owners and requires this data for reporting and other uses. Additionally, Tudor will apply a variety of technologies to the service to ensure the copyrights of the courses, and the copyrights created by Tudor are not misused, stolen or abused.</p>

            <h3 class="main-title">Copyright</h3>
            <p>Copyright © 2020 Tudor. All rights reserved. All materials presented on this site are copyrighted and owned by us, or other individuals or entities as designated. Any republication, retransmission, reproduction, downloading, storing or distribution of all or part of any materials found on this site is expressly prohibited.</p>

            <h3 class="main-title"> Contact</h3>
            <p>For more information regarding our Terms and Conditions, please send us an email at:<a href="mailto:support&#64;tudorlearning.com">support&#64;tudorlearning.com</a></p>
    </div>
</section>