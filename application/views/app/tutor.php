<section>
    <div class="banner-sec faq-sec tutor-sec1 tutor-sec-banner-new">
        <div class="inner-sec">
            <div class="left-sec">
                <div class="left-cont">
                    <h1 class="main-title">
                    Your new classroom  
                    </h1>
                    <p>Start taking advantage of peer to peer learning and simplify your teaching experience.</p>
                    <a href="http://localhost/tudor/login?redirect_url=videos/add" class="cmn-btn">Get started</a>
                    
                </div>
            </div>
            <div class="right-sec">
                <div class="img-sec"><img src="<?= ms_site_url('assets/images/frontend/teacher1-new.png') ?>" alt="Your new classroom"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section>
    <div class="peer-sec">
        <div class="cont-sec">
            <h1 class="main-title">
                    Peer learning creates more confidence and independence in learning
             </h1>
        </div>
    </div>
</section>
<section class="features-block features-teachers-block common-typo feature-block-new">
    <div class="custm-container">
        <div class="row feature-row">
            <div class="col-md-6 left-img">
                <img src="<?= ms_Site_url('assets/images/frontend/tutor-2.png') ?>" alt="">
            </div>
            <div class="col-md-6 cont-mob">
                 <div class="knoledge-content-wrap">
                <h2>Deliver lessons from anywhere, anytime</h2>
                <p>Tudor makes it so that learning has no set time or place. Simply upload a video or note and notify your students. </p>
               
               </div>
            </div>

            
        </div>

        <div class="row feature-row">
          
            <div class="col-md-6 learner-student-content cont-mob">
                 <div class="knoledge-content-wrap">
                <h2>Give your students a more YOU experience</h2>
                <p>Learning is more fun and effective with video lessons that have a touch of what makes you. Share engaging lessons that will make your students want to participate in the discussions. </p>
               
               </div>

            </div>
              <div class="col-md-6 right-img">
                <img src="<?= ms_Site_url('assets/images/frontend/tutor-3.png') ?>" alt="">
            </div>
        </div>
         <div class="row feature-row">
            <div class="col-md-6 left-img">
                <img src="<?= ms_Site_url('assets/images/frontend/tutor-4.png') ?>" alt="">
            </div>
            <div class="col-md-6 cont-mob">
                 <div class="knoledge-content-wrap">
                <h2>Connect with motivated teachers just like you</h2>
                <p>Teacher’s can also get in on the action. Join the Tudor community and may be learn a bit from a peer yourself.  </p>
               
               
            </div>

            
        </div>
</div>
        <div class="row feature-row">
          
            <div class="col-md-6 learner-student-content cont-mob">
                 <div class="knoledge-content-wrap">
                <h2>Reinforcing the learning environment</h2>
                <p>Learning happens best when everyone is on the same page. Our community guidelines will ensure both teachers and students stay safe and clear of harm on Tudor. </p>
               
               </div>

            </div>
              <div class="col-md-6 right-img">
                <img src="<?= ms_Site_url('assets/images/frontend/tutor-5.png') ?>" alt="">
            </div>
        </div>
    </div>
</section>

<section class="discover-block discover-new">
    <div class="custm-container text-center">
         
        <h2>Ready to get your students collaborating?
        </h2>
        <a href="<?= ms_Site_url('login') ?>" class="btn-outline-theme">
            <span class="sign_in_btn"> Get Started </span>
        </a>
    </div>
</section>