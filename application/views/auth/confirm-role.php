<section>
	<div class="contact-sec reset-page login-page register-form-cnt">
		<div class="cust-container">
			<div class="inner-sec">
				<div class="right-sec">
					<div class="right-cont">
						<h4>Please Select Your Role </h4>
						<form class="outer-form" method="POST" action="confirm-role" id="register_form">
							<ul>
								<li class="tal">
									<div class="radio-btn">
										<input type="radio" class="check_user_role" value="0" name="user_role" id="user_type">
										<label for="user_type">Learner</label>
									</div>
									<div class="radio-btn">
										<input type="radio" class="check_user_role" value="1" name="user_role" id="teacher_type">
										<label for="teacher_type">Teacher</label>
									</div>
									<?= form_error('user_role', '<span class="error">', '</span>'); ?>
									<div>
										<p id="check_mesasge"></p>
									</div>
								</li>

								<li>
									<input type="submit" class="cmn-btn" name="submit" value="submit">
									<div class="clearfix"></div>
								</li>
							</ul>
						</form>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</section>