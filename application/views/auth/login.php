<section>
	<div class="contact-sec reset-page login-page login-from-cnt">
		<div class="cust-container">
			<div class="inner-sec">
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-success text-center"><?= $this->session->flashdata('success') ?></div>
				<?php } else if ($this->session->flashdata('error')) {?>
					<div class="alert alert-danger text-center"><?= $this->session->flashdata('error') ?></div>
				<?php } ?>
				<div class="right-sec">
					<div class="right-cont">
						<h4>Sign in using email address </h4>
						<form class="outer-form" method="POST" id="login_form">
							<ul>
								<li> <input class="form-control" type="email" value="<?= set_value('email') ?>" name="email" required placeholder="Email Address"> 
									<?= form_error('email', '<span class="error">', '</span>'); ?>
								</li>
								<li> <input class="form-control" type="password" value="<?= set_value('password') ?>" name="password" required placeholder="Password"> 
									<?= form_error('password', '<span class="error">', '</span>'); ?>
								</li>
								<li class="hw forger-wrap">
									<div class="checkbox">
										<input type="checkbox" name="remember" id="remember">
										<label for="remember">Remember me</label>
									</div>
									<a class="forget" href="#"> Forgot your password? </a>
								</li>

								<li>
									<input type="submit" class="cmn-btn" name="submit" value="Sign in">
									<div class="clearfix"></div>
								</li>
								<li>
									<div class="loginor"><span>or</span></div>
									<?= '<div class="google-login" align="center">'.$login_button . '</div>'; ?>
								</li>
								<li class="dnt-have-acc-wrap">
									<p>Don’t have an account? <a class="signup-popup" href="<?= ms_site_url('register') ?>"> Sign up here </a></p>
								</li>
							</ul>
						</form>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</section>
<div id="forget-popup" class="signin-popup">
	<div class="inner-sec">
		<div class="logo"><a href="index.html">
			<img src="<?= ms_site_url('assets/images/frontend/logo.png') ?>">
		</a>
	</div>
	<h4>Forgot Password</h4>
	<?php echo $this->session->flashdata('msg'); ?>
	<form action="<?= ms_site_url('forgot-password') ?>" id="forgot_password_form" method="POST">
		<ul>
			<li> <input class="form-control" type="email" name="email" placeholder="Enter Email Address" required> </li>
			<li>
				<input type="submit" class="cmn-btn" name="submit" value="Send">
				<div class="clearfix"></div>
			</li>
			<li><span class="close-icon"><img src="<?= ms_site_url('assets/images/frontend/close-icon2.png') ?>"></span></li>
		</ul>
	</form>
</div>
</div>