<section>
    <div class="contact-sec reset-page login-page register-form-cnt">
        <div class="cust-container">
            <div class="inner-sec">
                <div class="right-sec">
                    <div class="right-cont">
                        <h4>Sign up </h4>
                        <form class="outer-form" method="POST" id="register_form">
                            <ul>
                                <li> <input class="form-control" type="text" value="<?= set_value('name') ?>" name="name" required="required" placeholder="Full Name">
                                    <?= form_error('name', '<span class="error">', '</span>'); ?>
                                </li>
                                <li> <input class="form-control" type="email" value="<?= set_value('email') ?>" name="email" required="required" placeholder="Email Address"> 
                                    <?= form_error('email', '<span class="error">', '</span>'); ?>
                                </li>
                                <li> <input class="form-control" type="password" value="<?= set_value('password') ?>" name="password" id="password" required="required" placeholder="Password"> 
                                    <?= form_error('password', '<span class="error">', '</span>'); ?>
                                </li>
                                <li> <input class="form-control" type="password" value="<?= set_value('confirm_password') ?>" name="confirm_password" required="required" placeholder="Confirm Password">
                                    <?= form_error('confirm_password', '<span class="error">', '</span>'); ?>
                                </li>
                                <li class="tal">
                                    <div class="radio-btn">
                                        <input type="radio" class="check_user_role" value="0" name="user_role" id="user_type">
                                        <label for="user_type">Learner</label>
                                    </div>
                                    <div class="radio-btn">
                                        <input type="radio" class="check_user_role" value="1" name="user_role" id="teacher_type">
                                        <label for="teacher_type">Teacher</label>
                                    </div>
                                    <?= form_error('user_role', '<span class="error">', '</span>'); ?>
                                    <div>
                                        <p id="check_mesasge"></p>
                                    </div>
                                </li>

                                <li class="hw forger-wrap signup-foget-wrap">
                                  <!-- <div class="remeber-signup">
                                        <div class="checkbox">
                                            <input type="checkbox" name="remember" id="remember">
                                            <label for="remember">Remember me</label>
                                        </div>
                                        <a class="forget" href="#"> Forgot your password? </a>
                                    </div> -->

                                    <div class="checkbox-privacy-wrap">
                                        <div class="checkbox agree-checkbox">
                                            <input type="checkbox" name="privacy_policy" id="privacy_policy" value='1'>
                                            <label for="privacy_policy">I have read and agree to the Tudor <a href="<?= ms_Site_url('terms-of-service') ?>" target="_blank"> Terms of Service</a> and  <a href="<?= ms_Site_url('privacy-policy') ?>" target="_blank"> Privacy Policy</a></label>
                                            
                                            <?= form_error('privacy_policy', '<span class="error">', '</span>'); ?>
                                        </div>
                                    </div>
                                </li>
                                
                                <li>
                                    <input type="submit" class="cmn-btn" name="submit" value="create my account">
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="loginor"><span>or</span></div>
                                    <?= '<div class="google-login" align="center">'.$login_button . '</div>'; ?>
                                </li>
                                <li class="dnt-have-acc-wrap">
                                    <p>Already have an account? <a class="signin-popup-new" href="<?= ms_site_url('login') ?>"> 
                                    Sign in here </a> 
                                </p>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</section>