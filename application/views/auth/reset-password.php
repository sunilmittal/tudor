<section>
    <div class="contact-sec reset-page">
        <div class="cust-container">
            <div class="inner-sec">
                <div class="right-sec">
                    <div class="right-cont">
                        <h2 class="sub-heading">Reset Password</h2>
                        <form class="outer-form" method="POST" id="reset_password_form">
                            <ul>
                                <li> <input class="form-control" type="email" value="<?= set_value('email') ?>" name="email" required="required" placeholder="Email Address"> 
                                    <?= form_error('email', '<span class="error">', '</span>'); ?>
                                </li>
                                <li> <input class="form-control" type="password" value="<?= set_value('password') ?>" name="password" id="password" required="required" placeholder="Password"> 
                                    <?= form_error('password', '<span class="error">', '</span>'); ?>
                                </li>
                                <li> <input class="form-control" type="password" value="<?= set_value('confirm_password') ?>" name="confirm_password" required="required" placeholder="Confirm Password">
                                    <?= form_error('confirm_password', '<span class="error">', '</span>'); ?>
                                </li>
                                <li>
                                    <input type="submit" class="cmn-btn" name="submit" value="Submit">
                                    <div class="clearfix"></div>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>