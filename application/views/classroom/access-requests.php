<section>
	<div class="dashboard-sec add-video my-videos">
		<div class="inner-sec">
			<?php $this->load->view('common/sidebar') ?>
			<div class="right-sec">
				<div class="right-inner">
					<h4>Classroom Requests</h4>
					<div class="clearfix"></div>
					<div class="video-inner">
				
						<div class="table-responsive">
    						<table class="table">
							<?php if (count($classroom_user_listing)) { ?>
								<tr>
									<th>Classroom</th>
									<th>Learner</th>
									<th>Permission</th>
									<th>Action</th>
								</tr>
								<?php
								if (count($classroom_user_listing)) {
									foreach ($classroom_user_listing as $classroom_users) {
										foreach ($classroom_users as $classroom_user) {
											?>
											<tr>
												<td><?= getClassName($classroom_user['classroom_id']) ?></td>
												<td><?= getUserName($classroom_user['user_id']) ?></td>
												<td>
													<?php if($classroom_user['request_access'] == 1){ ?>
														<a title="Give Permission" href="<?= ms_site_url('classroom/grant_access/'.$classroom_user['classroom_id'].'/'.$classroom_user['user_id']) ?>" class="classroom_request">
															Allow Access
														</a>
													<?php }else{ ?>
														<a title="Cancel Permission" href="<?= ms_site_url('classroom/grant_access/'.$classroom_user['classroom_id'].'/'.$classroom_user['user_id']) ?>" class="classroom_request">
															Cancel Access
														</a>
													<?php } ?>
												</td>
												<td>
													<a title="Cancel Permission" href="<?= ms_site_url('classroom/delete_classroom_user/'.$classroom_user['classroom_id'].'/'.$classroom_user['user_id']) ?>" class="delete_video">
														<i title="Delete Request" class="fa fa-trash-o" aria-hidden="true"></i>
													</td>
												</tr>
											<?php } }
										} else { ?>
											<tr>
												<td colspan="9"></td>
											</tr>
											<tr>
												<td class="no-data" colspan="9">No classroom requests yet</td>
											</tr>
										<?php } ?>
									<?php }else{ ?>
										<tr class="subscription">
											<td class="no-data" colspan="9">
												<img src="<?= ms_site_url('assets/images/frontend/classroom-requests.png') ?>"/>
												<h6>No classroom requests yet</h6>
											</td> 
										</tr>
									<?php } ?>
								</table>
							</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</section>