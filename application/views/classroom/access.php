<section>
	<div class="video-outer diff-video row">
		<div class="single-video-sec col-lg-8">
			<div class="shadow-card-bg">
			<div class="category-cont">
				<div class="category-left">
					<div class="credentials-sec">
						<p>The credentials are restricted for members only. Please request access to the tutor in order to view the content.</p>
					</div>
					<div class="comment-sec">
						<div class="col-lg-6 col-12">
							<h3 class="main-title"><?= $classroom_details->title ?></h3>
							<p>
								<span>
									<?php
									if ($classroom_details->image && file_exists(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR . $classroom_details->image)) { ?>
										<img height="200px" width="200px" src="<?= ms_site_url('uploads/thumbnails/'.$classroom_details->image) ?>">
									<?php }else{ ?>
										<img src="<?= ms_site_url('assets/images/frontend/learn3.png') ?>">
									<?php }?>
								</span> 
							</p>
						</div>
						<!-- <div class="comment-cont">
							<p><?= getDateInFormat($classroom_details->created_at) ?></p>
						</div> -->
					</div>
					<!-- <p><?= $classroom_details->description ?></p> -->
				</div>
				<div class="col-lg-6 col-12 right-part cmn-space2">
					<?php $access_text = ($access_status) ? 'Remove Classroom' : 'Join Classroom' ?>
					<input class="upload-vid cmn-btn" type="submit" value="<?= $access_text ?>" name="submit" onclick="location='<?= ms_site_url('send-access-request') ?>'">
				</div>
				<div class="clearfix"></div>
			</div>
									</div>
		</div>
		<div class="advert-sec col-lg-4">
			<div class="shadow-card-bg">
			<div class="advert-top">
				<h3 class="main-title">
					Advertise<br>
					Here
				</div>
				<?php if (count($related_videos)) { ?>
					<div class="advert-bottom-sec">
						<h4>More Videos</h4>
						<?php 
						foreach ($related_videos as $video) { ?>
							<a href="<?= ms_site_url('videos/'.$video->id) ?>">
								<div class="advert-video">
									<div class="advert-video-left">
										<div class="img-sec"><img src="<?= ms_site_url('uploads/thumbnails/'.$video->thumbnail) ?>"></div>
										<div class="video-icon"><img src="<?= ms_site_url('assets/images/frontend/video-icon-1.png') ?>"></div>
									</div>
									<div class="advert-video-right">
										<p><?= $video->user_and_channel->user_name ?></p>
										<p><span><?= getDateInFormat($video->created_at) ?></span></p>
									</div>
									<div class="clearfix"></div>
								</div>
							</a>
						<?php }
						?>
					</div>
				<?php } ?>
						</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>