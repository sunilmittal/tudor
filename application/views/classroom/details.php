
<section>
	<div class="video-outer diff-video row">
		<?php $embed_url = getEmbedYouTubeURL($video_details->external_video_id); ?>
		<div class="single-video-sec col-lg-8">
			<div class="shadow-card-bg">
			<div class="single-video-top">
				<!-- <div class="img-sec"><img src="<?= ms_site_url('uploads/thumbnails/'.$video_details->thumbnail) ?>"></div>
				<div class="video-icon"><img src="<?= ms_site_url('assets/images/frontend/video-icon.png') ?>"></div> -->
				<iframe src="<?= $embed_url ?>" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
			<div class="category-cont">
				<div class="category-left">
					<h3 class="main-title"><?= $video_details->title ?></h3>
					<div class="comment-sec">
						<div class="comment-cont">
							<p>
								<a href="<?= ms_site_url('user/'.$video_details->user_and_channel->user_id.'/profile') ?>"> 
									<span>
									<?php
										if ($video_details->user_and_channel->image) { ?>
											<img src="<?= ms_site_url('uploads/avatars/'.$video_details->user_and_channel->image) ?>">
										<?php } else { ?>
											<?= strtoupper($video_details->user_and_channel->user_name[0]); ?>
									<?php } ?>
									</span> 
									<?= $video_details->user_and_channel->user_name ?> 
								</a>
							</p>
						</div>
						<div class="comment-cont">
							<p><?= getDateInFormat($video_details->created_at) ?></p>
						</div>
						<div class="comment-cont">
							<p><img src="<?= ms_site_url('assets/images/frontend/eyes-icon.png') ?>"><span><?= kConverter($video_details->view_count) ?></span></p>
						</div>
						<div class="category-right">
							<div class="category-right-bottom">
								<div class="category-right-bottom-left">
									<ul>
										<li><a href="javascript:void(0)"><img src="<?= ms_site_url('assets/images/frontend/like-icon.png') ?>"><span><?= kConverter($video_details->like_count) ?></span></a></li>
										<li><a href="javascript:void(0)"><img src="<?= ms_site_url('assets/images/frontend/like-icon-1.png') ?>"><span><?= kConverter($video_details->dislike_count) ?></span></a></li>
									</ul>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="category-right-top">
								<p><strong>Category</strong> : <?= getCategoryNamesLine($video_details) ?></p>
							</div>
							
						</div>
					</div>
					<p><?= $video_details->description ?></p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="all-comments-sec">
				<div class="sort-left">
					<p><?= kConverter($video_details->comment_count) ?> Comments</p>
					<!-- <p><a href="javascript:void(0)"><img src="<?= ms_site_url('assets/images/frontend/sort-icon.png') ?>">Sort By</a></p> -->
				</div>
				<?php if ($show_subscribe_btn) { ?>
					<?php if (in_array($video_details->user_and_channel->user_id, $user_data->subscribed_user_ids)) { ?>
                        <div class="subscribe-sec">
							<a href="<?= ms_site_url('user/'.$video_details->user_and_channel->user_id.'/unsubscribe/?redirect_url=videos/'.$video_details->id) ?>" class="cmn-btn">UNSUBSCRIBE</a>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
                    <?php } else { ?>
                        <div class="subscribe-sec">
							<a href="<?= ms_site_url('user/'.$video_details->user_and_channel->user_id.'/subscribe/?redirect_url=videos/'.$video_details->id) ?>" class="cmn-btn">SUBSCRIBE</a>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
                    <?php } ?>
					
				<?php } ?>
			</div>
			<?php 
				if (false && $user_data) { ?>
					<div class="public-comment-sec">
						<div class="public-comment-left">
							<a href="javascript:void(0)">
								<?php
									if ($video_details->user_and_channel->image) { ?>
										<img src="<?= ms_site_url('uploads/avatars/'.$video_details->user_and_channel->image) ?>">
									<?php } else { ?>
										<img src="<?= ms_site_url('assets/images/frontend/p-pic.png') ?>">
								<?php }
								?>
							</a>
						</div>
						<div class="public-comment-right">
							<form method="post" action="">
								<input type="text" name="comment" placeholder="Add a public comment...">
							</form>

						</div>
						<div class="clearfix"></div>
						<a href="javascript:void(0);" class="cmn-btn">Comment</a>
						<div class="clearfix"></div>
					</div>
			<?php	}
			?>

			<?php 
				foreach ($video_details->comments as $comment) { ?>
					<div class="view-comment">
						<div class="view-comment-img">
							<a href="<?= ms_site_url('user/'.$comment->user_id.'/profile') ?>">
								<?php
									if ($comment->image) { ?>
										<img src="<?= ms_site_url('uploads/avatars/'.$comment->image) ?>">
									<?php } else { ?>
										<?= strtoupper($comment->user_name[0]); ?>
								<?php }
								?>
							</a>
						</div>
						<div class="view-comment-reply">
							<p><a href="<?= ms_site_url('user/'.$comment->user_id.'/profile') ?>"><?= $comment->user_name ?></a></p>
							<p><span><?= $comment->comment ?></span></p>
						</div>
						<div class="clearfix"></div>
					</div>
			<?php	}
			?>			
		</div>
									</div>
		<div class="advert-sec col-lg-4">
			<div class="shadow-card-bg">
			<div class="advert-top">
				<h3 class="main-title">
				Advertise<br>
				Here
			</div>
			<?php if (count($related_videos)) { ?>
				<div class="advert-bottom-sec">
					<h4>More Videos</h4>
					<?php 
						foreach ($related_videos as $video) { ?>
							<a href="<?= ms_site_url('videos/'.$video->id) ?>">
								<div class="advert-video">
									<div class="advert-video-left">
										<div class="img-sec"><img src="<?= ms_site_url('uploads/thumbnails/'.$video->thumbnail) ?>"></div>
										<div class="video-icon"><img src="<?= ms_site_url('assets/images/frontend/video-icon-1.png') ?>"></div>
									</div>
									<div class="advert-video-right">
										<p><?= $video->user_and_channel->user_name ?></p>
										<p><span><?= getDateInFormat($video->created_at) ?></span></p>
									</div>
									<div class="clearfix"></div>
								</div>
							</a>
					<?php }
					?>
				</div>
			<?php } ?>
						</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>