<section>
	<div class="dashboard-sec add-video">
		<div class="inner-sec">
			<?php $this->load->view('common/sidebar') ?>
			<div class="right-sec">
				<form method="post" enctype="multipart/form-data">
					<div class="right-inner">
						<h4>Edit Classroom</h4>
						<div class="row add-inner">
							<div class="col-lg-12 col-12 left-part cmn-space">
								<span>Classroom Title</span>
								<input class="form-control" type="text" name="title" 
								placeholder="" value="<?= set_value('title', $classroom_details->title) ?>">
								<?= form_error('title', '<span class="error">', '</span>'); ?>
							</div>

							<div class="col-lg-12 cmn-space">
								<span>Description</span>
								<textarea class="form-control" name="description" placeholder="What’s your video about?"><?= set_value('description', $classroom_details->description) ?></textarea>
								<?= form_error('description', '<span class="error">', '</span>'); ?>
							</div>

							<div class="col-lg-6 col-12 left-part cmn-space">
								<span>Thumbnail</span>
								<div class="file-input form-control">
									<label for="image">
										<span><img src="<?= ms_site_url('assets/images/frontend/upload-vid.png') ?>"></span> 
										<span> upload </span>
									</label>
									<input type="file" id="thumbnail" name="image">
								</div>
							</div>

							<div class="col-lg-6 col-12 right-part cmn-space">
								<input class="upload-vid cmn-btn" type="submit" value="Update Classroom" name="submit">
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>