<section>
	<div class="dashboard-sec add-video my-videos">
		<div class="inner-sec">
			<?php $this->load->view('common/sidebar') ?>
			<div class="right-sec">
				<div class="right-inner">
					<h4>My Classrooms</h4>
					<h5>
						<a href="<?= ms_site_url('classroom/add') ?>">
							<span><img src="<?= ms_Site_url('assets/images/frontend/classroom.png') ?>"></span> Add classroom
						</a>
					</h5>
					<div class="clearfix"></div>
					<div class="video-inner">
					<div class="table-responsive">
						<table class="table">
							<?php if (count($classrooms)) { ?>
							<tr>
								<th>Thumbnail</th>
								<th>Class Title</th>
								<th>Date  </th>
								<th>Actions</th>
							</tr>
							<?php
							// if (count($classrooms)) {
								foreach ($classrooms as $classroom) { ?>
									<tr>
										<td>
											<a href="<?= ms_site_url('classroom/edit/'.$classroom->id) ?>">
												<div class="img-sec">
													<?php if($classroom->image && file_exists(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR . $classroom->image)){ ?>
														<img src="<?= ms_site_url('uploads/thumbnails/'.$classroom->image) ?>">
													<?php }else{ ?>
														<img src="<?= ms_site_url('assets/images/frontend/learn3.png') ?>">
													<?php }?>
												</div>
											</a>
										</td>
										<td><?= $classroom->title ?></td>
										<td><?= getDateInFormat($classroom->created_at) ?></td>
										<td>
											<a href="<?= ms_site_url('classroom/edit/'.$classroom->id) ?>">
												<button class="edit-icon"><i class="fa fa-pencil" aria-hidden="true"></i></button>
											</a>
											<a href="<?php echo site_url('classroom/delete/'.$classroom->id); ?>" class="delete_video">
												<i class="fa fa-trash-o" aria-hidden="true"></i>
											</a>
										</td>
									</tr>
								<?php }
							} else { ?>
								<tr>
									<td colspan="9"></td>
								</tr>
								<tr class="subscription">
									<td class="no-data" colspan="9">
										<img src="<?= ms_site_url('assets/images/frontend/classroom-xl.png') ?>"/>
										<h6>No Classroom Found</h6>
									</td>
								</tr>
							<?php }
							?>
							
						</table>
					</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>