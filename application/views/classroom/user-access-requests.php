<section>
	<div class="dashboard-sec add-video my-videos">
		<div class="inner-sec">
			<?php $this->load->view('common/sidebar') ?>
			<div class="right-sec">
				<div class="right-inner">
					<h4>Classroom Requests</h4>
					<div class="clearfix"></div>
					<div class="video-inner">
					<div class="table-responsive">
						<table class="table">
							<tr>
								<th>Classroom</th>
								<th>Permission</th>
								<th>Action</th>
							</tr>
							<?php
							if (count($classroom_user_listing)) {
								foreach ($classroom_user_listing as $classroom_user) { ?>
									<tr>
										<td><?= getClassName($classroom_user->classroom_id) ?></td>

										<td>
											<?php if($classroom_user->request_access == 2){ ?>
												Access
											<?php }else{ ?>
												No Access
											<?php } ?>
										</td>
										<td>
											<?php if($classroom_user->request_access == 2){ ?>
												<a href="<?= ms_site_url('classroom-videos/'.$classroom_user->classroom_id) ?>">
													<button class="edit-icon"><i class="fa fa-eye" aria-hidden="true"></i></button>
												</a>
											<?php } ?>
											<a href="<?= ms_site_url('classroom/'.$classroom_user->classroom_id) ?>">
												<button class="edit-icon"><i class="fa fa-pencil" aria-hidden="true"></i></button>
											</a>
										</td>
									</tr>
								<?php }
							} else { ?>
								<tr>
									<td colspan="9"></td>
								</tr>
								<tr class="subscription">
									<td class="no-data" colspan="9">
										<img src="<?= ms_site_url('assets/images/frontend/classroom-permissons.png') ?>"/>
										<h6>No classroom requests yet</h6>
									</td>
								</tr>
							<?php }
							?>
							
						</table>
					</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>