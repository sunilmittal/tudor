<?php
if($comments){
    foreach($comments as $comment){
?>
<div class="view-comment view-custom-comments">
	<div class="view-comment-img">
		<p class="user-profile-thumb"><a href="#">
			<span>
			<?php
				if ($comment->user->image) { ?>
			<img src="<?= ms_site_url('uploads/avatars/'.$comment->user->image) ?>">
			<?php } else { ?>
			<?= strtoupper($comment->user->user_name[0]); ?>
			<?php } ?></span> 
			</a>
		</p>
	</div>
	<div class="view-comment-reply">
		<div class="main-comments-cnt">
			<p><a href="#"><?= $comment->user->user_name; ?></a><span class="comment_date_time"><?= $comment->created_at; ?></span></p>
			<p><span><?= $comment->comment; ?></span></p>
			<form id="edit-comment-form" class="main-comment-edit-form" data-cmt_id="<?= $comment->id; ?>">
				<textarea rows="1" name="comment" data-autoresize> <?= $comment->comment; ?></textarea>
				<div class="cancel-confirm-block">
					<a href="javascript:void(0)" class="cmn-btn cancel">Cancel</a>
					<a href="javascript:void(0)" class="cmn-btn" id="edit-comment-btn">Save</a>
				</div>
			</form>
			<?php if (isUserLoggedIn()) { ?>
			<ul class="access-icons">
				<li>
					<?php 
					$user_id = getLoggedInUserId();
					$resource_auther_id = getResourceAutherId($comment->resource_id);
					if($comment->user_id==$user_id){ ?>
					<p class="edit-comment"><a href="javascript:void(0);"> <i class="fa fa-pencil" aria-hidden="true"> </i>Edit</a></p>
					<?php } if($comment->user_id==$user_id || $resource_auther_id==$user_id){ ?>
					<p>
					<a data-toggle="modal" data-target="#commentDeleteModal"
					data-cmt_id="<?= $comment->id ?>" class="open_commentDeleteModal"><i class="fa fa-trash" aria-hidden="true"> </i>Delete</a></p>
					<?php } ?>
					<p>
						<a href="javascript:void(0)" class="comment-reply"><i class="fa fa-reply" aria-hidden="true"></i>Reply</a>
					</p>
				</li>
			</ul>
			<form id="reply-comment-form" data-cmt_id="<?= $comment->id; ?>">
				<textarea rows="1" name="reply" data-autoresize placeholder="Add a reply..."></textarea>
				<span class="error reply-error"></span>
				<div class="">
					<input type="hidden" name="resource_id" value="<?= $comment->resource_id; ?>" />
					<a href="javascript:void(0)" class="cancel">Cancel</a>
					<a href="javascript:void(0)" class="" id="reply-btn">Reply</a>
				</div>
			</form>
			<?php } ?>
		</div>
        <?php if($comment->replies){ ?>
		<div class="view-reply-icon reply-block">
			<?php if($comment->replies_count>1){ ?>
				<a href="javascript:void(0);" class="view-rply" data-replies_count="<?= $comment->replies_count; ?>">View <?= $comment->replies_count; ?> replies</a>
			<?php } else{  ?>
				<a href="javascript:void(0);" class="view-rply" data-replies_count="<?= $comment->replies_count; ?>">View <?= $comment->replies_count; ?> reply</a>
			<?php } ?>
			<div class="hide-reply-block" id="replies-block-<?= $comment->id ?>">
                <?php foreach($comment->replies as $reply){ ?>
				<div class="view-comment">
					<div class="view-comment-img">
						<p class="user-profile-thumb"><a href="#">
							<span>
							<?php
								if ($reply->user->image) { ?>
							<img src="<?= ms_site_url('uploads/avatars/'.$reply->user->image) ?>">
							<?php } else { ?>
							<?= strtoupper($reply->user->user_name[0]); ?>
							<?php } ?></span> 
							</a>
						</p>
					</div>
					<div class="view-comment-reply comments-reply">
						<p><a href="#"><?= $reply->user->user_name; ?></a><span class="comment_date_time"><?= $reply->created_at; ?></span></p>
						<p><span><?= $reply->comment; ?></span></p>
						<form id="edit-comment-form" data-cmt_id="<?= $reply->id; ?>">
							<textarea rows="1" name="comment" data-autoresize> <?= $reply->comment; ?></textarea>
							<div class="cancel-confirm-block">
								<a href="javascript:void(0)" class="cmn-btn cancel">Cancel</a>
								<a href="javascript:void(0)" class="cmn-btn" id="edit-comment-btn">Save</a>
							</div>
						</form>
						<ul class="access-icons">
							<li>
								<?php 
								if (isUserLoggedIn()) {
								if($reply->user_id==$user_id){ ?>
								<p class="edit-comment"> <a href="javascript:void(0);"><i class="fa fa-pencil" aria-hidden="true"> </i>Edit</a>
								</p>
								<?php } if($reply->user_id==$user_id || $resource_auther_id==$user_id){ ?>
								<p><a data-toggle="modal" data-target="#commentDeleteModal"
									data-cmt_id="<?= $reply->id ?>" class="open_commentDeleteModal"><i class="fa fa-trash" aria-hidden="true"> </i>Delete</a></p>
								<?php } } ?>
							</li>
						</ul>
					</div>
				</div>
                <?php } ?>
				<?php if($comment->replies_count > $limit){ ?>
				<a href="javascript:void(0)" class="load_more_btn" data-cmt_id=<?= $reply->parent_id ?> data-offset="<?php echo $limit; ?>" >Load More</a>
				<?php } ?>
			</div>
        </div>
        <?php } ?>
	</div>
	<div class="clearfix"></div>
</div>
<?php }
}
?>
<input type="hidden" id="page-number" value="" />
<!-- Modal -->
<div class="modal fade" id="commentDeleteModal" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form method="post" action="" id="delete-comment-form">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title d-inline" id="staticBackdropLabel">Are you sure?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<input type="hidden" id="cmt_id" value="" />
					<p>Are you sure you wish to delete this record? Please re-confirm this action.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Delete</button>
				</div>
			</div>
		</form>
	</div>
</div>