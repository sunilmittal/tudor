<?php if($replies){ ?>
<?php foreach($replies as $reply){ ?>
<div class="view-comment">
    <div class="view-comment-img">
        <p class="user-profile-thumb"><a href="#">
            <span>
            <?php
                if ($reply->user->image) { ?>
            <img src="<?= ms_site_url('uploads/avatars/'.$reply->user->image) ?>">
            <?php } else { ?>
            <?= strtoupper($reply->user->user_name[0]); ?>
            <?php } ?></span> 
            </a>
        </p>
    </div>
    <div class="view-comment-reply comments-reply">
        <p><a href="#"><?= $reply->user->user_name; ?></a><span class="comment_date_time"><?= $reply->created_at; ?></span></p>
        <p><span><?= $reply->comment; ?></span></p>
        <form id="edit-comment-form" data-cmt_id="<?= $reply->id; ?>">
            <textarea rows="1" name="comment" data-autoresize> <?= $reply->comment; ?></textarea>
            <div class="cancel-confirm-block">
                <a href="javascript:void(0)" class="cmn-btn cancel">Cancel</a>
                <a href="javascript:void(0)" class="cmn-btn" id="edit-comment-btn">Save</a>
            </div>
        </form>
        <ul class="access-icons">
            <li>
                <?php 
                $user_id = getLoggedInUserId();
                if($reply->user_id==$user_id){ ?>
                <p class="edit-comment"> <a href="javascript:void(0);"<i class="fa fa-pencil" aria-hidden="true"> </i>Edit</a>
                </p>
                <p><a data-toggle="modal" data-target="#commentDeleteModal"
                    data-cmt_id="<?= $reply->id ?>" class="open_commentDeleteModal"><i class="fa fa-trash" aria-hidden="true"> </i>Delete</a></p>
                <?php } ?>
            </li>
        </ul>
    </div>
</div>
<?php } ?>
<?php
if($replies_count > $offset){ ?>
<a href="javascript:void(0)" class="load_more_btn" data-cmt_id=<?= $reply->parent_id ?> data-offset=<?= $offset ?> >Load More</a>
<?php } } ?>