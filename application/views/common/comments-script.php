<script>
    
    $(document).ready(function(){
        jQuery.each(jQuery('textarea[data-autoresize]'), function() {
        var offset = this.offsetHeight - this.clientHeight;
        var resizeTextarea = function(el) {
        jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
        };
        jQuery(this).on('keyup input', function() { resizeTextarea(this); }).removeAttr('data-autoresize');
        });
        getResourceComments("load");
        let processing = false;

        if (processing)
            return false;

        $(window).scroll(function() {
            //if($(window).scrollTop() + $("#comments-listing").height() >= $(document).height()) {
            if ($(window).scrollTop() >= ($(document).height() - $(window).height())*0.7){
                if(page < total_pages) {
                    page++;
                    getResourceComments("load", page);
                    processing = false;
                }
            }
		});

        // Comment post without age refreshing //
        $("#comment_form").on("submit", function (e) {
        $(".comment-error").hide();
        var dataString = $(this).serialize();
        var note_id = $(this).find("#note_id").val();
        var comment = $(this).find("#comment-text").val();
        $.ajax({
                type: "POST",
                url: `${SITE_URL}/notes/${note_id}/comments`,
                cache: false,
                async: true,
                data: dataString,
                error: function(jqXHR, error, errorThrown) {
                    console.error(error);
                },
                success: function(data, textStatus, jqXHR) {
                    if (data.status) {
                        page = 0;
                        getResourceComments("add");
                        $("#comment-text").val("");
                        $("#comments-count").text(data.totalComments);
                    } else {
                        $(".comment-error").text(data.error);
                        $(".comment-error").show();
                    }
                }
            });    
        e.preventDefault();
    });

    });

    // Get Comments based on url (video/notes) //
    function getResourceComments(type="",page=0){
        var pathArray = window.location.pathname.split('/');
        var resource_name = pathArray[pathArray.length-2];
        var resource_slug = pathArray[pathArray.length-1];
        
        $.ajax({
            url: `${SITE_URL}/${resource_name}/${resource_slug}/comments?page=${page}&resource_type=${resource_name}`,
            type: 'GET',
            beforeSend: function()
            {
                $('.loader').show();
            },
            success: function(data){
                $('.loader').hide();
                if(type=="add" || type=="delete"){
                    $('#comments-listing').html(data);
                }
                else if(type=="edit"){
                    $('#comments-listing').html(data);
                }
                else{
                    $('#comments-listing').append(data);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                $('#content').html(errorMsg);
                }
        });
    }

    $(document).on("click",".view-rply",function(){
        $(this).parent().children(".hide-reply-block").slideToggle();
        $(this).toggleClass("open");
        var replies_count = $(this).data("replies_count");
        if($(this).is('.open')){
            if(replies_count > 1){
                $(this).text("Hide "+replies_count+" replies");
            }
            else{
                $(this).text("Hide "+replies_count+" reply");
            } 
        }
        else
        {
            if(replies_count > 1){
                $(this).text("View "+replies_count+" replies");
            }
            else{
                $(this).text("View "+replies_count+" reply");
            }
        }
    });
     
    $(document).on("click",".main-comments-cnt .access-icons .edit-comment",function(){
        $(this).parents(".main-comments-cnt").toggleClass("to-edit-comment-block");
    });

    $(document).on("click",".comments-reply .access-icons .edit-comment",function(){
        $(this).parents(".comments-reply").toggleClass("to-edit-comment-block");
    });

    $(document).on("click",".main-comments-cnt .cancel-confirm-block .cancel",function(){
        $(this).parents(".main-comments-cnt").toggleClass("to-edit-comment-block");
    });

    $(document).on("click",".comments-reply .cancel-confirm-block .cancel",function(){
        $(this).parents(".comments-reply").toggleClass("to-edit-comment-block");
    });

    $(document).on("click",".access-icons .comment-reply, #reply-comment-form .cancel",function(){
        $(this).parents(".view-comment-reply").toggleClass("to-reply-comment-block");
        $(this).parents(".view-comment-reply").find(".access-icons").toggleClass("display_none");
    });

    // Comment Publish section start //
    $(document).on("click","#comment_form .cancel-cmt",function(){
        $(this).parents("#comment_form").find(".cancel-confirm-block").hide();
        $(this).parents("#comment_form").find("textarea[name='comment']").val("");
    });

    $(document).on("click", "textarea[name='comment']", function () {
        $(this).parents("#comment_form").find(".cancel-confirm-block").show();
    });
    // Comment Publish section end //

    $(document).on("click", ".open_commentDeleteModal", function () {
        var cmt_id = $(this).data("cmt_id");
        $(".modal-body #cmt_id").val(cmt_id);
    });

    // Comment delete without page refreshing //
    $(document).on("submit", "#delete-comment-form", function (e) {
        var cmt_id = $(this).find("#cmt_id").val();
        $.ajax({
                url: `${SITE_URL}/comments/${cmt_id}`,
                type: "DELETE",
                cache: false,
                async: true,
                error: function(jqXHR, error, errorThrown) {
                    console.error(error);
                },
                success: function(data, textStatus, jqXHR) {
                    if (data.status) {
                        $("#commentDeleteModal").modal("hide");
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                        page = 0;
                        getResourceComments("delete");
                        $("#comments-count").text(data.totalComments);
                    } else {
                        console.error(data.message);
                    }
                }
            });    
        e.preventDefault();
    });

    // Comment Edit without page refreshing //
    $(document).on("click", "#edit-comment-btn", function (e) {
        var dataString = $(this).parents("#edit-comment-form").serialize();
        var cmt_id = $(this).parents("#edit-comment-form").data("cmt_id");
        $.ajax({
                url: `${SITE_URL}/comments/${cmt_id}`,
                type: "PUT",
                cache: false,
                async: true,
                data: dataString,
                error: function(jqXHR, error, errorThrown) {
                    console.error(error);
                },
                success: function(data, textStatus, jqXHR) {
                    if (data.status) {
                        page = 0;
                        getResourceComments("edit");
                        $("#comments-count").text(data.totalComments);
                    } else {
                        console.error(data.message);
                    }
                }
            });    
        e.preventDefault();
    });

    // Comment Reply without page refreshing //
    $(document).on("click", "#reply-btn", function (e) {
        $(".reply-error").hide();
        var pathArray = window.location.pathname.split('/');
        var resource_name = pathArray[pathArray.length-2];
        var dataString = $(this).parents("#reply-comment-form").serialize();
        var cmt_id = $(this).parents("#reply-comment-form").data("cmt_id");
        $.ajax({
                url: `${SITE_URL}/comments/${cmt_id}/replies?resource_type=${resource_name}`,
                type: "POST",
                cache: false,
                async: true,
                data: dataString,
                error: function(jqXHR, error, errorThrown) {
                    console.error(error);
                },
                success: function(data, textStatus, jqXHR) {
                    if (data.status) {
                        getResourceComments("add");
                    } else {
                        $("#reply-comment-form[data-cmt_id='" + cmt_id + "'] .reply-error").text(data.error); 
                        $(".reply-error").show();
                    }
                }
            });    
        e.preventDefault();
    });

    // Load More Replies //
    $(document).on("click", ".load_more_btn", function (e) {
    var cmt_id = $(this).data("cmt_id");
    var offset = $(this).data("offset");
    $.ajax({
        url:`${SITE_URL}/comments/${cmt_id}/replies`,
        data:{
          offset : offset,
        },
        success :function(data){
            $('#replies-block-'+cmt_id).append(data);
           // $('#offset').val(data.offset); 
        }
    });
    $(this).remove();
});
</script>