<footer>
    <div class="cust-container">
        <div class="inner-sec">
            <div class="box box-1">
                <div class="footer-logo">
                    <a href="<?= ms_Site_url() ?>"><img src="<?= ms_Site_url('assets/images/frontend/footer-logo.png') ?>"></a>
                </div>
              
            </div>
            <div class="box box-2">
                <h5>Company</h5>
                <p><a href="<?= ms_Site_url() ?>"> Home</a> </p>
                <p><a href="<?= ms_Site_url('about-us') ?>"> About Us</a> </p>
                <p><a href="<?= ms_Site_url('privacy-policy') ?>"> Privacy Policy</a></p>
                <p><a href="<?= ms_Site_url('terms-of-service') ?>"> Terms of Service</a></p>
                <!-- <p><a href="#"> TERMS & CONDITIONS </a></p>
                <p><a href="#">  SUPPORT </a></p> -->
            </div>

            <div class="box box-2">
                <h5>For You</h5>
                <p><a href="<?= ms_Site_url('getting-started') ?>"> Getting Started</a></p>
                <p><a href="<?= ms_Site_url('learners') ?>"> Learners</a> </p>
                <p><a href="<?= ms_Site_url('tutors') ?>">Teachers</a> </p>
                <p><a href="<?= ms_Site_url('spread-the-word') ?>"> Spread the Word</a></p>
                <!-- <p><a href="#"> Tudor Stories </a></p> -->
                 <!-- <p><a href="#"> TERMS & CONDITIONS </a></p>
                <p><a href="#">  SUPPORT </a></p> -->
            </div>

            <div class="box box-3">
                <h5>Resources</h5>
                <p><a href="<?= ms_Site_url('faq') ?>"> FAQ </a></p>
                <p><a href="<?= ms_Site_url('community-guidelines') ?>"> Community Guidelines</a></p>
                <p><a href="<?= ms_Site_url('contact-us') ?>"> Contact Us </a></p>
                <p><a href="<?= ms_Site_url('sitemap') ?>"> Sitemap </a></p>
            </div>
            <!-- <div class="box box-5">
                <h5>Subjects</h5>
                <?php
                    if (count($all_categories)) {
                        foreach ($all_categories as $key => $category) { ?>
                       <p><a href="<?= ms_site_url('/subjects/'.$category['slug']) ?>"><?= $category['name'] ?></a></p>
                 <?php } } ?>
            </div> -->
            <div class="box box-4">
                <h5>Stay Connected</h5>
                <div class="social-icos">
                    <p>
                        <a href="https://www.facebook.com/tudorlearning-106000797822171" target="__blank"><i class="fa fa-facebook" aria-hidden="true"></i></a> 
                    </p>
                    <p><a href="https://www.instagram.com/tudorlearning/" target="__blank"> <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </p>
                    <p><a href="https://twitter.com/Tudor_learning" target="__blank"><i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                    </p>
                </div>
            </div>
        </div>

    </div>
    
    <div class="cust-container">
    <div class="inner-sec copyright-web">
                <p class="yt"><a href="javascript:void(0);">  © <?= date('Y') ?> Tudor </a></p>
        </div>
    </div>
</footer>

</div>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>-->
