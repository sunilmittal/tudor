	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Global Site Verification -->
		<meta name="google-site-verification" content="6DBWHlVfyWU2StaLPheSX3ltV1sRnWkcz-HB07kgGeM">
		<title><?= @$page_title ? $page_title : config_item('company_name') ?></title>
		<meta name="title" content="<?= @$page_title ? $page_title : config_item('company_name') ?>" />
		<meta name="description" content="<?= @$page_meta_description ? $page_meta_description : config_item('company_name') ?>" />
		<meta name="keywords" content="<?= @$page_meta_keyword ? $page_meta_keyword : config_item('company_name') ?>" />
		<meta property="og:title" content="<?= @$page_title ? $page_title : config_item('company_name') ?>" />
		<meta property="og:image" content="<?php echo @$settings->value; ?>" />
		<meta property="og:site_name" content="<?= config_item('company_name') ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:description" content="<?= @$page_meta_description ? $page_meta_description : config_item('company_name') ?>" />
		<meta property="og:url" content="<?php echo @$url ? $url : ms_site_url(); ?>" />
		<!-- <link rel="stylesheet" href="<?= ms_site_url('assets/css/frontend/bootstrap.min.css') ?>"> -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<link rel="stylesheet" href="<?= ms_site_url('assets/css/frontend/style.css') ?>">
		<link rel="stylesheet" href="<?= ms_site_url('assets/css/frontend/responsive.min.css') ?>">
		<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= ms_site_url('assets/css/frontend/animate.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
		<!-- <script src="https://www.google.com/recaptcha/api.js?render=6LemgNMUAAAAAOaZkA2LEAlf8uU4WEXzX-PCJC6h"></script> -->
		<link type="image/x-icon" rel="shortcut icon" href="<?php echo ms_site_url('assets/images/frontend/favicon.png'); ?>" />
		<link type="image/x-icon" rel="icon" href="<?php echo ms_site_url('assets/images/frontend/favicon.png'); ?>" />
		<?php //echo @$analytic->value; ?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" integrity="sha512-aEe/ZxePawj0+G2R+AaIxgrQuKT68I28qh+wgLrcAJOz3rxCP+TwrK5SPN+E5I+1IQjNtcfvb96HDagwrKRdBw==" crossorigin="anonymous" />

		<link rel="canonical" href="<?php echo @$url ? $url : ms_site_url(); ?>">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

		<!-- Global Site Tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-1YYLSTC96Q"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'G-1YYLSTC96Q');
		</script>
		<!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1638316539693222');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1638316539693222&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
	</head>