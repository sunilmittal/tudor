<div class="outer-wrapper">

  <header class="<?php echo $path == 'app/home' ? 'home_header' : '' ?>">
    <div class="inner-header blog-header">
      <div class="logo-sec">
        <a href="<?= ms_Site_url('blog-overview') ?>">
          <img src="<?= ms_Site_url('assets/images/frontend/logo-blog.png') ?>">
        </a>
      </div>

      <div class="menu-sec">
        <ul>
         <li>
          <a href="<?= ms_Site_url('/') ?>">Visit Our Site</a>
        </li>
        <li>
          <a href="<?= ms_Site_url('contact-us') ?>">Contact Us</a>
        </li>

        <li>
          <?php if (isUserLoggedIn()) { ?>
            <a href="<?= ms_Site_url('/me') ?>">Profile</a>
          <?php } else { ?>
            <a href="<?= ms_Site_url('/') ?>"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a>
          <?php } ?>
        </li>
      </ul>
    </div>

    <div class="right-drop">
      <div class="social-icos social-icons-blog">
        <p>
          <a href="https://www.facebook.com/tudorlearning-106000797822171" target="__blank"><i class="fa fa-facebook" aria-hidden="true"></i></a> 
        </p>
        <p><a href="https://www.instagram.com/tudorlearning/" target="__blank"> <i class="fa fa-instagram" aria-hidden="true"></i>
        </a>
      </p>
      <p><a href="https://twitter.com/Tudor_learning" target="__blank"><i class="fa fa-twitter" aria-hidden="true"></i>
      </a>
    </p>
  </div>
</div>



</div>
</header>
<div class="menu-icon m-visible"></div>
