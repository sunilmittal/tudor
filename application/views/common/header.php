<div class="outer-wrapper">

	<header class="<?php echo $path == 'app/home' ? 'home_header' : '' ?>">
		<div class="inner-header">
			<div class="logo-sec">
				<?php if (isUserLoggedIn()) { ?>
					<a href="<?= ms_Site_url('/home') ?>">
					<?php } else { ?>
						<a href="<?= ms_Site_url('/') ?>">
						<?php } ?>
						<img src="<?= ms_Site_url('assets/images/frontend/logo.png') ?>">
					</a>
				</div>
				<div class="right-sec">

					<div class="menu-sec">
						<ul>
							<li class="up-vid">
								<a>Subjects</a>
								<div class="dropdown-menu">
									<?php
									if (count($all_categories)) {
										foreach ($all_categories as $key => $category) { 
											?>
											<div class="box box<?= $key+1 ?>">
												<h5><a href="<?= ms_site_url('/subjects/'.$category['slug']) ?>"><?= $category['name'] ?></a></h5>
												<?php foreach ($category['sub_cat'] as $sub_cat) { 
													$slug = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($sub_cat['name'])));
													?>
													<p><a href="<?= ms_site_url('/subjects/'.$category['slug'].'/'.$sub_cat['slug']) ?>"><?= $sub_cat['name'] ?></a></p>
												<?php } ?>
											</div>
										<?php }
									}
									?>
								</div>
							</li>
							<?php if(!isUserLoggedIn()){ ?>
								<li class="add-vid">
									<a href="<?= ms_site_url('login?redirect_url=videos/add') ?>">
										Add video
									</a>
								</li>
							<?php } else if (isUserLoggedIn() && userRoleIsTeacher()) { ?>
								<li class="add-vid">
									<a href="<?= ms_site_url('videos/add') ?>">
										Add video
									</a>
								</li>
							<?php } ?>
							<?php if(!isUserLoggedIn()){ ?>
								<li class="add-vid">
									<a href="<?= ms_site_url('login?redirect_url=notes/add') ?>">
										Create Notes
									</a>
								</li>
							<?php } else { ?>
								<li class="add-vid">
									<a href="<?= ms_site_url('notes/add') ?>">
										Create Notes
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
					<div class="search-sec">
						<form method="get" action="<?= ms_Site_url('/search') ?>">
							<div class="search-inner">
								<input type="text" name="term" placeholder="Search for subjects, people, and more" value="<?= set_value_get('term') ?>">
								<button type="submit"></button>
							</div>
						</form>
					</div>
					<?php if(!isUserLoggedIn()){ ?>
						<div class="right-drop">
							<a href="<?= ms_site_url('register') ?>" class="s-popup cmn-btn">
								<span class="sign_in_btn"> Get Started </span>
							</a>
						</div>
						<?php if(isset($login_button)){ ?>
							<div class="right-drop">
								<?= '<div class="google-login" align="center">'.$login_button . '</div>'; ?>
							</div>
						<?php } ?>
					<?php } else { ?>
						<div class="right-drop">
							<h3>
								<a href="#">
									<span>
										<?php
										if ($user_data->image) { ?>
											<img src="<?= ms_site_url('uploads/avatars/'.$user_data->image) ?>">
										<?php } else { ?>
											<?= strtoupper($user_data->name[0]); ?>
										<?php }
										?>
									</span>
								</a>
							</h3>
							<div class="dropdown">
								<ul>
									<li>
										<a href="javascript:void(0);"><span><?= $user_data->name ?></span></a>
										<?= $user_data->email ?><br/>
										<?php if( $this->uri->segment(1) !=  CONFIRM_ROLE){ ?>
											<?= $user_data->user_role == USER_ROLE ? 'Learner' : 'Teacher'; ?>
										<?php } ?>
									</li>
									<li>
										<a href="<?= ms_site_url('me') ?>">Profile</a>
										<a href="<?= ms_site_url('settings') ?>">Settings</a>
										<a href="<?= ms_site_url('auth/logout') ?>">Sign Out</a>
									</li>
								</ul>
							</div>
						</div>

					<?php } ?> 
				</div>
				<div class="clearfix"></div>

			</div>
		</header>
		<div class="menu-icon m-visible"></div>
