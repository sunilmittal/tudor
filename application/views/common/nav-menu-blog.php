<div class="mob-menu mob-menu-blog"> 
    <div class="full-menu-mobile">
        <div class="menu-navigation-mobile">
            <ul>             
              <li>
                <a href="<?= ms_site_url('settings') ?>">Visit Our Site</a>
            </li>
            <li>
                <a href="<?= ms_site_url('logout') ?>">Contact Us</a>
            </li>
            <li class="add-vid">
                <a href="<?= ms_site_url('login') ?>" class="s-popup cmn-btn">
                    <span> Login</span>
                </a>
            </li>




        </ul>

    </div>
    <div class="social-icon">
        <ul>
            <li><a href="https://www.facebook.com/tudorlearning-106000797822171"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="https://www.instagram.com/tudorlearning/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <li><a href="https://twitter.com/Tudor_learning"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        </ul>
    </div>
</div>
</div>
