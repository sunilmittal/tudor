
<div class="mob-menu">
	<div class="full-menu-mobile">
		<div class="menu-navigation-mobile">
			<ul>
				<li class="up-vid">
					<p>Subjects</p>
					<div class="dropdown-menu">
						<?php
						if (count($all_categories)) {
							foreach ($all_categories as $key => $category) { ?>
								<div class="box box<?= $key+1 ?>">
									<h5><a href="<?= ms_site_url('/videos/?category='.$category['name']) ?>"><?= $category['name'] ?></a></h5>
									<?php foreach ($category['sub_cat'] as $sub_cat) { ?>
										<p><a href="<?= ms_site_url('/videos/?category='.$sub_cat['name']) ?>"><?= $sub_cat['name'] ?></a></p>
									<?php } ?>
								</div>
							<?php }
						}
						?>
					</div>
				</li>
				<li class="add-vid">
					<a href="<?= ms_site_url('login?redirect_url=notes/add') ?>">
						<span><i class="fa fa-file" aria-hidden="true"></i></span> Create Notes
					</a>
				</li>

				<?php if(!isUserLoggedIn()){ ?>
					<li class="add-vid">
						<a href="<?= ms_site_url('login?redirect_url=videos/add') ?>">
							<span><i class="fa fa-video-camera" aria-hidden="true"></i></span> Add video
						</a>
					</li>
				<?php } else if (isUserLoggedIn() && userRoleIsTeacher()) { ?>
					<li class="add-vid">
						<a href="<?= ms_site_url('videos/add') ?>">
							<span><i class="fa fa-video-camera" aria-hidden="true"></i></span> Add video
						</a>
					</li>
				<?php } ?>
				<?php if(!isUserLoggedIn()){ ?>
					<li class="add-vid">
						<a href="<?= ms_site_url('login') ?>" class="s-popup cmn-btn">
							<span> Sign in </span>
						</a>
					</li>
					<li>
						<?= '<div class="google-login">'.$login_button . '</div>'; ?>
					</li>
				<?php } else { ?>

					<li>
						<a href="#">
							<span class="icon-pic">
								<?php
								if ($user_data->image) { ?>
									<img src="<?= ms_site_url('uploads/avatars/'.$user_data->image) ?>">
								<?php } else { ?>
									<?= strtoupper($user_data->name[0]); ?>
									<!-- <img src="<?= ms_Site_url('assets/images/frontend/john-icon.png') ?>"> -->
								<?php }
								?>
							</span>
							<?= makeNameShort($user_data->name) ?>
						</a>
					</li>
					<li>
						<a href="<?= ms_site_url('settings') ?>">Settings</a>
					</li>
					<li>
						<a href="<?= ms_site_url('logout') ?>">Sign Out</a>
					</li>

				<?php } ?>
			</ul>

		</div>
		<div class="social-icon">
			<ul>
				<li><a href="https://www.facebook.com/tudorlearning-106000797822171"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="https://www.instagram.com/tudorlearning/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				<li><a href="https://twitter.com/Tudor_learning"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
</div>
