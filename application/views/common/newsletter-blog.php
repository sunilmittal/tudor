<section class="newsletter-blog">
  <div class="custm-container">
    <div class="newsletter-inner-blog">
      <div class="newsletter-left">
        <h4>Subscribe to stay updated!</h4>
        <p>We hope you are having a great experience with us. When you subscribe via email, we will notify you on relevant Content and updates about us. You may unsubscribe at any time.</p>
      </div>
      <div class="newsletter-right">
        <form id="blogNewsletter">
          <div class="email-sec">                    
            <input type="email" name="email" id="email" placeholder="Enter your email here" required>
            <button type="submit" id="blog-submit" class="send-btn"><img src="<?= ms_Site_url('assets/images/frontend/send.png') ?>" alt="Send"></button>
          </div>
        </form>   
        <div class="response-msg"></div>             
      </div>
    </div>
  </div>
</section>