<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="<?= ms_site_url('assets/js/frontend/wow.min.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<!--<script type="text/javascript">
    <?php if ($this->session->flashdata('success')) { ?>
        toastr.success("<?php echo $this->session->flashdata('success'); ?>");
    <?php } else if ($this->session->flashdata('error')) {  ?>
        toastr.error("<?php echo $this->session->flashdata('error'); ?>");
    <?php } else if ($this->session->flashdata('warning')) {  ?>
        toastr.warning("<?php echo $this->session->flashdata('warning'); ?>");
    <?php } else if ($this->session->flashdata('info')) {  ?>
        toastr.info("<?php echo $this->session->flashdata('info'); ?>");
    <?php } ?>
</script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.19.1/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo ms_site_url('assets/js/frontend/validation.js'); ?>"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone-with-data-2012-2022.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js" integrity="sha512-GDey37RZAxFkpFeJorEUwNoIbkTwsyC736KNSYucu1WJWFK9qTdzYub8ATxktr6Dwke7nbFaioypzbDOQykoRg==" crossorigin="anonymous"></script>
<script type="text/javascript">
    const SITE_URL = "<?= ms_site_url('/') ?>";

    function createCookie(name,value,days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/; domain="+getServerName();
    }

    function getClientTz() {
        return moment.tz.guess() ? moment.tz.guess() : getServerTz();
    }
    function getServerTz() {
        return "<?= date_default_timezone_get() ?>";
    }
    function getServerName() {
        return "<?= $_SERVER['SERVER_NAME'] ?>";
    }
    createCookie("client_tz", getClientTz());

    $('.delete_video').confirm({
        buttons: {
            tryAgain: {
                text: 'Yes, delete',
                btnClass: 'btn-red',
                action: function () {
                    location.href = this.$target.attr('href');
                }
            },
            cancel: function () {
            }
        },
        icon: 'fa fa-exclamation-triangle',
        title: 'Are you sure?',
        content: 'Are you sure you want to remove this Video? It will be deleted here but will still be saved on Youtube.',
        type: 'red',
        typeAnimated: true,
        boxWidth: '30%',
        useBootstrap: false,
        theme: 'modern',
        animation: 'scale',
        backgroundDismissAnimation: 'shake',
        draggable: false
    });
    $('.delete_note').confirm({
        buttons: {
            tryAgain: {
                text: 'Yes, delete',
                btnClass: 'btn-red',
                action: function () {
                    location.href = this.$target.attr('href');
                }
            },
            cancel: function () {
            }
        },
        icon: 'fa fa-exclamation-triangle',
        title: 'Are you sure?',
        content: 'Are you sure you want to remove this Note? It will not be saved and will be deleted forever.',
        type: 'red',
        typeAnimated: true,
        boxWidth: '30%',
        useBootstrap: false,
        theme: 'modern',
        animation: 'scale',
        backgroundDismissAnimation: 'shake',
        draggable: false
    });

    $('.classroom_request').confirm({
        buttons: {
            tryAgain: {
                text: 'Yes, change',
                btnClass: 'btn-red',
                action: function () {
                    location.href = this.$target.attr('href');
                }
            },
            cancel: function () {
            }
        },
        icon: 'fa fa-exclamation-triangle',
        title: 'Are you sure?',
        content: 'Are you sure you wish to change the permission? Please re-confirm this action.',
        type: 'blue',
        typeAnimated: true,
        boxWidth: '30%',
        useBootstrap: false,
        theme: 'modern',
        animation: 'scale',
        backgroundDismissAnimation: 'shake',
        draggable: false
    });

    var readURL = function(input, elementId) {
        if (input.files && input.files[0] && $("#" + elementId)) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("#" + elementId).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".file-upload").on('change', function(){
        readURL(this, $(this).data('img_id'));
    });
    
    $(".upload-button").on('click', function() {
        $(".file-upload").click();
    });

    $(".user-file-upload").on('change', function(){
        readURL(this, $(this).data('img_id'));
    });
    
    $(".upload-user-img-button").on('click', function() {
        $(".user-file-upload").click();
    });

</script>
<script>
    // new WOW().init();
    $(".menu-icon").click(function(){
      $(".mob-menu").fadeToggle("slow");
      $(".outer-wrapper").toggleClass('body-fix');
      $("body").toggleClass('body-fix');       
      $(".menu-icon").toggleClass('mobile-menu-res'); 

  });
</script>
<script> 
    $(document).ready(function () {


        /*$(".s-popup").click(function () {
            $(".signin-popup").slideDown("slow");
        });
        $(".signin-popup li .close-icon").click(function () {
            $(".signin-popup").slideUp("slow");
        });*/
        $(".tudor-faq .accordion > .card .card-header").click(function(){
            $(this).next(".card-body").slideToggle("fast");
            $(this).toggleClass("collapsed")
            $(this).find("a").toggleClass("open")
        })
        $(".forget").click(function(){
            $("#forget-popup").slideDown("slow");
        });
        $(".signin-popup li .close-icon").click(function(){
            $("#forget-popup").slideUp("slow");
        });



        /**********
        $('a[href*="#"]').click(function (event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                location.hostname == this.hostname
                ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length && !target.hasClass('collapsed')) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000, function () {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        };
                    });
                }
            }
        });

        ************** */




       /* var todaysDate = moment();
        $("input[name='client_tz']").val(getClientTz());
        if ($("#add_note").length) {
            $('#notes_datetime').datetimepicker({
                format: 'YYYY-MM-DD',
                minDate: new Date(),
            });
        } else {
            let dateTimeObject = new Object();
            dateTimeObject.format = 'YYYY-MM-DD';
            console.log($("form#edit_note input[name='note_date']").val())
            var note_date = moment($("form#edit_note input[name='note_date']").val());
            if(note_date < todaysDate){
                dateTimeObject.minDate = new Date();
                dateTimeObject.date = new Date();
            } else{
                dateTimeObject.minDate = new Date();
                dateTimeObject.date = note_date;
            }
            $('#notes_datetime').datetimepicker(dateTimeObject);
        } */
        $('#notes_datetime').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false,
            minDate: new Date().setHours(0,0,0,0),
            defaultDate: new Date(),
        });  
        setTimeout(function(){
            $('.alert').fadeOut('slow');
        },3000); 
    });
</script>

<script type="text/javascript">
    let visibility = `<?= (set_value('visibility')) ? set_value('visibility') : '' ?>`;
    if(visibility){
        generateClassrooms(visibility);
    }
    $("#category_id").on("change", function(e) {
        $("select#sub_category_id").html("<option value=''>Select Sub Category</option>");
        if(!isNaN(parseInt(e.target.value))){
            generateSubCategories(parseInt(e.target.value));
        }
    });

    function generateSubCategories(category_id) {
        $.ajax({
            type: "GET",
            url: `${SITE_URL}app/categories/${category_id}`,
            cache: false,
            async: true,
            error: function(jqXHR, error, errorThrown) {
                console.error(error);
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status) {
                    let category_list = data.categories;
                    let optionHtml = "<option value=''>Select Sub Category</option>";
                    for (let category of category_list) {
                        optionHtml += "<option value='"+category.id+"'>"+category.name+"</option>"
                    }
                    $("select#sub_category_id").html(optionHtml);
                } else {
                    console.error(data.message);
                }
            }
        });    
    }

    $("#visible_class").on("change", function(e) {
        // $("select#sub_category_id").html("<option value=''>Select Sub Category</option>");
        if(!isNaN(parseInt(e.target.value)) && parseInt(e.target.value) == 1){
            generateClassrooms(parseInt(e.target.value));
        }else{
            $("#ShowClassroom").css('display','none');
            $("#ShowClassroom").html('');
        }
    });

    $(".check_user_role").on('change',function(){
        let value = $(this).val();
        $("#check_mesasge").text('');
        if(value == 0){
            $("#check_mesasge").text('Learn with your friends and share your knowledge with the community.');
        }else{
           $("#check_mesasge").text('Create engaging video lessons and articles and watch your students’ learning improve.'); 
       }
   });
    function generateClassrooms(ifclassroom) {
        $.ajax({
            type: "GET",
            url: `${SITE_URL}videos/classroom/${ifclassroom}`,
            cache: false,
            async: true,
            error: function(jqXHR, error, errorThrown) {
                console.error(error);
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status) {
                    let classroom_list = data.classroom;
                    let optionHtml = "<span>Select Classroom</span><select class='form-control' name='classroom_id' id='classroom_id'><option value=''>Select Classroom</option>";
                    for (let classroom of classroom_list) {
                        optionHtml += "<option value='"+classroom.id+"'>"+classroom.title+"</option>"
                    }
                    optionHtml += "</select>"+`<?= form_error('classroom_id', '<span class="error">', '</span>'); ?>`;
                    $("#ShowClassroom").css('display','block');
                    $("#ShowClassroom").html(optionHtml);
                } else {
                    console.error(data.message);
                }
            }
        });    
    }
</script>

<script type="text/javascript">
    $('.delete_video').click(function() {
        var href = $(this).data('target');
        var id = $(this).data('id');
        console.log(href);
        console.log($(href));
        $(href).data('id', id);
    });

</script>
<script>
    function previewFile(input){
        var file = $("input[type=file]").get(0).files[0];

        if(file){
            var reader = new FileReader();
            reader.onload = function(){
                $("#previewImg").attr("src", reader.result);
                $("#previewImg").show();
                $(".preview-img-close").show();
            }

            reader.readAsDataURL(file);
        }
    }
</script>
<script>
    function myStringLessMoreFunction() {
      var dots = document.getElementById("dots");
      var moreText = document.getElementById("more");
      var btnText = document.getElementById("myBtn");

      if (dots.style.display === "none") {
        dots.style.display = "inline";
        btnText.innerHTML = "Read more"; 
        moreText.style.display = "none";
    } else {
        dots.style.display = "none";
        btnText.innerHTML = "Read less"; 
        moreText.style.display = "inline";
    }
}
$(document).on("click", ".open_noteFlageModal", function () {
 var note_id = $(this).attr('data_note_id');
 $(".modal-body #note_id").val(note_id);
});
// Remove Image //
$(document).on("click", ".remove-image-button", function(e){
    let delete_section = $(this).data("delete_section");
    let file_element = $(this).data("file_element");
    $("#" + delete_section).hide();
    $("#" + file_element).val("");
    if($("input[name='delete_image']").length) {
        $("input[name='delete_image']").val("1");
    }
    $(this).hide();
    //$(".toggle-initial-preview").show();
});
$(document).on("click", ".remove-second-image-button", function(e){
    let delete_section = $(this).data("delete_section");
    let file_element = $(this).data("file_element");
    $("#" + delete_section).hide();
    $("#" + file_element).val("");
    if($("input[name='delete_second_image']").length) {
        $("input[name='delete_second_image']").val("1");
    }
    $(this).hide();
    //$(".toggle-initial-preview").show();
});
</script>
<script>
    var readURL = function(input, elementId) {
        if (input.files && input.files[0] && $("#" + elementId)) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#" + elementId).attr('src', e.target.result).css('display', 'block');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".file-input input[type='file']").change(function (e) {
        let uplocvfile = '';
        if (e.target.files.length) {
            uplocvfile = e.target.files[0].name;
        }
        $('#' + $(this).data('label_id')).text(uplocvfile);
        $(this).parents(".img_main_cnt").find(".remove-image-button").show();
        $(this).parents(".img_main_cnt").find(".profile-img-text").text("");
        $(this).parents(".img_main_cnt").find(".remove-second-image-button").show();
    });
    $(".file-upload-preview").on('change', function() {
        let data_mg_id = $(this).data('img_id');
        readURL(this, $(this).data('img_id'));
        let label_id = '#'+$(this).data('label_id');
        $(label_id).hide();    
        $(".remove-image-button").show();
    });
    $(".upload-button").on('click', function(e) {
        e.preventDefault();
        $("#"+$(this).data('preview_section')).click();
    });
</script>
<script>

</script>

<script>
    $(document).ready(function(){
      $(".up-vid").click(function(){
        $(this).find(".dropdown-menu").slideToggle();
    });

      $("header .right-drop").click(function(){
        $(this).find(".dropdown").toggle();
    });

      $(".filter-icon").click(function(){
        $(".to-filter-block").slideToggle(300);
    });





  });

// Home Page Filter //
$(document).on("click",".inner_filter_btn",function(e){
    e.preventDefault();
    var $this = $(this);
    var type = $this.data("type");
    $(".to-filter-block").find(".to-filter-cell a").removeClass('active');
    $this.addClass('active');
    if(type=="user"){
        $(".filter-subscribe-block").show();
        $(".tabs-notes-videos").hide();
        /*var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' +hashes + '&type=user';
        window.history.pushState({path:newurl},'',newurl);*/
    }
    else{
        $(".filter-subscribe-block").hide();
        $(".tabs-notes-videos").show();
    }
    
});

// Copy Text to clipboard //
function myFunction() {
    var copyText = document.getElementById("myInput");
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand("copy");
    document.getElementById('myTooltip').innerHTML = "Copied";
}

function outFunc() {
    var tooltip = document.getElementById("myTooltip");
    tooltip.innerHTML = "Copy to clipboard";
}

// ajax searching for notes
var page = 0;
var total_pages = ($("#total_page_count").val()) ? $("#total_page_count").val() : 1;

$(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() >= $(document).height()) {
        page++;
        if(page < total_pages) {
            loadMore(page);
        }
    }
});


function loadMore(page){
    $.ajax({
        type: "GET",
        url: `${SITE_URL}home?page_id=`+page,
        cache: false,
        async: true,
        beforeSend: function()
        {
            $('.loader').show();
        }
    }).done(function(data)
    {              
        $('.loader').hide();
        $('.notes-repeat').last().after(data);
        // $("#ajax-post-container").append(data);
    });
}

//  blogs social sharing

function share_linkedin(e) {
    window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + $(e).data('url'), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
    return false;
}

function share_twitter(e) {
    window.open('https://twitter.com/intent/tweet?url=' + $(e).data('url'), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
    return false;
}

function share_facebook(e) {
    window.open('https://www.facebook.com/sharer/sharer.php?u=' + $(e).data('url'), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
    return false;
}

// ajax newsletter submit

$( "#blogNewsletter" ).on( "submit", function( event ) {
    event.preventDefault();
    let data = $(this).serialize();
    postNewsletter(data);
});

async function postNewsletter(data){
    $.ajax({
        type:'POST',
        data:data,
        url:`${SITE_URL}app/newsletter`,
        success:function(data) {
            // var data = JSON.parse(data);
          if(data.status){
            $(".response-msg").append(data.message).css('color', 'green').delay(5000).fadeOut(800);
        }else{
            $(".response-msg").append(data.message).css('color', 'red').delay(5000).fadeOut(800);
        }
    }
});
}

</script>


