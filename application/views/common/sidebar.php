<script src="<?= ms_site_url('assets/js/frontend/sidebar.js') ?>"></script>
<div class="left-sec nav-menu">
	<?php
		if (isUserLoggedIn()) { ?>
	<div class="top-sec">
		<div class="img-sec">
			<?php
				if ($user_data->image) { ?>
			<img src="<?= ms_site_url('uploads/avatars/'.$user_data->image) ?>">
			<?php } else { ?>
			<?= strtoupper($user_data->name[0]); ?>
			<?php }
				?>
		</div>
		<div class="img-cont">
			<p><?= $user_data->name ?></p>
			<p>
				<?php 
					$subscribers_count = count($user_data->subscriber_user_ids);
					$subscribed_count = count($user_data->subscribed_user_ids); 
					?>
				<?= $subscribers_count ?> 
				<?php echo "Subscribers"; ?>
			</p>
			<p>
				<?= $subscribed_count ?> 
				<?php echo "subscriptions"; ?>
			</p>
		</div>
	</div>
	<?php	}
		?>
	<div class="bottom-sec"  id="accordion">
		<?php if (userRoleIsTeacher()) { ?>
		<div class="card" id="dashboard">
			<div class="card-header sidebar-menu">
				<div class="">
					<a href="<?= ms_site_url('dashboard') ?>">
						<div class="icon-sec">
							<img src="<?= ms_site_url('assets/images/frontend/dashboard-icon.png') ?>">
						</div>
						<div class="icon-cont">
							<p>Dashboard</p>
						</div>
					</a>
					</li>			
				</div>
			</div>
		</div>
		<div class="card" id="home">
			<div class="card-header sidebar-menu">
				<div class="">
					<a href="<?= ms_site_url('home') ?>">
						<div class="icon-sec">
							<img src="<?= ms_site_url('assets/images/frontend/home.png') ?>">
						</div>
						<div class="icon-cont">
							<p>Home</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="card" id="user-subscriptions">
			<div class="card-header sidebar-menu">
				<div class="">
					<a href="<?= ms_site_url('user/subscriptions') ?>">
						<div class="icon-sec">
							<img src="<?= ms_site_url('assets/images/frontend/subscription.png') ?>">
						</div>
						<div class="icon-cont">
							<p>Subscriptions</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="card" id="user-subscribers">
			<div class="card-header sidebar-menu">
				<div class="">
					<a href="<?= ms_site_url('user/subscribers') ?>">
						<div class="icon-sec">
							<img src="<?= ms_site_url('assets/images/frontend/subscribers.png') ?>">
						</div>
						<div class="icon-cont">
							<p>Subscribers</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="card" id="me">
			<div class="card-header sidebar-menu">
				<div class="">
					<a href="<?= ms_site_url('me') ?>">
						<div class="icon-sec">
							<img src="<?= ms_site_url('assets/images/frontend/profile.png') ?>">
						</div>
						<div class="icon-cont">
							<p>Profile</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<!-- todo need to add teacher as user requested classes -->
		<!-- <li>
			<a href="javascript:void(0)">
				<div class="icon-sec">
					<img src="<?= ms_site_url('assets/images/frontend/comments-icon.png') ?>">
				</div>
				<div class="icon-cont">
					<p>Comments</p>
				</div>
			</a>
			</li>
			<li>
			<a href="javascript:void(0)">
				<div class="icon-sec">
					<img src="<?= ms_site_url('assets/images/frontend/quizs-icon.png') ?>">
				</div>
				<div class="icon-cont">
					<p>Quizzes</p>
				</div>
			</a>
			</li>
			<li>
			<a href="javascript:void(0)">
				<div class="icon-sec">
					<img src="<?= ms_site_url('assets/images/frontend/flash-card-icon.png') ?>">
				</div>
				<div class="icon-cont">
					<p>Flashcards</p>
				</div>
			</a>
			</li> -->
		<?php } else { ?>
		<div class="card" id="home">
			<div class="card-header sidebar-menu">
				<div class="">
					<a href="<?= ms_site_url('home') ?>">
						<div class="icon-sec">
							<img src="<?= ms_site_url('assets/images/frontend/home.png') ?>">
						</div>
						<div class="icon-cont">
							<p>Home</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="card" id="user-subscriptions">
			<div class="card-header sidebar-menu">
				<div class="">
					<a href="<?= ms_site_url('user/subscriptions') ?>">
						<div class="icon-sec">
							<img src="<?= ms_site_url('assets/images/frontend/subscription.png') ?>">
						</div>
						<div class="icon-cont">
							<p>Subscriptions</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="card" id="user-subscribers">
			<div class="card-header sidebar-menu">
				<div class="">
					<a href="<?= ms_site_url('user/subscribers') ?>">
						<div class="icon-sec">
							<img src="<?= ms_site_url('assets/images/frontend/subscribers.png') ?>">
						</div>
						<div class="icon-cont">
							<p>Subscribers</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="card" id="me">
			<div class="card-header sidebar-menu">
				<div class="">
					<a href="<?= ms_site_url('me') ?>">
						<div class="icon-sec">
							<img src="<?= ms_site_url('assets/images/frontend/profile.png') ?>">
						</div>
						<div class="icon-cont">
							<p>Profile</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<!-- <li>
			<a href="<?= ms_site_url('user/history') ?>">
					<div class="icon-sec">
						<img src="<?= ms_site_url('assets/images/frontend/history.png') ?>">
					</div>
					<div class="icon-cont">
						<p>History</p>
					</div>
				</a>
			</li>
			<li>
				<a href="<?= ms_site_url('user/liked-videos') ?>">
					<div class="icon-sec">
						<img src="<?= ms_site_url('assets/images/frontend/liked.png') ?>">
					</div>
					<div class="icon-cont">
						<p>Liked Videos</p>
					</div>
				</a>
			</li> -->
		<?php } ?>
		<?php if (isUserLoggedIn()) { ?>
		<div class="card" id="my-content">
			<div class="card-header sidebar-menu" id="menumycontent">
				<div class="">
					<a class="collapsed card-link" data-toggle="collapse" data-target="#content" aria-expanded="false" aria-controls="content">
						<div class="icon-sec">
							<img src="<?= ms_site_url('assets/images/frontend/edit-icon-blue.png') ?>">
						</div>
						<div class="icon-cont">
							<p>My Content</p>
						</div>
					</a>
				</div>
			</div>
			<div id="content" class="collapse" aria-labelledby="menumycontent" data-parent="#accordion">
				<div class="card-body">
					<ul class="sub-down">
						<?php if (userRoleIsTeacher()) { ?>
						<li class=""><a href="<?= ms_site_url('me/videos') ?>" data-resource_slug="videos">Videos</a></li>
						<?php } ?>
						<li class=""><a href="<?= ms_site_url('notes') ?>" data-resource_slug="notes">Notes</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!--<div class="card" id="my-classroom">
			<div class="card-header sidebar-menu" id="menuClassrooms">
				<div class="">
					<a class="collapsed card-link" data-toggle="collapse" data-target="#Classrooms" aria-expanded="false" aria-controls="Classrooms">
						<div class="icon-sec">
							<img src="<?= ms_site_url('assets/images/frontend/classroom.png') ?>">
						</div>
						<div class="icon-cont">
							<p>Classrooms</p>
						</div>
					</a>
				</div>
			</div>
			<div id="Classrooms" class="collapse" aria-labelledby="menuClassrooms" data-parent="#accordion">
				<div class="card-body">
					<ul class="sub-down">
						<?php if (userRoleIsTeacher()) { ?>
						<li class=""><a href="<?= ms_site_url('classroom') ?>">List</a></li>
						<?php } else{ ?>
						<li class=""><a href="<?= ms_site_url('user/classroom') ?>">List</a></li>
						<?php } ?>
						<?php if (userRoleIsTeacher()) { ?>
						<li class=""><a href="<?= ms_site_url('classroom-requests') ?>">Requests</a></li>
						<li class=""><a href="<?= ms_site_url('user/classroom') ?>">Permissions</a></li>
						<?php } ?>
						<li class=""><a href="<?= ms_site_url('classroom-videos') ?>">Videos</a></li>
					</ul>
				</div>
			</div>
			</div>-->
		<div class="card" id="user-history">
			<div class="card-header sidebar-menu">
				<div class="">
					<a href="<?= ms_site_url('user/history') ?>">
						<div class="icon-sec">
							<img src="<?= ms_site_url('assets/images/frontend/history.png') ?>">
						</div>
						<div class="icon-cont">
							<p>Videos History</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="card" id="settings">
			<div class="card-header sidebar-menu">
				<div class="">
					<a href="<?= ms_site_url('settings') ?>">
						<div class="icon-sec">
							<img src="<?= ms_site_url('assets/images/frontend/setting-icon.png') ?>">
						</div>
						<div class="icon-cont">
							<p>Settings</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
</div>