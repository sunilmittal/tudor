<?php if (isUserLoggedIn()) { ?>
<div class="right-inner custom-recently-video-block">
	<div class="right-outer">
		<h4>Recently Watched</h4>
	  <?php 
      //if (count($history_videos) && isset($history_videos)) {
      if (count($history_videos)) { 
      ?>
		<div class="clearfix"></div>
		<div class="full-box">
			<?php
				$ycount = 1;
				$count = 1;
				$checkotherDates = '';
				foreach ($history_videos as $video) {
				   if(isset($video->video_details) && ($video->video_details)){
				      if(date('Y-m-d') == date('Y-m-d',strtotime($video->created_at))){
				         if($count == 1){
				            echo "<div class='col-md-12 time-period'><h5>Today</h5></div><div class='clearfix'></div>";
				         }
				      }elseif (date('Y-m-d',strtotime("-1 days")) == date('Y-m-d',strtotime($video->created_at))) {
				         if($ycount == 1){
				            echo "<div class='col-md-12'><h5>Yesterday</h5></div>
				            <div class='clearfix'></div>";
				            $ycount++;
				         }
				      }else{
				         $prevoius_date = date('M d',strtotime($video->created_at));
				         if($prevoius_date != $checkotherDates){
				            $checkotherDates = $prevoius_date;
				            echo "<div class='col-md-12 time-period'><h5>$prevoius_date</h5></div>
				            <div class='clearfix'></div>";
				         }
				      }
				      ?>
			<div class="box1">
				<a href="<?= ms_site_url('videos/'.$video->video_details->slug) ?>">
					<div class="img-outer">
						<div class="img-sec">
							<?php if ($video->video_details->thumbnail) { ?>
							<img src="<?= ms_site_url('uploads/thumbnails/'.$video->video_details->thumbnail) ?>">
							<?php } else {  ?>
							<img src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
							<?php } ?>
						</div>
						<div class="video-icon"><img src="<?= ms_site_url('assets/images/frontend/video-play.png') ?>"></div>
						<span class="video-duration"><?= convert_seconds($video->video_details->duration) ?></span>
					</div>
					<div class="box-cont">
						<p><strong><?= generateDotsForMoreInfo($video->video_details->title, 30) ?></strong></p>
						<div class="box-content">
							<div class="box-content-left">
								<div class="p-pic profile-name-wrap">
									<span>
									<?php
										if ($video->video_details->user_and_channel->image) { ?>
									<img src="<?= ms_site_url('uploads/avatars/'.$video->video_details->user_and_channel->image) ?>">
									<?php } else { ?>
									<?= strtoupper($video->video_details->user_and_channel->user_name[0]) ?>
									<?php }
										?>
									</span> 
									<p><?= $video->video_details->user_and_channel->user_name ?></p>
								</div>
								<div class="pic-cont">
									
									<p><img class="eye-view" src="<?= ms_site_url('assets/images/frontend/eyes-icon.png') ?>"> <?php if($video->video_details->view_count) { ?>
									<span class="view-counts"><?= thousandsFormat($video->video_details->view_count) ?></span>
									<?php } else{ ?><span class="view-counts">0</span> <?php } ?> <span class="views">views</span></p>
								</div>
							</div>
							<div class="box-content-right">
								<p><?= getDateInFormat($video->video_details->created_at) ?></p>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</a>
			</div>
			<?php } $count++; } ?>
		</div>
		<?php } else{ ?>
		<div class="no-data">
			<div class="no-rec">
				<img src="<?= ms_site_url('assets/images/frontend/no-record-found.png') ?>"/>
				<h6>No Videos Watched Yet</h6>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
<?php } ?>