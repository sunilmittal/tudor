<section>
    <div class="dashboard-sec">
        <div class="inner-sec">
            <?php $this->load->view('common/sidebar') ?>
            <div class="right-sec">
                <div class="right-inner blank-page custom-blank-page">
                <div class="img-sec"><img src="<?= ms_site_url('assets/images/frontend/blank-icon.png') ?>"/></div>
                   <h4><?= isset($main_line) ? $main_line : 'Don\'t miss new videos' ?></h4>
                   <p><?= isset($main_sub_line) ? $main_sub_line : 'Sign in to see updates from your favorite YouTube channels' ?></p>
                   <a href="<?= ms_site_url('/login') ?>" class="s-popup cmn-btn">
                  <span> Sign in </span></a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>