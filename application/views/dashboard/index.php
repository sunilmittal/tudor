<section>
    <div class="dashboard-sec">
        <div class="inner-sec">
            <?php $this->load->view('common/sidebar') ?>
            <div class="right-sec">
                <div class="shadow-card-bg dashboard-block">
                <h4>Dashboard</h4>
                <div class="box-outer row">
                    <div class="box col-lg-3 col-md-4 col-sm-6">
                        <div class="box-cell">
                            <h5>Total Subscribers</h5>
                            <h3><?= kConverter($stat['subscribers']['total']) ?></h3>
                        </div>
                    </div>
                    <!--<div class="box">
                        <h5>Total Comments</h5>
                        <h3><?= kConverter($stat['comments']['total']) ?></h3>
                    </div>-->
                    <div class="box col-lg-3 col-md-4 col-sm-6">
                         <div class="box-cell">
                            <h5>Total Views</h5>
                            <h3><?= kConverter($stat['views']['total']) ?></h3>
                        </div>
                    </div>
                    <div class="box col-lg-3 col-md-4 col-sm-6">
                         <div class="box-cell">
                            <h5>Subscribers in past 7 Days</h5>
                            <h3><?= kConverter($stat['subscribers']['last_seven_days']) ?></h3>
                        </div>
                    </div>
                    <!--<div class="box">
                        <h5>Comments in past 7 Days</h5>
                        <h3><?= kConverter($stat['comments']['last_seven_days']) ?></h3>
                    </div>-->
                    <div class="box col-lg-3 col-md-4 col-sm-6">
                           <div class="box-cell">
                                <h5>Views  in past 7 days</h5>
                                <h3><?= kConverter($stat['views']['last_seven_days']) ?></h3>
                            </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>