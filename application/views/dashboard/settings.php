<section>
    <div class="dashboard-sec add-video profile-setting">
        <div class="inner-sec">
            <?php $this->load->view('common/sidebar') ?>
            <div class="right-sec">
                <div class="right-inner shadow-card-bg">
                    <div class="inner-container-sm">
                    <h4>Settings</h4>
                    <?= form_error('image', '<span class="error">', '</span>'); ?>
                    <form method="post" enctype="multipart/form-data" autocomplete="off">
                        <div class="row set-inner">
                            <div class="col-lg-2 pos-r cmn-space2 img_main_cnt">

                                <div class="icon-sec img-sec file-input profile-img-inner">
									<?php if(!($user_data->image)){ $delete_icon_class = "display_none"; $user_img_src = ""; ?>
									<label class="profile-img-text">
                                        <?= strtoupper($user_data->name[0]); ?>
                                    </label>
                                    <img id="image-preview" class="display_none" />
									<?php } else { $delete_icon_class = ""; ?>
                                         <img src="<?= ms_site_url('uploads/avatars/'.$user_data->image) ?>" alt="" id="image-preview" />
									<?php } ?>
                                    
                                    <input type="file" id="user-image" name="image" class="user-file-upload" accept="image/*" data-img_id="image-preview" data-msg-accept="Please select only jpeg, bmp, png, jpg file." data-label_id="image-preview-label">
                                    
                                    <span>
                                        <a class="upload-user-img-button" data-preview_section="user-image"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a class="remove-image-button <?= $delete_icon_class ?>" data-file_element="user-image" data-delete_section="image-preview"><i class="fa fa-trash" aria-hidden="true"></i> </a>
                                        
                                        <input type="hidden" name="delete_image" value="0" />
                                    </span>
								</div>
								
                            </div>
                            <div class="col-lg-5 col-12 left-part cmn-space2">
                                <span>Full Name</span>
                                <input class="form-control" type="text" name="name" placeholder="Full name" value="<?= set_value('name', $user_data->name) ?>" />
                                <?= form_error('name', '<span class="error">', '</span>'); ?>
                            </div>
                            <div class="col-lg-5 col-12 left-part cmn-space2">
                                <span>Email Address</span>
                                <input class="form-control" type="email" name="email" readonly="readonly"
                                    placeholder="Email address" value="<?= $user_data->email ?>" />
                            </div>
                            <div class="col-lg-12 cmn-space2 img_main_cnt default-banner-block">
                                <div class="img-sec">
                                    <?php
                                    if ($user_data->banner) {  $delete_banner_icon_class = "";  ?>
                                        <img src="<?= ms_site_url('uploads/avatars/'.$user_data->banner) ?>" id="banner_preview">
                                    <?php } else { $delete_banner_icon_class="display_none"; ?>
                                    <div class="default-banner-img">
                                        <img id="banner_preview" class="banner_img_preview">
                                        <span class="default-user-name"><?= $user_data->name ?> </span>
                                    </div>
                                    <?php } ?>
                       

                                    <div class="profile-banner-setting-block">                                  
                                        <div class="file-input edit-icon" class="upload-button">
                                            <label for="user_banner"><span><i class="fa fa-pencil" aria-hidden="true"></i> </span> 
                                            </label>
                                            
                                            <input type="file" id="user_banner" name="user_banner" class="file-upload" data-img_id="banner_preview" accept="image/*">
                                            <input type="hidden" name="delete_second_image" value="0" />
                                        </div>
                                        <span>
                                            <a class="remove-second-image-button <?= $delete_banner_icon_class; ?>" data-file_element="user_banner" data-delete_section="banner_preview"><i class="fa fa-trash" aria-hidden="true"></i> </a>
                                        </span>
                                        <?= form_error('user_banner', '<span class="error">', '</span>'); ?>
                                    </div>
                                
                                </div>
                                <?php if (userRoleIsTeacher()) { ?>
                                        <p>Talking about yourself is almost just as important as the lessons you teach. Here is a great place to tell everyone your experience with your subjects as well as why you are passionate about what you teach.</p>
                                <?php } else { ?>
                                    <p>Your bio helps other users know you better. Try telling them a bit about yourself including what you’re interested in learning and what you’re good at.</p>
                                        <!-- <p>What are your academic interests? What clubs are you in? What are your favorite sports? Feel free to talk about anything school related.</p> -->

                                <?php } ?>
                            </div>
                            <div class="col-lg-12 cmn-space2">
                                <span>Profile Bio</span>
                                <textarea class="form-control" placeholder="Tell us about yourself!" name="description"><?= set_value('description', $user_data->description) ?></textarea>
                                <?= form_error('description', '<span class="error">', '</span>'); ?>
                            </div>
                            <div class="col-lg-4 col-12 left-part cmn-space2">
                                <span>Current Password</span>
                                <input class="form-control" type="password" name="current_password" placeholder="*******" autocomplete="new-password">
                                <?= form_error('current_password', '<span class="error">', '</span>'); ?>
                            </div>
                            <div class="col-lg-4 col-12 left-part cmn-space2">
                                <span>New Password</span>
                                <input class="form-control" type="password" name="password" 
                                    placeholder="*******">
                                <?= form_error('password', '<span class="error">', '</span>'); ?>
                            </div>
                            <div class="col-lg-4 col-12 left-part cmn-space2">
                                <span>Confirm Password</span>
                                <input class="form-control" type="password" name="confirm_password"  placeholder="*******">
                                <?= form_error('confirm_password', '<span class="error">', '</span>'); ?>
                            </div>
                            <div class="col-lg-6 col-12 right-part cmn-space2">
                                <input class="upload-vid cmn-btn" type="submit" value="Update" name="submit">
                            </div>

                    </form>
                     
                    <div class="col-md-12 manage-account-dropdown text-right">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Manage Account
                                <span class="caret"></span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      
                                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#manage-account">
                                Delete My Account</button>
               
                                </div>
                        </div>
                    </div>               
           
                </div>
            </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>


<!-- manage account Modal -->
<div class="modal manage-account fade" id="manage-account" tabindex="-1" role="dialog" aria-labelledby="manage-accountLabel">
  <div class="modal-dialog" role="document">
    <form method="post" action="<?= ms_site_url('user/delete') ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <!-- <h4 class="modal-title" id="manage-accountLabel">Are you sure you want to delete your account?</h4> -->
            </div>
            <div class="modal-body">
                <h2>Are you sure you want to delete your account?</h2>
            <p>Deleting your account will also remove all content added to the platform by you and you will lose all your subscribers and subscriptions. Keep in mind you will not be able to reactivate or retrieve your account or any information added by you.</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Delete My Account</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Cancel</button>
            </div>
        </div>
    </form>
  </div>
</div>