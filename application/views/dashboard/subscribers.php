<section>
	<div class="dashboard-sec add-video my-videos">
		<div class="inner-sec">
			<?php $this->load->view('common/sidebar') ?>
			<div class="right-sec">
				<div class="right-inner shadow-card-bg">
					<h4>Subscribers</h4>
					<div class="clearfix"></div>
					<div class="video-inner subscribe">
						<div class="table-responsive">
						<?php if (count($subscribers)) {
							foreach ($subscribers as $subscriber) { ?>
							<div class="filter-subscribe-block" style="">
								<div class="subscribe-row custom-subc-row">
									<div class="rounder-image-wrap">
										<a href="<?= ms_site_url('user/'.$subscriber->id.'/profile') ?>" target="_blank">
											<?php
											if ($subscriber->image) { ?>
												<img src="<?= ms_site_url('uploads/avatars/'.$subscriber->image) ?>">
											<?php } else { ?>
												<span><?= strtoupper($subscriber->name[0]); ?></span>
											<?php }
											?>
                                        </a>
									</div>
									<div class="filter-content">
										<a href="<?= ms_site_url('user/'.$subscriber->id.'/profile') ?>" target="_blank">
								
											<div class="user-role-name-wrap"><p><?= $subscriber->name ?></p><span><?= getUserRole($subscriber) ?></span></div>
											<p><?= $subscriber->total_videos ?> Videos | <?= $subscriber->total_notes ?> Notes</p>
										</a>
									</div>
									<div class="video-right-subscribe">
										<?php if (userRoleIsTeacher() && $subscriber->user_role != TEACHER_ROLE) { ?>
                                        <?php  } else { ?>
                                            <?php if (in_array($subscriber->id, $user_data->subscribed_user_ids)) { ?>
                                                <div class="video-right-subscribe">
                                                    <a href="<?= ms_site_url('user/'.$subscriber->id.'/unsubscribe/?redirect_url=user/subscribers') ?>" class="cmn-btn">UNSUBSCRIBE</a>
                                                </div>
                                            <?php } else { ?>
                                                <div class="video-right-subscribe">
                                                    <a href="<?= ms_site_url('user/'.$subscriber->id.'/subscribe/?redirect_url=user/subscribers') ?>" class="cmn-btn">SUBSCRIBE</a>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
									</div>
								</div>
							</div>
						<?php } } else { ?>
						<div class="subscription no-data">
								<div class="no-data" colspan="9">
									<img src="<?= ms_site_url('assets/images/frontend/subscribers-xl.png') ?>"/>
									<h6>No subscribers yet</h6>
								</div>
                        	</div>
                        <?php } ?>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>