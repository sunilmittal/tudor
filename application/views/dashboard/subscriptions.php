<section>
	<div class="dashboard-sec add-video my-videos">
		<div class="inner-sec">
			<?php $this->load->view('common/sidebar') ?>
			<div class="right-sec">
				<div class="right-inner shadow-card-bg">
					<h4>Subscriptions</h4>
					<div class="clearfix"></div>
					<div class="video-inner subscribe">
					<div class="table-responsive">
						<?php if (count($subscriptions)) {
							foreach ($subscriptions as $subscription) { ?>
							<div class="filter-subscribe-block" style="">
								<div class="subscribe-row custom-subc-row">
									<div class="rounder-image-wrap">
										<a href="<?= ms_site_url('user/'.$subscription->id.'/profile') ?>" target="_blank">
											<?php
											if ($subscription->image) { ?>
												<img src="<?= ms_site_url('uploads/avatars/'.$subscription->image) ?>">
											<?php } else { ?>
												<span><?= strtoupper($subscription->name[0]); ?></span>
											<?php }
											?>
                                        </a>
									</div>
									<div class="filter-content">
										<a href="<?= ms_site_url('user/'.$subscription->id.'/profile') ?>" target="_blank">
											<div class="user-role-name-wrap"><p><?= $subscription->name ?></p><span><?= getUserRole($subscription) ?></span></div>
											<p><?= $subscription->total_videos ?> Videos | <?= $subscription->total_notes ?> Notes</p>
										</a>
									</div>
									<div class="video-right-subscribe">
										<?php if (userRoleIsTeacher() && $subscription->user_role != TEACHER_ROLE) { ?>
                                        <?php  } else { ?>
                                            <?php if (in_array($subscription->id, $user_data->subscribed_user_ids)) { ?>
                                                <div class="video-right-subscribe">
                                                    <a href="<?= ms_site_url('user/'.$subscription->id.'/unsubscribe/?redirect_url=user/subscriptions') ?>" class="cmn-btn">UNSUBSCRIBE</a>
                                                </div>
                                            <?php } else { ?>
                                                <div class="video-right-subscribe">
                                                    <a href="<?= ms_site_url('user/'.$subscription->id.'/subscribe/?redirect_url=user/subscriptions') ?>" class="cmn-btn">SUBSCRIBE</a>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
									</div>
								</div>
							</div>
						<?php } } else { ?>
						<div class="subscription no-data">
								<td class="no-data" colspan="9">
									<img src="<?= ms_site_url('assets/images/frontend/subscriptions.png') ?>"/>
									<h6>No subscriptions yet</h6>
								</td>
						</div>
                        <?php } ?>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>