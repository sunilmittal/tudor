<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="x-apple-disable-message-reformatting">
		<title>Newsletter</title>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
		<!-- CSS Reset : BEGIN -->
		<style type="text/css">
			/* What it does: Remove spaces around the email design added by some email clients. */
			/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
			html,
			body {
			margin: 0 auto !important;
			padding: 0 !important;
			height: 100% !important;
			width: 100% !important;
			}
			/* What it does: Stops email clients resizing small text. */
			* {
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
			}
			/* What it does: Centers email on Android 4.4 */
			div[style*="margin: 16px 0"] {
			margin: 0 !important;
			}
			/* What it does: Stops Outlook from adding extra spacing to tables. */
			table,
			td {
			mso-table-lspace: 0pt !important;
			mso-table-rspace: 0pt !important;
			}
			/* What it does: Fixes webkit padding issue. */
			table {
			border-spacing: 0 !important;
			border-collapse: collapse !important;
			}
			/* What it does: Uses a better rendering method when resizing images in IE. */
			img {
			-ms-interpolation-mode:bicubic;
			}
			/* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
			a {
			text-decoration: none;
			}
			/* What it does: A work-around for email clients meddling in triggered links. */
			a[x-apple-data-detectors],  /* iOS */
			.unstyle-auto-detected-links a,
			.aBn {
			border-bottom: 0 !important;
			cursor: default !important;
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
			}
			/* What it does: Prevents Gmail from changing the text color in conversation threads. */
			.im {
			color: inherit !important;
			}
			/* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
			.a6S {
			display: none !important;
			opacity: 0.01 !important;
			}
			/* If the above doesn't work, add a .g-img class to any image in question. */
			img.g-img + div {
			display: none !important;
			}
			* {
			margin: 0;
			padding: 0
			}
			body {
			font-family: 'Open Sans', sans-serif;
			margin: 0;
			padding: 0;
			background: #EAECED;
			}
			html, table, tr, td {
			margin:0;padding:0; 
			}
			h2 {
			margin: 0;
			padding: 0
			}
			table {
			width: 100%
			}
			p{
			font-size:13px; font-weight:400; color:#252b33; 
			}
			.responsive-table {
			width: calc(33.3% - 4px);
			display: inline-block;
			vertical-align: middle;
			text-align: center;
			}
			img {
			max-width: 100%
			}
			.conatiner-space {
			padding-left: 30px;
			padding-right: 30px;
			}
			@media (max-width:600px) {
			.responsive-table {
			width: 100%;
			min-width: 100% !important;
			}
			}
		</style>
	</head>
	<body>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" role="presentation">
			<tr>
				<td align="center" valign="top">
					<!--[if (mso)|(IE)]>
					<table width="600" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left" valign="top" width="100%">
								<![endif]-->
								<!--[if mso 16]>
								<table width="600" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td align="left" valign="top" width="100%">
											<![endif]-->
											<table width="100%" style="max-width:600px;" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td  style="padding-top: 25px;" colspan="2" class="conatiner-space">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td>
																	<a href="<?= ms_site_url() ?>" target="_blank"><img alt="logo" src="https://teqdeftdev.com/mail/logo.png" style="border:0" width="176" height="61"></a>
																</td>
																<td style="text-align: right;">
																	<a href="<?= ms_site_url() ?>" style="font-weight:200!important;font-size:12px;color:#989ca5!important;padding:1px 0;border-bottom:1px solid #bfc4c8;text-decoration:none!important" target="_blank">View in
																	browser</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td class="conatiner-space" style="padding-top: 15px; padding-bottom: 15px;" colspan="2">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td colspan="2">
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td class="conatiner-space" style="padding-top: 25px; padding-bottom: 25px; background: #fff" colspan="2">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td>
																	<h2 style="margin:0 0 10px 0!important;font-size:19px!important;line-height:26px!important;font-weight:700!important;color:#1076bd!important;background: transparent !important">
																		Hello <?php echo $to_name; ?>,
																	</h2>
																</td>
															</tr>
															<tr>
																<td valign="top" style="font-size:16px; font-weight:600; color:#252b33;   font-style: italic;">
																	Thanks for joining the  <?php echo $company_name; ?> community, glad to have
																	you on board!
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td class="conatiner-space" style="padding-top: 15px; padding-bottom: 15px;" colspan="2">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td colspan="2">
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td class="conatiner-space" style="padding-top: 25px; padding-bottom: 25px; background: #fff" colspan="2">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td>
																	<h2 style="margin:0 0 10px 0!important;font-size:19px!important;line-height:26px!important;font-weight:700!important;color:#1076bd!important;background: transparent !important">
																		So what is <?php echo $company_name; ?>?
																	</h2>
																</td>
															</tr>
															<tr>
																<td valign="top">
																	<p style="padding-bottom: 20px"><?php echo $company_name; ?> is a place to create, share, and view academic content provided by a network of knowledgeable users happy to help anybody who needs it.</p>
																	<p style="padding-bottom: 20px">Whether you&rsquo;re a teacher or a learner, <?php echo $company_name; ?> wants to provide you with everything you need to keep doing what you&rsquo;re doing, but better.</p>
																	<p style="padding-bottom: 20px">Provide the community with videos and short articles known as Notes on academic subjects ranging from biology and physics to music and literature.</p>
																	<p style="padding-bottom: 20px">Use your academic strengths to provide learners with a unique and fresh take on a subject or topic. While there may already be plenty of articles available about what you teach, what really matters is your personal touch and perspective on it.</p>
																	<p style="padding-bottom: 20px">Access a diverse community of people who want to grow and want to help you grow. Never feel like you have one option or method of learning. Always know there is someone there to give you your ideal learning experience.</p>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td class="conatiner-space" style="padding-top: 25px; padding-bottom: 25px;" colspan="2">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td>
																	<h2 style="margin:0 0 10px 0!important;font-size:19px!important;line-height:26px!important;font-weight:700!important;color:#1076bd!important;background: transparent !important; text-align: center;">
																		Wondering what to do next? 
																	</h2>
																</td>
															</tr>
															<tr>
																<td valign="top" style="text-align: center;">
																	<p style="padding-bottom: 20px"> Be sure to visit our Getting Started page for tips on creating notes and how to start adding videos.</p>
																	<p style="padding-bottom: 20px">If you have any questions about <?php echo $company_name; ?> or would like to provide feedback, feel free to check out our Contact Us page and fill out our form or email our team at <a href="mailto:support@tudorlearning.com" style="color: #252b33">support@tudorlearning.com</a> </p>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td valign="top" style="text-align: center;" >
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td>
																	<h2 style="margin:0 0 10px 0!important;font-size:19px!important;line-height:26px!important;font-weight:700!important;color:#1076bd!important;background: transparent !important;vertical-align: middle;">
																	Check us out on social media!
																	</h2>
																</td>
															</tr>
															<tr>
																<td class="responsive-table" valign="top">
																	<table>
																		<tr>
																			<td style="vertical-align: middle;">
																				<a href="https://www.facebook.com/tudorlearning-106000797822171" style="line-height:50px;display:block;text-decoration:none!important;width:100%" target="_blank">
																				<img alt="Like On Facebook" src="https://teqdeftdev.com/mail/facebook.png">
																				</a>
																			</td>
																		</tr>
																	</table>
																</td>
																<td class="responsive-table"  valign="top">
																	<table>
																		<tr>
																			<td style="vertical-align: middle;">
																				<a href="https://www.instagram.com/tudorlearning/" style="line-height:50px;display:block;text-decoration:none!important;width:100%" target="_blank">
																				<img alt="Follow on Instagram" src="https://teqdeftdev.com/mail/instagram.png">
																				</a>
																			</td>
																		</tr>
																	</table>
																</td>
																<td class="responsive-table" valign="top">
																	<table>
																		<tr>
																			<td style="vertical-align: middle;">
																				<a href="https://twitter.com/Tudor_learning" style="line-height:50px;display:block;text-decoration:none!important;width:100%" target="_blank">
																				<img alt="Follow on Twitter" src="https://teqdeftdev.com/mail/twitter.png">
																				</a>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td class="conatiner-space" style="padding-top: 25px; padding-bottom: 25px;" colspan="2">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td valign="top" style="text-align: center;">
																	<p style="padding-bottom: 20px"><a href="<?= ms_site_url() ?>" style="font-size:12px!important;color:#252b33!important;text-decoration:underline!important" target="_blank" ><?= ms_site_url() ?></a><br/>  © <?= date('Y') ?> </p>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
											<!--[if mso 16]>
										</td>
									</tr>
								</table>
								<![endif]-->
								<!--[if (mso)|(IE)]>
							</td>
						</tr>
					</table>
					<![endif]-->
				</td>
			</tr>
		</table>
	</body>
</html>