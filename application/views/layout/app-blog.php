<!doctype html>
<html lang="en">
	<!-- common/head.php -->
	<?php $this->view('common/head'); ?>
	<!-- common/head.php -->
	<body>
		<!-- common/nav-menu.php -->
		<?php $this->view('common/nav-menu-blog'); ?>
		<!-- common/nav-menu.php -->
		<!-- common/header.php -->
		<?php $this->view('common/header-blog'); ?>
		<!-- common/header.php -->
		<!-- dynamic pages content -->
		<?php $this->view($path); ?>
		<!-- dynamic pages content -->
		<!-- footer.php -->
		<?php $this->view('common/footer.php'); ?>
		<!-- footer.php -->
		<!-- script.php -->
		<?php $this->view('common/script.php'); ?>
		<!-- script.php -->
	</body>
</html>