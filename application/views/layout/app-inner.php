<!doctype html>
<html lang="en">
<!-- common/head.php -->
  <?php $this->view('common/head'); ?>
<!-- common/head.php -->
<body>
<!-- common/nav-menu.php -->
  <?php $this->view('common/nav-menu'); ?>
<!-- common/nav-menu.php -->
<!-- common/header.php -->
  <?php $this->view('common/header'); ?>
<!-- common/header.php -->
<!-- dynamic pages content -->
  <?php $this->view($path); ?>
<!-- dynamic pages content -->
<!-- footer.php -->
  <?php $this->view('common/footer.php'); ?>
<!-- footer.php -->
<!-- footer.php -->
  <?php $this->view('common/script.php'); ?>
<!-- footer.php -->
</body>
</html>