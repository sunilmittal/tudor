<section>
	<div class="dashboard-sec add-video custom-add-v-block">
		<div class="inner-sec">
			<?php $this->load->view('common/sidebar') ?>
			<div class="right-sec"> 
				<div class="shadow-card-bg custom-add-note-student">
				<form method="post" enctype="multipart/form-data" id="add_note">
					<div class="right-inner">
						<h4>Add Note</h4>
						<div class="common-tooltip">

						<span class="info-icon"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
						<span class="tooltiptext">Need help or tips on how to start making Notes? <a href="<?= ms_site_url('sharing-notes') ?>" target="_blank">Click here for our Notes help page.</a></span>
						</div>
						<div class="row add-inner">
							<div class="col-lg-12 cmn-space">
								<span>Title <sup>*</sup></span>
								<input type="text" class="form-control" name="title" placeholder="eg. Note Title" value="<?= set_value('title') ?>" />
								<?= form_error('title', '<span class="error">', '</span>'); ?>
							</div>
							<div class="col-lg-12 cmn-space">
								<span>Select Category</span>
								<select class="form-control" name="category_id" id="category_id">
									<option value="">Select Category</option>
									<?php if (isset($categories) && is_array($categories) && count($categories) > 0) { ?>
										<?php foreach ($categories as $category) { ?>
											<option value="<?php echo $category->id; ?>" <?php echo set_value('category_id') == $category->id ? 'selected' : ''; ?>>
												<?php echo $category->name; ?>
											</option>
										<?php } ?>
									<?php } ?>
								</select>
							</div>
							<div class="col-lg-12 cmn-space">
								<span>Select Sub Category</span>
								<select class="form-control" name="sub_category_id" id="sub_category_id">
									<option value="">Select Category First</option>
									<?php if (isset($sub_categories) && is_array($sub_categories) && count($sub_categories) > 0) { ?>
										<?php foreach ($sub_categories as $sub_category) { ?>
											<option value="<?php echo $sub_category->id; ?>" <?php echo set_value('sub_category_id') == $sub_category->id ? 'selected' : ''; ?>>
												<?php echo $sub_category->name; ?>
											</option>
										<?php } ?>
									<?php } ?>
								</select>
							</div>
							<div class="col-lg-12 cmn-space">
								<span>Note <sup>*</sup></span>
								<textarea id="content11" class="form-control" name="description" placeholder="A great place to share your insightful notes"><?= set_value('description') ?></textarea>
								<?= form_error('description', '<span class="error">', '</span>'); ?>
                            </div>
                            
                            <!--<div class="col-lg-12 cmn-space">
                                <span for="date">
                                    Publish Date <sup>*</sup>
								</span>
								<div class="form-group">
									<input type="hidden" name="client_tz" value="">
									<div class='input-group date' id='notes_datetime'>
										<input type='text' class="form-control input-lg m-btm" name="date" value="<?= set_value('date') ?>" />
										<?= form_error('date', '<span class="error">', '</span>'); ?>
										<label class="input-group-addon">
											<label class="glyphicon glyphicon-calendar"></label>
										</label>
									</div>
								</div>
                            </div>-->
							<div class="col-lg-6 left-part cmn-space">
								<div>
									<span>Image</span>
										<div class="form-control file-input min-height">
											<label for="note-image" id="image-preview-label">
												<img src="<?= ms_site_url('assets/images/frontend/upload-vid.png') ?>"><br/>Note image
											</label>
										                    
											<input type="file" id="note-image" name="image" class="file-upload-preview" accept="image/*" data-img_id="image-preview" data-msg-accept="Please select only jpeg, bmp, png, jpg file." data-label_id="image-preview-label">
											<img src="" id="image-preview" class="display_none" />
										</div>
										<div class="common-image-info">
										   <p>Only use supported image file types such as .jpg, .png, .gif.</p>
										</div>
										<div class="image-actions">
											<a class="upload-button" data-preview_section="note-image"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
											<a class="remove-image-button display_none" data-file_element="edit-note-image" data-delete_section="image-preview"><i class="fa fa-trash" aria-hidden="true"></i> </a>
										</div>
										<input type="hidden" name="delete_image" value="0" />
								</div>
							</div>

							<!--<div class="col-lg-6 col-12 left-part cmn-space">
								<span>Image</span>
								
								<div class="img-preview-wrapper">
									<img id="previewImg" src="" style="display:none">
									<span class="preview-img-close" style="display:none"></span>
								</div>
								<div class="file-input form-control min-height">
									<label for="cv">
										<span>
											<img src="<?= ms_site_url('assets/images/frontend/upload-vid.png') ?>">
										</span> 
										<span> upload </span>
									</label>
									<input type="file" id="image" name="image" class="upload-img" accept="image/*" onchange="previewFile(this);">
								</div>
								<?= form_error('image', '<span class="error">', '</span>'); ?>

								<div class="edit">
								</div>
							</div>-->
							<div class="col-lg-6 col-12 right-part cmn-space add-note-btn-space">
								<a href="<?php echo site_url('notes'); ?>" class="upload-danger btn">CANCEL</a>
								<input class="upload-vid cmn-btn" type="submit" value="Add Note" name="submit">
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</form>
			</div>
										</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>
<link rel="stylesheet" href="<?php echo ms_base_url('assets/plugins/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') ?>">
<script src="<?php echo ms_base_url('assets/plugins/ckeditor/ckeditor.js') ?>"></script>
<script src="<?php echo ms_base_url('assets/plugins/ckeditor/samples/js/sample.js'); ?>"></script>
<script>
    var company_name = '<?php echo isset($company_name) ? $company_name : ''; ?>';
    var website_url = '<?php echo ms_site_url(); ?>';

    $(function() {
               var editor = CKEDITOR.replace('content11', {
            htmlEncodeOutput: false,
            wordcount: {
                showWordCount: true,
                showCharCount: true,
                countSpacesAsChars: true,
                countHTML: false,
            },
            removePlugins: 'zsuploader',

            filebrowserUploadUrl: "<?= ms_base_url('ck_upload.php') ?>", // define project BASE_PATH here located on root
            filebrowserUploadMethod: 'form'
        });
    });
</script>
