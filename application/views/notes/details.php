<div class="video-outer note-detail-block row">
	<div class="single-video-sec single-notes-detail col-lg-8">
		<div class="shadow-card-bg">
		<div class="notes-repeat">
			<div class="img-sec">
				<?php if ($note_details->image) { ?>
				<img src="<?= ms_site_url('uploads/notes/'.$note_details->image.'') ?>">
				<?php } else{  ?>
					<img src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
				<?php } ?>
			</div>
			<div class="category-cont">
				<div class="category-left">
				    <div class="videonoterap">
					<h3 class="main-title"><?php echo $note_details->title; ?></h3>
				    <div class="profile-share videonote">
                                <i class="fa fa-share-alt" data-toggle="modal" data-target="#sharenote"></i>
                            </div>
                            </div>
					<div class="comment-sec">
						<div class="comment-cont">
							<p>
								<a href="<?= ms_site_url('user/'.$note_details->user->user_id.'/profile') ?>">
								<span>
								<?php
									if ($note_details->user->image) { ?>
								<img src="<?= ms_site_url('uploads/avatars/'.$note_details->user->image) ?>">
								<?php } else { ?>
								<?= strtoupper($note_details->user->user_name[0]); ?>
								<?php } ?></span>
								<?= $note_details->user->user_name; ?>
								<span class="user-role-updated"><?= ($note_details->user->user_role==0) ? "Learner" : "Teacher"  ?></span>
								</a>
							</p>
						</div>
						<div class="comment-cont">
							<p><i class="fa fa-calendar" aria-hidden="true"></i>
								<?= getDateInFormat($note_details->date) ?>
							</p>
						</div>
						
						<?php if (isUserLoggedIn()) { ?>
						<div class="comment-cont">
							<p> <?php if(!$note_details->flag_exists && $note_details->user_id!=getLoggedInUserId()){ ?>
								<a data-toggle="modal" data-target="#noteFlageModal"
									data_note_id="<?= $note_details->id ?>" class="open_noteFlageModal"><i
									class="fa fa-flag" aria-hidden="true"></i> Flag</a>
								<?php } ?>
							</p>
						</div>
						<?php } ?>
							
						<div class="category-right">
							<div class="category-right-top">
								<p><strong>Category</strong> : <?= getCategoryNamesLine($note_details) ?>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="description">
					<p><?php echo nl2br($note_details->description); ?></p>
				</div>
			</div>
		</div>
		<div class="all-comments-sec">
			<div class="sort-left">
			    	
					<?php if (!isUserLoggedIn()) { ?>
					<h3 class="main-title">Want to get in on the conversation?</h3>
			    	  <a href="<?= ms_Site_url('login') ?>" class="btn-outline-theme">
                        <span class="sign_in_btn"> Login </span>
                    </a>
			    	<?php } ?>
				<?php if($comments_count>1){ ?>
				<p><span id="comments-count"><?= $comments_count; ?></span> Comments</p>
				<?php } else{ ?>
					<p><span id="comments-count"><?= $comments_count; ?></span> Comment</p>
				<?php } ?>
				<!--<p><a href="#"><img src="../assets/images/frontend/sort-icon.png">Sort By</a></p>-->
			</div>
			<div class="subscribe-sec">
				<!--<a href="javascript:void(0)" class="cmn-btn">SUBSCRIBE</a>-->
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php if (isUserLoggedIn()) { ?>
		<div class="public-comment-sec">
			<div class="public-comment-left">
				<p class="user-profile-thumb"><a href="#">
					<span>
					<?php
						$logged_in_user_details = getLoggedInUserDetails();
						if ($logged_in_user_details->image) { ?>
					<img src="<?= ms_site_url('uploads/avatars/'.$logged_in_user_details->image) ?>">
					<?php } else { ?>
					<?= strtoupper($logged_in_user_details->name[0]); ?>
					<?php } ?></span>
					</a>
				</p>
			</div>
			<form id="comment_form" action="<?= ms_site_url('note/'.$note_details->id.'/comment') ?>" method="post">
				<div class="public-comment-right">
					<div class="form-group">
						<textarea class="form-control" name="comment" id="comment-text" placeholder="Start a discussion..."></textarea>
						<span class="error comment-error"></span>
					</div>
					<input type='hidden' name='note_id' value="<?= $note_details->id ?>" id='note_id'/>
					<input type='hidden' name='resource_type' value="notes" />
				</div>
				<div class="clearfix"></div>
				<div class="cancel-confirm-block">
					<a href="javascript:void(0)" class="cmn-btn cancel-cmt">Cancel</a>
					<!--<a href="javascript:void(0)" class="cmn-btn">Comment</a>-->
					<input class="btn btn-success" type="submit" name="submit" value="comment"/>
				</div>
			</form>
		</div>
		<?php } ?>
		<div id="comments-listing">
			
		</div>
		<div class="loader" style="display:none">
			<img src="<?= ms_site_url('assets/images/frontend/loader.gif') ?>">
		</div>
	</div>
</div>

<div class="advert-sec col-lg-4">
<?php if (count($related_notes)) { ?>
	<div class="shadow-card-bg">
	<!-- <div class="advert-top">
		<h3 class="main-title">
		Advertise<br>
		Here
	</div> -->
	<div class="advert-bottom-sec">
		<h4>More Notes</h4>
		<?php 
			foreach ($related_notes as $note) { ?>
		<div class="advert-note">
			<a href="<?= ms_site_url('notes/'.$note->slug) ?>">
				<div class="advert-note-left">
					<div class="img-sec">
						<?php if ($note->image) { ?>
						<img src="<?= ms_site_url('uploads/notes/'.$note->image.'') ?>">
						<?php } else{  ?>
						<img src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
						<?php } ?>
					</div>
				</div>
				<div class="advert-note-right">
					<p><?= $note->title ?></p>
					<p><span><?= getDateInFormat($note->created_at) ?></span></p>
				</div>
				<div class="clearfix"></div>
			</a>
		</div>
		<?php }
			?>
	</div>
<?php } ?>
		</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<!-- Modal -->
<div class="modal fade" id="noteFlageModal" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form method="post" action="<?= ms_site_url('resource_inappropriate/flag') ?>">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title d-inline" id="staticBackdropLabel">Note Inappropriate Flag</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Flag Message</p>
					<input type="hidden" name="note_id" id="note_id" value="" />
					<input type="hidden" name="resource_type" id="resource_type" value="" />
					<textarea class="form-control" id="flag_message" name="message"></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>


<div class="modal fade" id="sharenote" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="share-profile-popup">
                <h3>Share Note</h3>

                <div class="share-profile-icons">
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?= ms_site_url('notes/'.$note_details->slug) ?>"
                                target="_blank" class="social-fb social-share-inner-btn">
                                <div class="icon-holder facebook">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </div>
                                <span>Facebook</span>
                            </a>
                        </li>

                        <li>
                            <a href="https://twitter.com/intent/tweet?url=<?= ms_site_url('notes/'.$note_details->slug) ?>"
                                target="_blank">
                                <div class="icon-holder twitter">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </div>
                                <span>Twitter</span>
                            </a>
                        </li>

                        <li>
                            <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?= ms_site_url('notes/'.$note_details->slug) ?>"
                                target="_blank">
                                <div class="icon-holder linkedin">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </div>
                                <span>Linkedin</span>
                            </a>
                        </li>

                        <li>
                            <a href="http://pinterest.com/pin/create/button/?url=<?= ms_site_url('notes/'.$note_details->slug) ?>"
                                target="_blank">
                                <div class="icon-holder pinterest">
                                    <i class="fa fa-pinterest-square" aria-hidden="true"></i>
                                </div>
                                <span>Pinterest</span>
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="share-profile-textarea">
                    <textarea id="myInput"><?= ms_site_url('notes/'.$note_details->slug) ?></textarea>
                    <div class="copy-link">
                        <button onclick="myFunction()" onmouseout="outFunc()">
                            Copy
                        </button>
                        <span class="tooltiptext" id="myTooltip">Copy to clipboard</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<script>
	var page = 0;
	var total_pages = <?= $total_pages;  ?>;			
</script>
<?php $this->load->view('common/comments-script'); ?>