 <section>
	<div class="dashboard-sec add-video">
		<div class="inner-sec">
			<?php $this->load->view('common/sidebar') ?>
			<div class="right-sec">
				<div class="shadow-card-bg custom-add-note-student">
					<div class="inner-container-sm">
					<form id="edit_note" method="post" enctype="multipart/form-data">
					<div class="right-inner">
						<h4>Edit Note</h4>
						<div class="row add-inner">
							<div class="col-lg-12 col-12 left-part cmn-space">
								<span>Title <sup>*</sup></span>
								<input class="form-control" type="text" name="title" placeholder="eg. Video Title" 
								value="<?= set_value('title', $note_details->title) ?>">
								<?= form_error('title', '<span class="error">', '</span>'); ?>
							</div>

							<div class="col-lg-12 col-12 left-part cmn-space">
								<span>Select Category</span>
								<select class="form-control" name="category_id" id="category_id">
				                    <option value="">Select category</option>
				                    <?php if (isset($categories) && is_array($categories) && count($categories) > 0) { ?>
				                        <?php foreach ($categories as $category) { ?>
				                            <option value="<?php echo $category->id; ?>" <?php echo set_value('category_id', $note_details->category_id) == $category->id ? 'selected' : ''; ?>>
				                                <?php echo $category->name; ?>
				                            </option>
				                        <?php } ?>
				                    <?php } ?>
				                </select>
							</div>
							<div class="col-lg-12 col-12 left-part cmn-space">
								<span>Select Sub-category</span>
								<select class="form-control" name="sub_category_id" id="sub_category_id">
				                    <option value="">Select sub-category</option>
				                    <?php if (isset($sub_categories) && is_array($sub_categories) && count($sub_categories) > 0) { ?>
				                        <?php foreach ($sub_categories as $sub_category) { ?>
				                            <option value="<?php echo $sub_category->id; ?>" <?php echo set_value('sub_category_id', $note_details->sub_category_id) == $sub_category->id ? 'selected' : ''; ?>>
				                                <?php echo $sub_category->name; ?>
				                            </option>
				                        <?php } ?>
				                    <?php } ?>
				                </select>
							</div>

							<div class="col-lg-12 cmn-space">
								<span>Description <sup>*</sup></span>
								<textarea id="content11" class="form-control" name="description" placeholder="What’s your text about?"><?= set_value('description', $note_details->description) ?></textarea>
								<?= form_error('description', '<span class="error">', '</span>'); ?>
							</div>

							<!--<div class="col-lg-12 cmn-space">
                                <span>
                                    Publish Date <sup>*</sup>
								</span>
								<div class="form-group">
									<input type="hidden" name="client_tz" value="">
									<div class='input-group date' id='notes_datetime'>
										<input type="hidden" name="note_date" value="<?= $note_details->date ?>">
										<input type='text' class="form-control input-lg m-btm" name="date" value="<?= date('m/d/Y H:i A',strtotime($note_details->date)) ?>" />
										<?= form_error('date', '<span class="error">', '</span>'); ?>
										<label class="input-group-addon">
											<label class="glyphicon glyphicon-calendar"></label>
										</label>
									</div>
								</div>
                            </div>-->

							<!--<div class="col-lg-6 col-12 left-part cmn-space">
								<span>Image</span>
								<div class="img-preview-wrapper">
									<?php if(!empty($note_details->image)){ ?>
									<div class="edit-image-cnt">
										<label for="edit-image">
											<i class="fa fa-pencil edit-icon-notes" aria-hidden="true"></i> 
										</label>
										<input type="file" id="edit-image" name="image" accept="image/*" onchange="previewFile(this);">
									</div>
									<img id="previewImg" src="<?= ms_site_url('uploads/notes/'.$note_details->image.'') ?>">
									<span class="preview-img-close"></span>
									<?php } else{ ?>
									<img id="previewImg" src="" style="display:none">
									<span class="preview-img-close" style="display:none"></span>
									<?php } ?>
									<input type="hidden" name="delete_image" value="0" />
								</div>
								<div class="file-input form-control min-height">
									<label for="image">
										<span><img src="<?= ms_site_url('assets/images/frontend/upload-vid.png') ?>"></span> 
										<span> upload </span>
									</label>
									<input type="file" id="thumbnail" name="image" accept="image/*" onchange="previewFile(this);">
								</div>
							</div>-->
							<div class="col-lg-6 col-12 left-part cmn-space img_main_cnt">
								<div>
								<div class="form-control file-input min-height">
									<?php if(!($note_details->image)){ $delete_icon_class = "display_none"; ?>
									<label for="note-image" id="image-preview-label"><img src="<?= ms_site_url('assets/images/frontend/upload-vid.png') ?>"><br/>Note Image</label>
									<?php } if($note_details->image){ $delete_icon_class = ""; ?>
										<img src="<?= ms_site_url('uploads/notes/'.$note_details->image.'') ?>" alt="" id="image-preview" />
									<?php } else{ ?>
										<img src="" id="image-preview" class="display_none" />
									<?php } ?>
									<input type="file" id="note-image" name="image" class="file-upload-preview" accept="image/*" data-img_id="image-preview" data-msg-accept="Please select only jpeg, bmp, png, jpg file." data-label_id="image-preview-label">
									
								</div>

								<div class="image-actions">
									<a class="upload-button" data-preview_section="note-image"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
									<a class="remove-image-button <?= $delete_icon_class ?>" data-file_element="note-image" data-delete_section="image-preview"><i class="fa fa-trash" aria-hidden="true"></i> </a>
									</div>
									<input type="hidden" name="delete_image" value="0" />
								</div>
							</div>
							<div class="col-lg-6 col-12 right-part cmn-space update-cancel-btn">
					
									<a href="<?php echo site_url('notes'); ?>" class="upload-danger btn">CANCEL</a>
					
									<input class="upload-vid cmn-btn" type="submit" value="Update Note" name="submit">
						
							</div>
						</div>
					</div>
									</div>
				</form>
			</div>
		</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>
<link rel="stylesheet" href="<?php echo ms_base_url('assets/plugins/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') ?>">
<script src="<?php echo ms_base_url('assets/plugins/ckeditor/ckeditor.js') ?>"></script>
<script src="<?php echo ms_base_url('assets/plugins/ckeditor/samples/js/sample.js'); ?>"></script>
<script>
    var company_name = '<?php echo isset($company_name) ? $company_name : ''; ?>';
    var website_url = '<?php echo ms_site_url(); ?>';

    $(function() {
               var editor = CKEDITOR.replace('content11', {
            htmlEncodeOutput: false,
            wordcount: {
                showWordCount: true,
                showCharCount: true,
                countSpacesAsChars: true,
                countHTML: false,
            },
            removePlugins: 'zsuploader',

            filebrowserUploadUrl: "<?= ms_base_url('ck_upload.php') ?>", // define project BASE_PATH here located on root
            filebrowserUploadMethod: 'form'
        });
    });
</script>