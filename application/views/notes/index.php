<section>
	<div class="dashboard-sec add-video my-videos studen-notes">
		<div class="inner-sec">
			<?php $this->load->view('common/sidebar') ?>
			<div class="right-sec">
				<div class="right-inner shadow-card-bg">
					<h4>My Notes</h4>
					<h5>
						<a href="<?= ms_site_url('notes/add') ?>">
                        	<span><img src="<?= ms_Site_url('assets/images/frontend/add-icon-blue.png') ?>"></span> Add Note
                        </a>
					</h5>
					<div class="clearfix"></div>
					<div class="video-inner notes-inner">
						<div class="table-responsive">
						<table class="table">
							<tr>
								<th>Picture</th>
								<th>Title</th>
								<th>description</th>
								<th>Publish Date</th>
								<th>Actions</th>
							</tr>
							<?php
								if (count($notes)) {
									foreach ($notes as $note) { ?>
										<tr>
											<td>
												<a href="<?= ms_site_url('notes/'.$note->slug) ?>" target="_blank">
													<div class="img-sec">
														<?php if ($note->image) { ?>
														<img src="<?= ms_site_url('uploads/notes/'.$note->image.'') ?>">
														<?php } else{  ?>
														<img src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
														<?php } ?>
													</div>
												</a>
											</td>
											<td><?= $note->title ?></td>
											<?php if ($note->description) { ?>
												<td class="wrap-td-note-descp"><p class="td-note-descp"><?php echo generateReadMore($note->description, 'notes', $note->slug); ?></p></td>
											<?php }else{ ?>
												<td>n/a</td>
											<?php } ?>
											<td><?= getDateInFormat($note->date); ?></td>
											<td>
												<?php if($note->status==1){ ?>
												<a href="<?= ms_Site_url('notes/edit/'.$note->slug) ?>">
													<button class="edit-icon"><i class="fa fa-pencil" aria-hidden="true"></i></button>
												</a>
												<?php } ?>
												<a href="<?php echo site_url('notes/delete/'.$note->id); ?>" class="delete_note">
												    <i class="fa fa-trash-o" aria-hidden="true"></i>
												</a>
											</td>
										</tr>
								<?php }
								} else { ?>
									<tr>
										<td colspan="9"></td>
									</tr>
									<tr>
									<td class="no-data" colspan="9">No Notes Found</td>
								</tr>
								<?php }
							?>
							
						</table>
								</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>