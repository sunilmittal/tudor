<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        body {
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: left;
            background: #F6F6F6;
        }

        .main-wrapper {
            margin: 0 auto;
            width: 100%;
            height: auto;
            overflow-x: hidden;
        }

        .sitemap {
            margin: 0 auto;
            width: 100%;
            height: auto;
            padding: 20px 0px;
            position: relative;

        }

        .container {
            margin: 0 auto;
            width: 100%;
            height: auto;
            max-width: 1024px;
            padding-left: 20px;
            padding-right: 20px;
        }

        .row {
            margin-left: -10px;
            margin-right: -10px;
        }

        .sitemap .sitemap_inner {
            margin: 0 auto;
            max-width: 650px;
        }

        .sitemap h2 {
            text-align: center;
            color: #1F4F46;
            font-size: 32px;
            letter-spacing: 2px;
            margin: 15px;
        }

        h1,
        h2,
        h3 {
            font-family: sweetlife;
        }

        .sitemap .sitemap_inner ol {
            margin: 0 auto;
        }

        .sitemap .sitemap_inner ol li {
            font-size: 14px;
            color: #3B4245;
            width: 100%;
            letter-spacing: 1px;
            cursor: pointer;
            padding-left: 15px
        }

        .sitemap .sitemap_inner ol li a {
            font-size: 14px;
            color: #3B4245;
            display: block;
            padding: 5px 0px;
            border-bottom: thin solid #dee4e4;
            font-weight: 300;
        }

        .sitemap .sitemap_inner ol li ul {
            padding-left: 20px;
            /* display: none; */
        }

        h1 {
            font-size: 30px;
        }

        li {
            font-size: 20px;
            line-height: 35px;
        }

        a {
            color: #000;
            text-decoration: none;
        }

        a:hover {
            color: red;
            text-decoration: underline;
        }
    </style>
</head>

<body>

    <div class="main-wrapper">
        <section class="sitemap" id="sitemap">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sitemap_inner">
                            <h2>SITEMAP</h2>
                            <ol>
                                <li>
                                    <a target="blank" href="<?= ms_site_url() ?>">Home</a>
                                </li>

                                <?php foreach($pages as $page) { ?>
                                <li>
                                    <a target="blank" href="<?= ms_site_url($page['slug']) ?>"><?= $page['name'] ?></a>
                                </li>
                                <?php } ?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</body>

</html>