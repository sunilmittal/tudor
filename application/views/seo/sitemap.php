<?= '<?xml version="1.0" encoding="UTF-8" ?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?= ms_site_url();?></loc>
        <priority>1.0</priority>
    </url>
    <?php foreach($slugs as $slug) { ?>
    <url>
        <loc><?= ms_site_url().$slug['slug'] ?></loc>
        <priority>0.5</priority>
    </url>
    <?php } ?>
</urlset>