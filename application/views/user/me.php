<section>
    <div class="dashboard-sec add-video my-videos recommended-page">
        <div class="inner-sec">
            <?php $this->load->view('common/sidebar') ?>
            <div class="right-sec">
                <?php if ($user_info) { ?>
                <section>
                    <?php //echo "<pre>"; print_r($user_info); die; ?>
                    <div class="video-outer-section author-page">
                        <div class="video-outer">
                            <div class="single-video-sec">
                                <div class="video-t-b-wrap">
                                    <div class="single-video-top">
                                        <div class="img-sec">
                                            <?php
                                 if ($user_info->banner) { ?>
                                            <img src="<?= ms_site_url('uploads/avatars/'.$user_info->banner) ?>"
                                                id="banner_preview">
                                            <?php } else { ?>
                                            <div class="default-banner-img">
                                                <span class="default-user-name"><?= $user_info->name ?> </span>
                                            </div>
                                            <?php }
                                 ?>



                                        </div>
                                    </div>
                                    <div class="single-video-bottom">
                                        <div class="video-left-logo p-pic profile-name-wrap">
                                            <div class="icon-sec img-sec">
                                                <?php
                                    if ($user_info->image) { ?>
                                                <img src="<?= ms_site_url('uploads/avatars/'.$user_info->image) ?>">
                                                <?php } else { ?>
                                                <span> <?= strtoupper($user_info->name[0]); ?></span>
                                                <?php }
                                    ?>
                                            </div>

                                            <p><?= ucfirst($user_info->name); ?></p>
                                            &nbsp;<p class="user-role-updated">
                                                <?= $user_info->user_role == USER_ROLE ? 'Learner' : 'Teacher'; ?>
                                            </p>
                                            </span>
                                            <?php $subscribers_count = count($user_info->subscriber_user_ids); ?>
                                            <span class="subscribers-profile"><?= $subscribers_count ?>
                                                Subscribers</span>
                                        </div>

                                        <div class="profile-share">
                                            <i class="fa fa-share-alt" data-toggle="modal"
                                                data-target="#shareprofile"></i>
                                        </div>
                                        <?php
                              if ($show_subscribe_btn) { ?>
                                        <?php if (in_array($user_info->id, $user_data->subscribed_user_ids)) { ?>
                                        <div class="video-right-subscribe">
                                            <a href="<?= ms_site_url('user/'.$user_info->id.'/unsubscribe') ?>"
                                                class="cmn-btn">UNSUBSCRIBE</a>
                                        </div>
                                        <?php } else { ?>
                                        <div class="video-right-subscribe">
                                            <a href="<?= ms_site_url('user/'.$user_info->id.'/subscribe') ?>"
                                                class="cmn-btn">SUBSCRIBE</a>
                                        </div>
                                        <?php } ?>
                                        <?php  }
                              ?>


                                        <!--<div class="location-sec">
                                            <ul>
                                                <li>
                                                    <p><span>Joined</span> :
                                                        <?= getDateInFormat($user_info->created_at) ?></p>
                                                </li>
                                            </ul>
                                        </div>-->
                                        <div class="clearfix"></div>

                                        <?php if ($user_info->description) { ?>
                                        <p class="dummy-text"><?= $user_info->description ?></p>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                           if (isset($teacherClassrooms)) { ?>
                                <div class="feature-sec author-sec">
                                    <div class="inner-sec category-inner">
                                        <h3>Classrooms</h3>
                                        <div class="box-outer">
                                            <?php 
                                    foreach ($teacherClassrooms as $classroom) { 
                                        ?>
                                            <div class="full-box">
                                                <a href="<?= ms_site_url('classroom/'.$classroom->id) ?>">
                                                    <div class="box-1">
                                                        <div class="img-sec">
                                                            <?php if($classroom->image && file_exists(FCPATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR . $classroom->image)){ ?>
                                                            <img
                                                                src="<?= ms_site_url('uploads/thumbnails/'.$classroom->image) ?>">
                                                            <?php }else{ ?>
                                                            <img
                                                                src="<?= ms_site_url('assets/images/frontend/learn3.png') ?>">
                                                            <?php }?>
                                                        </div>
                                                    </div>
                                                    <div class="box-cont">
                                                        <p><strong><?= $classroom->title ?></strong></p>
                                                        <div class="box-content">
                                                            <!-- <div class="box-content-right"> -->
                                                            <div class="">
                                                                <p><?= getDateInFormat($classroom->created_at) ?></p>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>




                                <div class="dev-with-yt-wrap meprofile">
                                    <ul class="nav nav-tabs">
                                        <?php if (userRoleIsTeacher()) { ?>
                                        <li class="active"><a data-toggle="tab" href="#latestuploadsme">Home</a></li>
                                        <li><a data-toggle="tab" href="#allvideosme">All Videos</a></li>
                                        <li><a data-toggle="tab" href="#allnotesme">All Notes</a></li>
                                        <?php } else { ?>
                                        <li class="active"><a data-toggle="tab" href="#latestuploadsme">Home</a></li>
                                        <?php } ?>
                                    </ul>
                                </div>

                                <div class="tab-content">
                                    <div id="latestuploadsme" class="tab-pane active">
                                        <div class="feature-sec author-sec custom-card-bg f-sec-latest">
                                            <div class="inner-sec category-inner">
                                                <h3>Latest Uploads</h3>
                                                <?php if($feeds->isEmpty()) { ?>
                                                <div class="no-content">
                                                    <p>There are no uploads yet</p>
                                                </div>
                                                <?php } else { ?>
                                                <div class="box-outer">
                                                    <?php foreach ($feeds as $feed) { ?>
                                                    <div class="full-box">
                                                        <a href="<?= ms_site_url($feed->resource_type.'/'.$feed->slug) ?>">
                                                            <div class="box-1">
                                                            <?php if($feed->resource_type=='videos') { ?>
                                                                <div class="img-sec">
                                                                <?php if ($feed->thumbnail) { ?>
                                                                    <img
                                                                        src="<?= ms_site_url('uploads/thumbnails/'.$feed->thumbnail) ?>">
                                                                        <?php } else{  ?>
                                                                    <img
                                                                        src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
                                                                    <?php } ?>
                                                                </div>

                                                                <div class="video-icon"><img
                                                                        src="<?= ms_site_url('assets/images/frontend/video-play.png') ?>">
                                                                </div>
                                                                <?php } else { ?>
                                                                <div class="img-sec">
                                                                    <?php if ($feed->image) { ?>
                                                                    <img
                                                                        src="<?= ms_site_url('uploads/notes/'.$feed->image.'') ?>">
                                                                    <?php } else{  ?>
                                                                    <img
                                                                        src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
                                                                    <?php } ?>
                                                                </div>
                                                                <?php } ?>
                                                            </div>
                                                            
                                                            <div class="box-cont">
                                                                <p><strong><?= $feed->title ?></strong></p>
                                                                <div class="box-content">
                                                                    <?php if($feed->resource_type=='videos') { ?>
                                                                    <div class="box-content-left">
                                                                        <div class="p-pic">
                                                                            <span>
                                                                                <?php if ($feed->user_and_channel->image) { ?>
                                                                                <img
                                                                                    src="<?= ms_site_url('uploads/avatars/'.$feed->user_and_channel->image) ?>">
                                                                                <?php } else { ?>
                                                                                <?= strtoupper($feed->user_and_channel->user_name[0]) ?>
                                                                                <?php } ?>
                                                                            </span>
                                                                        </div>
                                                                        <div class="pic-cont">
                                                                            <p><?= $feed->user_and_channel->user_name ?>
                                                                            </p>
                                                                            <p><img
                                                                                    src="<?= ms_site_url('assets/images/frontend/eyes-icon.png') ?>"><span><?= kConverter($feed->view_count) ?></span>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <?php } else {  ?>
                                                                    <div class="box-content-left">
                                                                        <div class="p-pic">
                                                                            <span>
                                                                                <?php if ($feed->user->image) { ?>
                                                                                <img
                                                                                    src="<?= ms_site_url('uploads/avatars/'.$feed->user->image) ?>">
                                                                                <?php } else { ?>
                                                                                <?= strtoupper($feed->user->user_name[0]) ?>
                                                                                <?php } ?>
                                                                            </span>
                                                                        </div>
                                                                        <div class="pic-cont">
                                                                            <p><?= $feed->user->user_name ?></p>
                                                                        </div>
                                                                    </div>
                                                                    <?php } ?>
                                                                    <div class="box-content-right">
                                                                        <p><?= getDateInFormat($feed->created_at) ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="allvideosme" class="tab-pane">
                                        <div class="feature-sec author-sec-1 custom-card-bg f-sec-all">
                                            <div class="inner-sec category-inner">
                                                <h3>All Videos</h3>
                                                <?php if($all_videos->isEmpty()) { ?>
                                                <div class="no-content">
                                                    <p>There are no uploads yet</p>
                                                </div>
                                                <?php } else { ?>
                                                <div class="box-outer">
                                                    <?php foreach ($all_videos as $video) { ?>
                                                    <div class="full-box">
                                                        <a href="<?= ms_site_url('videos/'.$video->slug) ?>">
                                                            <div class="box-1">
                                                                <div class="img-sec">
                                                                    <?php if($video->thumbnail) { ?>
                                                                    <img
                                                                        src="<?= ms_site_url('uploads/thumbnails/'.$video->thumbnail) ?>">
                                                                    <?php } else { ?>
                                                                        <img
                                                                        src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="video-icon"><img
                                                                        src="<?= ms_site_url('assets/images/frontend/video-play.png') ?>">
                                                                </div>
                                                            </div>
                                                            <div class="box-cont">
                                                                <p><strong><?= $video->title ?></strong></p>
                                                                <div class="box-content">
                                                                    <div class="box-content-left">
                                                                        <div class="p-pic">
                                                                            <span>
                                                                                <?php
                                                      if ($video->user_and_channel->image) { ?>
                                                                                <img
                                                                                    src="<?= ms_site_url('uploads/avatars/'.$video->user_and_channel->image) ?>">
                                                                                <?php } else { ?>
                                                                                <?= strtoupper($video->user_and_channel->user_name[0]) ?>
                                                                                <?php }
                                                      ?>
                                                                            </span>
                                                                        </div>
                                                                        <div class="pic-cont">
                                                                            <p><?= $video->user_and_channel->user_name ?>
                                                                            </p>
                                                                            <p><img
                                                                                    src="<?= ms_site_url('assets/images/frontend/eyes-icon.png') ?>"><span><?= kConverter($video->view_count) ?></span>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-content-right">
                                                                        <p><?= getDateInFormat($video->created_at) ?>
                                                                        </p>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <?php }
                                    ?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="allnotesme" class="tab-pane">
                                        <div class="feature-sec author-sec-1 custom-card-bg f-sec-all-notes">
                                            <div class="inner-sec category-inner">
                                                <h3>All Notes</h3>
                                                <?php if($all_notes->isEmpty()) { ?>
                                                <div class="no-content">
                                                    <p>There are no uploads yet</p>
                                                </div>
                                                <?php } else { ?>
                                                <div class="box-outer">
                                                    <?php foreach ($all_notes as $note) { ?>
                                                    <div class="full-box">
                                                        <a href="<?= ms_site_url('notes/'.$note->slug) ?>">
                                                            <div class="box-1">
                                                                <div class="img-sec">
                                                                    <?php if ($note->image) { ?>
                                                                    <img
                                                                        src="<?= ms_site_url('uploads/notes/'.$note->image.'') ?>">
                                                                    <?php } else{  ?>
                                                                    <img
                                                                        src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <div class="box-cont">
                                                                <p><strong><?= $note->title ?></strong></p>
                                                                <div class="box-content">
                                                                    <div class="box-content-left">
                                                                        <div class="p-pic">
                                                                            <span>
                                                                                <?php if ($note->user->image) { ?>
                                                                                <img
                                                                                    src="<?= ms_site_url('uploads/avatars/'.$note->user->image) ?>">
                                                                                <?php } else { ?>
                                                                                <?= strtoupper($note->user->user_name[0]) ?>
                                                                                <?php } ?>
                                                                            </span>
                                                                        </div>
                                                                        <div class="pic-cont">
                                                                            <p><?= $note->user->user_name ?></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-content-right">
                                                                        <p><?= getDateInFormat($note->created_at) ?></p>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <?php }
                                    ?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>




                                <!-- <div class="load-more author-load-more"><a href="javascript:void(0)"><img src="../assets/images/frontend/load-more.png"></a></div> -->
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </section>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>


<!-- Modal -->
<div class="modal fade" id="shareprofile" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="share-profile-popup">
                <h3>Share Profile</h3>

                <div class="share-profile-icons">
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?= ms_site_url('user/'.$user_info->id.'/profile') ?>"
                                target="_blank" class="social-fb social-share-inner-btn">
                                <div class="icon-holder facebook">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </div>
                                <span>Facebook</span>
                            </a>
                        </li>

                        <li>
                            <a href="https://twitter.com/intent/tweet?url=<?= ms_site_url('user/'.$user_info->id.'/profile') ?>"
                                target="_blank">
                                <div class="icon-holder twitter">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </div>
                                <span>Twitter</span>
                            </a>
                        </li>

                        <li>
                            <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?= ms_site_url('user/'.$user_info->id.'/profile') ?>"
                                target="_blank">
                                <div class="icon-holder linkedin">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </div>
                                <span>Linkedin</span>
                            </a>
                        </li>

                        <li>
                            <a href="http://pinterest.com/pin/create/button/?url=<?= ms_site_url('user/'.$user_info->id.'/profile') ?>"
                                target="_blank">
                                <div class="icon-holder pinterest">
                                    <i class="fa fa-pinterest-square" aria-hidden="true"></i>
                                </div>
                                <span>Pinterest</span>
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="share-profile-textarea">
                    <textarea id="myInput"><?= ms_site_url('user/'.$user_info->id.'/profile') ?></textarea>
                    <div class="copy-link">
                        <button onclick="myFunction()" onmouseout="outFunc()">
                            Copy
                        </button>
                        <span class="tooltiptext" id="myTooltip">Copy to clipboard</span>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>