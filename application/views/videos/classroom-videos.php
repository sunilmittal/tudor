<section>
   <div class="dashboard-sec add-video my-videos recommended-page">
      <div class="inner-sec">
         <?php $this->load->view('common/sidebar') ?>
         <div class="right-sec">
            <div class="video-sec diff-sec">
               <div class="video-top">
                  <div class="right-sec">
                     <p class="cat">Start searching for the right video and teacher for you.</p>
                     <div class="clearfix"></div>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
            <div class="right-inner">
               <?php if (count($videos)) { ?>
                  <div class="right-outer diff-sec">
                     <h4>Private Videos</h4>
                     <div class="clearfix"></div>
                     <div class="full-box">
                        <?php 
                        foreach ($videos as $video) { ?>
                           <div class="box1">
                              <a href="<?= ms_site_url('videos/'.$video->id) ?>">
                                 <div class="img-outer">
                                    <div class="img-sec">
                                       <img src="<?= ms_site_url('uploads/thumbnails/'.$video->thumbnail) ?>">
                                    </div>
                                    <div class="video-icon"><img src="<?= ms_site_url('assets/images/frontend/video-play.png') ?>"></div>
                                    <span class="video-duration"><?= convert_seconds($video->duration) ?></span>
                                 </div>
                                 <div class="box-cont">
                                    <p><strong><?= substr($video->title, 0,30) ?>...</strong></p>
                                    <div class="box-content">
                                       <div class="box-content-left">
                                          <div class="p-pic">
                                             <span>
                                                <?php
                                                if ($video->user_and_channel->image) { ?>
                                                   <img src="<?= ms_site_url('uploads/avatars/'.$video->user_and_channel->image) ?>">
                                                <?php } else { ?>
                                                   <?= strtoupper($video->user_and_channel->user_name[0]) ?>
                                                <?php }
                                                ?>
                                             </span> 
                                          </div>
                                          <div class="pic-cont">
                                             <p><?= $video->user_and_channel->user_name ?></p>
                                             <p> Views
                                                <!-- <img src="<?= ms_site_url('assets/images/frontend/eyes-icon.png') ?>"> -->
                                                <span><?= kConverter($video->view_count) ?></span></p>
                                             </div>
                                          </div>
                                          <div class="box-content-right">
                                             <p><?= getDateInFormat($video->created_at) ?></p>
                                          </div>
                                          <div class="clearfix"></div>
                                       </div>
                                    </div>
                                 </a>
                              </div>
                           <?php } ?>
                        </div>
                     </div>
                  <?php } else { ?>
                     <div class="no-data">
                        <div class="no-rec">
                           <img src="<?= ms_site_url('assets/images/frontend/no-record-found.png') ?>"/>
                           <h6>No results found</h6>
                           <p>Try different keywords or remove search filters</p>
                        </div>
                     </div>
                  <?php } ?>
               </div>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </section>