<section>
	<div class="video-outer diff-video row">
		<?php $embed_url = getEmbedYouTubeURL($video_details->external_video_id); ?>
		<div class="single-video-sec col-lg-8">
			<div class="shadow-card-bg">
			<div class="single-video-top">
				<!-- <div class="img-sec"><img src="<?= ms_site_url('uploads/thumbnails/'.$video_details->thumbnail) ?>"></div>
				<div class="video-icon"><img src="<?= ms_site_url('assets/images/frontend/video-icon.png') ?>"></div> -->
				<!--<iframe src="<?= $embed_url ?>" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>-->
				<iframe src="<?= $embed_url.'?modestbranding=0&showinfo=0&controls=0' ?>" style="position:absolute;top:0;left:0;width:100%;height:100%;" allow="accelerometer; autoplay; modestbranding; encrypted-media; gyroscope; picture-in-picture" frameborder="0"></iframe>
			</div>

			<div class="category-cont ">
				<div class="category-left">
				    <div class="videonoterap">
					<h3 class="main-title"><a href="<?= ms_site_url('user/'.$video_details->user_and_channel->user_id.'/profile') ?>"><?= $video_details->title ?></a></h3>
					<div class="profile-share videonote">
                                <i class="fa fa-share-alt" data-toggle="modal" data-target="#sharevideo"></i>
                            </div>
                     </div>       
					<div class="comment-sec">
						<div class="comment-cont">
							<p>
								<a href="<?= ms_site_url('user/'.$video_details->user_and_channel->user_id.'/profile') ?>"> 
									<span>
									<?php
										if ($video_details->user_and_channel->image) { ?>
											<img src="<?= ms_site_url('uploads/avatars/'.$video_details->user_and_channel->image) ?>">
										<?php } else { ?>
											<?= strtoupper($video_details->user_and_channel->user_name[0]); ?>
									<?php } ?>
									</span> 
									<?= $video_details->user_and_channel->user_name ?> 
								</a>
							</p>
						</div>
						<div class="comment-cont">
							<p><?= getDateInFormat($video_details->created_at) ?></p>
						</div>
						<div class="comment-cont">
							<p><img src="<?= ms_site_url('assets/images/frontend/eyes-icon.png') ?>"><span><?= kConverter($video_details->view_count) ?></span>Views</p>
						</div>
						<div class="category-right">
							<div class="category-right-bottom">
								<div class="category-right-bottom-left">
									<!--<ul>
										<li><a href="javascript:void(0)"><img src="<?= ms_site_url('assets/images/frontend/like-icon.png') ?>"><span><?= kConverter($video_details->like_count) ?></span></a></li>
										<li><a href="javascript:void(0)"><img src="<?= ms_site_url('assets/images/frontend/like-icon-1.png') ?>"><span><?= kConverter($video_details->dislike_count) ?></span></a></li>
									</ul>-->
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="category-right-top">
								<p><strong>Category</strong> : <?= getCategoryNamesLine($video_details) ?></p>
							</div>
							
						</div>
					</div>
					<p><?= nl2br($video_details->description) ?></p>
				</div>
				<div class="clearfix"></div>
			</div>
			<!-- comment section start -->
			<div class="all-comments-sec">
				<div class="sort-left">
						  <?php if (!isUserLoggedIn()) { ?>
						  <h3 class="main-title">Want to get in on the conversation?</h3>
				    	  <a href="<?= ms_Site_url('login') ?>" class="btn-outline-theme">
                        	<span class="sign_in_btn">login</span>
                    	  </a>
						  <?php } ?>
					<?php if($comments_count>1){ ?>
				
					<p><span id="comments-count"><?= $comments_count; ?></span> Comments</p>
					<?php } else{ ?>
				
						<p><span id="comments-count"><?= $comments_count; ?></span> Comment</p>
					<?php } ?>
					<!--<p><a href="#"><img src="../assets/images/frontend/sort-icon.png">Sort By</a></p>-->
				</div>
				<div class="subscribe-sec">
					<!--<a href="javascript:void(0)" class="cmn-btn">SUBSCRIBE</a>-->
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php if (isUserLoggedIn()) { ?>
			<div class="public-comment-sec">
				<div class="public-comment-left">
					<p class="user-profile-thumb"><a href="#">
						<span>
						<?php
							$logged_in_user_details = getLoggedInUserDetails();
							if ($logged_in_user_details->image) { ?>
						<img src="<?= ms_site_url('uploads/avatars/'.$logged_in_user_details->image) ?>">
						<?php } else { ?>
						<?= strtoupper($logged_in_user_details->name[0]); ?>
						<?php } ?></span>
						</a>
					</p>
				</div>
				<form id="comment_form" action="<?= ms_site_url('note/'.$video_details->id.'/comment') ?>" method="post">
					<div class="public-comment-right">
						<div class="form-group">
							<textarea class="form-control" name="comment" id="comment-text" placeholder="Start a discussion..."></textarea>
							<span class="error comment-error"></span>
						</div>
						<input type='hidden' name='note_id' value="<?= $video_details->id ?>" id='note_id' />
						<input type='hidden' name='resource_type' value="videos" />
					</div>
					<div class="clearfix"></div>
					<div class="cancel-confirm-block">
						<a href="javascript:void(0)" class="cmn-btn cancel-cmt">Cancel</a>
						<!--<a href="javascript:void(0)" class="cmn-btn">Comment</a>-->
						<input class="btn btn-success" type="submit" name="submit" value="comment"/>
					</div>
				</form>
			</div>
			<?php } ?>
			<div id="comments-listing">
				
			</div>
			<div class="loader" style="display:none">
				<img src="<?= ms_site_url('assets/images/frontend/loader.gif') ?>">
			</div>
			<!-- comment section end -->
						</div>
		</div>
		<div class="advert-sec col-lg-4">
		<?php if (count($related_videos)) { ?>
			<div class="shadow-card-bg">
				<!-- <div class="advert-top">
					<h3 class="main-title">
					Advertise<br>
					Here
				</div> -->
			
				<div class="advert-bottom-sec">
					<h4>More Videos</h4>
					<?php 
						foreach ($related_videos as $video) { ?>
							<div class="advert-video">
								<a href="<?= ms_site_url('videos/'.$video->slug) ?>">
									<div class="advert-video-left">
										<div class="img-sec">
											<?php if ($video->thumbnail) { ?>
											<img src="<?= ms_site_url('uploads/thumbnails/'.$video->thumbnail) ?>">
											<?php } else {  ?>
											<img src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
											<?php } ?>
										</div>
										<div class="video-icon"><img src="<?= ms_site_url('assets/images/frontend/video-icon-1.png') ?>"></div>
									</div>
									<div class="advert-video-right">
										<p><?= $video->title ?></p>
										<p><span><?= getDateInFormat($video->created_at) ?></span></p>
									</div>
									<div class="clearfix"></div>
								</a>
							</div>
					<?php }
					?>
				</div>
			<?php } ?>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>

<div class="modal fade" id="sharevideo" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="share-profile-popup">
                <h3>Share Video</h3>

                <div class="share-profile-icons">
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?= ms_site_url('videos/'.$video_details->slug) ?>"
                                target="_blank" class="social-fb social-share-inner-btn">
                                <div class="icon-holder facebook">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </div>
                                <span>Facebook</span>
                            </a>
                        </li>

                        <li>
                            <a href="https://twitter.com/intent/tweet?url=<?= ms_site_url('videos/'.$video_details->slug) ?>"
                                target="_blank">
                                <div class="icon-holder twitter">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </div>
                                <span>Twitter</span>
                            </a>
                        </li>

                        <li>
                            <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?= ms_site_url('videos/'.$video_details->slug) ?>"
                                target="_blank">
                                <div class="icon-holder linkedin">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </div>
                                <span>Linkedin</span>
                            </a>
                        </li>

                        <li>
                            <a href="http://pinterest.com/pin/create/button/?url=<?= ms_site_url('videos/'.$video_details->slug) ?>"
                                target="_blank">
                                <div class="icon-holder pinterest">
                                    <i class="fa fa-pinterest-square" aria-hidden="true"></i>
                                </div>
                                <span>Pinterest</span>
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="share-profile-textarea">
                    <textarea id="myInput"><?= ms_site_url('videos/'.$video_details->slug) ?></textarea>
                    <div class="copy-link">
                        <button onclick="myFunction()" onmouseout="outFunc()">
                            Copy
                        </button>
                        <span class="tooltiptext" id="myTooltip">Copy to clipboard</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
	var page = 0;
	var total_pages = <?= $total_pages;  ?>;			
</script>
<?php $this->load->view('common/comments-script'); ?>