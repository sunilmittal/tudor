<section>
	<div class="dashboard-sec add-video">
		<div class="inner-sec">
			<?php $this->load->view('common/sidebar') ?>
			<div class="right-sec">
				<div class="shadow-card-bg custom-add-video">
				<form method="post" enctype="multipart/form-data">
					<div class="right-inner">
						<h4>Edit Video</h4>
						<div class="row add-inner">
							<div class="col-lg-12 col-12 left-part cmn-space">
								<span>Video Title</span>
								<input class="form-control" type="text" name="title" 
									placeholder="" value="<?= set_value('title', $video_details->title) ?>">
								<?= form_error('title', '<span class="error">', '</span>'); ?>
							</div>

							<div class="col-lg-6 col-12 left-part pos-r cmn-space">
								<span>Youtube video ID :</span>
								<input class="form-control" type="text" name="video_id" 
									placeholder="eg. CL2GKxcmKuM" value="<?= set_value('video_id', $video_details->external_video_id) ?>" readonly="readonly">
								<?= form_error('video_id', '<span class="error">', '</span>'); ?>
							</div>
							<!--<div class="col-lg-6 col-12 right-part cmn-space">
								<span>Visibility</span>
								<select name="visibility" id="visible_class" class="form-control">
									<option value="<?= VIDEO_PUBLIC_VISIBILITY ?>" <?php echo set_value('visibility', $video_details->visibility) == VIDEO_PUBLIC_VISIBILITY ? 'selected' : ''; ?>>Public</option>
								<?php if($user_data->my_classroom_ids){ ?>
									<option value="<?= VIDEO_PRIVATE_VISIBILITY ?>" <?php echo set_value('visibility', $video_details->visibility) == VIDEO_PRIVATE_VISIBILITY ? 'selected' : ''; ?>>Classroom</option>
								<?php } ?>
								</select>
								<?= form_error('visibility', '<span class="error">', '</span>'); ?>
							</div>-->

							<?php if($video_details->classroom_id && $classrooms){ ?>
								<div class="col-lg-6 col-12 left-part cmn-space" id="ShowClassroom">
									<span>Select Classroom</span>
									<select class='form-control' name='classroom_id' id='classroom_id'>
										<option value=''>Select Classroom</option>
										<?php foreach ($classrooms as $classroom) { 
											$checked = $classroom->id == $video_details->classroom_id ? "selected=selected" : '';
										?>
											<option value="<?= $classroom->id ?>" <?= $checked ?> ><?= $classroom->title ?></option>
										<?php } ?>
									</select>
									<?= form_error('classroom_id', '<span class="error">', '</span>'); ?>
								</div>
							<?php }else{ ?>
								<div class="col-lg-6 col-12 left-part cmn-space" id="ShowClassroom" style="display: none;"></div>
							<?php } ?>
							<div class="col-lg-6 col-12 left-part cmn-space">
								<span>Select Category</span>
								<select class="form-control" name="category_id" id="category_id">
				                    <option value="">Select category</option>
				                    <?php if (isset($categories) && is_array($categories) && count($categories) > 0) { ?>
				                        <?php foreach ($categories as $category) { ?>
				                            <option value="<?php echo $category->id; ?>" <?php echo set_value('category_id', $video_details->category_id) == $category->id ? 'selected' : ''; ?>>
				                                <?php echo $category->name; ?>
				                            </option>
				                        <?php } ?>
				                    <?php } ?>
				                </select>
							</div>
							<div class="col-lg-6 col-12 right-part cmn-space">
								<span>Select Sub-category</span>
								<select class="form-control" name="sub_category_id" id="sub_category_id">
				                    <option value="">Select sub-category</option>
				                    <?php if (isset($sub_categories) && is_array($sub_categories) && count($sub_categories) > 0) { ?>
				                        <?php foreach ($sub_categories as $sub_category) { ?>
				                            <option value="<?php echo $sub_category->id; ?>" <?php echo set_value('sub_category_id', $video_details->sub_category_id) == $sub_category->id ? 'selected' : ''; ?>>
				                                <?php echo $sub_category->name; ?>
				                            </option>
				                        <?php } ?>
				                    <?php } ?>
				                </select>
							</div>

							
							<div class="col-lg-12 cmn-space">
								<span>Description</span>
								<textarea class="form-control" name="description" placeholder="What’s your video about?"><?= set_value('description', $video_details->description) ?></textarea>
								<?= form_error('description', '<span class="error">', '</span>'); ?>
							</div>
							<div class="col-lg-6 col-12 left-part cmn-space">
								<span>Thumbnail</span>
								<!-- <div class="file-input form-control min-height">
									<label for="thumbnail">
										<span><img src="<?= ms_site_url('assets/images/frontend/upload-vid.png') ?>" accept="image/*"></span> 
										<span> upload </span>
									</label>
									<input type="file" id="thumbnail" name="thumbnail" accept="image/*">
								</div> -->
								<div class="form-control file-input min-height">
									<?php if(!($video_details->thumbnail)){ $delete_icon_class = "display_none"; ?>
									<label for="thumbnail" id="image-preview-label"><img src="<?= ms_site_url('assets/images/frontend/upload-vid.png') ?>"><br/>Video Thumbnail</label>
									<?php } if($video_details->thumbnail){ $delete_icon_class = ""; ?>
										<img src="<?= ms_site_url('uploads/thumbnails/'.$video_details->thumbnail.'') ?>" alt="" id="image-preview" />
									<?php } else{ ?>
										<img src="" id="image-preview" class="display_none" />
									<?php } ?>
									<input type="file" id="thumbnail" name="thumbnail" class="file-upload-preview" accept="image/*" data-img_id="image-preview" data-msg-accept="Please select only jpeg, bmp, png, jpg file." data-label_id="image-preview-label">
									
								</div>

								<div class="image-actions">
									<a class="upload-button" data-preview_section="thumbnail"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
									<a class="remove-image-button <?= $delete_icon_class ?>" data-file_element="thumbnail" data-delete_section="image-preview"><i class="fa fa-trash" aria-hidden="true"></i> </a>
								</div>
								<input type="hidden" name="delete_image" value="0" />
								
							</div>
							<div class="col-lg-6 col-12 right-part cmn-space">
								<input class="upload-vid cmn-btn" type="submit" value="Update Video" name="submit">
							</div>
						</div>
					</div>
				</form>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>