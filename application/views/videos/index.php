<section>
	<div class="dashboard-sec add-video my-videos recommended-page">
		<div class="inner-sec">
			<?php $this->load->view('common/sidebar') ?>
			<div class="right-sec">
				<?php $full_url = basename($_SERVER['REQUEST_URI']); ?>

				<!-- filter box -->
				<div class="filter-box <?= $show_filter_class; ?>">
					<button class="filter-icon"> filter</button>

					<div class="to-filter-block">
						<div class="to-filter-inner">
							<div class="to-filter-cell">
								<h2>Type :</h2>
								<span>
									<a class="inner_filter_btn active" href="" data-type="content">content</a>
									<a class="inner_filter_btn" href="" data-type="user">Users</a>
								</span>
							</div>
						</div>
					</div>
				</div>
				<!-- filter box ends -->

				<!-- filter subscribe -->
				<div class="filter-subscribe-block" style="display:none">
					<?php if(count($filter_user_type)){
						foreach($filter_user_type as $single){
							?>
							<div class="subscribe-row custom-subc-row">
								<div class="rounder-image-wrap">
									<a href="<?= ms_site_url('user/'.$single->user_id.'/profile') ?>" target="_blank">
										<?php
										if ($single->image) { ?>
											<img src="<?= ms_site_url('uploads/avatars/'.$single->image) ?>">
										<?php } else { ?>
											<span> <?= strtoupper($single->user_name[0]); ?></span>
										<?php }
										?>
									</a>
								</div>
								<div class="filter-content">
									<a href="<?= ms_site_url('user/'.$single->user_id.'/profile') ?>" target="_blank">
										<p><?= $single->user_name; ?></p>
										<p><?= $single->total_videos; ?> Videos | <?= $single->total_notes; ?> Notes</p>
									</a>
								</div>

								<!--<p><a href="#" class="btn">Subscribe</a></p>-->
								<?php
								if ($single->show_subscribe_btn) { ?>
									<?php if (in_array($single->user_id, $user_data->subscribed_user_ids)) { ?>
										<div class="video-right-subscribe">
											<a href="<?= ms_site_url('user/'.$single->user_id.'/unsubscribe?redirect_url='.$full_url.'') ?>"
												class="cmn-btn">UNSUBSCRIBE</a>
											</div>
										<?php } else { ?>
											<div class="video-right-subscribe">
												<a href="<?= ms_site_url('user/'.$single->user_id.'/subscribe?redirect_url='.$full_url.'') ?>"
													class="cmn-btn">SUBSCRIBE</a>
												</div>
											<?php } ?>
										<?php  } else{ ?>
											<p><a href="#"></a></p>
										<?php } ?>

									</div>
								<?php } } else{ ?>
									<div class="no-data user-no-data">
										<div class="no-rec">
											<img src="<?= ms_site_url('assets/images/frontend/user.png') ?>" />
											<h6>No results found</h6>
											<p>Try different keywords or remove search filters</p>
										</div>
									</div>
								<?php } ?>
							</div>
							<!-- filter subscribe ends -->


							<div class="tabs-notes-videos">
								<div class="dev-with-yt-wrap">
									<span class="youtub-developed"><a>Developed with <b>YouTube</b></a></span>
									<ul class="nav nav-tabs">
										<li class="active"><a data-toggle="tab" href="#notesec">Notes</a></li>
										<li><a data-toggle="tab" href="#videosec">Videos</a></li>
									</ul>
								</div>
								<div class="tab-content">
									<div id="notesec" class="tab-pane active">
										<div class="notes-by-content-cnt">
											<?php if (!isUserLoggedIn()) { ?>
												<?php if ($recommended_notes) { ?>
                                <!-- <div class="right-outer diff-sec pt-none">
                                    <h4>Recommended Notes</h4>
                                    <div class="clearfix"></div>
                                    <div class="full-box p-btm">
                                        <?php 
													foreach ($recommended_notes as $note) { ?>
                                        <div class="box1">
                                            <a href="<?= ms_site_url('notes/'.$note->slug) ?>">
                                                <div class="img-outer">
                                                    <div class="img-sec">
                                                        <?php if ($note->image) { ?>
                                                        <img src="<?= ms_site_url('uploads/notes/'.$note->image.'') ?>">
                                                        <?php } else{  ?>
                                                        <img
                                                            src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="box-cont">
                                                    <p><strong><?= substr($note->title, 0,30) ?>...</strong></p>
                                                    <div class="box-content">
                                                        <div class="box-content-left">
                                                            <div class="p-pic">
                                                                <span>
                                                                    <?php
																			if ($note->user->image) { ?>
                                                                    <img
                                                                        src="<?= ms_site_url('uploads/avatars/'.$note->user->image) ?>">
                                                                    <?php } else { ?>
                                                                    <?= strtoupper($note->user->user_name[0]) ?>
                                                                    <?php }
																			?>
                                                                </span>
                                                            </div>
                                                            <div class="pic-cont">
                                                                <p><?= $note->user->user_name ?></p>
                                                            </div>
                                                        </div>
                                                        <div class="box-content-right">
                                                            <p><?= getDateInFormat($note->created_at) ?></p>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <?php }
													?>
                                    </div>
                                </div> -->
                                <div class="notes">
                                	<h4>Recommended Notes</h4>
                                	<div class="">
                                		<?php foreach($recommended_notes as $note) { ?>

                                			<div class="notes-repeat">
                                				<div class="user-name">
                                					<a href="<?= ms_site_url('user/'.$note->user_id.'/profile') ?>">
                                						<h5><?= $note->user->user_name; ?>
                                						<span
                                						class="user-role-custom"><?= ($note->user->user_role==0) ? "Learner" : "Teacher"  ?></span>
                                					</h5>
                                					<span class="user-role-badge">
                                						<?php
                                						if ($note->user->image) { ?>
                                							<img
                                							src="<?= ms_site_url('uploads/avatars/'.$note->user->image) ?>">
                                						<?php } else { ?>
                                							<?= strtoupper($note->user->user_name[0]) ?>
                                						<?php }
                                						?>
                                					</span>
                                				</a>
                                			</div>

                                			<a href="<?= ms_site_url('notes/'.$note->slug) ?>">
                                				<div class="img-sec">
                                					<?php if ($note->image) { ?>
                                						<img src="<?= ms_site_url('uploads/notes/'.$note->image.'') ?>">
                                					<?php } else{  ?>
                                						<img src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
                                					<?php } ?>
                                				</div>
                                			</a>
                                			<div class="notes-right-part">
                                				<a href="<?= ms_site_url('notes/'.$note->slug) ?>">
                                					<div class="title custom-title-notes">
                                						<h5><?php echo $note->title; ?></h5>
                                					</div>
                                				</a>

                                				<div class="clearfix"></div>
                                				<div class="description">
                                					<p><?php echo generateReadMore($note->description, 'notes', $note->slug); ?>
                                				</p>
                                			</div>
                                			<div class="date"><span> <i class="fa fa-calendar" aria-hidden="true"></i>
                                				<?= getDateInFormat($note->date) ?>
                                			</span>
                                		</div>
                                		<?php if(!$note->flag_exists && $note->user_id!=getLoggedInUserId()){ ?>
                                			<div class="flag-icon"><a data-toggle="modal" data-target="#noteFlageModal"
                                				data_note_id="<?= $note->id ?>" class="open_noteFlageModal"><i
                                				class="fa fa-flag" aria-hidden="true"></i> Flag</a></div>
                                			<?php } ?>
                                			<div class="notes-category">
                                				<span><i class="fa fa-list-alt" aria-hidden="true"></i>
                                					<?= getCategoryNamesLine($note) ?>
                                				</div>
                                			</div>
                                		</div>

                                	<?php } ?>
                                </div>
                            </div>
                        <?php } else { ?>
                        	<div class="no-data">
                        		<div class="no-rec">
                        			<img src="<?= ms_site_url('assets/images/frontend/no-notes.png') ?>" />
                        			<h6>No results found</h6>
                        			<p>Try different keywords or remove search filters</p>
                        		</div>
                        	</div>
                        <?php } ?>
                    <?php } else { ?>
                    	<?php if($notes) { ?>
                    		<div class="notes">
                    			<h4>Notes</h4>
                    			<div class="">
                    				<?php foreach($notes as $note) { ?>
                    					<div class="notes-repeat">
                    						<div class="user-name">
                    							<a href="<?= ms_site_url('user/'.$note->user_id.'/profile') ?>">
                    								<h5><?= $note->user->user_name; ?>
                    								<span
                    								class="user-role-custom"><?= ($note->user->user_role==0) ? "Learner" : "Teacher"  ?></span>
                    							</h5>
                    							<span class="user-role-badge">
                    								<?php
                    								if ($note->user->image) { ?>
                    									<img
                    									src="<?= ms_site_url('uploads/avatars/'.$note->user->image) ?>">
                    								<?php } else { ?>
                    									<?= strtoupper($note->user->user_name[0]) ?>
                    								<?php }
                    								?>
                    							</span>
                    						</a>
                    					</div>

                    					<a href="<?= ms_site_url('notes/'.$note->slug) ?>">
                    						<div class="img-sec">
                    							<?php if ($note->image) { ?>
                    								<img src="<?= ms_site_url('uploads/notes/'.$note->image.'') ?>">
                    							<?php } else{  ?>
                    								<img src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
                    							<?php } ?>
                    						</div>
                    					</a>
                    					<div class="notes-right-part">
                    						<a href="<?= ms_site_url('notes/'.$note->slug) ?>">
                    							<div class="title custom-title-notes">
                    								<h5><?php echo $note->title; ?></h5>
                    							</div>
                    						</a>

                    						<div class="clearfix"></div>
                    						<div class="description">
                    							<p><?php echo generateReadMore($note->description, 'notes', $note->slug); ?>
                    						</p>
                    					</div>
                    					<div class="date"><span> <i class="fa fa-calendar" aria-hidden="true"></i>
                    						<?= getDateInFormat($note->date) ?>
                    					</span>
                    				</div>
                    				<?php if(!$note->flag_exists && $note->user_id!=getLoggedInUserId()){ ?>
                    					<div class="flag-icon"><a data-toggle="modal" data-target="#noteFlageModal"
                    						data_note_id="<?= $note->id ?>" class="open_noteFlageModal"><i
                    						class="fa fa-flag" aria-hidden="true"></i> Flag</a></div>
                    					<?php } ?>
                    					<div class="notes-category">
                    						<span><i class="fa fa-list-alt" aria-hidden="true"></i>
                    							<?= getCategoryNamesLine($note) ?>
                    						</div>
                    					</div>
                    				</div>
                    			<?php } ?>
                    		</div>
                    	</div>
                    	<center>
                    	</center>
                    	<input type="hidden" id="total_page_count" value="<?= $total_pages ?>">
                    <?php } else{ ?>
                    	<div class="no-data">
                    		<div class="no-rec">
                    			<img src="<?= ms_site_url('assets/images/frontend/no-notes.png') ?>" />
                    			<h6>No results found</h6>
                    			<p>Try different keywords or remove search filters</p>
                    		</div>
                    	</div>
                    <?php } } ?>
                    <img style="display: none;" height="300px" width="300px" class="loader" src="<?= ms_site_url('assets/images/frontend/loader.gif') ?>">
                </div>
            </div>

            <!-- searched or recommended videos section start -->
            <div id="videosec" class="tab-pane fade">

            	<div class="right-inner videos-by-content-cnt">
            		<?php if (count($searched_videos)) { ?>
            			<div class="right-outer diff-sec">
            				<h4>Searched Videos</h4>
            				<div class="clearfix"></div>
            				<div class="full-box p-btm">
            					<?php foreach ($searched_videos as $video) { ?>
            						<div class="box1">
            							<a href="<?= ms_site_url('videos/'.$video->slug) ?>">
            								<div class="img-outer">
            									<div class="img-sec">
            										<?php if ($video->thumbnail) { ?>
            											<img src="<?= ms_site_url('uploads/thumbnails/'.$video->thumbnail) ?>">
            										<?php } else {  ?>
            											<img src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
            										<?php } ?>
            									</div>
            									<div class="video-icon"><img
            										src="<?= ms_site_url('assets/images/frontend/video-play.png') ?>">
            									</div>
            									<span
            									class="video-duration"><?= convert_seconds($video->duration) ?></span>
            								</div>
            								<div class="box-cont">
            									<p><strong><?= generateDotsForMoreInfo($video->title, 30) ?></strong></p>
            									<div class="box-content">
            										<div class="box-content-left">
            											<div class="p-pic profile-name-wrap">
            												<span>
            													<?php
            													if ($video->user_and_channel->image) { ?>
            														<img
            														src="<?= ms_site_url('uploads/avatars/'.$video->user_and_channel->image) ?>">
            													<?php } else { ?>
            														<?= strtoupper($video->user_and_channel->user_name[0]) ?>
            													<?php }
            													?>
            												</span>
            												<p><?= $video->user_and_channel->user_name ?></p>
            											</div>
            											<div class="pic-cont">

                                                                <!-- <p><img
                                                                        src="<?= ms_site_url('assets/images/frontend/eyes-icon.png') ?>"><span><?= thousandsFormat($video->view_count) ?></span>Views
                                                                    </p> -->

                                                                    <p><img class="eye-view" src="<?= ms_site_url('assets/images/frontend/eyes-icon.png') ?>"> 
                                                                    	<?php if($video->view_count) { ?>
                                                                    		<span class="view-counts"><?= thousandsFormat($video->view_count) ?></span>
                                                                    		<?php } else{ ?><span class="view-counts">0</span> <?php } ?> <span class="views">views</span></p>
                                                                    	</div>
                                                                    </div>
                                                                    <div class="box-content-right">
                                                                    	<p><?= getDateInFormat($video->created_at) ?></p>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } else if(count($videos)){ ?>
                                    	<div class="right-outer diff-sec pt-none custom-rcmd-block">
                                    		<h4>Recommended Videos</h4>
                                    		<div class="clearfix"></div>
                                    		<div class="full-box p-btm">
                                    			<?php foreach ($videos as $video) { ?>
                                    				<div class="box1">
                                    					<a href="<?= ms_site_url('videos/'.$video->slug) ?>">
                                    						<div class="img-outer">
                                    							<div class="img-sec">
                                    								<?php if ($video->thumbnail) { ?>
                                    									<img src="<?= ms_site_url('uploads/thumbnails/'.$video->thumbnail) ?>">
                                    								<?php } else {  ?>
                                    									<img src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
                                    								<?php } ?>
                                    							</div>
                                    							<div class="video-icon"><img
                                    								src="<?= ms_site_url('assets/images/frontend/video-play.png') ?>">
                                    							</div>
                                    							<span
                                    							class="video-duration"><?= convert_seconds($video->duration) ?></span>
                                    						</div>
                                    						<div class="box-cont">
                                    							<p><strong><?= generateDotsForMoreInfo($video->title, 30) ?></strong></p>
                                    							<div class="box-content">
                                    								<div class="box-content-left">
                                    									<div class="p-pic profile-name-wrap">
                                    										<span>
                                    											<?php
                                    											if ($video->user_and_channel->image) { ?>
                                    												<img
                                    												src="<?= ms_site_url('uploads/avatars/'.$video->user_and_channel->image) ?>">
                                    											<?php } else { ?>
                                    												<?= strtoupper($video->user_and_channel->user_name[0]) ?>
                                    											<?php }
                                    											?>
                                    										</span>
                                    										<p><?= $video->user_and_channel->user_name ?></p>
                                    									</div>
                                    									<div class="pic-cont">

                                    										<p><img class="eye-view" src="<?= ms_site_url('assets/images/frontend/eyes-icon.png') ?>"> <span class="view-counts"><?php if($video->view_count) { ?>
                                    											<span class="view-counts"><?= thousandsFormat($video->view_count) ?></span>
                                    											<?php } else{ ?><span class="view-counts">0</span> <?php } ?> <span class="views">views</span></p>
                                    										</div>
                                    									</div>
                                    									<div class="box-content-right">
                                    										<p><?= getDateInFormat($video->created_at) ?></p>
                                    									</div>
                                    									<div class="clearfix"></div>
                                    								</div>
                                    							</div>
                                    						</a>
                                    					</div>
                                    				<?php }
                                    				?>
                                    			</div>
                                    		</div>
                                    	<?php }  else { if(empty($videos)){ ?>
                                    		<h4>Recommended Videos</h4>
                                    	<?php } else{ ?>
                                    		<h4>Searched Videos</h4>
                                    	<?php } ?>
                                    	<div class="no-data">
                                    		<div class="no-rec">
                                    			<img src="<?= ms_site_url('assets/images/frontend/no-record-found.png') ?>" />
                                    			<h6>No results found</h6>
                                    			<p>Try different keywords or remove search filters</p>
                                    		</div>
                                    	</div>
                                    <?php } ?>
                                    <!-- searched or recommended videos section end -->

                                    <?php if (count($featured_videos)) { ?>
                                    	<div class="right-outer">
                                    		<h4>Featured Videos</h4>
                                    		<div class="clearfix"></div>
                                    		<div class="full-box">
                                    			<?php foreach ($featured_videos as $video) { ?>
                                    				<div class="box1">
                                    					<a href="<?= ms_site_url('videos/'.$video->slug) ?>">
                                    						<div class="img-outer">
                                    							<div class="img-sec">
                                    								<?php if ($video->thumbnail) { ?>
                                    									<img src="<?= ms_site_url('uploads/thumbnails/'.$video->thumbnail) ?>">
                                    								<?php } else {  ?>
                                    									<img src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
                                    								<?php } ?>
                                    							</div>
                                    							<div class="video-icon"><img
                                    								src="<?= ms_site_url('assets/images/frontend/video-play.png') ?>">
                                    							</div>
                                    							<span
                                    							class="video-duration"><?= convert_seconds($video->duration) ?></span>
                                    						</div>
                                    						<div class="box-cont">
                                    							<p><strong><?= generateDotsForMoreInfo($video->title, 30) ?></strong></p>


                                    							<div class="box-content">
                                    								<div class="box-content-left">
                                    									<div class="p-user-data-left">
                                    										<div class="p-pic">
                                    											<span>
                                    												<?php
                                    												if ($video->user_and_channel->image) { ?>
                                    													<img
                                    													src="<?= ms_site_url('uploads/avatars/'.$video->user_and_channel->image) ?>">
                                    												<?php } else { ?>
                                    													<?= strtoupper($video->user_and_channel->user_name[0]) ?>
                                    												<?php }
                                    												?>
                                    											</span>
                                    										</div>

                                    										<p><?= $video->user_and_channel->user_name ?></p>
                                    									</div>
                                    									<div class="pic-cont">

                                    										<p><img
                                    											src="<?= ms_site_url('assets/images/frontend/eyes-icon.png') ?>"><?php if($video->view_count) { ?>
                                    												<span class="view-counts"> <?= thousandsFormat($video->view_count) ?></span>
                                    												<?php } else{ ?> <span class="view-counts">0</span> <?php } ?> <span class="views">views</span>
                                    											</p>
                                    										</div>
                                    									</div>
                                    									<div class="box-content-right">
                                    										<p><?= getDateInFormat($video->created_at) ?></p>
                                    									</div>
                                    									<div class="clearfix"></div>
                                    								</div>
                                    							</div>
                                    						</a>
                                    					</div>
                                    				<?php } ?>
                                    			</div>
                                    		</div>
                                    	<?php } ?>
                                    	<?php $this->load->view('common/video-history') ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </section>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section></section>
<!-- Modal -->
<div class="modal fade" id="noteFlageModal" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form method="post" action="<?= ms_site_url('resource_inappropriate/flag') ?>">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title d-inline" id="staticBackdropLabel">Note Inappropriate Flag</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Flag Message</p>
					<input type="hidden" name="note_id" id="note_id" value="" />
					<input type="hidden" name="resource_type" id="resource_type" value="" />
					<textarea class="form-control" id="flag_message" name="message"></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>