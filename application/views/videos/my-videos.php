<section>
	<div class="dashboard-sec add-video my-videos">
		<div class="inner-sec">
			<?php $this->load->view('common/sidebar') ?>
			<div class="right-sec my-custom-videos-wrap">
				<div class="right-inner shadow-card-bg">
					<h4>My Videos</h4>
					<h5>
						<a href="<?= ms_site_url('videos/add') ?>">
                        	<span><img src="<?= ms_Site_url('assets/images/frontend/add-video-icon.png') ?>"></span> Add video
                        </a>
					</h5>
					<div class="clearfix"></div>
					<div class="video-inner">
					<div class="table-responsive">
						<table class="table">
							<tr>
								<th>Video</th>
								<th>Video Title</th>
								<!--<th>Visibility</th>-->
								<th>Date   </th>
								<th>Views</th>
								<th>Comments </th>
								<th>Actions</th>
							</tr>
							<?php
								if (count($videos)) {
									foreach ($videos as $video) { ?>
										<tr>
											<td>
												<a href="<?= ms_site_url('videos/'.$video->slug) ?>" target="_blank">
													<div class="img-sec">
														<?php if ($video->thumbnail) { ?>
														<img src="<?= ms_site_url('uploads/thumbnails/'.$video->thumbnail) ?>">
														<?php } else {  ?>
														<img src="<?= ms_site_url('assets/images/frontend/dumy-img.jpg') ?>">
														<?php } ?>
														<img src="<?= ms_Site_url('assets/images/frontend/video-play.png') ?>" class="video-icon">
														<span class="video-duration"><?= convert_seconds($video->duration) ?></span>
													</div>
												</a>
											</td>
											<td><?= $video->title ?></td>
											<!--<td><?= getVideoVisibility($video) ?></td>-->
											<td><?= getDateInFormat($video->created_at) ?></td>
											<td><?= kConverter($video->view_count) ?></td>
											<td><?= kConverter($video->comment_count) ?></td>
											<td>
												<a href="<?= ms_Site_url('videos/edit/'.$video->slug) ?>">
													<button class="edit-icon"><i class="fa fa-pencil" aria-hidden="true"></i></button>
												</a>
												<a href="<?php echo site_url('videos/delete/'.$video->id); ?>" class="delete_video">
												    <i class="fa fa-trash-o" aria-hidden="true"></i>
												</a>
											</td>
										</tr>
								<?php }
								} else { ?>
									<tr>
										<td colspan="9"></td>
									</tr>
									<tr>
									<td class="no-data" colspan="9">No Videos Found</td>
								</tr>
								<?php }
							?>
							
						</table>
					</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>