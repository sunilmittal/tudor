"use strict";

let ajaxUrl,
    files,
    ajaxPermit = [],
    intervals = [],
    ajaxVars = {},
    GLOBAL_URL = ''; // GLOBAL_URL


function clearFiles() { files = null; }



$.fn.hasAttr = function (name) {
    return this.attr(name) !== undefined;
};

// $(function () {
//     // Cancel transitions
//     $(".wrap").on('click', 'a', function (e) {
//         if ($(this).hasAttr('onclick'))
//             e.preventDefault();
//         //if (!$(this).hasClass('nob'))
//     });
// });


window.addEventListener('popstate', function(event) {
    // back to prev page
    load(window.location.pathname);
    // history.pushState(null, null, window.location.pathname);
}, false);


/**
 * Put it to "onclick" when upload file
 * @param el
 */
function initFile(el) {
    files = el.files;
}


// Select
function chooseOption(el, valueField, nameField) {
    if (valueField)
        $(valueField).val( $(el).attr('data-option') );

    if (nameField)
        $(nameField).html( $(el).html() );
}

function removeActive(el) {
    $(el).parent().find(".active").removeClass('active');
    $(el).toggleClass('active');
}




function load(url = null, ...fileds) {
    let data = new FormData();
    let type = 'POST';


    // Files
    // let fieldOfFiles = $("#files");
    // if (typeof fieldOfFiles[0] != "undefined")
    //     files = fieldOfFiles[0].files;

    $.each(files, function (key, value) {
        data.append(key, value);
        // data[key] = value;
    });


    // #text - значення input з id='text'
    // .text - значення input з class='text'
    // age=25 - передаємо ключ та значення
    // name#field - передаємо ключ та значення форми ( ключ=name, значення=field )
    // !name#el - передаємо ключ та значення елементу(не input, ex: <p id="el">Text</p>)
    // form:#login - передаємо форму з id='login'
    // json:{} - передаємо json // TODO ...

    for (let i = 0; i < fileds.length; i++) {
        if (fileds[i].charAt(0) === '#' || fileds[i].charAt(0) === '.') {
            data.append(fileds[i], $(fileds[i]).val());
        } else {

            // if (fileds[i].indexOf('=') >= 0) {
            if (/^\w{1,32}=(.*?)$/i.test(fileds[i])) {
                let arr = fileds[i].split('=');
                data.append(arr[0], arr[1]);
            } else {
                if (/^!?\w{1,32}#(.*?)$/i.test(fileds[i])) { // name#field
                    let arr = fileds[i].split('#');
                    if (arr[0].charAt(0) === '!')
                        data.append(arr[0].replace('!', ''), $('#'+arr[1]).text());
                    else
                        data.append(arr[0], $('#'+arr[1]).val());

                } else if (/^!?\w{1,32}\.(.*?)$/i.test(fileds[i])) { // name.field
                    let arr = fileds[i].split('.');

                    if (arr[0].charAt(0) === '!')
                        data.append(arr[0].replace('!', ''), $('.'+arr[1]).text());
                    else
                        data.append(arr[0], $('.'+arr[1]).val());
                } else {
                    // form serialize
                    if (/^form:#(.*?)$/i.test(fileds[i])) {
                        let arr = fileds[i].split('#');
                        let elements = document.forms[arr[1]].elements;
                        // console.log($('#' + arr[1]).serialize());

                        for (let i = 0; i < elements.length; i++){
                            let formField = $(elements[i]);

                            if (formField[0].type === 'radio' || formField[0].type === 'checkbox') {
                                if (formField[0].checked === true)
                                    data.append(formField.attr("name"), formField[0].value);
                            } else {
                                data.append(formField.attr("name"), formField.val());
                            }
                        }
                    }
                    // console.log(fileds[i]);
                }
            }
        }
    }



    let contentType = false;
    if (/Edge/.test(navigator.userAgent)) {
        // contentType = "application/x-www-form-urlencoded"; // [-----------------------------7e314734a0746 Content-Disposition:_form-data;_name] => "email" shloserb@gmail.com
        // contentType = "multipart/form-data;"; // Missing boundary in multipart/form-data POST data in <b>Unknown</b> on line <b>0</b>
        // contentType = "application/json; charset=utf-8;"; // empty
        // contentType = "multipart/form-data; charset=utf-8; boundary=" + Math.random() .toString().substr(2); // empty
    }

    $.ajax({
        url:        trim(url, '/'),
        type:       type,
        data:       data,
        cache:      false,
        dataType:   'json',
        processData: false,
        contentType: contentType,


        success: function (result) {
            if (result.error == false) {
                //prev page
                ajaxUrl = window.location.href;

                // Parse result
                for (let key in result.res) {
                    processField(result.res[key]);
                }
            } else {
                if (Array.isArray(result.error)) {
                    result.error.forEach(function (item, i, arr) {
                        $('.' + item.key).addClass('error');
                        $('.' + item.key + ' span.error_text').text(item.value);
                    });
                } else {
                    alert(result.error);
                }
            }
        },
        error: function (result) {
            //alert("Error!");
        }
    });

    callAfterClick();
}


function processField(jsonObj) {
    try {
        if (jsonObj.action === 'delete') {
            // Deleting
            $(jsonObj.key).remove();
        } else if (jsonObj.action === 'prepend') {
            // Insert at begin
            $(jsonObj.key).prepend(jsonObj.value);
        } else if (jsonObj.action === 'append') {
            // Insert at end
            $(jsonObj.key).append(jsonObj.value);

        } else if (jsonObj.action === 'after') {
            // Вставка в конец
            $(jsonObj.key).after(jsonObj.value);

        } else if (jsonObj.action === 'before') {
            // Вставка в конец
            $(jsonObj.key).before(jsonObj.value);

        } else if (jsonObj.action === 'attr') {
            // set attr
            $(jsonObj.key).attr(jsonObj.attrName, jsonObj.value);

        } else if (jsonObj.action === 'addClass') {
            // set attr
            $(jsonObj.key).addClass(jsonObj.value);
        } else if (jsonObj.action === 'removeClass') {
            // set attr
            $(jsonObj.key).removeClass(jsonObj.value);
        } else if (jsonObj.action === 'remove') {
            // remove element
            $(jsonObj.key).remove();

        } else if (jsonObj.action === 'val') {
            // set value
            $(jsonObj.key).val(jsonObj.value);

        } else if (jsonObj.action === 'func') {
            // function
            window[jsonObj.key](jsonObj.value);

        } else if (jsonObj.action === 'jvar') {
            ajaxVars[jsonObj.key] = jsonObj.value;

        } else if (jsonObj.action === 'load') {
            // load page
            load(jsonObj.key);

        } else if (jsonObj.action === 'redirect') {
            window.location.href = jsonObj.value;

        } else if (jsonObj.action === 'title') {
            // set title
            document.title = jsonObj.value;

        } else if (jsonObj.action === 'url') { //url
            // add history
            history.pushState('', '', jsonObj.value);
            GLOBAL_URL = jsonObj.value;

        } else if (jsonObj.url != false && typeof jsonObj.url !== 'undefined') {
            // add history
            history.pushState('', '', jsonObj.url);
            GLOBAL_URL = jsonObj.url;

        } else if (jsonObj.click != false && typeof jsonObj.click !== 'undefined') {
            // trigger click
            $(jsonObj.click).trigger("click");

        } else {
            // вставка html кода
            $(jsonObj.key).html(jsonObj.value);
        }
        return;
    } catch (e) {
        console.log("Error processField");
        console.log(e);
        return;
    }
}




/**
 * Function calling after click loadMe() OR ajaxLoad()
 */
function callAfterClick() {

    // Hide boards panel
    // $(document).on('click', function(e) {
    //     if (!$(e.target).closest('.boards_window').length) {
    //         console.log("click by page");
    //         $('#boards').removeClass('open');
    //     }
    //
    //     if ($(e.target).closest('.boards_window .board_choose a').length) {
    //         console.log("click by page");
    //         $('#boards').removeClass('open');
    //     }
    //     e.stopPropagation();
    // });
}


function closePopupListener(redirectUrl) {
    var $body = $('body');
    $body.addClass('open');

    $(".close_popup, .popup_fon").click(function () {
        //, .overlayModal
        $body.removeClass('open');
        $(".popup").fadeOut(200);

        if (redirectUrl) {
            window.location.href = redirectUrl;
            // load(redirectUrl);
        }
    });
}

function removeErrors() {
    $('.error_text').text('');
}

function closePopup(redirectUrl) {
    $('body').removeClass('open');
    $(".popup").fadeOut(200);

    if (redirectUrl) {
        window.location.href = redirectUrl;
        // load(redirectUrl);
    }
}


/**
 * Function event_stop
 * - event.preventDefault() stop go by link
 * - event.stopPropagation() stop next events
 */
function stopEv() {
    event.preventDefault(); // cancel switch href
    event.stopPropagation();
}


function trim ( str, charlist ) {
    charlist = !charlist ? ' \s\xA0' : charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\$1');
    let re = new RegExp('^[' + charlist + ']+|[' + charlist + ']+$', 'g');
    return str.replace(re, '');
}

function formatString(str) {
    return str
        .replace(/\\\'/g, '\'')
        .replace(/\\\"/g, '\"')
        .replace(/\\\//g, '\/')
        .replace(/\\\\/g, '\\');
}


function putText(target, html) {
    $(target).html(html);
}


function getTime (mode) {
    mode = mode || 's';

    if (mode === 'ms')
        return new Date().getTime(); // Результат в мс
    else
        return Math.floor(new Date().getTime() / 1000); // Результат в сек
}


function printDate(sec, sep) {
    if (!sec) return sec;
    sep = sep || '/';

    let date = new Date(sec * 1000);
    return date.getDate() + sep + (date.getMonth() + 1) + sep + date.getFullYear();
}

function showTime(minutes) {
    if (!minutes) return minutes;

    let hh = parseInt(minutes / 60);
    minutes -= hh * 60;

    if (minutes.toString().length === 1)
        minutes = '0' + minutes;

    return hh + ':' + minutes;
}


function addZero(value) {
    if (value.toString().length === 1)
        value = '0' + value;

    return value;
}

function focusMe(key) {
    $(key).focus();
}


function scrollToEl(el) {
    let $page = $('html, body'),
        speed = 0;

    el = el || 'header'; //#content

    $page.animate({
        scrollTop: 0
    }, 0);

    if (/^(.*?)\|\w{1,32}$/i.test(el)) {
        let arr = el.split('|');
        el      = arr[0];
        speed   = arr[1];
    }

    $page.animate({
        scrollTop: $(el).offset().top
    }, speed);
}

function popupScrollToEl(el) {
    let $page = $('.popup_body'),
        speed = 0;

    console.log("popupScrollToEl");
    console.log(el);
    console.log($page);

    el = el || 'head'; //#content

    $page.animate({
        scrollTop: 0
    }, 0);


    // if (/^(.*?)\|\w{1,32}$/i.test(el)) {
    //     let arr = el.split('|');
    //     el      = arr[0];
    //     speed   = arr[1];
    // }
    //
    // $page.animate({
    //     scrollTop: $(el).offset().top
    // }, speed);
}


/**
 * isObjEmpty
 * @param obj
 * @returns {boolean}
 */
function isObjEmpty(obj) {
    for (let key in obj)
        return false;

    return true;
}


/**
 * checkInt - перевіряє чи значення є чилом і повертає його, якщо ні - повертається значення за замовчуванням
 * @param value
 * @param defaultValue
 */
function checkInt(value, defaultValue) {
    if (!defaultValue && defaultValue !== false)
        defaultValue = 0;

    if (!isNaN(parseFloat(value)) && isFinite(value))
        return parseInt(value); // added parseInt 12.11.2018
    else
        return defaultValue;
}

/**
 * getStartWeek - Початок поточного тижня
 * @param dateObj
 * @returns {Date}
 */
function getStartWeek(dateObj) {
    let copyDate = new Date(dateObj.getTime());
    let firstDayOfTheWeek = copyDate.getDate() - copyDate.getDay(); // First day is the day of the month - the day of the week

    copyDate.setDate(firstDayOfTheWeek);
    return new Date(copyDate.getFullYear(), copyDate.getMonth(), copyDate.getDate(), 0, 0, 0, 0); // Початок поточного дня
}

function getStartMonth(ms) {
    let copyDate = new Date(ms);
    return new Date(copyDate.getFullYear(), copyDate.getMonth(), 1, 0, 0, 0, 0); // Початок поточного дня
}


/**
 * incrementAction
 * @param el
 */

function incrementAction(el) {
    let value = parseInt( $(el).text() );
    if (isNaN(value))
        value = 0;

    $(el).text(value + 1);
}

function hideNotice(id) {
    setTimeout(function(){ $('#'+id).fadeOut(2000); }, 7000);
}

function randomInteger(min, max) {
    return Math.round( min + Math.random() * (max - min) );
}

function docW() { return $(document).width(); }
function docH() { return $(document).height(); }
function winW() { return $(window).width(); }
function winH() { return $(window).height(); }
