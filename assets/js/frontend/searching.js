let hasValue = false;
let typingTimer, sliderTimer; //timer identifier
let doneTypingInterval = 1000; //time in ms, 1 second for example
let lastTerm = null;
let searchFormEle = $("form#search-form");
let searchTermEle = $("input#search-term");
const slider = document.querySelector('[type="range"]');
const output = document.querySelector("[data-output]");
let postcodeSugEle = $("ul#suggestion-postcode");

if (slider && output) {
  rangeSlider.create(slider, {
    onInit: val => {
      output.textContent = val;
    },
    onSlide: function(position, value) {
      output.textContent = position;
      clearTimeout(sliderTimer);
      sliderTimer = setTimeout(async () => {
        if (
          !$("input#search-postcode")
            .val()
            .trim()
        ) {
          doneTyping();
        }
      }, doneTypingInterval);
    }
  });
}

$("#job_filter_value").on('click','li',function (){
  var filter_value = $(this).data('value');
  var filter_text = $(this).text();
  $("#set_filter_value").val(filter_value);
  $("#set_filter_text").val(filter_text);
  searchJob();
});

// add suggestion on postcode event.
$(document).on("keyup", "input#search-postcode", async function(e) {
    let html = "";
    if (!$("input#search-postcode").val().trim()) {
        postcodeSugEle.hide().html(html);
    }
    if ($("input#search-postcode").val().trim().length > 2) {
        const postcodes = await getPostcodes($("input#search-postcode").val().trim());
        if (postcodes.status && postcodes.data.length > 0) {
            postcodes.data.forEach(function(postcode) {
                html += `<li value='${postcode.postcode}' data-value='${postcode.postcode}' data-lat='${postcode.latitude}' data-lng='${postcode.longitude}'>${postcode.postcode}</li>`;
            });
            postcodeSugEle.html(html).show();
        } else {
            html = "";
            postcodeSugEle.hide().html(html);
        }
    }
});

$(document).on("focus", "input#search-postcode", async function(e) {
    let html = "";
    if (!$("input#search-postcode").val().trim()) {
        postcodeSugEle.hide().html(html);
    }
    if ($("input#search-postcode").val().trim().length > 2) {
        const postcodes = await getPostcodes($("input#search-postcode").val().trim());
        if (postcodes.status && postcodes.data.length > 0) {
            postcodes.data.forEach(function(postcode) {
                html += `<li value='${postcode.postcode}' data-value='${postcode.postcode}' data-lat='${postcode.latitude}' data-lng='${postcode.longitude}'>${postcode.postcode}</li>`;
            });
            postcodeSugEle.html(html).show();
        } else {
            html = "";
            postcodeSugEle.hide().html(html);
        }
    }
});

// update lat long based on click event on suggestion list.
$(document).on("click", "ul#suggestion-postcode li", async function(e) {
    let lat = $(this).data("lat");
    let lng = $(this).data("lng");
    let value = $(this).data("value");

    $("input#search-postcode").val(value);
    $("input[name='latitude']").val(lat);
    $("input[name='longitude']").val(lng);

    postcodeSugEle.hide().html("");
});

$("form#search-form input").each(function() {
  if (
      ($(this)[0].type == 'checkbox' && $(this).attr('checked'))
     ) {
    hasValue = true;
  }
});
if (!hasValue) {
  // console.log('no value select for filter');
} else {
  searchJob();
}
$(document).on("change","form#search-form :input[type='checkbox'], form#search-form select",function() {
  searchJob();
}
);
$(document).on("submit", "form#search-form", function(e) {
  e.preventDefault();
  searchJob();
});

// on slider value change, start the countdown
/*$(document).on("change", "form#search-form input#salary-range", function(e) {
    clearTimeout(sliderTimer);
    sliderTimer = setTimeout(() => { doneTyping() }, doneTypingInterval);
});*/

// on keyup, start the countdown
$(document).on(
  "keyup",
  "form#search-form input#search-term, input#search-postcode",
  function(e) {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(() => {
      doneTyping();
    }, doneTypingInterval);
  }
);

// on keydown, clear the countdown
$(document).on(
  "keydown",
  "form#search-form input#search-term, input#search-postcode",
  function(e) {
    clearTimeout(typingTimer);
  }
);

// user is "finished typing," do something
async function doneTyping() {
  try {
    clearTimeout(sliderTimer);
    if (
      $("input#search-postcode")
        .val()
        .trim()
    ) {
      const location = await getLatLongFromPostcode(
        $("input#search-postcode")
          .val()
          .trim()
      );
      $("input[name='latitude']").val(location.lat);
      $("input[name='longitude']").val(location.lng);
    } else {
      $("input[name='latitude']").val("");
      $("input[name='longitude']").val("");
    }
  } catch (e) {
    $("input[name='latitude']").val("");
    $("input[name='longitude']").val("");
    // console.error(e);
  }
  // searchJob();
}

async function getPostcodes(postcode = null) {
    return new Promise((resolve, reject) => {
        if (!postcode || postcode.length < 3) {
            return reject('No postcode provided');
        }
        $.getJSON(`${SITE_URL}getPostcodes?postcode=${postcode}`).done(function(data) {
            resolve(data);
        }).fail(function( jqxhr, textStatus, error ) {
          let err = textStatus + ", " + error;         
          return reject(err);
        });
    });
}

async function getLatLongFromPostcode(postcode = null) {
  return new Promise((resolve, reject) => {
    if (!postcode || postcode.length < 4) {
      return reject("No postcode provided");
    }
    $.getJSON(`${SITE_URL}geocode?address=${postcode}`)
      .done(function(data) {
        if (data.status == "OK") {
          resolve(data);
        } else {
          return reject("Error while getting data from local.");
        }
      })
      .fail(function(jqxhr, textStatus, error) {
        let err = textStatus + ", " + error;
        return reject(err);
      });
  });
}

$(document).click(function(e) {
    var id = e.target.id;
    if (id != 'search-postcode') {
      postcodeSugEle.hide().html("");
    }
});

function searchJob() {  
  $.ajax({
    type: "GET",
    url: `${SITE_URL}/search`,
    data: searchFormEle.serialize(),
    /*contentType: false,
        processData: false,
        dataType: "html",*/
    cache: false,
    async: true,
    error: function(jqXHR, error, errorThrown) {
      console.error(error);
    },
    success: function(data, textStatus, jqXHR) {
      $("div#search-result-container").html(data);
      // $("input#search-term").focus();
      if ($("#search-result").length) {
        $("html, body").animate(
          {
            scrollTop: $("#search-result").offset().top
          },
          1000
        );
      }
    }
  });
}