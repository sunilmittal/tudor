$(document).ready(function () {
	let segments = location.pathname.split('/').filter(Boolean).filter(function(segment) {
    	return segment != 'tudor' && isNaN(segment)
    });
    let joinedSegment = segments.join('-');

    /*let resource_type_ele = $("input[name='resource_type']");
    if (resource_type_ele.length) {
        let resource_type = resource_type_ele.val();
        if (resource_type) {
            joinedSegment += '-'+resource_type;
        }
    }*/

    let menuOption = mapSegmentToMenuOption[joinedSegment];
    let menuSlug = mapSegmentToMenuSlug[joinedSegment] || joinedSegment;

    $.expr[':'].containsexactly = function(obj, index, meta, stack) {
        return $(obj).text() === meta[3];
    }; 

    if (menuOption == 'my-content' || menuOption == 'my-classroom') {
	    $('div#'+menuOption+'').addClass('active');
        $('div#'+menuOption+'').find('.card-link').removeClass('collapsed');
        $('div#'+menuOption+'').find('.card-link').attr('aria-expanded','true');
        $('div#'+menuOption+'').find('.collapse').addClass('in');
        $('a[data-resource_slug='+menuSlug+']').addClass('active');
    } else {
        $('div#'+menuSlug+'').addClass('active');
    }
});

let mapSegmentToMenuOption = [];
let mapSegmentToMenuSlug = [];

mapSegmentToMenuOption['notes-add'] = 'my-content';
mapSegmentToMenuOption['notes'] = 'my-content';
mapSegmentToMenuOption['notes-edit'] = 'my-content';

mapSegmentToMenuOption['me-videos'] = 'my-content';
mapSegmentToMenuOption['videos-add'] = 'my-content';
mapSegmentToMenuOption['videos-edit'] = 'my-content';

mapSegmentToMenuOption['classroom'] = 'my-classroom';
mapSegmentToMenuOption['classroom-requests'] = 'my-classroom';
mapSegmentToMenuOption['classroom-add'] = 'my-classroom';
mapSegmentToMenuOption['classroom-edit'] = 'my-classroom';


mapSegmentToMenuSlug['videos-edit'] = 'videos';
mapSegmentToMenuSlug['videos-add'] = 'videos';
mapSegmentToMenuSlug['me-videos'] = 'videos';

mapSegmentToMenuSlug['notes-add'] = 'notes';
mapSegmentToMenuSlug['notes'] = 'notes';
mapSegmentToMenuSlug['notes-edit'] = 'notes';