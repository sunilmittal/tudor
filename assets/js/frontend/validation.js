$(document).ready(function () {
	let phone_pattern = "([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})";
	let registerFormEle = $("#register_form");
	let loginFormEle = $("#login_form");
	let forgotPasswordForm = $("#forgot_password_form");
	let resetPasswordForm = $("#reset_password_form");
	let contactusFormEle = $("#contact_us");
	let jobApplyForm = $("#job_apply_form");
	let updateProfile = $("#update_profile");

	$.validator.addMethod("validate_email", function (value, element) {
		if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
			return true;
		} else {
			return false;
		}
	}, "Please enter a valid email address.");

	$.validator.addMethod('filesize', function (value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	});

	$.validator.addMethod("in_list", function (value, element, params) {
		return params.split(",").map(Number).includes(parseInt(value))
	}, "The selected value for this field is invalid");

	$.validator.addMethod("string_in_list", function (value, element, params) {
		console.log(params);
		return params.split(",").map(String).includes(value);
	}, "The selected value for this field is invalid");

	$.validator.addMethod('matches', function (value, element, param) {
		let re = new RegExp(param);
		return this.optional(element) || re.test(value);
	});

	// Register Form
	if (registerFormEle[0]) {
		registerFormEle.validate({
			rules: {
				name: {
					required: true,
					minlength: 3,
					maxlength: 50
				},
				email: {
					required: true,
					validate_email: true,
					maxlength: 100
				},
				password: {
					required: true,
					minlength: 6,
					maxlength: 20
				},
				confirm_password: {
					required: true,
					minlength: 6,
					maxlength: 20,
					equalTo: "#password"
				},
				user_role: {
					required: true
				}
			},
			messages: {
				name: {
					required: 'The full name field is required.',
					minlength: 'The full name must be at least 3 characters.',
					maxlength: 'The full name may not be greater than 50 characters.'
				},
				email: {
					required: 'The email field is required.',
					validate_email: 'The email must be a valid email address.',
					maxlength: 'The email may not be greater than 100 characters.'
				},
				password: {
					required: 'The password field is required.',
					minlength: 'The password must be at least 6 characters.',
					maxlength: 'The password may not be greater than 20 characters.'
				},
				confirm_password: {
					required: 'The confirm password field is required.',
					minlength: 'The confirm password must be at least 6 characters.',
					maxlength: 'The confirm password may not be greater than 20 characters.',
					equalTo: 'The confirm password confirmation does not match.'
				},
				user_role: {
					required: 'The user role field is required.',
				}
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
				let type = $(element).attr("type");
				if (type === "radio") {
					error.appendTo(element.parent().parent());
				} else {
					error.insertAfter(element);
				}
			}
		});
	}

	// Update user profile Form
	if (updateProfile[0]) {
		updateProfile.validate({
			rules: {
				first_name: {
					required: true,
					minlength: 3,
					maxlength: 50
				},
				last_name: {
					required: true,
					minlength: 3,
					maxlength: 50
				},
				phone: {
					required: true,
					digits: true,
					minlength: 10,
					maxlength: 10
				}
			},
			messages: {
				first_name: {
					required: 'The first name field is required.',
					minlength: 'The first name must be at least 3 characters.',
					maxlength: 'The first name may not be greater than 50 characters.'
				},
				last_name: {
					required: 'The last name field is required.',
					minlength: 'The last name must be at least 3 characters.',
					maxlength: 'The last name may not be greater than 50 characters.'
				},
				phone: {
					required: 'The phone field is required.',
					digits: 'The phone must contain number only.',
					minlength: 'The phone must be at least 10 characters.',
					maxlength: 'The phone may not be greater than 10 characters.'
				}
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
				let type = $(element).attr("type");
				if (type === "checkbox") {
					error.insertAfter(element.next());
				} else {
					error.insertAfter(element);
				}
			}
		});
	}

	// Login Form
	if (loginFormEle[0]) {
		loginFormEle.validate({
			rules: {
				email: {
					required: true,
					validate_email: true,
					maxlength: 100
				},
				password: {
					required: true,
					minlength: 6,
					maxlength: 20
				}
			},
			messages: {
				email: {
					required: 'The email field is required.',
					validate_email: 'The email must be a valid email address.',
					maxlength: 'The email may not be greater than 100 characters.'
				},
				password: {
					required: 'The password field is required.',
					minlength: 'The password must be at least 6 characters.',
					maxlength: 'The password may not be greater than 20 characters.'
				}
			},
			errorElement: 'span'
		});
	}

	// Forgot Password Form
	if (forgotPasswordForm[0]) {
		forgotPasswordForm.validate({
			rules: {
				email: {
					required: true,
					validate_email: true,
					maxlength: 100
				}
			},
			messages: {
				email: {
					required: 'The email field is required.',
					validate_email: 'The email must be a valid email address.',
					maxlength: 'The email may not be greater than 100 characters.'
				}
			},
			errorElement: 'span'
		});
	}

	// Register Form
	if (resetPasswordForm[0]) {
		resetPasswordForm.validate({
			rules: {
				email: {
					required: true,
					validate_email: true,
					maxlength: 100
				},
				password: {
					required: true,
					minlength: 6,
					maxlength: 20
				},
				confirm_password: {
					required: true,
					minlength: 6,
					maxlength: 20,
					equalTo: "#password"
				}
			},
			messages: {
				name: {
					required: 'The full name field is required.',
					minlength: 'The full name must be at least 3 characters.',
					maxlength: 'The full name may not be greater than 50 characters.'
				},
				email: {
					required: 'The email field is required.',
					validate_email: 'The email must be a valid email address.',
					maxlength: 'The email may not be greater than 100 characters.'
				},
				password: {
					required: 'The password field is required.',
					minlength: 'The password must be at least 6 characters.',
					maxlength: 'The password may not be greater than 20 characters.'
				},
				confirm_password: {
					required: 'The confirm password field is required.',
					minlength: 'The confirm password must be at least 6 characters.',
					maxlength: 'The confirm password may not be greater than 20 characters.',
					equalTo: 'The confirm password confirmation does not match.'
				}
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
				let type = $(element).attr("type");
				if (type === "checkbox") {
					error.insertAfter(element.next());
				} else {
					error.insertAfter(element);
				}
			}
		});
	}

	// Ready To Work With Us?
	if (contactusFormEle[0]) {
		contactusFormEle.validate({
			rules: {
				name: {
					required: true,
					minlength: 3,
					maxlength: 50
				},
				email: {
					required: true,
					validate_email: true,
					maxlength: 100
				},
				subject: {
					required: true,
					minlength: 2
				},
				message: {
					required: true,
					minlength: 6
				}
			},
			messages: {
				name: {
					required: 'The name field is required.',
					minlength: 'The name must be at least 3 characters.',
					maxlength: 'The name may not be greater than 50 characters.'
				},
				email: {
					required: 'The email field is required.',
					validate_email: 'The email must be a valid email address.',
					maxlength: 'The email may not be greater than 100 characters.'
				},
				message: {
					required: 'The message field is required.',
					minlength: 'The message must be at least 6 characters.'
				},
				subject: {
					required: 'The subject field is required.',
					minlength: 'The subject must be at least 2 characters.'
				}
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
				let type = $(element).attr("type");
				if (type === "checkbox") {
					error.insertAfter(element.next());
				} else {
					error.insertAfter(element);
				}
			}
		});
	}
	// Apply Job Form (JOBS DETAIL PAGE).
	if (jobApplyForm[0]) {
		jobApplyForm.validate({
			rules: {
				first_name: {
					required: true,
					minlength: 3,
					maxlength: 50
				},
				last_name: {
					required: true,
					minlength: 3,
					maxlength: 50
				},
				email: {
					required: true,
					validate_email: true,
					maxlength: 100
				},
				phone: {
					required: true,
					digits: true,
					minlength: 10,
					maxlength: 10
				},
				cv: {
					required: true,
					extension: "pdf|doc|docx|txt|rtf",
					filesize: 2000000
				},
				accept_terms: {
					required: true
				}
			},
			messages: {
				first_name: {
					required: 'The first name field is required.',
					minlength: 'The first name must be at least 3 characters.',
					maxlength: 'The first name may not be greater than 50 characters.'
				},
				last_name: {
					required: 'The last name field is required.',
					minlength: 'The last name must be at least 3 characters.',
					maxlength: 'The last name may not be greater than 50 characters.'
				},
				email: {
					required: 'The email field is required.',
					validate_email: 'The email must be a valid email address.',
					maxlength: 'The email may not be greater than 100 characters.'
				},
				phone: {
					required: 'The phone field is required.',
					digits: 'The phone must contain number only.',
					minlength: 'The phone must be at least 10 characters.',
					maxlength: 'The phone may not be greater than 10 characters.'
				},
				cv: {
					required: 'The cv field is required.',
					extension: "Please select only pdf, doc, docx, txt, rtf file.",
					filesize: "The selected file should be less than 2 MB."
				},
				accept_terms : {
					required: 'The terms must be accepted.'
				}
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
				let type = $(element).attr("type");
				if (type === "checkbox") {
					error.insertAfter(element.next());
				} else {
					error.insertAfter(element);
				}
			}
		});
	}
});
