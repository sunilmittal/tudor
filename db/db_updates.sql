
-- 09-feb-2021

ALTER TABLE `users` ADD `oauth_provider` VARCHAR(10) NULL AFTER `deleted_at`, ADD `oauth_uid` VARCHAR(50) NULL AFTER `oauth_provider`, ADD `gender` VARCHAR(10) NULL AFTER `oauth_uid`, ADD `locale` VARCHAR(10) NULL AFTER `gender`, ADD `link` VARCHAR(255) NULL AFTER `locale`;

-- 12 feb 2021

ALTER TABLE `blog` ADD `is_featured` TINYINT NOT NULL DEFAULT '0' COMMENT '\"1\"=> true, \"0\"=> false' AFTER `blog_id`;

ALTER TABLE `blog` CHANGE `sector_id` `category_id` INT(11) NULL DEFAULT NULL;

