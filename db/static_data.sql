--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `slug`, `parent_id`, `created_at`, `deleted_at`) VALUES
(1, 'Arts', 'arts', NULL, '2020-07-03 15:22:56', NULL),
(2, 'Painting', 'painting', 1, '2020-07-03 15:23:19', NULL),
(3, 'Sculpture', 'sculpture', 1, '2020-07-03 15:23:28', NULL),
(4, 'Music', 'music', 1, '2020-07-03 15:25:16', NULL),
(5, 'Literature', 'literature', 1, '2020-07-03 15:25:46', NULL),
(6, 'Architecture', 'architecture', 1, '2020-07-03 15:26:17', NULL),
(7, 'Performing arts', 'performing-arts', 1, '2020-07-03 15:26:37', NULL),
(8, 'Social Studies', 'social-studies', NULL, '2020-07-03 15:27:05', NULL),
(9, 'Civics and Government', 'civics-and-government', 8, '2020-07-03 15:27:29', NULL),
(10, 'History', 'history', 8, '2020-07-03 15:27:57', NULL),
(11, 'Geography', 'geography', 8, '2020-07-03 15:28:23', NULL),
(12, 'Economics', 'economics', 8, '2020-07-03 15:28:41', NULL),
(13, 'Mathematics', 'mathematics', NULL, '2020-07-03 15:28:58', NULL),
(14, 'Algebra', 'algebra', 13, '2020-07-03 15:29:17', NULL),
(15, 'Trigonometry', 'trigonometry', 13, '2020-07-03 15:30:01', NULL),
(16, 'Precalculus', 'precalculus', 13, '2020-07-03 15:30:23', NULL),
(17, 'Statistics', 'statistics', 13, '2020-07-03 15:30:47', NULL),
(18, 'Calculus', 'calculus', 13, '2020-07-03 15:31:06', NULL),
(19, 'Pre-Algebra', 'pre-algebra', 13, '2020-07-03 15:31:27', NULL),
(20, 'Science', 'science', NULL, '2020-07-03 15:31:45', NULL),
(21, 'Astronomy', 'astronomy', 20, '2020-07-03 15:32:07', NULL),
(22, 'Computer Science', 'computer-science', 20, '2020-07-03 15:32:29', NULL),
(23, 'Biology', 'biology', 20, '2020-07-03 15:32:55', NULL),
(24, 'Physics', 'physics', 20, '2020-07-03 15:33:45', NULL),
(25, 'Chemistry', 'chemistry', 20, '2020-07-03 15:34:02', NULL),
(26, 'Social Sciences', 'social-sciences', 20, '2020-07-03 15:34:19', NULL),
(27, 'Health and Medicine', 'health and medicine', 20, '2020-07-03 15:34:37', NULL),
(28, 'World Languages', 'world-languages', NULL, '2020-07-03 15:34:56', NULL),
(29, 'Chinese/Mandarin', 'chinese-mandarin', 28, '2020-07-03 15:35:14', NULL),
(30, 'Spanish', 'spanish', 28, '2020-07-03 15:35:32', NULL),
(31, 'Japanese', 'japanese', 28, '2020-07-03 15:35:52', NULL),
(32, 'German', 'german', 28, '2020-07-03 15:36:09', NULL),
(33, 'Russian', 'russian', 28, '2020-07-03 15:36:30', NULL),
(34, 'French', 'french', 28, '2020-07-03 15:36:50', NULL),
(35, 'Italian', 'italian', 28, '2020-07-03 15:37:11', NULL),
(36, 'other', 'other', 28, '2020-07-03 15:37:33', NULL);

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`user_id`, `firstname`, `lastname`, `email`, `tel`, `password`, `image`, `title`, `description`, `linkedin`, `twitter`, `skype`, `slug`, `deleted`, `active`, `only_image`, `recruitment_type`, `secret_talent`, `info`, `sort_order`) VALUES
(18, 'Tudor', 'Videos', 'info@tudor.com', NULL, '*6BB4837EB74329105EE4568DDA7DC67ED2CA2AD9', '', 'Consultant', NULL, '', NULL, NULL, '', 0, NULL, NULL, '', '', '', NULL);