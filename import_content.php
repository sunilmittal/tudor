<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set("allow_url_fopen", 1);

if ($_SERVER['HTTP_HOST'] == 'bolddev7.co.uk') {
    $conn = mysqli_connect("localhost", "bolddev7co_root", 'j$yK#Ga9p(bZ', "bolddev7co_celeritas");
} else {
    $conn = mysqli_connect("localhost", "root", "", "bolddev7co_celeritas");
}

if (!$conn) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

$titles = [
    'home',
    'about us',
    'blogs',
    'jobs',
    'contact us',

    'meet team',
    'business development',
    'consulting',
    'corporate',
    'ediscovery',
    'law firm',
    'software',
    
    'register',
    'profile',

    'privacy policy',
    'cookie policy',
    'terms condition'

];



$titles = array_values(array_unique(array_filter($titles)));
//deleteContents($conn);
//importContents($conn, $titles);


function specialCharToHyphen()
{
    $args = func_get_args();
    $string = join(" ", $args);
    $string = str_replace(' ', '-', $string);
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    return strtolower(preg_replace('/-+/', '-', $string));
}

// delete 
function deleteContents($conn)
{
    $sql = "DELETE FROM content_pages";
    $result = mysqli_query($conn, $sql);
    if (!empty($result)) {
        die("data deleted successfully.");
    } else {
        $error_message = mysqli_error($conn) . "\n";
        die($error_message);
    }
}

// insert
function importContents($conn, $titles)
{

    $date = date('Y-m-d H:i:s');
    $affectedRow = 0;

    if ($titles) {
        foreach ($titles as $title) {
            $sql = "INSERT INTO content_pages(
		        title, 
		        subtitle,
		        meta_title,
		        meta_keywords,
		        date,
		        slug
		    ) VALUES (
		        '" . $title . "',
		        '" . $title . "',
		        '" . $title . "',
		        '" . $title . "',
		        '" . $date . "',
		        '" . specialCharToHyphen($title) . "'
		    )";

            $result = mysqli_query($conn, $sql);
            if (!empty($result)) {
                $affectedRow++;
            } else {
                $error_message = mysqli_error($conn) . "\n";
                die($error_message);
            }
            if ($affectedRow > 0) {
                $message = $affectedRow . " records inserted";
            } else {
                $message = "No records inserted";
            }
        }
        die($message);
    } else {
        die("please provide titles");
    }
}
